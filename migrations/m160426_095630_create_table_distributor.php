<?php
use yii\db\cubrid\Schema;
use yii\db\Migration;

class m160426_095630_create_table_distributor extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('distributor', [
			'id'      => Schema::TYPE_PK,
			'logo'    => Schema::TYPE_STRING . '(255) NOT NULL',
			'website' => Schema::TYPE_STRING . '(255) NOT NULL',
			'status'  => Schema::TYPE_INTEGER . '(1) NOT NULL',
			'area'    => Schema::TYPE_STRING . '(255)',
			'city'    => Schema::TYPE_STRING . '(255)',
			'type'    => Schema::TYPE_INTEGER . '(11) NOT NULL',
		], $tableOptions);
	}

	public function down() {
		$this->dropTable('distributor');
	}
}
