<?php
use yii\db\Migration;

class m160517_040137_delete_view_banner extends Migration {

	public function up() {
		$this->dropColumn('banner', 'view');
	}

	public function down() {
		echo "m160517_040137_delete_view_banner cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
