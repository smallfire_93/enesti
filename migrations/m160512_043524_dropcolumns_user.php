<?php

use yii\db\Migration;

class m160512_043524_dropcolumns_user extends Migration
{
    public function up()
    {
        $this->dropColumn('user', 'distributor_id');
    }

    public function down()
    {
        echo "m160512_043524_dropcolumns_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
