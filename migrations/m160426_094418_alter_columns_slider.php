<?php
use yii\db\Migration;

class m160426_094418_alter_columns_slider extends Migration {

	public function up() {
		$this->renameColumn('slider', 'slider_id', 'id');
	}

	public function down() {
		echo "m160426_094418_alter_columns_slider cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
