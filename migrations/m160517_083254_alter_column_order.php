<?php
use yii\db\Migration;

class m160517_083254_alter_column_order extends Migration {

	public function up() {
		$this->alterColumn('order', 'status', \yii\db\cubrid\Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0');
	}

	public function down() {
		echo "m160517_083254_alter_column_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
