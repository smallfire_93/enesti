<?php
use yii\db\Migration;

class m160510_113814_alter_column_category extends Migration {

	public function up() {
		$this->alterColumn('category', 'order', \yii\db\mysql\Schema::TYPE_INTEGER . ' NULL');
	}

	public function down() {
		echo "m160510_113814_alter_column_category cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
