<?php
use yii\db\Migration;

class m160517_075240_addcolumn_status extends Migration {

	public function up() {
		$this->addColumn('order', 'status', \yii\db\mysql\Schema::TYPE_INTEGER . ' NULL');
	}

	public function down() {
		echo "m160517_075240_addcolumn_status cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
