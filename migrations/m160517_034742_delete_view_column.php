<?php
use yii\db\Migration;

class m160517_034742_delete_view_column extends Migration {

	public function up() {
		$this->dropColumn('recruitment', 'view');
		$this->dropColumn('post', 'view');
		$this->dropColumn('promotion', 'view');
		$this->dropTable('showroom');
		$this->dropTable('showroom_lang');
	}

	public function down() {
		echo "m160517_034742_delete_view_column cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
