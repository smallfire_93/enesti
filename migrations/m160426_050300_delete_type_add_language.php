<?php
use yii\db\Migration;

class m160426_050300_delete_type_add_language extends Migration {

	public function up() {
		$this->dropColumn('showroom', 'type');
		$this->addColumn('showroom_lang', 'language', \yii\db\cubrid\Schema::TYPE_STRING . '(11) NOT NULL');
	}

	public function down() {
		echo "m160426_050300_delete_type_add_language cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
