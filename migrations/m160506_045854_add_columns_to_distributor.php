<?php
use yii\db\Migration;

/**
 * Handles adding columns to table `distributor`.
 */
class m160506_045854_add_columns_to_distributor extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->addColumn('distributor', 'user_id', \yii\db\Schema::TYPE_INTEGER . " NOT NULL");
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
	}
}
