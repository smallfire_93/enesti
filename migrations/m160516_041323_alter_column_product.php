<?php
use yii\db\Migration;

class m160516_041323_alter_column_product extends Migration {

	public function up() {
		$this->alterColumn('product', 'in_stock', \yii\db\mysql\Schema::TYPE_INTEGER . ' NULL');
	}

	public function down() {
		echo "m160516_041323_alter_column_product cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
