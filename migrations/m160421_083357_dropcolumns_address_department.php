<?php
use yii\db\Migration;

class m160421_083357_dropcolumns_address_department extends Migration {

	public function up() {
		$this->dropColumn('department', 'address');
	}

	public function down() {
		echo "m160421_083357_dropcolumns_address_department cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
