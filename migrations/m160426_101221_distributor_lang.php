<?php
use yii\db\cubrid\Schema;
use yii\db\Migration;

class m160426_101221_distributor_lang extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('distributor_lang', [
			'id'             => Schema::TYPE_PK,
			'distributor_id' => Schema::TYPE_INTEGER . " NOT NULL",
			'language'       => Schema::TYPE_STRING . "(11) NOT NULL",
			'name'           => Schema::TYPE_STRING . '(255) NOT NULL',
		], $tableOptions);
	}

	public function down() {
		echo "m160426_101221_distributor_lang cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
