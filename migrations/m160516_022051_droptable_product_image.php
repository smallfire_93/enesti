<?php
use yii\db\Migration;

class m160516_022051_droptable_product_image extends Migration {

	public function up() {
		$this->dropTable('product_image');
	}

	public function down() {
		echo "m160516_022051_droptable_product_image cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
