<?php
use yii\db\Migration;

class m160429_041822_alter_column_distributor extends Migration {

	public function up() {
		$this->alterColumn('distributor', 'area', \yii\db\cubrid\Schema::TYPE_INTEGER . " NOT NULL");
	}

	public function down() {
		echo "m160429_041822_alter_column_distributor cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
