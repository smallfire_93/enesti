<?php
use yii\db\Migration;

class m160429_083047_add_column_to_distributor extends Migration {

	public function up() {
		$this->addColumn('distributor_lang', 'address', \yii\db\cubrid\Schema::TYPE_STRING . " NOT NULL");
	}

	public function down() {
	}
}
