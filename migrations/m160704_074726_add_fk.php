<?php
use yii\db\Migration;

class m160704_074726_add_fk extends Migration {

	public function up() {
		$this->addForeignKey('order_item_fk_order', '{{%order_item}}', 'order_id', '{{%order}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m160704_074726_add_fk cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
