<?php
use yii\db\cubrid\Schema;
use yii\db\Migration;

class m160421_084144_add_columns_address extends Migration {

	public function up() {
		$this->addColumn('department_lang', 'address', Schema::TYPE_TEXT);
	}

	public function down() {
		echo "m160421_084144_add_columns_address cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
