<?php
use yii\db\Migration;

class m160509_032032_alter_columns_contact_reply extends Migration {

	public function up() {
		$this->renameColumn('reply', 'reply_id', 'id');
		$this->renameColumn('contact', 'contact_id', 'id');
	}

	public function down() {
		echo "m160509_032032_alter_columns_contact_reply cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
