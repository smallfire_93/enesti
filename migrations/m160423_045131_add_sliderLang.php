<?php
use yii\db\cubrid\Schema;
use yii\db\Migration;

class m160423_045131_add_sliderLang extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('slider_lang', [
			'id'        => Schema::TYPE_PK,
			'slider_id' => Schema::TYPE_INTEGER,
			'language'  => Schema::TYPE_STRING . '(255) NOT NULL',
			'title'     => Schema::TYPE_STRING . '(255) NOT NULL',
		], $tableOptions);
		$this->dropColumn('slider', 'title');
	}

	public function down() {
		echo "m160423_045131_add_sliderLang cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
