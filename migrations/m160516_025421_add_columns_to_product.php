<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Handles adding columns to table `product`.
 */
class m160516_025421_add_columns_to_product extends Migration
{
    /**
     * @inheritdoc
     */
   
        public function up() {
        $this->addColumn('product', 'instructions', Schema::TYPE_STRING . ' NULL');
        $this->addColumn('product', 'capability', Schema::TYPE_STRING . ' NULL');
        $this->addColumn('product', 'sale_off', Schema::TYPE_DECIMAL . '(5,2) NULL');
    }

        /**
         * @inheritdoc
         */
        public function down() {
        $this->dropColumn('product', 'instructions');
        $this->dropColumn('product', 'capability');
        $this->dropColumn('product', 'sale_off');
    }
    

    /**
     * @inheritdoc
     */
}
