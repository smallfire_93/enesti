<?php
use yii\db\Migration;

class m160421_043307_delete_column_department extends Migration {

	public function up() {
		$this->dropColumn('department', 'name');
	}

	public function down() {
		echo "m160421_043307_delete_column_department cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
