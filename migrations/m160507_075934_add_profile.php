<?php
use yii\db\Migration;
use yii\db\Schema;

class m160507_075934_add_profile extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%profile}}', [
			'user_id'        => Schema::TYPE_INTEGER . ' PRIMARY KEY',
			'name'           => Schema::TYPE_STRING . '(255)',
			'public_email'   => Schema::TYPE_STRING . '(255)',
			'gravatar_email' => Schema::TYPE_STRING . '(255)',
			'gravatar_id'    => Schema::TYPE_STRING . '(32)',
			'location'       => Schema::TYPE_STRING . '(255)',
			'website'        => Schema::TYPE_STRING . '(255)',
			'bio'            => Schema::TYPE_TEXT,
		], $tableOptions);
	}

	public function down() {
		echo "m160507_075934_add_profile cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
