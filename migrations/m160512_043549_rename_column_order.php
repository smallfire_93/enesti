<?php
use yii\db\Migration;

class m160512_043549_rename_column_order extends Migration {

	public function up() {
		$this->renameColumn('order', 'order_id', 'id');
		$this->renameColumn('order_item', 'order_item_id', 'id');
	}

	public function down() {
		echo "m160512_043549_rename_column_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
