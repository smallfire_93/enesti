<?php
use yii\db\Migration;

class m160511_114630_move_distributor_to_user extends Migration {

	public function up() {
		$this->dropColumn('distributor', 'id');
		$this->dropColumn('distributor', 'user_id');
		$oldColumns = Yii::$app->db->createCommand("show columns from distributor")->queryAll();
		foreach ($oldColumns as $oldColumn) {
			$type = $oldColumn['Type'] . " NULL";
			$this->addColumn('{{%user}}', $oldColumn['Field'], $type);
		}
		$this->dropTable('distributor');
	}

	public function down() {
		echo "m160511_114630_move_distributor_to_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
