<?php
use yii\db\cubrid\Schema;
use yii\db\Migration;

class m160425_113020_add_showroom_lang extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('showroom_lang', [
			'id'          => Schema::TYPE_PK,
			'showroom_id' => Schema::TYPE_INTEGER . " NOT NULL",
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'address'     => Schema::TYPE_STRING . '(255) NOT NULL',
		], $tableOptions);
		$this->dropColumn('showroom', 'name');
		$this->dropColumn('showroom', 'address');
	}

	public function down() {
		echo "m160425_113020_add_showroom_lang cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
