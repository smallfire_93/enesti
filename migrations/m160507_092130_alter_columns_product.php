<?php
use yii\db\Migration;

class m160507_092130_alter_columns_product extends Migration {

	public function up() {
		$this->dropColumn('product', 'view');
	}

	public function down() {
		echo "m160507_092130_alter_columns_product cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
