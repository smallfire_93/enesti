<?php
use yii\db\Migration;

class m160512_030645_fk_to_user extends Migration {

	public function up() {
		$this->addForeignKey('distributor_lang_fk_user', '{{%distributor_lang}}', 'distributor_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m160512_030645_fk_to_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
