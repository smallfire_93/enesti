<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Handles the dropping for table `column_user_id`.
 */
class m160516_040746_drop_column_user_id extends Migration {

	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->dropColumn('product', 'user_id');
		$this->alterColumn('product', 'origin', Schema::TYPE_STRING . '(255) NULL');
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
	}
}
