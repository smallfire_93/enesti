-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2016 at 09:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nava_enesti`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `user_id`, `image`, `position`, `url`, `order`, `status`, `created_date`) VALUES
(6, 1, '6_image.jpg', '0', '123', 123, 1, '2016-05-17 03:42:44');

-- --------------------------------------------------------

--
-- Table structure for table `banner_lang`
--

CREATE TABLE `banner_lang` (
  `id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_lang`
--

INSERT INTO `banner_lang` (`id`, `banner_id`, `language`, `title`) VALUES
(5, 6, 'vi', '123');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `type`, `order`, `status`, `image`) VALUES
(32, 0, 1, 100, 1, ''),
(33, 0, 1, 100, 1, ''),
(34, 0, 1, 100, 1, ''),
(35, 0, 1, 100, 1, ''),
(38, 0, 2, NULL, 1, '38_image.jpg'),
(39, 0, 2, NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `category_lang`
--

CREATE TABLE `category_lang` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_lang`
--

INSERT INTO `category_lang` (`id`, `category_id`, `language`, `name`, `description`) VALUES
(32, 32, 'vi', 'Chăm sóc da', ''),
(33, 33, 'vi', 'Trang điểm', ''),
(34, 34, 'vi', 'Nước hoa', ''),
(35, 35, 'vi', 'Tóc', ''),
(38, 38, 'vi', '123', '123'),
(39, 39, 'vi', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `logo`, `website`, `created_date`, `status`) VALUES
(3, '3_image.jpg', '123', '2016-05-17 03:44:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client_lang`
--

CREATE TABLE `client_lang` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_lang`
--

INSERT INTO `client_lang` (`id`, `client_id`, `language`, `name`) VALUES
(4, 3, 'vi', '123');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `fullname`, `email`, `phone`, `title`, `content`, `created_date`, `status`) VALUES
(1, '123', '123', 123, '123', '123', '2016-04-25 11:07:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `email`, `phone`, `fax`, `status`, `order`, `created_date`) VALUES
(1, '123', '123', '123', 1, 123, '2016-04-21 10:28:17'),
(2, '12312', '123', '123', 0, 123, '2016-04-25 02:58:36'),
(3, '123', '1231', '123', 1, 123, '2016-04-25 03:09:39'),
(4, '23423', '2222', '2222', 1, 2, '2016-05-13 05:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `department_lang`
--

CREATE TABLE `department_lang` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_lang`
--

INSERT INTO `department_lang` (`id`, `department_id`, `language`, `name`, `address`) VALUES
(1, 1, 'vi', 'abc', '12312312'),
(2, 1, 'en', '123123', '123123123'),
(3, 2, 'vi', '123', '333'),
(4, 3, 'vi', '123', '1111'),
(5, 3, 'en', '123', '123'),
(6, 4, 'vi', '1321332', '2342');

-- --------------------------------------------------------

--
-- Table structure for table `distributor_lang`
--

CREATE TABLE `distributor_lang` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `language` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `distributor_lang`
--

INSERT INTO `distributor_lang` (`id`, `distributor_id`, `language`, `name`, `address`) VALUES
(26, 42, 'vi', '222', '222'),
(27, 48, 'vi', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `country`, `status`) VALUES
(1, 'Việt Nam', 'vi', 'vn', 1),
(2, 'United States', 'en', 'us', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m141208_201488_setting_init', 1462503922),
('m160204_045439_navatech_multi_language_init', 1457692979),
('m160226_063609_create_role', 1457693356),
('m160421_043307_delete_column_department', 1461213677),
('m160421_083357_dropcolumns_address_department', 1461228040),
('m160421_084144_add_columns_address', 1461228306),
('m160423_045131_add_sliderLang', 1461387582),
('m160425_044308_add_colum_to_distributor', 1461560572),
('m160425_051435_add_columns_to_distributor', 1461561704),
('m160425_113020_add_showroom_lang', 1461584355),
('m160426_050300_delete_type_add_language', 1461647098),
('m160426_094418_alter_columns_slider', 1461664079),
('m160426_095630_create_table_distributor', 1461665406),
('m160426_101221_distributor_lang', 1461665730),
('m160429_041529_alter_table_distributor', 1461903411),
('m160429_041822_alter_column_distributor', 1461903549),
('m160429_083047_add_column_to_distributor', 1461918711),
('m160504_091110_add_settingLang', 1462353993),
('m160504_092744_drop_columns_setting', 1462354186),
('m160504_093125_rename_columns_setting_id', 1462354333),
('m160504_172257_change_columns', 1462503922),
('m160505_071027_alter_column_setting', 1462432343),
('m160506_043343_modify_setting', 1462848400),
('m160506_045854_add_columns_to_distributor', 1462511150),
('m160506_091030_add_colums_to_user', 1462525939),
('m160507_075934_add_profile', 1462609679),
('m160507_092130_alter_columns_product', 1462612957),
('m160509_032032_alter_columns_contact_reply', 1462764170),
('m160509_164512_mass', 1462848401),
('m160510_113814_alter_column_category', 1462880391),
('m160511_114630_move_distributor_to_user', 1462967675),
('m160512_030645_fk_to_user', 1463027822),
('m160512_043524_dropcolumns_user', 1463027822),
('m160512_043549_rename_column_order', 1463027822),
('m160516_022051_droptable_product_image', 1463365333),
('m160516_022245_add_columns_to_product', 1463366950),
('m160516_025421_add_columns_to_product', 1463367342),
('m160516_040746_drop_column_user_id', 1463371773),
('m160516_041323_alter_column_product', 1463372089),
('m160517_034742_delete_view_column', 1463457114),
('m160517_040137_delete_view_banner', 1463457750),
('m160517_075240_addcolumn_status', 1463471654);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `full_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` int(11) NOT NULL,
  `phone_number` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `full_name`, `email`, `total_amount`, `phone_number`, `shipping_address`, `note`, `created_date`, `status`) VALUES
(1, '123', '123', 123, '123', '123', '123', '2016-05-12 04:22:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `order`, `status`) VALUES
(1, 123, 1),
(2, 123, 1),
(3, 123, 0),
(4, 123, 0),
(5, 123123, 0),
(6, 123, 0);

-- --------------------------------------------------------

--
-- Table structure for table `page_lang`
--

CREATE TABLE `page_lang` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_lang`
--

INSERT INTO `page_lang` (`id`, `page_id`, `language`, `title`, `intro`, `content`) VALUES
(1, 3, 'vi', '123', '123', '123'),
(2, 4, 'vi', 'My name''s Phuong', '123', '123'),
(3, 6, 'vi', '123123', '1231', '123'),
(4, 1, 'vi', '123', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `phrase`
--

CREATE TABLE `phrase` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phrase`
--

INSERT INTO `phrase` (`id`, `name`) VALUES
(1, 'language'),
(2, 'list_of_available_x'),
(3, 'add_a_new'),
(4, 'action'),
(5, 'delete_warning'),
(6, 'update'),
(7, 'delete'),
(8, 'language_code'),
(9, 'country_code'),
(10, 'status'),
(11, 'phrase'),
(12, 'double_click_to_edit'),
(13, 'success_add_update_x_successfully'),
(14, 'warning_x_existed'),
(15, 'error_mark_required'),
(16, 'ex_x_welcome_to_my_site'),
(17, 'support'),
(18, 'support_max_5_params_in_a_sentence'),
(19, 'params_is_started_from_1_and_ended_with_5'),
(20, 'create'),
(21, 'close'),
(22, 'category'),
(23, 'update_x'),
(24, 'back'),
(25, 'details'),
(26, 'view_x'),
(27, 'list_x'),
(28, 'Actions'),
(29, 'page'),
(30, 'create_x'),
(33, 'manage_x'),
(36, 'dashboard'),
(37, 'product'),
(38, 'x_category'),
(39, 'news'),
(40, 'project'),
(42, 'media'),
(43, 'picture'),
(44, 'audio'),
(45, 'video'),
(46, 'banner'),
(47, 'popup'),
(48, 'client'),
(49, 'recruitment'),
(50, 'user'),
(51, 'x_management'),
(52, 'user_role'),
(53, 'contact'),
(54, 'setting'),
(57, 'hello'),
(59, 'logout'),
(60, 'information'),
(61, 'account'),
(62, 'password'),
(64, 'login'),
(65, 'general_setting'),
(68, 'no'),
(69, 'yes'),
(72, 'slug'),
(73, 'type'),
(77, 'configuration'),
(78, 'save'),
(79, 'edit_x'),
(80, 'aboutus'),
(85, 'title'),
(86, 'image'),
(87, 'position'),
(89, 'user_id'),
(90, 'created_date'),
(92, 'translate'),
(99, 'home'),
(100, 'welcome_text'),
(101, 'remember_password'),
(102, 'change_password'),
(108, 'unread'),
(109, 'read'),
(111, 'full_name'),
(112, 'phone'),
(113, 'content'),
(115, 'sort'),
(116, 'feature'),
(117, 'completed'),
(118, 'uncompleted'),
(119, 'existed'),
(120, 'icon'),
(121, 'parent_category'),
(122, 'home_page'),
(123, 'about'),
(124, 'name'),
(125, 'view'),
(126, 'post'),
(127, 'list'),
(130, 'success'),
(132, 'permission'),
(133, 'is_backend_login'),
(134, 'background'),
(141, 'email'),
(142, 'username'),
(143, 'first_name'),
(144, 'last_name'),
(145, 'gender'),
(146, 'birthday'),
(147, 'update_date'),
(148, 'old_password'),
(149, 'new_password'),
(150, 'confirm_password'),
(153, 'executing'),
(154, 'executed'),
(155, 'price'),
(156, 'left'),
(157, 'right'),
(158, 'url'),
(159, 'description'),
(160, 'hot_news'),
(161, 'minutes_ago'),
(163, 'hours_ago'),
(164, 'contact_for_price'),
(165, 'view_all'),
(166, 'fullname'),
(167, 'send'),
(168, 'user_role_id'),
(169, 'online_support'),
(170, 'our_client'),
(171, 'our_clients'),
(172, 'search'),
(173, 'parent_id'),
(174, 'previous'),
(175, 'next'),
(176, 'order_by'),
(177, 'product_name'),
(178, 'asc'),
(179, 'desc'),
(180, 'display'),
(181, 'product_per_page'),
(184, 'company_information'),
(185, 'become_our_client'),
(186, 'content_our_client'),
(187, 'become_our_partners'),
(188, 'about_general_director'),
(189, 'about_company'),
(190, 'development_process'),
(191, 'award'),
(192, 'typical_project'),
(193, 'order'),
(194, 'total_amount'),
(195, 'shipping_address'),
(196, 'message'),
(197, 'invoice'),
(198, 'invoice_information'),
(199, 'order_no'),
(200, 'customer_information'),
(201, 'quantity'),
(203, 'export_to_excel'),
(204, 'add_image'),
(205, 'caption'),
(206, 'is_main_image'),
(209, 'list_categories'),
(210, 'headquarter_address'),
(211, 'factory_address'),
(212, 'office_address'),
(213, 'hotline'),
(214, 'connect_with_us'),
(215, 'sales_consultant'),
(217, 'technology_production'),
(218, 'slider'),
(219, 'search_product'),
(220, 'x_products'),
(221, 'product_newest'),
(222, 'price_asc'),
(223, 'price_desc'),
(224, 'feature_product'),
(225, 'add_to_cart'),
(226, 'product_information'),
(227, 'customer_comment'),
(228, 'related_product'),
(229, 'caption_in_x'),
(231, 'communication_news'),
(232, 'read_more'),
(233, 'your_cart'),
(234, 'product_price'),
(235, 'total'),
(236, 'cancel'),
(237, 'continue_shopping'),
(238, 'place_order'),
(239, 'cart_empty'),
(240, 'go_to_cart'),
(241, 'technical'),
(242, 'order_information'),
(243, 'personal_information'),
(244, 'thankyou_for_your_order'),
(245, 'thankyou'),
(246, 'company_name'),
(247, 'address'),
(249, 'showroom'),
(250, 'related_post'),
(251, 'prev_news'),
(252, 'next_news'),
(253, 'in_stock'),
(254, 'product_not_in_stock'),
(255, 'not_in_stock'),
(256, 'perform_indepth_development_goals'),
(257, 'technology'),
(258, 'equipment'),
(259, 'send_success'),
(260, 'send_false'),
(261, 'from'),
(262, 'to'),
(263, 'subject'),
(264, 'reply'),
(265, 'create_date'),
(266, 'project_intro'),
(267, 'cancelled'),
(268, 'new'),
(269, 'received'),
(270, 'cancel_order'),
(271, 'receive_order'),
(272, 'are_you_sure'),
(273, 'office'),
(274, 'factory'),
(275, 'map'),
(276, 'homepage_lager_text'),
(277, 'code'),
(278, 'origin'),
(279, 'group_product'),
(280, 'videos_gallery'),
(281, 'select_from_computer'),
(282, 'select_from_url'),
(283, 'only_youtube_supported'),
(284, 'search_by_x'),
(285, 'customer'),
(286, 'general'),
(287, 'payment'),
(288, 'lists'),
(289, 'country'),
(290, 'languages'),
(291, 'add_a_new_x'),
(292, 'in_use'),
(293, 'not_in_use'),
(294, 'fax'),
(295, 'intro'),
(296, 'distributor'),
(297, 'promotion'),
(298, 'city'),
(299, 'area'),
(300, 'agent'),
(301, 'not_read'),
(302, 'value'),
(303, 'key'),
(304, 'index'),
(305, 'desc_general'),
(306, 'site_name'),
(307, 'desc_site_name'),
(308, 'site_title'),
(309, 'desc_site_title'),
(310, 'site_keywords'),
(311, 'desc_site_keywords'),
(312, 'Bảo trì website?'),
(313, 'desc_Bảo trì website?'),
(314, 'general_active'),
(315, 'desc_general_active'),
(316, 'title_language'),
(317, 'desc_title_language'),
(318, 'north'),
(319, 'middle'),
(320, 'south'),
(321, 'not_active'),
(322, 'active'),
(323, 'isset_setting'),
(324, 'desc_isset_setting'),
(325, 'test_abc'),
(326, 'desc_test_abc'),
(327, 'test'),
(328, 'desc_test'),
(329, 'text_field'),
(330, 'desc_text_field'),
(331, 'email_field'),
(332, 'desc_email_field'),
(333, 'number_field'),
(334, 'desc_number_field'),
(335, 'textarea_field'),
(336, 'desc_textarea_field'),
(337, 'color_field'),
(338, 'desc_color_field'),
(339, 'date_field'),
(340, 'desc_date_field'),
(341, 'time_field'),
(342, 'desc_time_field'),
(343, 'password_field'),
(344, 'desc_password_field'),
(345, 'roxymce_field'),
(346, 'desc_roxymce_field'),
(347, 'select_field'),
(348, 'desc_select_field'),
(349, 'additional'),
(350, 'multiselect_field'),
(351, 'desc_multiselect_field'),
(352, 'file_field'),
(353, 'desc_file_field'),
(354, 'url_field'),
(355, 'desc_url_field'),
(356, 'percent_field'),
(357, 'desc_percent_field'),
(358, 'switch_field'),
(359, 'desc_switch_field'),
(360, 'checkbox_option'),
(361, 'desc_checkbox_option'),
(362, 'radio_option'),
(363, 'desc_radio_option'),
(364, 'department'),
(365, 'password_mail'),
(366, 'desc_password_mail'),
(367, 'note'),
(368, 'admin'),
(369, 'import_to_excel'),
(370, 'import_excel');

-- --------------------------------------------------------

--
-- Table structure for table `phrase_translate`
--

CREATE TABLE `phrase_translate` (
  `id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phrase_translate`
--

INSERT INTO `phrase_translate` (`id`, `phrase_id`, `language_id`, `value`) VALUES
(1, 1, 1, 'Ngôn ngữ'),
(2, 2, 1, 'Danh sách {1}'),
(3, 3, 1, 'Thêm mới'),
(4, 4, 1, 'Hành động'),
(5, 5, 1, 'Nếu bạn xóa mục này, toàn bộ dữ liệu đi kèm cũng sẽ bị xóa. Bạn có chắc chắn muốn xóa không?'),
(6, 6, 1, 'Cập nhật'),
(7, 7, 1, 'Xóa'),
(8, 8, 1, 'Mã ngôn ngữ'),
(9, 9, 1, 'Mã quốc gia'),
(10, 10, 1, 'Trạng thái'),
(11, 11, 1, 'Từ ngữ'),
(12, 12, 1, 'Clip đúp chuột để sửa'),
(13, 13, 1, '<strong>Chúc mừng!</strong> {1} {2} hoàn tất!'),
(14, 14, 1, '<strong>Cảnh báo!</strong> Những {1} này đã có trong dữ liệu, vui lòng tìm và cập nhật chúng!'),
(15, 15, 1, '<strong>Có lỗi!</strong> Các trường có dấu (*) là bắt buộc!'),
(16, 16, 1, 'VD(VI): Chào mừng ghé thăm website'),
(17, 17, 1, 'Hỗ trợ'),
(18, 18, 1, 'Hỗ trợ tối đa 5 tham số'),
(19, 19, 1, 'Tham số bắt đầu từ 1 đến 5'),
(20, 20, 1, 'Thêm mới'),
(21, 21, 1, 'Đóng'),
(22, 21, 2, 'Close'),
(23, 20, 2, 'Create'),
(24, 19, 2, 'Params is started from 1 and ended with 5'),
(25, 18, 2, 'System supports max 5 params in a sentence'),
(26, 17, 2, 'Support'),
(27, 16, 2, 'Ex(EN): Welcome to my site'),
(28, 15, 2, '<strong>Error!</strong> Field (*) is required!'),
(29, 14, 2, '<strong>Warning!</strong> This {1} has been existed, please find and update it!'),
(30, 13, 2, '<strong>Success!</strong> {1} {2} successfully!'),
(31, 12, 2, 'Double click to edit'),
(32, 11, 2, 'Phrase'),
(33, 10, 2, 'Status'),
(34, 9, 2, 'Country Code'),
(35, 8, 2, 'Language Code'),
(36, 7, 2, 'Delete'),
(37, 6, 2, 'Update'),
(38, 5, 2, 'If you delete this item, all related data is deleted. Are you sure ?'),
(39, 4, 2, 'Action'),
(40, 3, 2, 'Add a new'),
(41, 2, 2, 'List of available {1}'),
(42, 1, 2, 'Language'),
(43, 22, 1, 'Danh mục'),
(44, 23, 1, 'Cập nhật {1}'),
(45, 24, 1, 'Quay lại'),
(46, 25, 1, 'Chi tiết'),
(47, 26, 1, 'Xem {1}'),
(48, 27, 1, 'Danh sách {1}'),
(49, 28, 1, 'Thao tác'),
(50, 22, 2, 'Categories'),
(51, 23, 2, 'Update {1}'),
(52, 25, 2, 'Details'),
(53, 26, 2, 'View {1}'),
(54, 24, 2, 'Back'),
(55, 27, 2, 'List of {1}'),
(56, 28, 2, 'Actions'),
(57, 29, 1, 'Trang'),
(58, 30, 1, 'Thêm mới {1}'),
(61, 33, 1, 'Quản lý {1}'),
(62, 29, 2, 'Page'),
(63, 30, 2, 'Create a new {1}'),
(64, 33, 2, '{1} Management'),
(67, 36, 1, 'Bảng điều khiển'),
(68, 37, 1, 'Sản phẩm'),
(69, 38, 1, 'Danh mục {1}'),
(70, 39, 1, 'Tin tức'),
(71, 40, 1, 'Dự án'),
(73, 42, 1, 'Thư viện'),
(74, 43, 1, 'Hình ảnh'),
(75, 44, 1, 'Bài hát về Quang Trung'),
(76, 45, 1, 'Video'),
(77, 46, 1, 'Banner'),
(78, 47, 1, 'Popup'),
(79, 48, 1, 'Đối tác'),
(80, 49, 1, 'Tuyển dụng'),
(81, 50, 1, 'Người dùng'),
(82, 51, 1, 'Quản lý {1}'),
(83, 52, 1, 'Phân quyền'),
(84, 53, 1, 'Liên hệ'),
(85, 54, 1, 'Cấu hình hệ thống'),
(88, 57, 1, 'Xin chào'),
(90, 59, 1, 'Đăng xuất'),
(91, 36, 2, 'Dashboard'),
(92, 37, 2, 'Product'),
(93, 38, 2, '{1} category'),
(94, 39, 2, 'News'),
(95, 40, 2, 'Project'),
(96, 42, 2, 'Library'),
(97, 43, 2, 'Images'),
(98, 44, 2, 'The Song of Quang Trung'),
(99, 45, 2, 'Video'),
(100, 46, 2, 'Banner'),
(101, 47, 2, 'Popup'),
(102, 48, 2, 'Partners'),
(103, 49, 2, 'Recruitment'),
(104, 50, 2, 'User'),
(105, 51, 2, '{1} Management'),
(106, 52, 2, 'User Role'),
(107, 53, 2, 'Contact'),
(108, 54, 2, 'Settings'),
(110, 59, 2, 'Logout'),
(111, 60, 1, 'Thông tin'),
(112, 61, 1, 'Tài khoản'),
(113, 62, 1, 'Mật khẩu'),
(115, 64, 1, 'Đăng nhập'),
(116, 57, 2, 'Hello'),
(117, 65, 1, 'Cấu hình chung'),
(119, 60, 2, 'Information'),
(120, 61, 2, 'Account'),
(121, 62, 2, 'Password'),
(123, 65, 2, 'General settings'),
(124, 64, 2, 'Login'),
(125, 68, 1, 'Không'),
(126, 69, 1, 'Có'),
(129, 72, 1, 'Đường dẫn tĩnh'),
(130, 73, 1, 'Loại'),
(134, 69, 2, 'Yes'),
(135, 68, 2, 'No'),
(136, 72, 2, 'Slug '),
(137, 73, 2, 'Type '),
(138, 77, 1, 'Cấu hình'),
(139, 78, 1, 'Lưu'),
(140, 77, 2, 'Configuration '),
(141, 78, 2, 'Save '),
(142, 79, 1, 'Thay đổi {1}'),
(143, 79, 2, 'Update {1} '),
(144, 80, 1, 'Về chúng tôi'),
(149, 85, 1, 'Tiêu đề'),
(150, 86, 1, 'Hình ảnh'),
(151, 87, 1, 'Vị trí'),
(153, 89, 1, 'ID'),
(154, 90, 1, 'Ngày tạo'),
(155, 90, 2, 'Created date '),
(156, 89, 2, 'ID '),
(157, 85, 2, 'Title '),
(158, 87, 2, 'Position '),
(159, 86, 2, 'Image'),
(160, 80, 2, 'About us'),
(162, 99, 2, 'Home'),
(163, 100, 2, 'Welcome to Website Management page'),
(164, 101, 2, 'Remember password?  '),
(165, 102, 2, 'Change password'),
(166, 99, 1, 'Home'),
(169, 92, 1, 'Dịch '),
(170, 92, 2, 'Translate '),
(171, 100, 1, 'Xin chào. Bạn đang truy cập vào trang Quản trị hệ thống '),
(172, 101, 1, 'Ghi nhớ mật khẩu? '),
(173, 102, 1, 'Đổi mật khẩu '),
(179, 108, 1, 'Chưa đọc'),
(180, 109, 1, 'Đã đọc'),
(182, 111, 1, 'Họ tên'),
(183, 112, 1, 'Số điện thoại'),
(184, 113, 1, 'Nội dung'),
(186, 115, 1, 'Thứ tự'),
(187, 116, 1, 'Nổi bật'),
(188, 117, 1, 'Hoàn thành'),
(189, 118, 1, 'Chưa hoàn thành'),
(190, 119, 1, 'Đã tồn tại'),
(191, 120, 1, 'Biểu tượng'),
(193, 108, 2, 'Unread '),
(194, 109, 2, 'Read '),
(195, 111, 2, 'Full name '),
(196, 112, 2, 'Phone '),
(197, 113, 2, 'Content '),
(198, 115, 2, 'Order '),
(199, 116, 2, 'Featured '),
(200, 117, 2, 'Completed '),
(201, 118, 2, 'Uncompleted '),
(202, 119, 2, 'Existed '),
(203, 120, 2, 'icon'),
(204, 121, 1, 'Danh mục cha'),
(205, 121, 2, 'Parent category  '),
(206, 122, 1, 'Trang chủ'),
(207, 123, 1, 'Giới thiệu'),
(208, 122, 2, 'Home '),
(209, 123, 2, 'About us '),
(210, 124, 1, 'Tên'),
(211, 125, 1, 'Lượt xem'),
(212, 126, 1, 'Bài viết'),
(213, 127, 1, 'Danh sách'),
(216, 130, 1, 'Thành công'),
(218, 132, 1, 'Quyền hạn'),
(219, 133, 1, 'Được phép đăng nhập Quản lý'),
(220, 134, 1, 'Hình nền'),
(225, 125, 2, 'View '),
(228, 141, 1, 'E-mail'),
(229, 142, 1, 'Tài khoản'),
(230, 143, 1, 'Họ'),
(231, 144, 1, 'Tên'),
(232, 145, 1, 'Giới tính'),
(233, 146, 1, 'Ngày sinh'),
(234, 147, 1, 'Ngày cập nhật'),
(235, 148, 1, 'Mật khẩu cũ'),
(236, 149, 1, 'Mật khẩu mới'),
(237, 150, 1, 'Xác nhận mật khẩu'),
(238, 141, 2, 'E-mail '),
(239, 143, 2, 'First name '),
(240, 144, 2, 'Last name'),
(241, 142, 2, 'Username '),
(245, 150, 2, 'Confirm password '),
(246, 149, 2, 'New password '),
(247, 148, 2, 'Old password '),
(248, 147, 2, 'Updated date '),
(249, 146, 2, 'Birthday '),
(250, 145, 2, 'Gender '),
(251, 153, 1, 'Đang thực hiện'),
(252, 154, 1, 'Đã thực hiện'),
(253, 155, 1, 'Giá'),
(254, 156, 1, 'Bên trái'),
(255, 157, 1, 'Bên phải'),
(256, 158, 1, 'Liên kết'),
(257, 159, 1, 'Mô tả'),
(258, 160, 1, 'Tin nóng'),
(259, 161, 1, 'Phút trước'),
(261, 163, 1, 'giờ trước'),
(262, 156, 2, 'Bên trái'),
(263, 155, 2, 'Price '),
(264, 158, 2, 'URL '),
(265, 163, 2, 'hours ago '),
(266, 161, 2, 'minutes ago '),
(267, 160, 2, 'Hot news '),
(268, 159, 2, 'Description '),
(269, 157, 2, 'Bên phải  '),
(270, 154, 2, 'Completed '),
(271, 153, 2, 'Developing '),
(272, 124, 2, 'Name '),
(273, 126, 2, 'Post '),
(274, 127, 2, 'List '),
(275, 130, 2, 'Success '),
(276, 132, 2, 'Permission '),
(277, 133, 2, 'Login to backend '),
(278, 134, 2, 'Background '),
(279, 164, 1, 'Để trống nếu muốn hiển thị giá LIÊN HỆ'),
(280, 165, 1, 'Xem tất cả'),
(281, 165, 2, 'View all'),
(282, 164, 2, 'Leave blank for AGREEMENT price '),
(283, 166, 1, 'Tên đầy đủ'),
(284, 167, 1, 'Gửi'),
(285, 168, 1, 'Phân quyền'),
(286, 168, 2, 'Select permission '),
(287, 167, 2, 'Send '),
(288, 166, 2, 'Full name '),
(289, 169, 1, 'Hỗ trợ trực tuyến '),
(290, 169, 2, 'Support Online'),
(291, 170, 1, 'Đối tác của chúng tôi'),
(292, 170, 2, 'Our Partners'),
(293, 171, 1, 'Đối tác của chúng tôi'),
(294, 171, 2, 'Our Partners '),
(295, 172, 1, 'Tìm kiếm'),
(296, 172, 2, 'Search '),
(297, 173, 1, 'Danh mục cha'),
(298, 173, 2, 'Parent category '),
(299, 174, 1, 'Trước'),
(300, 175, 1, 'Sau'),
(301, 176, 1, 'Sắp xếp theo'),
(302, 177, 1, 'Tên sản phẩm'),
(303, 178, 1, 'Tăng dần'),
(304, 179, 1, 'Giảm dần'),
(305, 180, 1, 'Hiển thị'),
(306, 181, 1, 'Sản phẩm/Trang'),
(307, 175, 2, 'Next '),
(308, 174, 2, 'Previous '),
(309, 176, 2, 'Order by '),
(310, 177, 2, 'Product name '),
(311, 178, 2, 'Low to high '),
(312, 179, 2, 'High to low '),
(313, 180, 2, 'Display '),
(314, 181, 2, 'Product/Page '),
(315, 182, 1, 'error: phrase '),
(317, 184, 1, 'Thông tin Công ty'),
(318, 185, 1, 'Hãy trở thành Đối tác của chúng tôi'),
(319, 186, 1, 'Xí nghiệp Quang Trung hiện tại có hơn 100 đối tác trên Toàn quốc.....'),
(320, 187, 1, 'Liên hệ hoặc góp ý với Xí nghiệp Quang Trung'),
(321, 184, 2, 'Company info '),
(322, 185, 2, 'Become our Partners '),
(323, 188, 1, 'Giới thiệu về tổng giám đốc'),
(324, 189, 1, 'Giới thiệu về công ty'),
(325, 190, 1, 'Dự án đang triển khai'),
(326, 191, 1, 'Thành tích'),
(327, 192, 1, 'Dự án tiêu biểu'),
(328, 193, 1, 'Đơn hàng'),
(329, 194, 1, 'Tổng tiền hàng'),
(330, 195, 1, 'Địa chỉ chuyển hàng'),
(331, 196, 1, 'Tin nhắn'),
(332, 197, 1, 'Hóa đơn'),
(333, 198, 1, 'Thông tin đơn hàng'),
(334, 199, 1, 'Mã đơn hàng'),
(335, 200, 1, 'Thông tin khách hàng'),
(336, 201, 1, 'Số lượng mua'),
(338, 203, 1, 'Xuất ra excel'),
(339, 204, 1, 'Thêm ảnh'),
(340, 205, 1, 'Phụ đề'),
(341, 206, 1, 'Ảnh chính?'),
(344, 209, 1, 'Danh mục sản phẩm'),
(345, 210, 1, 'Trụ sở'),
(346, 211, 1, 'Nhà máy'),
(347, 212, 1, 'Văn phòng'),
(348, 213, 1, 'Hotline'),
(349, 214, 1, 'Kết nối với chúng tôi'),
(350, 215, 2, 'Sales consultant'),
(351, 215, 1, 'Tư vấn bán hàng '),
(353, 217, 1, 'Công nghệ & thiết bị sản xuất'),
(354, 217, 2, 'Technology & production equipment'),
(355, 218, 1, 'Slider'),
(356, 219, 1, 'Tìm kiếm sản phẩm'),
(357, 220, 1, 'Có {1} sản phẩm'),
(358, 221, 1, 'Sản phẩm mới nhất'),
(359, 222, 1, 'Giá: Từ thấp đến cao'),
(360, 223, 1, 'Giá: Từ cao xuống thấp'),
(361, 224, 1, 'Sản phẩm nổi bật'),
(362, 225, 1, 'Thêm vào giỏ hàng'),
(363, 226, 1, 'Thông tin sản phẩm'),
(364, 227, 1, 'Nhận xét của khách hàng'),
(365, 228, 1, 'Sản phẩm tương tự'),
(366, 229, 1, 'Chú thích {1}'),
(368, 231, 1, 'Tin tức'),
(369, 232, 1, 'Đọc tiếp'),
(370, 233, 1, 'Giỏ hàng của bạn'),
(371, 234, 1, 'Đơn giá'),
(372, 235, 1, 'Tổng tiền'),
(373, 236, 1, 'Hủy'),
(374, 237, 1, 'Tiếp tục mua sắm'),
(375, 238, 1, 'Đặt hàng'),
(376, 239, 1, 'Chưa có sản phẩm'),
(377, 240, 1, 'Đến giỏ hàng'),
(378, 241, 1, 'Công nghệ'),
(379, 242, 1, 'Thông tin đơn hàng'),
(380, 243, 1, 'Thông tin cá nhân'),
(381, 244, 1, 'Quý khách đã đặt hàng thành công! Chúng tôi sẽ liên hệ lại với Quý khách trong thời gian sớm nhất'),
(382, 245, 1, 'Cảm ơn Quý khách!'),
(383, 246, 1, 'Thông tin công ty'),
(384, 247, 1, 'Địa chỉ'),
(386, 249, 1, 'Showroom'),
(387, 250, 1, 'Tin tức liên quan'),
(388, 251, 1, 'Tin trước'),
(389, 252, 1, 'Tin sau'),
(390, 253, 1, 'Còn hàng'),
(391, 254, 1, 'Sản phẩm hiện tại đang hết hàng! Chúng tôi sẽ cập nhật trong thời gian sớm nhất'),
(392, 255, 1, 'Hết hàng'),
(393, 255, 2, 'Out of stock'),
(394, 256, 1, 'Thực hiện mục tiêu phát triển chiều sâu...'),
(395, 256, 2, 'Perform in-depth development goals'),
(396, 257, 1, 'Công nghệ'),
(397, 258, 1, 'Thiết bị'),
(398, 250, 2, 'Related Posts '),
(399, 253, 2, 'In stock '),
(400, 252, 2, 'Next '),
(401, 251, 2, 'Previous '),
(402, 249, 2, 'Showroom '),
(403, 259, 1, 'Xin cảm ơn. Tin nhắn của Quý vị đã được gửi thành công! Quang Trung sẽ liên hệ với Quý vị trong thời gian sớm nhất.'),
(404, 260, 1, 'Gửi không thành công! Vui lòng kiểm tra lại thông tin.'),
(405, 260, 2, 'Failed. Please check your information.  '),
(406, 259, 2, 'Thank you! Your message was sent successfully. Quang Trung will contact you as soon as possible.  '),
(407, 261, 1, 'Gửi từ'),
(408, 262, 1, 'Gửi đến'),
(409, 263, 1, 'Tiêu đề'),
(410, 264, 1, 'Trả lời'),
(411, 265, 1, 'Ngày gửi'),
(412, 265, 2, 'Sent date '),
(413, 264, 2, 'Reply '),
(414, 263, 2, 'Subject '),
(415, 262, 2, 'To '),
(416, 261, 2, 'From '),
(417, 266, 1, 'Các dự án tiêu biểu mà Tập đoàn Công nghiệp Quang Trung đã thực hiện'),
(418, 267, 1, 'Đã hủy'),
(419, 268, 1, 'Đơn hàng mới'),
(420, 269, 1, 'Đã xử lý'),
(421, 270, 1, 'Hủy đơn hàng'),
(422, 271, 1, 'Tiếp nhận đơn hàng'),
(423, 272, 1, 'Bạn có chắc không?'),
(424, 209, 2, 'Categories '),
(425, 273, 1, 'Văn phòng đại diện'),
(426, 274, 1, 'Nhà máy'),
(427, 275, 1, 'Bản đồ'),
(428, 276, 1, 'Những điểm mạnh của Sản phẩm Đèn Led Quang Trung'),
(429, 277, 1, 'Mã sản phẩm'),
(430, 278, 1, 'Xuất xứ'),
(431, 279, 1, 'Nhóm sản phẩm'),
(432, 280, 1, 'Thư viện Video'),
(433, 281, 1, 'Chọn từ máy tính'),
(434, 282, 1, 'Chọn từ URL'),
(435, 283, 1, 'Chỉ hỗ trợ link YouTube'),
(436, 284, 1, 'Kết quả tìm kiếm cho từ khóa: {1}'),
(437, 284, 2, 'Search results for keyword: {1} '),
(438, 285, 2, 'error: phrase [customer] not found'),
(439, 286, 2, 'error: phrase [general] not found'),
(440, 287, 2, 'error: phrase [payment] not found'),
(441, 288, 1, 'error: phrase [lists] not found'),
(442, 289, 1, 'error: phrase [country] not found'),
(443, 290, 1, 'error: phrase [languages] not found'),
(444, 291, 1, 'error: phrase [add_a_new_x] not found'),
(445, 292, 1, 'error: phrase [in_use] not found'),
(446, 293, 1, 'error: phrase [not_in_use] not found'),
(447, 302, 1, 'error: phrase [value] not found'),
(448, 303, 1, 'error: phrase [key] not found'),
(449, 304, 1, 'error: phrase [index] not found'),
(450, 305, 1, 'error: phrase [desc_general] not found'),
(451, 306, 1, 'error: phrase [site_name] not found'),
(452, 307, 1, 'error: phrase [desc_site_name] not found'),
(453, 308, 1, 'error: phrase [site_title] not found'),
(454, 309, 1, 'error: phrase [desc_site_title] not found'),
(455, 310, 1, 'error: phrase [site_keywords] not found'),
(456, 311, 1, 'error: phrase [desc_site_keywords] not found'),
(457, 312, 1, 'error: phrase [Bảo trì website?] not found'),
(458, 313, 1, 'error: phrase [desc_Bảo trì website?] not found'),
(459, 314, 1, 'error: phrase [general_active] not found'),
(460, 315, 1, 'error: phrase [desc_general_active] not found'),
(461, 316, 1, 'error: phrase [title_language] not found'),
(462, 317, 1, 'error: phrase [desc_title_language] not found'),
(463, 318, 1, 'error: phrase [north] not found'),
(464, 319, 1, 'error: phrase [middle] not found'),
(465, 320, 1, 'error: phrase [south] not found'),
(466, 321, 1, 'error: phrase [not_active] not found'),
(467, 322, 1, 'error: phrase [active] not found'),
(468, 323, 1, 'error: phrase [isset_setting] not found'),
(469, 324, 1, 'error: phrase [desc_isset_setting] not found'),
(470, 325, 1, 'error: phrase [test_abc] not found'),
(471, 326, 1, 'error: phrase [desc_test_abc] not found'),
(472, 327, 1, 'error: phrase [test] not found'),
(473, 328, 1, 'error: phrase [desc_test] not found'),
(474, 329, 1, 'error: phrase [text_field] not found'),
(475, 330, 1, 'error: phrase [desc_text_field] not found'),
(476, 331, 1, 'error: phrase [email_field] not found'),
(477, 332, 1, 'error: phrase [desc_email_field] not found'),
(478, 333, 1, 'error: phrase [number_field] not found'),
(479, 334, 1, 'error: phrase [desc_number_field] not found'),
(480, 335, 1, 'error: phrase [textarea_field] not found'),
(481, 336, 1, 'error: phrase [desc_textarea_field] not found'),
(482, 337, 1, 'error: phrase [color_field] not found'),
(483, 338, 1, 'error: phrase [desc_color_field] not found'),
(484, 339, 1, 'error: phrase [date_field] not found'),
(485, 340, 1, 'error: phrase [desc_date_field] not found'),
(486, 341, 1, 'error: phrase [time_field] not found'),
(487, 342, 1, 'error: phrase [desc_time_field] not found'),
(488, 343, 1, 'error: phrase [password_field] not found'),
(489, 344, 1, 'error: phrase [desc_password_field] not found'),
(490, 345, 1, 'error: phrase [roxymce_field] not found'),
(491, 346, 1, 'error: phrase [desc_roxymce_field] not found'),
(492, 347, 1, 'error: phrase [select_field] not found'),
(493, 348, 1, 'error: phrase [desc_select_field] not found'),
(494, 349, 1, 'error: phrase [additional] not found'),
(495, 350, 1, 'error: phrase [multiselect_field] not found'),
(496, 351, 1, 'error: phrase [desc_multiselect_field] not found'),
(497, 352, 1, 'error: phrase [file_field] not found'),
(498, 353, 1, 'error: phrase [desc_file_field] not found'),
(499, 354, 1, 'error: phrase [url_field] not found'),
(500, 355, 1, 'error: phrase [desc_url_field] not found'),
(501, 356, 1, 'error: phrase [percent_field] not found'),
(502, 357, 1, 'error: phrase [desc_percent_field] not found'),
(503, 358, 1, 'error: phrase [switch_field] not found'),
(504, 359, 1, 'error: phrase [desc_switch_field] not found'),
(505, 360, 1, 'error: phrase [checkbox_option] not found'),
(506, 361, 1, 'error: phrase [desc_checkbox_option] not found'),
(507, 362, 1, 'error: phrase [radio_option] not found'),
(508, 363, 1, 'error: phrase [desc_radio_option] not found'),
(509, 364, 1, 'error: phrase [department] not found'),
(510, 365, 1, 'error: phrase [password_mail] not found'),
(511, 366, 1, 'error: phrase [desc_password_mail] not found'),
(512, 367, 1, 'error: phrase [note] not found'),
(513, 368, 1, 'error: phrase [admin] not found'),
(514, 369, 1, 'error: phrase [import_to_excel] not found'),
(515, 370, 1, 'error: phrase [import_excel] not found');

-- --------------------------------------------------------

--
-- Table structure for table `popup`
--

CREATE TABLE `popup` (
  `id` int(11) NOT NULL,
  `background` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `popup`
--

INSERT INTO `popup` (`id`, `background`, `status`, `url`) VALUES
(85, '85_image.jpg', 1, '123');

-- --------------------------------------------------------

--
-- Table structure for table `popup_lang`
--

CREATE TABLE `popup_lang` (
  `id` int(11) NOT NULL,
  `popup_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `popup_lang`
--

INSERT INTO `popup_lang` (`id`, `popup_id`, `language`, `title`, `content`) VALUES
(1, 1, 'vi', 'sdfasd', '444'),
(2, 1, 'en', 'ádfasdf', 'ádfasdf'),
(3, 2, 'vi', 'sdfasd', 'ádfasdf'),
(4, 2, 'en', 'ádfasdf', 'ádfasdf'),
(5, 3, 'vi', 'dfasd', 'ádfasd'),
(6, 4, 'vi', 'dfasd', 'ádfasd'),
(7, 5, 'vi', 'dfasd', 'ádfasd'),
(8, 6, 'vi', 'dfasd', 'ádfasd'),
(9, 7, 'vi', 'dfgd', 'ag'),
(10, 8, 'vi', 'dfgd', 'ag'),
(11, 9, 'vi', 'dfgd', 'ag'),
(12, 10, 'vi', 'dfgd', 'ag'),
(13, 11, 'vi', 'ádf', 'ádf'),
(14, 12, 'vi', 'ádf', 'ádf'),
(15, 13, 'vi', 'ádf', 'ádf'),
(16, 14, 'vi', 'ádA', 'ÁDF'),
(17, 15, 'vi', 'ádA', 'ÁDF'),
(18, 16, 'vi', 'ádA', 'ÁDF'),
(19, 17, 'vi', 'ádA', 'ÁDF'),
(20, 18, 'vi', 'ádA', 'ÁDF'),
(21, 19, 'vi', 'sdfd', 'sdf'),
(22, 20, 'vi', 'fasdf', 'áda'),
(23, 21, 'vi', 'dsaf', 'ádf'),
(24, 22, 'vi', 'sdf', 'ádf'),
(25, 23, 'vi', 'fdasf', 'sdf'),
(26, 24, 'vi', 'ád', 'ádf'),
(27, 25, 'vi', 'ádf', 'ádf'),
(28, 26, 'vi', 'èasd', 'sdf'),
(29, 27, 'vi', '1231', 'sdf'),
(30, 28, 'vi', 'sdf', 'ádf'),
(31, 29, 'vi', 'sdf', 'ádf'),
(32, 30, 'vi', 'sdf', 'ádf'),
(33, 31, 'vi', 'sdf', '123'),
(34, 32, 'vi', 'sdzf', 'ádf'),
(35, 33, 'vi', 'sdzf', 'ádf'),
(36, 34, 'vi', 'sdzf', 'ádf'),
(37, 35, 'vi', 'ádf', 'ádf'),
(38, 36, 'vi', 'ádf', 'ádf'),
(39, 37, 'vi', 'ádf', 'ádf'),
(40, 38, 'vi', 'ádf', 'ádf'),
(41, 39, 'vi', 'ádf', 'ádf'),
(42, 40, 'vi', 'ádf', 'ádf'),
(43, 41, 'vi', 'ádf', 'ádf'),
(44, 42, 'vi', 'ádf', 'ádf'),
(45, 43, 'vi', 'ádf', 'ádf'),
(46, 44, 'vi', 'ádf', 'ádf'),
(47, 45, 'vi', 'ádf', 'ádf'),
(48, 46, 'vi', 'ádf', 'ádf'),
(49, 47, 'vi', 'ádf', 'ádf'),
(50, 48, 'vi', 'ádf', 'ádf'),
(51, 49, 'vi', 'ádf', 'ádf'),
(52, 50, 'vi', 'ádf', 'ádf'),
(53, 51, 'vi', 'ádf', 'ádf'),
(54, 52, 'vi', 'ádf', 'ádf'),
(55, 53, 'vi', 'ádf', 'ádf'),
(56, 54, 'vi', 'ádf', 'ádf'),
(57, 55, 'vi', 'ádf', 'ádf'),
(58, 56, 'vi', 'ádf', 'ádf'),
(59, 57, 'vi', 'ádf', 'ádf'),
(60, 58, 'vi', 'ádf', 'ádf'),
(61, 59, 'vi', 'ádf', 'ádf'),
(62, 60, 'vi', 'ádf', 'ádf'),
(63, 61, 'vi', 'ádf', 'ádf'),
(64, 62, 'vi', 'ádf', 'ádf'),
(65, 63, 'vi', 'ádf', 'ádf'),
(66, 64, 'vi', 'ádf', 'ádf'),
(67, 65, 'vi', 'ádf', 'ádf'),
(68, 66, 'vi', 'ádf', 'ádf'),
(69, 67, 'vi', 'cvasdf', 'ádf'),
(70, 68, 'vi', 'cvasdf', 'ádf'),
(71, 69, 'vi', 'ádf', 'ádf'),
(72, 70, 'vi', 'ádf', 'ádf'),
(73, 71, 'vi', 'ádf', 'ádf'),
(74, 72, 'vi', 'ádf', 'ádf'),
(75, 73, 'vi', 'ádf', 'ádf'),
(76, 74, 'vi', 'ádf', 'ádf'),
(77, 75, 'vi', 'ádf', 'ádf'),
(78, 76, 'vi', '11', '11'),
(79, 77, 'vi', 'ádf', 'ádf'),
(80, 78, 'vi', 'sdf', 'sdf'),
(81, 79, 'vi', 'ádf', 'ádf'),
(82, 80, 'vi', 'ádf', 'ádf'),
(83, 81, 'vi', 'ádf', 'ádf'),
(84, 82, 'vi', 'ádf', 'ád'),
(87, 85, 'vi', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `category_id`, `image`, `created_date`, `feature`, `status`) VALUES
(13, 38, '13_image.jpg', '2016-05-17 03:40:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_lang`
--

CREATE TABLE `post_lang` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_lang`
--

INSERT INTO `post_lang` (`id`, `post_id`, `language`, `title`, `description`, `content`) VALUES
(12, 13, 'vi', '122', '1221', '<p>1212</p>');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  `in_stock` int(11) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `capability` varchar(255) DEFAULT NULL,
  `sale_off` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `code`, `origin`, `image`, `created_date`, `feature`, `status`, `price`, `in_stock`, `instructions`, `capability`, `sale_off`) VALUES
(293, 32, '1', NULL, 'http://enesti.com.vn/uploads/shops/kem_tay/tra_xanh_146.jpg', '2016-05-16 08:51:03', 0, 1, 144000, 200, 'Test', '160g', '0.30'),
(294, 32, '2', NULL, 'http://enesti.com.vn/uploads/shops/kem_tay/gao_suatuoi_146.jpg', '2016-05-16 08:51:03', 0, 1, 144000, 200, 'Test', '160g', '0.30'),
(295, 32, '3', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/chanh_146.jpg', '2016-05-16 08:51:03', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(296, 32, '4', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/gao_146.jpg', '2016-05-16 08:51:03', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(297, 32, '5', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/lo_hoi_146.jpg', '2016-05-16 08:51:03', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(298, 32, '6', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/oliu_146.jpg', '2016-05-16 08:51:03', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(299, 32, '7', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/luu_146.jpg', '2016-05-16 08:51:03', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(300, 33, '8', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/1456217182195_4524013.jpg', '2016-05-16 08:51:03', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(301, 33, '9', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/damday-146.jpg', '2016-05-16 08:51:03', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(302, 33, '10', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/congvadai_146.jpg', '2016-05-16 08:51:03', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(303, 33, '11', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/chong_nuoc_146.jpg', '2016-05-16 08:51:03', 0, 1, 195000, 200, 'Test', '7ml', '0.00'),
(304, 32, '12', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/nuoc_hoa_146.jpg', '2016-05-16 08:51:03', 0, 1, 250000, 200, 'Test', '150ml', '0.00'),
(305, 32, '13', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/sua_duong_146.jpg', '2016-05-16 08:51:03', 0, 1, 250000, 200, 'Test', '150ml', '0.00'),
(306, 32, '14', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/kem_duong_146.jpg', '2016-05-16 08:51:03', 0, 1, 270000, 200, 'Test', '50ml', '0.00'),
(307, 32, '15', NULL, 'http://enesti.com.vn/uploads/shops/2015_01/dd_remine_collagen_700_700.jpg', '2016-05-16 08:51:03', 0, 1, 240000, 200, 'Test', '65ml', '0.00'),
(308, 32, '16', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/nuoc_hoa_hong_146.jpg', '2016-05-16 08:51:04', 0, 1, 776000, 200, 'Test', '130ml', '0.00'),
(309, 32, '17', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/sua_duong_146.jpg', '2016-05-16 08:51:04', 0, 1, 776000, 200, 'Test', '130ml', '0.00'),
(310, 32, '18', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/kem_duong_146.jpg', '2016-05-16 08:51:04', 0, 1, 848000, 200, 'Test', '50ml', '0.00'),
(311, 32, '19', NULL, 'http://enesti.com.vn/uploads/shops/crystal/nuoc_hoa_146.jpg', '2016-05-16 08:51:04', 0, 1, 240000, 200, 'Test', '150ml', '0.00'),
(312, 32, '20', NULL, 'http://enesti.com.vn/uploads/shops/crystal/sua_duong_146.jpg', '2016-05-16 08:51:04', 0, 1, 240000, 200, 'Test', '150ml', '0.00'),
(313, 32, '21', NULL, 'http://enesti.com.vn/uploads/shops/crystal/kem_duong_146.jpg', '2016-05-16 08:51:04', 0, 1, 264000, 200, 'Test', '50mg', '0.00'),
(314, 34, '22', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/freshcotton_146.jpg', '2016-05-16 08:51:04', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(315, 34, '23', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/golden_rose_146.jpg', '2016-05-16 08:51:04', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(316, 34, '24', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/babypowder_146.jpg', '2016-05-16 08:51:04', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(317, 34, '25', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/lovelyday_146.jpg', '2016-05-16 08:51:04', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(318, 34, '26', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/mayme_146.jpg', '2016-05-16 08:51:04', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(319, 33, '27', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo01_146.jpg', '2016-05-16 08:51:04', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(320, 33, '28', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo02_146.jpg', '2016-05-16 08:51:04', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(321, 33, '29', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo03_146.jpg', '2016-05-16 08:51:04', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(322, 33, '30', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo04_146.jpg', '2016-05-16 08:51:04', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(323, 33, '31', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/31_146.jpg', '2016-05-16 08:51:05', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(324, 33, '32', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/32_146.jpg', '2016-05-16 08:51:05', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(325, 33, '33', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/33_146.jpg', '2016-05-16 08:51:05', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(326, 33, '34', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/34_146.jpg', '2016-05-16 08:51:05', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(327, 33, '35', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/36_146.jpg', '2016-05-16 08:51:05', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(328, 33, '36', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/37_146.jpg', '2016-05-16 08:51:05', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(329, 35, '37', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/30_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(330, 35, '38', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/10_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(331, 35, '39', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/28_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(332, 35, '40', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/63_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(333, 35, '41', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/40_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(334, 35, '42', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/51_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(335, 35, '43', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/50_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(336, 35, '44', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/53_164.jpg', '2016-05-16 08:51:05', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(337, 32, '45', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/collagen_146.jpg', '2016-05-16 08:51:05', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(338, 32, '46', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/ngoc_trai_146.jpg', '2016-05-16 08:51:06', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(339, 32, '47', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/lo_hoi_146.jpg', '2016-05-16 08:51:06', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(340, 32, '48', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/gao_146.jpg', '2016-05-16 08:51:06', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(341, 33, '49', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/ghi-den.jpg', '2016-05-16 08:51:06', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(342, 33, '50', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/den.jpg', '2016-05-16 08:51:06', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(343, 33, '51', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau-ghi.jpg', '2016-05-16 08:51:06', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(344, 33, '52', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau.jpg', '2016-05-16 08:51:06', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(345, 33, '53', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau-den.jpg', '2016-05-16 08:51:06', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(346, 32, '54', NULL, 'http://enesti.com.vn/uploads/shops/kem_chong_nang/chong_nang_146.jpg', '2016-05-16 08:51:06', 0, 1, 165000, 200, 'Test', '40ml', '0.00'),
(347, 33, '55', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/da_chucnang_146.jpg', '2016-05-16 08:51:06', 0, 1, 318000, 200, 'Test', '50g', '0.00'),
(348, 33, '56', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/3_chucnang_146.jpg', '2016-05-16 08:51:06', 0, 1, 318000, 200, 'Test', '45g', '0.00'),
(349, 33, '57', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_cc_146.png', '2016-05-16 08:51:06', 0, 1, 300000, 200, 'Test', '30g', '0.00'),
(350, 33, '58', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_bb_xoyun_146.jpg', '2016-05-16 08:51:06', 0, 1, 354000, 200, 'Test', '40g', '0.00'),
(351, 33, '59', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/phan-21.jpg', '2016-05-16 08:51:06', 0, 1, 360000, 200, 'Test', '12g', '0.00'),
(352, 33, '60', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/phan-23.jpg', '2016-05-16 08:51:06', 0, 1, 360000, 200, 'Test', '12g', '0.00'),
(353, 35, '61', NULL, 'http://enesti.com.vn/uploads/shops/v_v/toc_oliu_146.jpg', '2016-05-16 08:51:06', 0, 1, 210000, 200, 'Test', '80ml', '0.00'),
(354, 32, '62', NULL, 'http://enesti.com.vn/uploads/shops/v_v/ocsen_146.jpg', '2016-05-16 08:51:06', 0, 1, 150000, 200, 'Test', '50ml', '0.00'),
(355, 33, '63', NULL, 'http://enesti.com.vn/uploads/shops/but/but-de-dang.jpg', '2016-05-16 08:51:06', 0, 1, 195000, 200, 'Test', '5ml', '0.00'),
(356, 33, '64', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/1456217645593_8896302.jpg', '2016-05-16 08:51:06', 0, 1, 149000, 200, 'Test', '0.5g', '0.00'),
(357, 32, '65', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-am-suansu-water-bank-capsule.png', '2016-05-16 08:51:07', 0, 1, 268000, 200, 'Test', '50mg', '0.00'),
(358, 32, '66', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-trang-crystal-whitening.png', '2016-05-16 08:51:07', 0, 1, 268000, 200, 'Test', '50mg', '0.00'),
(359, 32, '68', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-lo-hoi.png', '2016-05-16 08:51:07', 0, 1, 198000, 200, 'Test', '150ml+60ml', '0.00'),
(360, 32, '69', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-oliu.png', '2016-05-16 08:51:07', 0, 1, 198000, 200, 'Test', '150ml+60ml', '0.00'),
(361, 32, '72', NULL, 'http://enesti.com.vn/uploads/shops/v_v/gel_lohoi_146.jpg', '2016-05-16 08:51:07', 0, 1, 85000, 200, 'Test', '120ml', '0.00'),
(362, 32, '74', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/1441966298_d7h_1_20150911__3_chon_003.jpg', '2016-05-16 08:51:07', 0, 1, 150000, 200, 'Test', '300ml', '0.00'),
(363, 32, '1', NULL, 'http://enesti.com.vn/uploads/shops/kem_tay/tra_xanh_146.jpg', '2016-05-16 11:22:56', 0, 1, 144000, 200, 'Test', '160g', '0.30'),
(364, 32, '2', NULL, 'http://enesti.com.vn/uploads/shops/kem_tay/gao_suatuoi_146.jpg', '2016-05-16 11:22:56', 0, 1, 144000, 200, 'Test', '160g', '0.30'),
(365, 32, '3', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/chanh_146.jpg', '2016-05-16 11:22:56', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(366, 32, '4', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/gao_146.jpg', '2016-05-16 11:22:56', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(367, 32, '5', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/lo_hoi_146.jpg', '2016-05-16 11:22:56', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(368, 32, '6', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/oliu_146.jpg', '2016-05-16 11:22:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(369, 32, '7', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/luu_146.jpg', '2016-05-16 11:22:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(370, 33, '8', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/1456217182195_4524013.jpg', '2016-05-16 11:22:57', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(371, 33, '9', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/damday-146.jpg', '2016-05-16 11:22:57', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(372, 33, '10', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/congvadai_146.jpg', '2016-05-16 11:22:57', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(373, 33, '11', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/chong_nuoc_146.jpg', '2016-05-16 11:22:57', 0, 1, 195000, 200, 'Test', '7ml', '0.00'),
(374, 32, '12', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/nuoc_hoa_146.jpg', '2016-05-16 11:22:57', 0, 1, 250000, 200, 'Test', '150ml', '0.00'),
(375, 32, '13', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/sua_duong_146.jpg', '2016-05-16 11:22:57', 0, 1, 250000, 200, 'Test', '150ml', '0.00'),
(376, 32, '14', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/kem_duong_146.jpg', '2016-05-16 11:22:57', 0, 1, 270000, 200, 'Test', '50ml', '0.00'),
(377, 32, '15', NULL, 'http://enesti.com.vn/uploads/shops/2015_01/dd_remine_collagen_700_700.jpg', '2016-05-16 11:22:57', 0, 1, 240000, 200, 'Test', '65ml', '0.00'),
(378, 32, '16', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/nuoc_hoa_hong_146.jpg', '2016-05-16 11:22:57', 0, 1, 776000, 200, 'Test', '130ml', '0.00'),
(379, 32, '17', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/sua_duong_146.jpg', '2016-05-16 11:22:57', 0, 1, 776000, 200, 'Test', '130ml', '0.00'),
(380, 32, '18', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/kem_duong_146.jpg', '2016-05-16 11:22:57', 0, 1, 848000, 200, 'Test', '50ml', '0.00'),
(381, 32, '19', NULL, 'http://enesti.com.vn/uploads/shops/crystal/nuoc_hoa_146.jpg', '2016-05-16 11:22:57', 0, 1, 240000, 200, 'Test', '150ml', '0.00'),
(382, 32, '20', NULL, 'http://enesti.com.vn/uploads/shops/crystal/sua_duong_146.jpg', '2016-05-16 11:22:57', 0, 1, 240000, 200, 'Test', '150ml', '0.00'),
(383, 32, '21', NULL, 'http://enesti.com.vn/uploads/shops/crystal/kem_duong_146.jpg', '2016-05-16 11:22:57', 0, 1, 264000, 200, 'Test', '50mg', '0.00'),
(384, 34, '22', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/freshcotton_146.jpg', '2016-05-16 11:22:57', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(385, 34, '23', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/golden_rose_146.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(386, 34, '24', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/babypowder_146.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(387, 34, '25', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/lovelyday_146.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(388, 34, '26', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/mayme_146.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(389, 33, '27', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo01_146.jpg', '2016-05-16 11:22:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(390, 33, '28', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo02_146.jpg', '2016-05-16 11:22:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(391, 33, '29', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo03_146.jpg', '2016-05-16 11:22:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(392, 33, '30', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo04_146.jpg', '2016-05-16 11:22:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(393, 33, '31', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/31_146.jpg', '2016-05-16 11:22:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(394, 33, '32', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/32_146.jpg', '2016-05-16 11:22:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(395, 33, '33', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/33_146.jpg', '2016-05-16 11:22:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(396, 33, '34', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/34_146.jpg', '2016-05-16 11:22:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(397, 33, '35', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/36_146.jpg', '2016-05-16 11:22:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(398, 33, '36', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/37_146.jpg', '2016-05-16 11:22:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(399, 35, '37', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/30_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(400, 35, '38', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/10_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(401, 35, '39', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/28_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(402, 35, '40', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/63_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(403, 35, '41', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/40_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(404, 35, '42', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/51_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(405, 35, '43', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/50_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(406, 35, '44', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/53_164.jpg', '2016-05-16 11:22:58', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(407, 32, '45', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/collagen_146.jpg', '2016-05-16 11:22:58', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(408, 32, '46', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/ngoc_trai_146.jpg', '2016-05-16 11:22:58', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(409, 32, '47', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/lo_hoi_146.jpg', '2016-05-16 11:22:58', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(410, 32, '48', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/gao_146.jpg', '2016-05-16 11:22:59', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(411, 33, '49', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/ghi-den.jpg', '2016-05-16 11:22:59', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(412, 33, '50', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/den.jpg', '2016-05-16 11:22:59', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(413, 33, '51', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau-ghi.jpg', '2016-05-16 11:22:59', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(414, 33, '52', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau.jpg', '2016-05-16 11:22:59', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(415, 33, '53', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau-den.jpg', '2016-05-16 11:22:59', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(416, 32, '54', NULL, 'http://enesti.com.vn/uploads/shops/kem_chong_nang/chong_nang_146.jpg', '2016-05-16 11:22:59', 0, 1, 165000, 200, 'Test', '40ml', '0.00'),
(417, 33, '55', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/da_chucnang_146.jpg', '2016-05-16 11:22:59', 0, 1, 318000, 200, 'Test', '50g', '0.00'),
(418, 33, '56', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/3_chucnang_146.jpg', '2016-05-16 11:22:59', 0, 1, 318000, 200, 'Test', '45g', '0.00'),
(419, 33, '57', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_cc_146.png', '2016-05-16 11:22:59', 0, 1, 300000, 200, 'Test', '30g', '0.00'),
(420, 33, '58', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_bb_xoyun_146.jpg', '2016-05-16 11:22:59', 0, 1, 354000, 200, 'Test', '40g', '0.00'),
(421, 33, '59', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/phan-21.jpg', '2016-05-16 11:22:59', 0, 1, 360000, 200, 'Test', '12g', '0.00'),
(422, 33, '60', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/phan-23.jpg', '2016-05-16 11:22:59', 0, 1, 360000, 200, 'Test', '12g', '0.00'),
(423, 35, '61', NULL, 'http://enesti.com.vn/uploads/shops/v_v/toc_oliu_146.jpg', '2016-05-16 11:22:59', 0, 1, 210000, 200, 'Test', '80ml', '0.00'),
(424, 32, '62', NULL, 'http://enesti.com.vn/uploads/shops/v_v/ocsen_146.jpg', '2016-05-16 11:22:59', 0, 1, 150000, 200, 'Test', '50ml', '0.00'),
(425, 33, '63', NULL, 'http://enesti.com.vn/uploads/shops/but/but-de-dang.jpg', '2016-05-16 11:22:59', 0, 1, 195000, 200, 'Test', '5ml', '0.00'),
(426, 33, '64', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/1456217645593_8896302.jpg', '2016-05-16 11:23:00', 0, 1, 149000, 200, 'Test', '0.5g', '0.00'),
(427, 32, '65', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-am-suansu-water-bank-capsule.png', '2016-05-16 11:23:00', 0, 1, 268000, 200, 'Test', '50mg', '0.00'),
(428, 32, '66', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-trang-crystal-whitening.png', '2016-05-16 11:23:00', 0, 1, 268000, 200, 'Test', '50mg', '0.00'),
(429, 32, '68', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-lo-hoi.png', '2016-05-16 11:23:00', 0, 1, 198000, 200, 'Test', '150ml+60ml', '0.00'),
(430, 32, '69', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-oliu.png', '2016-05-16 11:23:00', 0, 1, 198000, 200, 'Test', '150ml+60ml', '0.00'),
(431, 32, '72', NULL, 'http://enesti.com.vn/uploads/shops/v_v/gel_lohoi_146.jpg', '2016-05-16 11:23:00', 0, 1, 85000, 200, 'Test', '120ml', '0.00'),
(432, 32, '74', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/1441966298_d7h_1_20150911__3_chon_003.jpg', '2016-05-16 11:23:00', 0, 1, 150000, 200, 'Test', '300ml', '0.00'),
(433, 32, '1', NULL, 'http://enesti.com.vn/uploads/shops/kem_tay/tra_xanh_146.jpg', '2016-05-16 11:33:57', 0, 1, 144000, 200, 'Test', '160g', '0.30'),
(434, 32, '2', NULL, 'http://enesti.com.vn/uploads/shops/kem_tay/gao_suatuoi_146.jpg', '2016-05-16 11:33:57', 0, 1, 144000, 200, 'Test', '160g', '0.30'),
(435, 32, '3', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/chanh_146.jpg', '2016-05-16 11:33:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(436, 32, '4', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/gao_146.jpg', '2016-05-16 11:33:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(437, 32, '5', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/lo_hoi_146.jpg', '2016-05-16 11:33:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(438, 32, '6', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/oliu_146.jpg', '2016-05-16 11:33:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(439, 32, '7', NULL, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/luu_146.jpg', '2016-05-16 11:33:57', 0, 1, 138000, 200, 'Test', '160g', '0.20'),
(440, 33, '8', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/1456217182195_4524013.jpg', '2016-05-16 11:33:57', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(441, 33, '9', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/damday-146.jpg', '2016-05-16 11:33:57', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(442, 33, '10', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/congvadai_146.jpg', '2016-05-16 11:33:57', 0, 1, 149000, 200, 'Test', '7ml', '0.00'),
(443, 33, '11', NULL, 'http://enesti.com.vn/uploads/shops/chuot_mi/chong_nuoc_146.jpg', '2016-05-16 11:33:57', 0, 1, 195000, 200, 'Test', '7ml', '0.00'),
(444, 32, '12', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/nuoc_hoa_146.jpg', '2016-05-16 11:33:57', 0, 1, 250000, 200, 'Test', '150ml', '0.00'),
(445, 32, '13', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/sua_duong_146.jpg', '2016-05-16 11:33:57', 0, 1, 250000, 200, 'Test', '150ml', '0.00'),
(446, 32, '14', NULL, 'http://enesti.com.vn/uploads/shops/remine_collagen/kem_duong_146.jpg', '2016-05-16 11:33:57', 0, 1, 270000, 200, 'Test', '50ml', '0.00'),
(447, 32, '15', NULL, 'http://enesti.com.vn/uploads/shops/2015_01/dd_remine_collagen_700_700.jpg', '2016-05-16 11:33:57', 0, 1, 240000, 200, 'Test', '65ml', '0.00'),
(448, 32, '16', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/nuoc_hoa_hong_146.jpg', '2016-05-16 11:33:57', 0, 1, 776000, 200, 'Test', '130ml', '0.00'),
(449, 32, '17', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/sua_duong_146.jpg', '2016-05-16 11:33:57', 0, 1, 776000, 200, 'Test', '130ml', '0.00'),
(450, 32, '18', NULL, 'http://enesti.com.vn/uploads/shops/xoyun/kem_duong_146.jpg', '2016-05-16 11:33:57', 0, 1, 848000, 200, 'Test', '50ml', '0.00'),
(451, 32, '19', NULL, 'http://enesti.com.vn/uploads/shops/crystal/nuoc_hoa_146.jpg', '2016-05-16 11:33:57', 0, 1, 240000, 200, 'Test', '150ml', '0.00'),
(452, 32, '20', NULL, 'http://enesti.com.vn/uploads/shops/crystal/sua_duong_146.jpg', '2016-05-16 11:33:58', 0, 1, 240000, 200, 'Test', '150ml', '0.00'),
(453, 32, '21', NULL, 'http://enesti.com.vn/uploads/shops/crystal/kem_duong_146.jpg', '2016-05-16 11:33:58', 0, 1, 264000, 200, 'Test', '50mg', '0.00'),
(454, 34, '22', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/freshcotton_146.jpg', '2016-05-16 11:33:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(455, 34, '23', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/golden_rose_146.jpg', '2016-05-16 11:33:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(456, 34, '24', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/babypowder_146.jpg', '2016-05-16 11:33:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(457, 34, '25', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/lovelyday_146.jpg', '2016-05-16 11:33:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(458, 34, '26', NULL, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/mayme_146.jpg', '2016-05-16 11:33:58', 0, 1, 125000, 200, 'Test', '15ml', '0.40'),
(459, 33, '27', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo01_146.jpg', '2016-05-16 11:33:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(460, 33, '28', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo02_146.jpg', '2016-05-16 11:33:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(461, 33, '29', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo03_146.jpg', '2016-05-16 11:33:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(462, 33, '30', NULL, 'http://enesti.com.vn/uploads/shops/son_bong/lo04_146.jpg', '2016-05-16 11:33:58', 0, 1, 149000, 200, 'Test', '8ml', '0.00'),
(463, 33, '31', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/31_146.jpg', '2016-05-16 11:33:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(464, 33, '32', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/32_146.jpg', '2016-05-16 11:33:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(465, 33, '33', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/33_146.jpg', '2016-05-16 11:33:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(466, 33, '34', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/34_146.jpg', '2016-05-16 11:33:58', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(467, 33, '35', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/36_146.jpg', '2016-05-16 11:33:59', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(468, 33, '36', NULL, 'http://enesti.com.vn/uploads/shops/son_enesti/37_146.jpg', '2016-05-16 11:33:59', 0, 1, 165000, 200, 'Test', '3.4g', '0.00'),
(469, 35, '37', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/30_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(470, 35, '38', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/10_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(471, 35, '39', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/28_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(472, 35, '40', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/63_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(473, 35, '41', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/40_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(474, 35, '42', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/51_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(475, 35, '43', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/50_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(476, 35, '44', NULL, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/53_164.jpg', '2016-05-16 11:33:59', 0, 1, 125000, 200, 'Test', '60ml+10ml', '0.10'),
(477, 32, '45', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/collagen_146.jpg', '2016-05-16 11:34:00', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(478, 32, '46', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/ngoc_trai_146.jpg', '2016-05-16 11:34:00', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(479, 32, '47', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/lo_hoi_146.jpg', '2016-05-16 11:34:00', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(480, 32, '48', NULL, 'http://enesti.com.vn/uploads/shops/mat_na/gao_146.jpg', '2016-05-16 11:34:00', 0, 1, 20000, 200, 'Test', '25g', '0.50'),
(481, 33, '49', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/ghi-den.jpg', '2016-05-16 11:34:00', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(482, 33, '50', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/den.jpg', '2016-05-16 11:34:00', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(483, 33, '51', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau-ghi.jpg', '2016-05-16 11:34:00', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(484, 33, '52', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau.jpg', '2016-05-16 11:34:00', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(485, 33, '53', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/nau-den.jpg', '2016-05-16 11:34:00', 0, 1, 75000, 200, 'Test', '5ml', '0.00'),
(486, 32, '54', NULL, 'http://enesti.com.vn/uploads/shops/kem_chong_nang/chong_nang_146.jpg', '2016-05-16 11:34:00', 0, 1, 165000, 200, 'Test', '40ml', '0.00'),
(487, 33, '55', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/da_chucnang_146.jpg', '2016-05-16 11:34:00', 0, 1, 318000, 200, 'Test', '50g', '0.00'),
(488, 33, '56', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/3_chucnang_146.jpg', '2016-05-16 11:34:00', 0, 1, 318000, 200, 'Test', '45g', '0.00'),
(489, 33, '57', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_cc_146.png', '2016-05-16 11:34:00', 0, 1, 300000, 200, 'Test', '30g', '0.00'),
(490, 33, '58', NULL, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_bb_xoyun_146.jpg', '2016-05-16 11:34:00', 0, 1, 354000, 200, 'Test', '40g', '0.00'),
(491, 33, '59', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/phan-21.jpg', '2016-05-16 11:34:00', 0, 1, 360000, 200, 'Test', '12g', '0.00'),
(492, 33, '60', NULL, 'http://enesti.com.vn/uploads/shops/2015_10/phan-23.jpg', '2016-05-16 11:34:00', 0, 1, 360000, 200, 'Test', '12g', '0.00'),
(493, 35, '61', NULL, 'http://enesti.com.vn/uploads/shops/v_v/toc_oliu_146.jpg', '2016-05-16 11:34:00', 0, 1, 210000, 200, 'Test', '80ml', '0.00'),
(494, 32, '62', NULL, 'http://enesti.com.vn/uploads/shops/v_v/ocsen_146.jpg', '2016-05-16 11:34:01', 0, 1, 150000, 200, 'Test', '50ml', '0.00'),
(495, 33, '63', NULL, 'http://enesti.com.vn/uploads/shops/but/but-de-dang.jpg', '2016-05-16 11:34:01', 0, 1, 195000, 200, 'Test', '5ml', '0.00'),
(496, 33, '64', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/1456217645593_8896302.jpg', '2016-05-16 11:34:01', 0, 1, 149000, 200, 'Test', '0.5g', '0.00'),
(497, 32, '65', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-am-suansu-water-bank-capsule.png', '2016-05-16 11:34:01', 0, 1, 268000, 200, 'Test', '50mg', '0.00'),
(498, 32, '66', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-trang-crystal-whitening.png', '2016-05-16 11:34:01', 0, 1, 268000, 200, 'Test', '50mg', '0.00'),
(499, 32, '68', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-lo-hoi.png', '2016-05-16 11:34:01', 0, 1, 198000, 200, 'Test', '150ml+60ml', '0.00'),
(500, 32, '69', NULL, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-oliu.png', '2016-05-16 11:34:01', 0, 1, 198000, 200, 'Test', '150ml+60ml', '0.00'),
(501, 32, '72', '', '501_image.jpg', '2016-05-16 11:34:01', 0, 1, 85000, 200, 'Test', '120ml', '0.00'),
(502, 32, '74', '', '502_image.jpg', '2016-05-16 11:34:01', 0, 1, 150000, 200, 'Test', '300ml', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `product_lang`
--

CREATE TABLE `product_lang` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_lang`
--

INSERT INTO `product_lang` (`id`, `product_id`, `language`, `title`, `content`) VALUES
(288, 293, 'vi', 'Kem tẩy trang Day-to-day / Trà xanh', 'Kem tẩy trang Day-to-day / Trà xanh'),
(289, 294, 'vi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi'),
(290, 295, 'vi', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh'),
(291, 296, 'vi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi'),
(292, 297, 'vi', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong'),
(293, 298, 'vi', 'Sữa rửa mặt Day-to-day / Oliu', 'Sữa rửa mặt Day-to-day / Oliu'),
(294, 299, 'vi', 'Sữa rửa mặt Day-to-Day / Lựu', 'Sữa rửa mặt Day-to-Day / Lựu'),
(295, 300, 'vi', 'Chuốt mi / Siêu dài', 'Chuốt mi / Siêu dài'),
(296, 301, 'vi', 'Chuốt mi / Đậm dày', 'Chuốt mi / Đậm dày'),
(297, 302, 'vi', 'Chuốt mi / Cong và dài', 'Chuốt mi / Cong và dài'),
(298, 303, 'vi', 'Chuốt mi chống nước / Cong và dài', 'Chuốt mi chống nước / Cong và dài'),
(299, 304, 'vi', 'Nước hoa hồng Remine Collagen', 'Nước hoa hồng Remine Collagen'),
(300, 305, 'vi', 'Sữa dưỡng da Remine Collagen', 'Sữa dưỡng da Remine Collagen'),
(301, 306, 'vi', 'Kem dưỡng da Remine Collagen', 'Kem dưỡng da Remine Collagen'),
(302, 307, 'vi', 'Dung dịch Remine collagen', 'Dung dịch Remine collagen'),
(303, 308, 'vi', 'Nước hoa hồng thảo dược Xơ-yun', 'Nước hoa hồng thảo dược Xơ-yun'),
(304, 309, 'vi', 'Sữa dưỡng thảo dược Xơ-yun', 'Sữa dưỡng thảo dược Xơ-yun'),
(305, 310, 'vi', 'Kem dưỡng thảo dược Xơ-yun', 'Kem dưỡng thảo dược Xơ-yun'),
(306, 311, 'vi', 'Nước hoa hồng Remine Crystal', 'Nước hoa hồng Remine Crystal'),
(307, 312, 'vi', 'Sữa dưỡng ẩm Remine Crystal', 'Sữa dưỡng ẩm Remine Crystal'),
(308, 313, 'vi', 'Kem dưỡng trắng Remine Crystal ', 'Kem dưỡng trắng Remine Crystal '),
(309, 314, 'vi', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton'),
(310, 315, 'vi', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng'),
(311, 316, 'vi', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ'),
(312, 317, 'vi', 'Nước hoa Enesti Lovely Day / Ngày tình yêu', 'Nước hoa Enesti Lovely Day / Ngày tình yêu'),
(313, 318, 'vi', 'Nước hoa Enesti May Me / Là chính tôi', 'Nước hoa Enesti May Me / Là chính tôi'),
(314, 319, 'vi', 'Son bóng dưỡng môi / L01 Hồng đỏ', 'Son bóng dưỡng môi / L01 Hồng đỏ'),
(315, 320, 'vi', 'Son bóng dưỡng môi / L02 Vàng Cam', 'Son bóng dưỡng môi / L02 Vàng Cam'),
(316, 321, 'vi', 'Son bóng dưỡng môi / L03 Hồng', 'Son bóng dưỡng môi / L03 Hồng'),
(317, 322, 'vi', 'Son bóng dưỡng môi / L04 Hồng tím', 'Son bóng dưỡng môi / L04 Hồng tím'),
(318, 323, 'vi', 'Son enesti số 31', 'Son enesti số 31'),
(319, 324, 'vi', 'Son enesti số 32', 'Son enesti số 32'),
(320, 325, 'vi', 'Son enesti số 33', 'Son enesti số 33'),
(321, 326, 'vi', 'Son enesti số 34', 'Son enesti số 34'),
(322, 327, 'vi', 'Son enesti số 36', 'Son enesti số 36'),
(323, 328, 'vi', 'Son enesti số 37', 'Son enesti số 37'),
(324, 329, 'vi', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)'),
(325, 330, 'vi', 'Thuốc nhuộm tóc Aroma / Đen (1/0)', 'Thuốc nhuộm tóc Aroma / Đen (1/0)'),
(326, 331, 'vi', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)'),
(327, 332, 'vi', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)'),
(328, 333, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)'),
(329, 334, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)'),
(330, 335, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)'),
(331, 336, 'vi', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)'),
(332, 337, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Collagen', 'Mặt nạ nước ấm từ thiên nhiên / Collagen'),
(333, 338, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai'),
(334, 339, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội'),
(335, 340, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Gạo', 'Mặt nạ nước ấm từ thiên nhiên / Gạo'),
(336, 341, 'vi', 'Bút kẻ mày / ghi đen ', 'Bút kẻ mày / ghi đen '),
(337, 342, 'vi', 'Bút kẻ mày / đen', 'Bút kẻ mày / đen'),
(338, 343, 'vi', 'Bút kẻ mày / Nâu ghi', 'Bút kẻ mày / Nâu ghi'),
(339, 344, 'vi', 'Bút kẻ mày / Nâu', 'Bút kẻ mày / Nâu'),
(340, 345, 'vi', 'Bút kẻ mày / Nâu đen', 'Bút kẻ mày / Nâu đen'),
(341, 346, 'vi', 'Kem chống nắng 50', 'Kem chống nắng 50'),
(342, 347, 'vi', 'Kem nền giữ ẩm đa chức năng', 'Kem nền giữ ẩm đa chức năng'),
(343, 348, 'vi', 'Kem nền ngọc trai 3 chức năng', 'Kem nền ngọc trai 3 chức năng'),
(344, 349, 'vi', 'Kem nền CC 3 chức năng', 'Kem nền CC 3 chức năng'),
(345, 350, 'vi', 'Kem nền thảo dược Xơ - yun', 'Kem nền thảo dược Xơ - yun'),
(346, 351, 'vi', 'Phấn nén chống nắng số 21', 'Phấn nén chống nắng số 21'),
(347, 352, 'vi', 'Phấn nén chống nắng số 23', 'Phấn nén chống nắng số 23'),
(348, 353, 'vi', 'Tinh chất dưỡng tóc Day-to-day / Oliu', 'Tinh chất dưỡng tóc Day-to-day / Oliu'),
(349, 354, 'vi', 'Kem dưỡng da Silk Snail Ốc Sên', 'Kem dưỡng da Silk Snail Ốc Sên'),
(350, 355, 'vi', 'Bút kẻ mắt chống nước', 'Bút kẻ mắt chống nước'),
(351, 356, 'vi', 'Bút kẻ mắt nước', 'Bút kẻ mắt nước'),
(352, 357, 'vi', 'Kem dưỡng ẩm SUANSU Water Bank Capsule', 'Kem dưỡng ẩm SUANSU Water Bank Capsule'),
(353, 358, 'vi', 'Kem dưỡng trắng SUANSU Crystal Whitening', 'Kem dưỡng trắng SUANSU Crystal Whitening'),
(354, 359, 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội'),
(355, 360, 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu'),
(356, 361, 'vi', 'Gel dưỡng da lô hội', 'Gel dưỡng da lô hội'),
(357, 362, 'vi', 'Gel dưỡng da lô hội SUANSU ( jar )', 'Gel dưỡng da lô hội SUANSU ( jar )'),
(358, 363, 'vi', 'Kem tẩy trang Day-to-day / Trà xanh', 'Kem tẩy trang Day-to-day / Trà xanh'),
(359, 364, 'vi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi'),
(360, 365, 'vi', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh'),
(361, 366, 'vi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi'),
(362, 367, 'vi', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong'),
(363, 368, 'vi', 'Sữa rửa mặt Day-to-day / Oliu', 'Sữa rửa mặt Day-to-day / Oliu'),
(364, 369, 'vi', 'Sữa rửa mặt Day-to-Day / Lựu', 'Sữa rửa mặt Day-to-Day / Lựu'),
(365, 370, 'vi', 'Chuốt mi / Siêu dài', 'Chuốt mi / Siêu dài'),
(366, 371, 'vi', 'Chuốt mi / Đậm dày', 'Chuốt mi / Đậm dày'),
(367, 372, 'vi', 'Chuốt mi / Cong và dài', 'Chuốt mi / Cong và dài'),
(368, 373, 'vi', 'Chuốt mi chống nước / Cong và dài', 'Chuốt mi chống nước / Cong và dài'),
(369, 374, 'vi', 'Nước hoa hồng Remine Collagen', 'Nước hoa hồng Remine Collagen'),
(370, 375, 'vi', 'Sữa dưỡng da Remine Collagen', 'Sữa dưỡng da Remine Collagen'),
(371, 376, 'vi', 'Kem dưỡng da Remine Collagen', 'Kem dưỡng da Remine Collagen'),
(372, 377, 'vi', 'Dung dịch Remine collagen', 'Dung dịch Remine collagen'),
(373, 378, 'vi', 'Nước hoa hồng thảo dược Xơ-yun', 'Nước hoa hồng thảo dược Xơ-yun'),
(374, 379, 'vi', 'Sữa dưỡng thảo dược Xơ-yun', 'Sữa dưỡng thảo dược Xơ-yun'),
(375, 380, 'vi', 'Kem dưỡng thảo dược Xơ-yun', 'Kem dưỡng thảo dược Xơ-yun'),
(376, 381, 'vi', 'Nước hoa hồng Remine Crystal', 'Nước hoa hồng Remine Crystal'),
(377, 382, 'vi', 'Sữa dưỡng ẩm Remine Crystal', 'Sữa dưỡng ẩm Remine Crystal'),
(378, 383, 'vi', 'Kem dưỡng trắng Remine Crystal ', 'Kem dưỡng trắng Remine Crystal '),
(379, 384, 'vi', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton'),
(380, 385, 'vi', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng'),
(381, 386, 'vi', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ'),
(382, 387, 'vi', 'Nước hoa Enesti Lovely Day / Ngày tình yêu', 'Nước hoa Enesti Lovely Day / Ngày tình yêu'),
(383, 388, 'vi', 'Nước hoa Enesti May Me / Là chính tôi', 'Nước hoa Enesti May Me / Là chính tôi'),
(384, 389, 'vi', 'Son bóng dưỡng môi / L01 Hồng đỏ', 'Son bóng dưỡng môi / L01 Hồng đỏ'),
(385, 390, 'vi', 'Son bóng dưỡng môi / L02 Vàng Cam', 'Son bóng dưỡng môi / L02 Vàng Cam'),
(386, 391, 'vi', 'Son bóng dưỡng môi / L03 Hồng', 'Son bóng dưỡng môi / L03 Hồng'),
(387, 392, 'vi', 'Son bóng dưỡng môi / L04 Hồng tím', 'Son bóng dưỡng môi / L04 Hồng tím'),
(388, 393, 'vi', 'Son enesti số 31', 'Son enesti số 31'),
(389, 394, 'vi', 'Son enesti số 32', 'Son enesti số 32'),
(390, 395, 'vi', 'Son enesti số 33', 'Son enesti số 33'),
(391, 396, 'vi', 'Son enesti số 34', 'Son enesti số 34'),
(392, 397, 'vi', 'Son enesti số 36', 'Son enesti số 36'),
(393, 398, 'vi', 'Son enesti số 37', 'Son enesti số 37'),
(394, 399, 'vi', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)'),
(395, 400, 'vi', 'Thuốc nhuộm tóc Aroma / Đen (1/0)', 'Thuốc nhuộm tóc Aroma / Đen (1/0)'),
(396, 401, 'vi', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)'),
(397, 402, 'vi', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)'),
(398, 403, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)'),
(399, 404, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)'),
(400, 405, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)'),
(401, 406, 'vi', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)'),
(402, 407, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Collagen', 'Mặt nạ nước ấm từ thiên nhiên / Collagen'),
(403, 408, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai'),
(404, 409, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội'),
(405, 410, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Gạo', 'Mặt nạ nước ấm từ thiên nhiên / Gạo'),
(406, 411, 'vi', 'Bút kẻ mày / ghi đen ', 'Bút kẻ mày / ghi đen '),
(407, 412, 'vi', 'Bút kẻ mày / đen', 'Bút kẻ mày / đen'),
(408, 413, 'vi', 'Bút kẻ mày / Nâu ghi', 'Bút kẻ mày / Nâu ghi'),
(409, 414, 'vi', 'Bút kẻ mày / Nâu', 'Bút kẻ mày / Nâu'),
(410, 415, 'vi', 'Bút kẻ mày / Nâu đen', 'Bút kẻ mày / Nâu đen'),
(411, 416, 'vi', 'Kem chống nắng 50', 'Kem chống nắng 50'),
(412, 417, 'vi', 'Kem nền giữ ẩm đa chức năng', 'Kem nền giữ ẩm đa chức năng'),
(413, 418, 'vi', 'Kem nền ngọc trai 3 chức năng', 'Kem nền ngọc trai 3 chức năng'),
(414, 419, 'vi', 'Kem nền CC 3 chức năng', 'Kem nền CC 3 chức năng'),
(415, 420, 'vi', 'Kem nền thảo dược Xơ - yun', 'Kem nền thảo dược Xơ - yun'),
(416, 421, 'vi', 'Phấn nén chống nắng số 21', 'Phấn nén chống nắng số 21'),
(417, 422, 'vi', 'Phấn nén chống nắng số 23', 'Phấn nén chống nắng số 23'),
(418, 423, 'vi', 'Tinh chất dưỡng tóc Day-to-day / Oliu', 'Tinh chất dưỡng tóc Day-to-day / Oliu'),
(419, 424, 'vi', 'Kem dưỡng da Silk Snail Ốc Sên', 'Kem dưỡng da Silk Snail Ốc Sên'),
(420, 425, 'vi', 'Bút kẻ mắt chống nước', 'Bút kẻ mắt chống nước'),
(421, 426, 'vi', 'Bút kẻ mắt nước', 'Bút kẻ mắt nước'),
(422, 427, 'vi', 'Kem dưỡng ẩm SUANSU Water Bank Capsule', 'Kem dưỡng ẩm SUANSU Water Bank Capsule'),
(423, 428, 'vi', 'Kem dưỡng trắng SUANSU Crystal Whitening', 'Kem dưỡng trắng SUANSU Crystal Whitening'),
(424, 429, 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội'),
(425, 430, 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu'),
(426, 431, 'vi', 'Gel dưỡng da lô hội', 'Gel dưỡng da lô hội'),
(427, 432, 'vi', 'Gel dưỡng da lô hội SUANSU ( jar )', 'Gel dưỡng da lô hội SUANSU ( jar )'),
(428, 433, 'vi', 'Kem tẩy trang Day-to-day / Trà xanh', 'Kem tẩy trang Day-to-day / Trà xanh'),
(429, 434, 'vi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi'),
(430, 435, 'vi', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh'),
(431, 436, 'vi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi'),
(432, 437, 'vi', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong'),
(433, 438, 'vi', 'Sữa rửa mặt Day-to-day / Oliu', 'Sữa rửa mặt Day-to-day / Oliu'),
(434, 439, 'vi', 'Sữa rửa mặt Day-to-Day / Lựu', 'Sữa rửa mặt Day-to-Day / Lựu'),
(435, 440, 'vi', 'Chuốt mi / Siêu dài', 'Chuốt mi / Siêu dài'),
(436, 441, 'vi', 'Chuốt mi / Đậm dày', 'Chuốt mi / Đậm dày'),
(437, 442, 'vi', 'Chuốt mi / Cong và dài', 'Chuốt mi / Cong và dài'),
(438, 443, 'vi', 'Chuốt mi chống nước / Cong và dài', 'Chuốt mi chống nước / Cong và dài'),
(439, 444, 'vi', 'Nước hoa hồng Remine Collagen', 'Nước hoa hồng Remine Collagen'),
(440, 445, 'vi', 'Sữa dưỡng da Remine Collagen', 'Sữa dưỡng da Remine Collagen'),
(441, 446, 'vi', 'Kem dưỡng da Remine Collagen', 'Kem dưỡng da Remine Collagen'),
(442, 447, 'vi', 'Dung dịch Remine collagen', 'Dung dịch Remine collagen'),
(443, 448, 'vi', 'Nước hoa hồng thảo dược Xơ-yun', 'Nước hoa hồng thảo dược Xơ-yun'),
(444, 449, 'vi', 'Sữa dưỡng thảo dược Xơ-yun', 'Sữa dưỡng thảo dược Xơ-yun'),
(445, 450, 'vi', 'Kem dưỡng thảo dược Xơ-yun', 'Kem dưỡng thảo dược Xơ-yun'),
(446, 451, 'vi', 'Nước hoa hồng Remine Crystal', 'Nước hoa hồng Remine Crystal'),
(447, 452, 'vi', 'Sữa dưỡng ẩm Remine Crystal', 'Sữa dưỡng ẩm Remine Crystal'),
(448, 453, 'vi', 'Kem dưỡng trắng Remine Crystal ', 'Kem dưỡng trắng Remine Crystal '),
(449, 454, 'vi', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton'),
(450, 455, 'vi', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng'),
(451, 456, 'vi', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ'),
(452, 457, 'vi', 'Nước hoa Enesti Lovely Day / Ngày tình yêu', 'Nước hoa Enesti Lovely Day / Ngày tình yêu'),
(453, 458, 'vi', 'Nước hoa Enesti May Me / Là chính tôi', 'Nước hoa Enesti May Me / Là chính tôi'),
(454, 459, 'vi', 'Son bóng dưỡng môi / L01 Hồng đỏ', 'Son bóng dưỡng môi / L01 Hồng đỏ'),
(455, 460, 'vi', 'Son bóng dưỡng môi / L02 Vàng Cam', 'Son bóng dưỡng môi / L02 Vàng Cam'),
(456, 461, 'vi', 'Son bóng dưỡng môi / L03 Hồng', 'Son bóng dưỡng môi / L03 Hồng'),
(457, 462, 'vi', 'Son bóng dưỡng môi / L04 Hồng tím', 'Son bóng dưỡng môi / L04 Hồng tím'),
(458, 463, 'vi', 'Son enesti số 31', 'Son enesti số 31'),
(459, 464, 'vi', 'Son enesti số 32', 'Son enesti số 32'),
(460, 465, 'vi', 'Son enesti số 33', 'Son enesti số 33'),
(461, 466, 'vi', 'Son enesti số 34', 'Son enesti số 34'),
(462, 467, 'vi', 'Son enesti số 36', 'Son enesti số 36'),
(463, 468, 'vi', 'Son enesti số 37', 'Son enesti số 37'),
(464, 469, 'vi', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)'),
(465, 470, 'vi', 'Thuốc nhuộm tóc Aroma / Đen (1/0)', 'Thuốc nhuộm tóc Aroma / Đen (1/0)'),
(466, 471, 'vi', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)'),
(467, 472, 'vi', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)'),
(468, 473, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)'),
(469, 474, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)'),
(470, 475, 'vi', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)'),
(471, 476, 'vi', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)'),
(472, 477, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Collagen', 'Mặt nạ nước ấm từ thiên nhiên / Collagen'),
(473, 478, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai'),
(474, 479, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội'),
(475, 480, 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Gạo', 'Mặt nạ nước ấm từ thiên nhiên / Gạo'),
(476, 481, 'vi', 'Bút kẻ mày / ghi đen ', 'Bút kẻ mày / ghi đen '),
(477, 482, 'vi', 'Bút kẻ mày / đen', 'Bút kẻ mày / đen'),
(478, 483, 'vi', 'Bút kẻ mày / Nâu ghi', 'Bút kẻ mày / Nâu ghi'),
(479, 484, 'vi', 'Bút kẻ mày / Nâu', 'Bút kẻ mày / Nâu'),
(480, 485, 'vi', 'Bút kẻ mày / Nâu đen', 'Bút kẻ mày / Nâu đen'),
(481, 486, 'vi', 'Kem chống nắng 50', 'Kem chống nắng 50'),
(482, 487, 'vi', 'Kem nền giữ ẩm đa chức năng', 'Kem nền giữ ẩm đa chức năng'),
(483, 488, 'vi', 'Kem nền ngọc trai 3 chức năng', 'Kem nền ngọc trai 3 chức năng'),
(484, 489, 'vi', 'Kem nền CC 3 chức năng', 'Kem nền CC 3 chức năng'),
(485, 490, 'vi', 'Kem nền thảo dược Xơ - yun', 'Kem nền thảo dược Xơ - yun'),
(486, 491, 'vi', 'Phấn nén chống nắng số 21', 'Phấn nén chống nắng số 21'),
(487, 492, 'vi', 'Phấn nén chống nắng số 23', 'Phấn nén chống nắng số 23'),
(488, 493, 'vi', 'Tinh chất dưỡng tóc Day-to-day / Oliu', 'Tinh chất dưỡng tóc Day-to-day / Oliu'),
(489, 494, 'vi', 'Kem dưỡng da Silk Snail Ốc Sên', 'Kem dưỡng da Silk Snail Ốc Sên'),
(490, 495, 'vi', 'Bút kẻ mắt chống nước', 'Bút kẻ mắt chống nước'),
(491, 496, 'vi', 'Bút kẻ mắt nước', 'Bút kẻ mắt nước'),
(492, 497, 'vi', 'Kem dưỡng ẩm SUANSU Water Bank Capsule', 'Kem dưỡng ẩm SUANSU Water Bank Capsule'),
(493, 498, 'vi', 'Kem dưỡng trắng SUANSU Crystal Whitening', 'Kem dưỡng trắng SUANSU Crystal Whitening'),
(494, 499, 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội'),
(495, 500, 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu'),
(496, 501, 'vi', 'Gel dưỡng da lô hội', '<p>Gel dưỡng da l&ocirc; hội</p>'),
(497, 502, 'vi', 'Gel dưỡng da lô hội SUANSU ( jar )', '<p>Gel dưỡng da l&ocirc; hội SUANSU ( jar )</p>');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`) VALUES
(44, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `category_id`, `image`, `created_date`, `feature`, `status`) VALUES
(8, 29, '8_image.jpg', '2016-04-23 10:06:33', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `promotion_lang`
--

CREATE TABLE `promotion_lang` (
  `id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promotion_lang`
--

INSERT INTO `promotion_lang` (`id`, `promotion_id`, `language`, `title`, `description`, `content`) VALUES
(5, 8, 'vi', '123', '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `recruitment`
--

CREATE TABLE `recruitment` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recruitment`
--

INSERT INTO `recruitment` (`id`, `user_id`, `image`, `created_date`, `feature`, `status`) VALUES
(11, 0, '11_image.jpg', '2016-05-17 03:46:05', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `recruitment_lang`
--

CREATE TABLE `recruitment_lang` (
  `id` int(11) NOT NULL,
  `recruitment_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recruitment_lang`
--

INSERT INTO `recruitment_lang` (`id`, `recruitment_id`, `language`, `title`, `content`) VALUES
(14, 11, 'vi', '123', '<p>123</p>');

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reply`
--

INSERT INTO `reply` (`id`, `contact_id`, `from`, `to`, `fullname`, `subject`, `content`, `created_date`) VALUES
(1, 1, 'phamxuanloc.01@gmail.com', '123', '123', '123', '<p>123</p>', '2016-05-10 09:15:45'),
(2, 1, 'phamxuanloc.01@gmail.com', '123', '123', 'cxaccasc', '<p>dsadasdasd</p>', '2016-05-10 09:47:38');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `is_backend_login` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `permissions`, `is_backend_login`) VALUES
(1, 'Administrator', '{"app\\\\modules\\\\admin\\\\controllers\\\\CategoryController":{"index":"1"},"navatech\\\\role\\\\controllers\\\\DefaultController":{"index":"1","create":"1","update":"1","delete":"1","view":"1"}}', 1),
(2, 'distributor', '{"app\\\\modules\\\\admin\\\\controllers\\\\AccountController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\BannerController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\CategoryController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ClientController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ContactController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\DepartmentController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\DistributorController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\OrderController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PageController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PopupController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PostController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ProductController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PromotionController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\RecruitmentController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ReplyController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\SliderController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"navatech\\\\role\\\\controllers\\\\DefaultController":{"index":"1","create":"1","update":"1","delete":"1","view":"1"}}', 1),
(3, 'agent', '{"app\\\\modules\\\\admin\\\\controllers\\\\AccountController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\BannerController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\CategoryController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ClientController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ContactController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\DepartmentController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\DistributorController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\OrderController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PageController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PopupController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PostController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ProductController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PromotionController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\RecruitmentController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ReplyController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\SliderController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"navatech\\\\role\\\\controllers\\\\DefaultController":{"index":"1","create":"1","update":"1","delete":"1","view":"1"}}', 1),
(4, 'quản lý 2', '{"app\\\\modules\\\\admin\\\\controllers\\\\AccountController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\BannerController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\CategoryController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ClientController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ContactController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\DepartmentController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\DistributorController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\OrderController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PageController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PopupController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PostController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ProductController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\PromotionController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\RecruitmentController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\ReplyController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"app\\\\modules\\\\admin\\\\controllers\\\\SliderController":{"index":"1","view":"1","create":"1","update":"1","delete":"1"},"navatech\\\\role\\\\controllers\\\\DefaultController":{"index":"1","create":"1","update":"1","delete":"1","view":"1"}}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8_unicode_ci,
  `type` smallint(2) NOT NULL DEFAULT '1',
  `store_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `sort_order` int(11) DEFAULT '50'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `parent_id`, `code`, `name`, `desc`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES
(19, 0, 'general', 'General tab', '', 0, '', '', '', 1),
(21, 19, 'text_field', 'Text Field', 'Example for text field', 1, '', '', 'test text', 1),
(22, 39, 'email_field', 'Email field', 'Example for email field', 2, '', '', 'phamxuanloc.01@gmail.com', 2),
(23, 19, 'number_field', 'Number field', 'Example for number field', 3, '', '', '123', 3),
(24, 19, 'textarea_field', 'Textarea Field', 'Example for Textarea field', 4, '', '', 'A long sentence', 4),
(25, 19, 'color_field', 'Color field', 'Example for color field', 5, '', '', '#ff0000', 5),
(26, 19, 'date_field', 'Date field', 'Example for Date field', 6, '', '', '2016-05-10', 6),
(27, 19, 'time_field', 'Time field', 'Example for time field', 7, '', '', '23:07:45', 7),
(28, 19, 'password_field', 'Password field', 'Example for Password field', 9, '', '', '12345678', 8),
(29, 19, 'roxymce_field', 'RoxyMCE field', 'Example for RoxyMCE field', 10, '', '', '<p>This is a paragraph</p>', 9),
(30, 0, 'additional', 'Additional Tab', '', 0, '', '', '', 2),
(31, 30, 'select_field', 'Select field', 'Example for Single Selection', 11, '1,2,3,abc,4,def,5,6', '', '3', 1),
(32, 30, 'multiselect_field', 'Multiple select field', 'Example for Multiple Selection', 12, 'ab,1,cd,2,3,4,de,ef,gh,7,8', '', '1,4,5', 2),
(33, 30, 'file_field', 'File field', 'Example for File field', 13, '', '@app/web/uploads', 'dhdg.png', 3),
(34, 30, 'url_field', 'Url field', 'Example for Url field', 14, '', '@app/web/uploads', 'footer-top-logo.png', 4),
(35, 30, 'percent_field', 'Percent field', 'Example for Percent field', 15, '', '', '68', 5),
(36, 30, 'switch_field', 'Switch Option', 'Example for Switch option', 16, 'no,yes', '', '', 6),
(37, 30, 'checkbox_option', 'Checkbox Option', 'Example for checkbox option', 17, '1,2,3,abc,4,def,5,6', '', '2,4,6', 7),
(38, 30, 'radio_option', 'Radio Option', 'Example for radio option', 18, '1,2,3,abc,4,def,5,6', '', 'def', 8),
(39, 0, 'Email', 'Email', '', 0, '', '', '', 2),
(40, 39, 'password_mail', 'Password', '', 9, '', '', 'locpro123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image`, `url`, `order`, `status`) VALUES
(5, '5_image.jpg', '123123', 1, 0),
(6, '6_image.jpg', '1231', 123, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slider_lang`
--

CREATE TABLE `slider_lang` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider_lang`
--

INSERT INTO `slider_lang` (`id`, `slider_id`, `language`, `title`) VALUES
(4, 5, 'vi', 'sdfas'),
(5, 6, 'vi', '1123123');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `logo` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `area` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `role_id`, `logo`, `website`, `status`, `area`, `city`, `type`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$YiHv/jRh.F3B41YlRDTZCOtT970o6nNybcy2wwYS3ncRhpjmDW3gi', 'X2eREcnF64l7EPaOCzEqC0y9Yt2EtFCH', 1456216036, NULL, NULL, '127.0.0.1', 1456216036, 1456216036, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'thucha', 'thuchm92@gmail.com', '$2y$10$yAbtdgnydxRuLkBWqhvh1ej.cykg6Ieoel00nI1vtYQq0Z.jYxhry', '0Qfg-uRVkYhjs7h6IufgZbXWDpa6ZUYk', 1456216722, NULL, NULL, '127.0.0.1', 1456216722, 1456216722, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '2', 'ádf', '$2y$10$81HNRxke47h4JgFZvjsiEeWr0T/JAoMvBT9D8gesDjaCxulpp2Shy', 'Hr76BYkKZArsn1mVKrnnBgps2Aptd3CQ', 1463130896, NULL, NULL, '127.0.0.1', 0, 0, 0, 2, '42_image.jpg', '2', 1, 0, '2', 2),
(48, '123', '123', '$2y$10$3ZEYWRaqhx82zK3.R4P.Hu7ym/jv2umc6i7o1ej3R5FMbjRahY5P2', 'uBAnd1NWwhVhHzCfg8BRyXlTMx7bVCZf', 1463133828, NULL, NULL, '127.0.0.1', 0, 0, 0, 2, '48_image.jpg', '123', 1, 2, '123', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_lang`
--
ALTER TABLE `banner_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bannerlang_ibfk_1` (`banner_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_lang`
--
ALTER TABLE `category_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_lang_ibfk_1` (`category_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_lang`
--
ALTER TABLE `client_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientlang_ibfk_1` (`client_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_lang`
--
ALTER TABLE `department_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `departmentlang_ibfk_1` (`department_id`);

--
-- Indexes for table `distributor_lang`
--
ALTER TABLE `distributor_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `distributor_lang_fk_user` (`distributor_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_lang`
--
ALTER TABLE `page_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pagelang_ibfk_1` (`page_id`);

--
-- Indexes for table `phrase`
--
ALTER TABLE `phrase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phrase_translate`
--
ALTER TABLE `phrase_translate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popup`
--
ALTER TABLE `popup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popup_lang`
--
ALTER TABLE `popup_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_lang`
--
ALTER TABLE `post_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postlang_ibfk_1` (`post_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_lang`
--
ALTER TABLE `product_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productlang_ibfk_1` (`product_id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion_lang`
--
ALTER TABLE `promotion_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postlang_ibfk_1` (`promotion_id`);

--
-- Indexes for table `recruitment`
--
ALTER TABLE `recruitment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recruitment_lang`
--
ALTER TABLE `recruitment_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recruitmentlang_ibfk_1` (`recruitment_id`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `code` (`code`),
  ADD KEY `sort_order` (`sort_order`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_lang`
--
ALTER TABLE `slider_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_email` (`email`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `banner_lang`
--
ALTER TABLE `banner_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `category_lang`
--
ALTER TABLE `category_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `client_lang`
--
ALTER TABLE `client_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `department_lang`
--
ALTER TABLE `department_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `distributor_lang`
--
ALTER TABLE `distributor_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `page_lang`
--
ALTER TABLE `page_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `phrase`
--
ALTER TABLE `phrase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=371;
--
-- AUTO_INCREMENT for table `phrase_translate`
--
ALTER TABLE `phrase_translate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=516;
--
-- AUTO_INCREMENT for table `popup`
--
ALTER TABLE `popup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `popup_lang`
--
ALTER TABLE `popup_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `post_lang`
--
ALTER TABLE `post_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=503;
--
-- AUTO_INCREMENT for table `product_lang`
--
ALTER TABLE `product_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=498;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `promotion_lang`
--
ALTER TABLE `promotion_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `recruitment`
--
ALTER TABLE `recruitment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `recruitment_lang`
--
ALTER TABLE `recruitment_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slider_lang`
--
ALTER TABLE `slider_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `banner_lang`
--
ALTER TABLE `banner_lang`
  ADD CONSTRAINT `bannerlang_ibfk_1` FOREIGN KEY (`banner_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_lang`
--
ALTER TABLE `category_lang`
  ADD CONSTRAINT `category_lang_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `department_lang`
--
ALTER TABLE `department_lang`
  ADD CONSTRAINT `departmentlang_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `distributor_lang`
--
ALTER TABLE `distributor_lang`
  ADD CONSTRAINT `distributor_lang_fk_user` FOREIGN KEY (`distributor_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_lang`
--
ALTER TABLE `page_lang`
  ADD CONSTRAINT `pagelang_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `post_lang`
--
ALTER TABLE `post_lang`
  ADD CONSTRAINT `postlang_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_lang`
--
ALTER TABLE `product_lang`
  ADD CONSTRAINT `productlang_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recruitment_lang`
--
ALTER TABLE `recruitment_lang`
  ADD CONSTRAINT `recruitmentlang_ibfk_1` FOREIGN KEY (`recruitment_id`) REFERENCES `recruitment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
