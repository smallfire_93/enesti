-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2016 at 04:52 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nava_enesti`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `user_id`, `image`, `position`, `url`, `view`, `order`, `status`, `created_date`) VALUES
(1, 1, '_image.png', '0', 'aaaaaa', 2, 3, 1, '2016-03-21 09:29:44'),
(2, 1, '_image.png', '0', 'aaaaaa', 2, 1, 1, '2016-03-21 09:52:28'),
(3, 1, '_image.png', '0', 'aaaaaa', 2, 3, 1, '2016-03-21 10:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `banner_lang`
--

CREATE TABLE IF NOT EXISTS `banner_lang` (
`id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_lang`
--

INSERT INTO `banner_lang` (`id`, `banner_id`, `language`, `title`) VALUES
(1, 3, 'vi', 'test'),
(2, 3, 'en', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `type`, `order`, `status`, `image`) VALUES
(29, 0, 1, 1, 1, 'sm9nx-iFIAtM09tilStNlLx6VeB5CyXY.png'),
(30, 0, 2, 1, 1, '_image.png');

-- --------------------------------------------------------

--
-- Table structure for table `category_lang`
--

CREATE TABLE IF NOT EXISTS `category_lang` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_lang`
--

INSERT INTO `category_lang` (`id`, `category_id`, `language`, `name`, `description`) VALUES
(53, 29, 'vi', 'cat1', 'des1'),
(54, 29, 'en', 'cat1_en', 'des1_en'),
(55, 30, 'vi', 'Bóng Đá', 'Tin tức bóng đá'),
(56, 30, 'en', 'Football', 'Football news');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`id` int(11) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client_lang`
--

CREATE TABLE IF NOT EXISTS `client_lang` (
`id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
`contact_id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department_lang`
--

CREATE TABLE IF NOT EXISTS `department_lang` (
`id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `country`, `status`) VALUES
(1, 'Việt Nam', 'vi', 'vn', 1),
(2, 'United States', 'en', 'us', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m160204_045439_navatech_multi_language_init', 1457692979),
('m160226_063609_create_role', 1457693356);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
`order_id` int(11) NOT NULL,
  `full_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` int(11) NOT NULL,
  `phone_number` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE IF NOT EXISTS `order_item` (
`order_item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
`id` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_lang`
--

CREATE TABLE IF NOT EXISTS `page_lang` (
`id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `language` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phrase`
--

CREATE TABLE IF NOT EXISTS `phrase` (
`id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phrase`
--

INSERT INTO `phrase` (`id`, `name`) VALUES
(1, 'language'),
(2, 'list_of_available_x'),
(3, 'add_a_new'),
(4, 'action'),
(5, 'delete_warning'),
(6, 'update'),
(7, 'delete'),
(8, 'language_code'),
(9, 'country_code'),
(10, 'status'),
(11, 'phrase'),
(12, 'double_click_to_edit'),
(13, 'success_add_update_x_successfully'),
(14, 'warning_x_existed'),
(15, 'error_mark_required'),
(16, 'ex_x_welcome_to_my_site'),
(17, 'support'),
(18, 'support_max_5_params_in_a_sentence'),
(19, 'params_is_started_from_1_and_ended_with_5'),
(20, 'create'),
(21, 'close'),
(22, 'category'),
(23, 'update_x'),
(24, 'back'),
(25, 'details'),
(26, 'view_x'),
(27, 'list_x'),
(28, 'Actions'),
(29, 'page'),
(30, 'create_x'),
(33, 'manage_x'),
(36, 'dashboard'),
(37, 'product'),
(38, 'x_category'),
(39, 'news'),
(40, 'project'),
(42, 'media'),
(43, 'picture'),
(44, 'audio'),
(45, 'video'),
(46, 'banner'),
(47, 'popup'),
(48, 'client'),
(49, 'recruitment'),
(50, 'user'),
(51, 'x_management'),
(52, 'user_role'),
(53, 'contact'),
(54, 'setting'),
(57, 'hello'),
(59, 'logout'),
(60, 'information'),
(61, 'account'),
(62, 'password'),
(64, 'login'),
(65, 'general_setting'),
(68, 'no'),
(69, 'yes'),
(72, 'slug'),
(73, 'type'),
(77, 'configuration'),
(78, 'save'),
(79, 'edit_x'),
(80, 'aboutus'),
(85, 'title'),
(86, 'image'),
(87, 'position'),
(89, 'user_id'),
(90, 'created_date'),
(92, 'translate'),
(99, 'home'),
(100, 'welcome_text'),
(101, 'remember_password'),
(102, 'change_password'),
(108, 'unread'),
(109, 'read'),
(111, 'full_name'),
(112, 'phone'),
(113, 'content'),
(115, 'sort'),
(116, 'feature'),
(117, 'completed'),
(118, 'uncompleted'),
(119, 'existed'),
(120, 'icon'),
(121, 'parent_category'),
(122, 'home_page'),
(123, 'about'),
(124, 'name'),
(125, 'view'),
(126, 'post'),
(127, 'list'),
(130, 'success'),
(132, 'permission'),
(133, 'is_backend_login'),
(134, 'background'),
(141, 'email'),
(142, 'username'),
(143, 'first_name'),
(144, 'last_name'),
(145, 'gender'),
(146, 'birthday'),
(147, 'update_date'),
(148, 'old_password'),
(149, 'new_password'),
(150, 'confirm_password'),
(153, 'executing'),
(154, 'executed'),
(155, 'price'),
(156, 'left'),
(157, 'right'),
(158, 'url'),
(159, 'description'),
(160, 'hot_news'),
(161, 'minutes_ago'),
(163, 'hours_ago'),
(164, 'contact_for_price'),
(165, 'view_all'),
(166, 'fullname'),
(167, 'send'),
(168, 'user_role_id'),
(169, 'online_support'),
(170, 'our_client'),
(171, 'our_clients'),
(172, 'search'),
(173, 'parent_id'),
(174, 'previous'),
(175, 'next'),
(176, 'order_by'),
(177, 'product_name'),
(178, 'asc'),
(179, 'desc'),
(180, 'display'),
(181, 'product_per_page'),
(184, 'company_information'),
(185, 'become_our_client'),
(186, 'content_our_client'),
(187, 'become_our_partners'),
(188, 'about_general_director'),
(189, 'about_company'),
(190, 'development_process'),
(191, 'award'),
(192, 'typical_project'),
(193, 'order'),
(194, 'total_amount'),
(195, 'shipping_address'),
(196, 'message'),
(197, 'invoice'),
(198, 'invoice_information'),
(199, 'order_no'),
(200, 'customer_information'),
(201, 'quantity'),
(203, 'export_to_excel'),
(204, 'add_image'),
(205, 'caption'),
(206, 'is_main_image'),
(209, 'list_categories'),
(210, 'headquarter_address'),
(211, 'factory_address'),
(212, 'office_address'),
(213, 'hotline'),
(214, 'connect_with_us'),
(215, 'sales_consultant'),
(217, 'technology_production'),
(218, 'slider'),
(219, 'search_product'),
(220, 'x_products'),
(221, 'product_newest'),
(222, 'price_asc'),
(223, 'price_desc'),
(224, 'feature_product'),
(225, 'add_to_cart'),
(226, 'product_information'),
(227, 'customer_comment'),
(228, 'related_product'),
(229, 'caption_in_x'),
(231, 'communication_news'),
(232, 'read_more'),
(233, 'your_cart'),
(234, 'product_price'),
(235, 'total'),
(236, 'cancel'),
(237, 'continue_shopping'),
(238, 'place_order'),
(239, 'cart_empty'),
(240, 'go_to_cart'),
(241, 'technical'),
(242, 'order_information'),
(243, 'personal_information'),
(244, 'thankyou_for_your_order'),
(245, 'thankyou'),
(246, 'company_name'),
(247, 'address'),
(249, 'showroom'),
(250, 'related_post'),
(251, 'prev_news'),
(252, 'next_news'),
(253, 'in_stock'),
(254, 'product_not_in_stock'),
(255, 'not_in_stock'),
(256, 'perform_indepth_development_goals'),
(257, 'technology'),
(258, 'equipment'),
(259, 'send_success'),
(260, 'send_false'),
(261, 'from'),
(262, 'to'),
(263, 'subject'),
(264, 'reply'),
(265, 'create_date'),
(266, 'project_intro'),
(267, 'cancelled'),
(268, 'new'),
(269, 'received'),
(270, 'cancel_order'),
(271, 'receive_order'),
(272, 'are_you_sure'),
(273, 'office'),
(274, 'factory'),
(275, 'map'),
(276, 'homepage_lager_text'),
(277, 'code'),
(278, 'origin'),
(279, 'group_product'),
(280, 'videos_gallery'),
(281, 'select_from_computer'),
(282, 'select_from_url'),
(283, 'only_youtube_supported'),
(284, 'search_by_x'),
(285, 'customer'),
(286, 'general'),
(287, 'payment'),
(288, 'lists'),
(289, 'country'),
(290, 'languages'),
(291, 'add_a_new_x'),
(292, 'in_use'),
(293, 'not_in_use');

-- --------------------------------------------------------

--
-- Table structure for table `phrase_translate`
--

CREATE TABLE IF NOT EXISTS `phrase_translate` (
`id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=447 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phrase_translate`
--

INSERT INTO `phrase_translate` (`id`, `phrase_id`, `language_id`, `value`) VALUES
(1, 1, 1, 'Ngôn ngữ'),
(2, 2, 1, 'Danh sách {1}'),
(3, 3, 1, 'Thêm mới'),
(4, 4, 1, 'Hành động'),
(5, 5, 1, 'Nếu bạn xóa mục này, toàn bộ dữ liệu đi kèm cũng sẽ bị xóa. Bạn có chắc chắn muốn xóa không?'),
(6, 6, 1, 'Cập nhật'),
(7, 7, 1, 'Xóa'),
(8, 8, 1, 'Mã ngôn ngữ'),
(9, 9, 1, 'Mã quốc gia'),
(10, 10, 1, 'Trạng thái'),
(11, 11, 1, 'Từ ngữ'),
(12, 12, 1, 'Clip đúp chuột để sửa'),
(13, 13, 1, '<strong>Chúc mừng!</strong> {1} {2} hoàn tất!'),
(14, 14, 1, '<strong>Cảnh báo!</strong> Những {1} này đã có trong dữ liệu, vui lòng tìm và cập nhật chúng!'),
(15, 15, 1, '<strong>Có lỗi!</strong> Các trường có dấu (*) là bắt buộc!'),
(16, 16, 1, 'VD(VI): Chào mừng ghé thăm website'),
(17, 17, 1, 'Hỗ trợ'),
(18, 18, 1, 'Hỗ trợ tối đa 5 tham số'),
(19, 19, 1, 'Tham số bắt đầu từ 1 đến 5'),
(20, 20, 1, 'Thêm mới'),
(21, 21, 1, 'Đóng'),
(22, 21, 2, 'Close'),
(23, 20, 2, 'Create'),
(24, 19, 2, 'Params is started from 1 and ended with 5'),
(25, 18, 2, 'System supports max 5 params in a sentence'),
(26, 17, 2, 'Support'),
(27, 16, 2, 'Ex(EN): Welcome to my site'),
(28, 15, 2, '<strong>Error!</strong> Field (*) is required!'),
(29, 14, 2, '<strong>Warning!</strong> This {1} has been existed, please find and update it!'),
(30, 13, 2, '<strong>Success!</strong> {1} {2} successfully!'),
(31, 12, 2, 'Double click to edit'),
(32, 11, 2, 'Phrase'),
(33, 10, 2, 'Status'),
(34, 9, 2, 'Country Code'),
(35, 8, 2, 'Language Code'),
(36, 7, 2, 'Delete'),
(37, 6, 2, 'Update'),
(38, 5, 2, 'If you delete this item, all related data is deleted. Are you sure ?'),
(39, 4, 2, 'Action'),
(40, 3, 2, 'Add a new'),
(41, 2, 2, 'List of available {1}'),
(42, 1, 2, 'Language'),
(43, 22, 1, 'Danh mục'),
(44, 23, 1, 'Cập nhật {1}'),
(45, 24, 1, 'Quay lại'),
(46, 25, 1, 'Chi tiết'),
(47, 26, 1, 'Xem {1}'),
(48, 27, 1, 'Danh sách {1}'),
(49, 28, 1, 'Thao tác'),
(50, 22, 2, 'Categories'),
(51, 23, 2, 'Update {1}'),
(52, 25, 2, 'Details'),
(53, 26, 2, 'View {1}'),
(54, 24, 2, 'Back'),
(55, 27, 2, 'List of {1}'),
(56, 28, 2, 'Actions'),
(57, 29, 1, 'Trang'),
(58, 30, 1, 'Thêm mới {1}'),
(61, 33, 1, 'Quản lý {1}'),
(62, 29, 2, 'Page'),
(63, 30, 2, 'Create a new {1}'),
(64, 33, 2, '{1} Management'),
(67, 36, 1, 'Bảng điều khiển'),
(68, 37, 1, 'Sản phẩm'),
(69, 38, 1, 'Danh mục {1}'),
(70, 39, 1, 'Tin tức'),
(71, 40, 1, 'Dự án'),
(73, 42, 1, 'Thư viện'),
(74, 43, 1, 'Hình ảnh'),
(75, 44, 1, 'Bài hát về Quang Trung'),
(76, 45, 1, 'Video'),
(77, 46, 1, 'Banner'),
(78, 47, 1, 'Popup'),
(79, 48, 1, 'Đối tác'),
(80, 49, 1, 'Tuyển dụng'),
(81, 50, 1, 'Người dùng'),
(82, 51, 1, 'Quản lý {1}'),
(83, 52, 1, 'Phân quyền'),
(84, 53, 1, 'Liên hệ'),
(85, 54, 1, 'Cấu hình hệ thống'),
(88, 57, 1, 'Xin chào'),
(90, 59, 1, 'Đăng xuất'),
(91, 36, 2, 'Dashboard'),
(92, 37, 2, 'Product'),
(93, 38, 2, '{1} category'),
(94, 39, 2, 'News'),
(95, 40, 2, 'Project'),
(96, 42, 2, 'Library'),
(97, 43, 2, 'Images'),
(98, 44, 2, 'The Song of Quang Trung'),
(99, 45, 2, 'Video'),
(100, 46, 2, 'Banner'),
(101, 47, 2, 'Popup'),
(102, 48, 2, 'Partners'),
(103, 49, 2, 'Recruitment'),
(104, 50, 2, 'User'),
(105, 51, 2, '{1} Management'),
(106, 52, 2, 'User Role'),
(107, 53, 2, 'Contact'),
(108, 54, 2, 'Settings'),
(110, 59, 2, 'Logout'),
(111, 60, 1, 'Thông tin'),
(112, 61, 1, 'Tài khoản'),
(113, 62, 1, 'Mật khẩu'),
(115, 64, 1, 'Đăng nhập'),
(116, 57, 2, 'Hello'),
(117, 65, 1, 'Cấu hình chung'),
(119, 60, 2, 'Information'),
(120, 61, 2, 'Account'),
(121, 62, 2, 'Password'),
(123, 65, 2, 'General settings'),
(124, 64, 2, 'Login'),
(125, 68, 1, 'Không'),
(126, 69, 1, 'Có'),
(129, 72, 1, 'Đường dẫn tĩnh'),
(130, 73, 1, 'Loại'),
(134, 69, 2, 'Yes'),
(135, 68, 2, 'No'),
(136, 72, 2, 'Slug '),
(137, 73, 2, 'Type '),
(138, 77, 1, 'Cấu hình'),
(139, 78, 1, 'Lưu'),
(140, 77, 2, 'Configuration '),
(141, 78, 2, 'Save '),
(142, 79, 1, 'Thay đổi {1}'),
(143, 79, 2, 'Update {1} '),
(144, 80, 1, 'Về chúng tôi'),
(149, 85, 1, 'Tiêu đề'),
(150, 86, 1, 'Hình ảnh'),
(151, 87, 1, 'Vị trí'),
(153, 89, 1, 'ID'),
(154, 90, 1, 'Ngày tạo'),
(155, 90, 2, 'Created date '),
(156, 89, 2, 'ID '),
(157, 85, 2, 'Title '),
(158, 87, 2, 'Position '),
(159, 86, 2, 'Image'),
(160, 80, 2, 'About us'),
(162, 99, 2, 'Home'),
(163, 100, 2, 'Welcome to Website Management page'),
(164, 101, 2, 'Remember password?  '),
(165, 102, 2, 'Change password'),
(166, 99, 1, 'Home'),
(169, 92, 1, 'Dịch '),
(170, 92, 2, 'Translate '),
(171, 100, 1, 'Xin chào. Bạn đang truy cập vào trang Quản trị hệ thống '),
(172, 101, 1, 'Ghi nhớ mật khẩu? '),
(173, 102, 1, 'Đổi mật khẩu '),
(179, 108, 1, 'Chưa đọc'),
(180, 109, 1, 'Đã đọc'),
(182, 111, 1, 'Họ tên'),
(183, 112, 1, 'Số điện thoại'),
(184, 113, 1, 'Nội dung'),
(186, 115, 1, 'Thứ tự'),
(187, 116, 1, 'Nổi bật'),
(188, 117, 1, 'Hoàn thành'),
(189, 118, 1, 'Chưa hoàn thành'),
(190, 119, 1, 'Đã tồn tại'),
(191, 120, 1, 'Biểu tượng'),
(193, 108, 2, 'Unread '),
(194, 109, 2, 'Read '),
(195, 111, 2, 'Full name '),
(196, 112, 2, 'Phone '),
(197, 113, 2, 'Content '),
(198, 115, 2, 'Order '),
(199, 116, 2, 'Featured '),
(200, 117, 2, 'Completed '),
(201, 118, 2, 'Uncompleted '),
(202, 119, 2, 'Existed '),
(203, 120, 2, 'icon'),
(204, 121, 1, 'Danh mục cha'),
(205, 121, 2, 'Parent category  '),
(206, 122, 1, 'Trang chủ'),
(207, 123, 1, 'Giới thiệu'),
(208, 122, 2, 'Home '),
(209, 123, 2, 'About us '),
(210, 124, 1, 'Tên'),
(211, 125, 1, 'Lượt xem'),
(212, 126, 1, 'Bài viết'),
(213, 127, 1, 'Danh sách'),
(216, 130, 1, 'Thành công'),
(218, 132, 1, 'Quyền hạn'),
(219, 133, 1, 'Được phép đăng nhập Quản lý'),
(220, 134, 1, 'Hình nền'),
(225, 125, 2, 'View '),
(228, 141, 1, 'E-mail'),
(229, 142, 1, 'Tài khoản'),
(230, 143, 1, 'Họ'),
(231, 144, 1, 'Tên'),
(232, 145, 1, 'Giới tính'),
(233, 146, 1, 'Ngày sinh'),
(234, 147, 1, 'Ngày cập nhật'),
(235, 148, 1, 'Mật khẩu cũ'),
(236, 149, 1, 'Mật khẩu mới'),
(237, 150, 1, 'Xác nhận mật khẩu'),
(238, 141, 2, 'E-mail '),
(239, 143, 2, 'First name '),
(240, 144, 2, 'Last name'),
(241, 142, 2, 'Username '),
(245, 150, 2, 'Confirm password '),
(246, 149, 2, 'New password '),
(247, 148, 2, 'Old password '),
(248, 147, 2, 'Updated date '),
(249, 146, 2, 'Birthday '),
(250, 145, 2, 'Gender '),
(251, 153, 1, 'Đang thực hiện'),
(252, 154, 1, 'Đã thực hiện'),
(253, 155, 1, 'Giá'),
(254, 156, 1, 'Bên trái'),
(255, 157, 1, 'Bên phải'),
(256, 158, 1, 'Liên kết'),
(257, 159, 1, 'Mô tả'),
(258, 160, 1, 'Tin nóng'),
(259, 161, 1, 'Phút trước'),
(261, 163, 1, 'giờ trước'),
(262, 156, 2, 'Bên trái'),
(263, 155, 2, 'Price '),
(264, 158, 2, 'URL '),
(265, 163, 2, 'hours ago '),
(266, 161, 2, 'minutes ago '),
(267, 160, 2, 'Hot news '),
(268, 159, 2, 'Description '),
(269, 157, 2, 'Bên phải  '),
(270, 154, 2, 'Completed '),
(271, 153, 2, 'Developing '),
(272, 124, 2, 'Name '),
(273, 126, 2, 'Post '),
(274, 127, 2, 'List '),
(275, 130, 2, 'Success '),
(276, 132, 2, 'Permission '),
(277, 133, 2, 'Login to backend '),
(278, 134, 2, 'Background '),
(279, 164, 1, 'Để trống nếu muốn hiển thị giá LIÊN HỆ'),
(280, 165, 1, 'Xem tất cả'),
(281, 165, 2, 'View all'),
(282, 164, 2, 'Leave blank for AGREEMENT price '),
(283, 166, 1, 'Tên đầy đủ'),
(284, 167, 1, 'Gửi'),
(285, 168, 1, 'Phân quyền'),
(286, 168, 2, 'Select permission '),
(287, 167, 2, 'Send '),
(288, 166, 2, 'Full name '),
(289, 169, 1, 'Hỗ trợ trực tuyến '),
(290, 169, 2, 'Support Online'),
(291, 170, 1, 'Đối tác của chúng tôi'),
(292, 170, 2, 'Our Partners'),
(293, 171, 1, 'Đối tác của chúng tôi'),
(294, 171, 2, 'Our Partners '),
(295, 172, 1, 'Tìm kiếm'),
(296, 172, 2, 'Search '),
(297, 173, 1, 'Danh mục cha'),
(298, 173, 2, 'Parent category '),
(299, 174, 1, 'Trước'),
(300, 175, 1, 'Sau'),
(301, 176, 1, 'Sắp xếp theo'),
(302, 177, 1, 'Tên sản phẩm'),
(303, 178, 1, 'Tăng dần'),
(304, 179, 1, 'Giảm dần'),
(305, 180, 1, 'Hiển thị'),
(306, 181, 1, 'Sản phẩm/Trang'),
(307, 175, 2, 'Next '),
(308, 174, 2, 'Previous '),
(309, 176, 2, 'Order by '),
(310, 177, 2, 'Product name '),
(311, 178, 2, 'Low to high '),
(312, 179, 2, 'High to low '),
(313, 180, 2, 'Display '),
(314, 181, 2, 'Product/Page '),
(315, 182, 1, 'error: phrase '),
(317, 184, 1, 'Thông tin Công ty'),
(318, 185, 1, 'Hãy trở thành Đối tác của chúng tôi'),
(319, 186, 1, 'Xí nghiệp Quang Trung hiện tại có hơn 100 đối tác trên Toàn quốc.....'),
(320, 187, 1, 'Liên hệ hoặc góp ý với Xí nghiệp Quang Trung'),
(321, 184, 2, 'Company info '),
(322, 185, 2, 'Become our Partners '),
(323, 188, 1, 'Giới thiệu về tổng giám đốc'),
(324, 189, 1, 'Giới thiệu về công ty'),
(325, 190, 1, 'Dự án đang triển khai'),
(326, 191, 1, 'Thành tích'),
(327, 192, 1, 'Dự án tiêu biểu'),
(328, 193, 1, 'Đơn hàng'),
(329, 194, 1, 'Tổng tiền hàng'),
(330, 195, 1, 'Địa chỉ chuyển hàng'),
(331, 196, 1, 'Tin nhắn'),
(332, 197, 1, 'Hóa đơn'),
(333, 198, 1, 'Thông tin đơn hàng'),
(334, 199, 1, 'Mã đơn hàng'),
(335, 200, 1, 'Thông tin khách hàng'),
(336, 201, 1, 'Số lượng mua'),
(338, 203, 1, 'Xuất ra excel'),
(339, 204, 1, 'Thêm ảnh'),
(340, 205, 1, 'Phụ đề'),
(341, 206, 1, 'Ảnh chính?'),
(344, 209, 1, 'Danh mục sản phẩm'),
(345, 210, 1, 'Trụ sở'),
(346, 211, 1, 'Nhà máy'),
(347, 212, 1, 'Văn phòng'),
(348, 213, 1, 'Hotline'),
(349, 214, 1, 'Kết nối với chúng tôi'),
(350, 215, 2, 'Sales consultant'),
(351, 215, 1, 'Tư vấn bán hàng '),
(353, 217, 1, 'Công nghệ & thiết bị sản xuất'),
(354, 217, 2, 'Technology & production equipment'),
(355, 218, 1, 'Slider'),
(356, 219, 1, 'Tìm kiếm sản phẩm'),
(357, 220, 1, 'Có {1} sản phẩm'),
(358, 221, 1, 'Sản phẩm mới nhất'),
(359, 222, 1, 'Giá: Từ thấp đến cao'),
(360, 223, 1, 'Giá: Từ cao xuống thấp'),
(361, 224, 1, 'Sản phẩm nổi bật'),
(362, 225, 1, 'Thêm vào giỏ hàng'),
(363, 226, 1, 'Thông tin sản phẩm'),
(364, 227, 1, 'Nhận xét của khách hàng'),
(365, 228, 1, 'Sản phẩm tương tự'),
(366, 229, 1, 'Chú thích {1}'),
(368, 231, 1, 'Tin tức'),
(369, 232, 1, 'Đọc tiếp'),
(370, 233, 1, 'Giỏ hàng của bạn'),
(371, 234, 1, 'Đơn giá'),
(372, 235, 1, 'Tổng tiền'),
(373, 236, 1, 'Hủy'),
(374, 237, 1, 'Tiếp tục mua sắm'),
(375, 238, 1, 'Đặt hàng'),
(376, 239, 1, 'Chưa có sản phẩm'),
(377, 240, 1, 'Đến giỏ hàng'),
(378, 241, 1, 'Công nghệ'),
(379, 242, 1, 'Thông tin đơn hàng'),
(380, 243, 1, 'Thông tin cá nhân'),
(381, 244, 1, 'Quý khách đã đặt hàng thành công! Chúng tôi sẽ liên hệ lại với Quý khách trong thời gian sớm nhất'),
(382, 245, 1, 'Cảm ơn Quý khách!'),
(383, 246, 1, 'Thông tin công ty'),
(384, 247, 1, 'Địa chỉ'),
(386, 249, 1, 'Showroom'),
(387, 250, 1, 'Tin tức liên quan'),
(388, 251, 1, 'Tin trước'),
(389, 252, 1, 'Tin sau'),
(390, 253, 1, 'Còn hàng'),
(391, 254, 1, 'Sản phẩm hiện tại đang hết hàng! Chúng tôi sẽ cập nhật trong thời gian sớm nhất'),
(392, 255, 1, 'Hết hàng'),
(393, 255, 2, 'Out of stock'),
(394, 256, 1, 'Thực hiện mục tiêu phát triển chiều sâu...'),
(395, 256, 2, 'Perform in-depth development goals'),
(396, 257, 1, 'Công nghệ'),
(397, 258, 1, 'Thiết bị'),
(398, 250, 2, 'Related Posts '),
(399, 253, 2, 'In stock '),
(400, 252, 2, 'Next '),
(401, 251, 2, 'Previous '),
(402, 249, 2, 'Showroom '),
(403, 259, 1, 'Xin cảm ơn. Tin nhắn của Quý vị đã được gửi thành công! Quang Trung sẽ liên hệ với Quý vị trong thời gian sớm nhất.'),
(404, 260, 1, 'Gửi không thành công! Vui lòng kiểm tra lại thông tin.'),
(405, 260, 2, 'Failed. Please check your information.  '),
(406, 259, 2, 'Thank you! Your message was sent successfully. Quang Trung will contact you as soon as possible.  '),
(407, 261, 1, 'Gửi từ'),
(408, 262, 1, 'Gửi đến'),
(409, 263, 1, 'Tiêu đề'),
(410, 264, 1, 'Trả lời'),
(411, 265, 1, 'Ngày gửi'),
(412, 265, 2, 'Sent date '),
(413, 264, 2, 'Reply '),
(414, 263, 2, 'Subject '),
(415, 262, 2, 'To '),
(416, 261, 2, 'From '),
(417, 266, 1, 'Các dự án tiêu biểu mà Tập đoàn Công nghiệp Quang Trung đã thực hiện'),
(418, 267, 1, 'Đã hủy'),
(419, 268, 1, 'Đơn hàng mới'),
(420, 269, 1, 'Đã xử lý'),
(421, 270, 1, 'Hủy đơn hàng'),
(422, 271, 1, 'Tiếp nhận đơn hàng'),
(423, 272, 1, 'Bạn có chắc không?'),
(424, 209, 2, 'Categories '),
(425, 273, 1, 'Văn phòng đại diện'),
(426, 274, 1, 'Nhà máy'),
(427, 275, 1, 'Bản đồ'),
(428, 276, 1, 'Những điểm mạnh của Sản phẩm Đèn Led Quang Trung'),
(429, 277, 1, 'Mã sản phẩm'),
(430, 278, 1, 'Xuất xứ'),
(431, 279, 1, 'Nhóm sản phẩm'),
(432, 280, 1, 'Thư viện Video'),
(433, 281, 1, 'Chọn từ máy tính'),
(434, 282, 1, 'Chọn từ URL'),
(435, 283, 1, 'Chỉ hỗ trợ link YouTube'),
(436, 284, 1, 'Kết quả tìm kiếm cho từ khóa: {1}'),
(437, 284, 2, 'Search results for keyword: {1} '),
(438, 285, 2, 'error: phrase [customer] not found'),
(439, 286, 2, 'error: phrase [general] not found'),
(440, 287, 2, 'error: phrase [payment] not found'),
(441, 288, 1, 'error: phrase [lists] not found'),
(442, 289, 1, 'error: phrase [country] not found'),
(443, 290, 1, 'error: phrase [languages] not found'),
(444, 291, 1, 'error: phrase [add_a_new_x] not found'),
(445, 292, 1, 'error: phrase [in_use] not found'),
(446, 293, 1, 'error: phrase [not_in_use] not found');

-- --------------------------------------------------------

--
-- Table structure for table `popup`
--

CREATE TABLE IF NOT EXISTS `popup` (
`popup_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `background` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `category_id`, `image`, `view`, `created_date`, `feature`, `status`) VALUES
(1, 30, '_image.png', 5, '2016-03-18 10:53:56', 1, 1),
(2, 30, '2_image.png', 5, '2016-03-21 02:54:48', 1, 1),
(3, 30, '_image.png', 5, '2016-03-21 10:06:16', 1, 1),
(4, 30, '_image.png', 5, '2016-03-21 10:15:53', 1, 1),
(5, 30, '_image.png', 6, '2016-03-21 10:18:18', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_lang`
--

CREATE TABLE IF NOT EXISTS `post_lang` (
`id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_lang`
--

INSERT INTO `post_lang` (`id`, `post_id`, `language`, `title`, `description`, `content`) VALUES
(1, 5, 'vi', 'tin bóng đá', 'tin bóng đá', 'tin bóng đá'),
(2, 5, 'en', 'football news', 'football news', 'football news');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `origin` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  `in_stock` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
`image_id` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `is_main_image` tinyint(1) DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_lang`
--

CREATE TABLE IF NOT EXISTS `product_lang` (
`id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recruitment`
--

CREATE TABLE IF NOT EXISTS `recruitment` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `recruitment_lang`
--

CREATE TABLE IF NOT EXISTS `recruitment_lang` (
`id` int(11) NOT NULL,
  `recruitment_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE IF NOT EXISTS `reply` (
`reply_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `is_backend_login` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `permissions`, `is_backend_login`) VALUES
(1, 'Administrator', '{"navatech\\\\role\\\\controllers\\\\DefaultController":{"index":1,"create":1,"update":1,"delete":1}}', 1),
(2, 'Staff', '{"navatech\\\\role\\\\controllers\\\\DefaultController":{"index":0,"create":0,"update":0,"delete":0}}', 1),
(3, 'Member', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
`setting_id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL COMMENT 'json',
  `type` varchar(255) NOT NULL,
  `key` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `showroom`
--

CREATE TABLE IF NOT EXISTS `showroom` (
`showroom_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `address` text CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(255) CHARACTER SET latin1 NOT NULL,
  `fax` varchar(255) CHARACTER SET latin1 NOT NULL,
  `map` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
`slider_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `role_id`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$YiHv/jRh.F3B41YlRDTZCOtT970o6nNybcy2wwYS3ncRhpjmDW3gi', 'X2eREcnF64l7EPaOCzEqC0y9Yt2EtFCH', 1456216036, NULL, NULL, '127.0.0.1', 1456216036, 1456216036, 0, 1),
(3, 'thucha', 'thuchm92@gmail.com', '$2y$10$yAbtdgnydxRuLkBWqhvh1ej.cykg6Ieoel00nI1vtYQq0Z.jYxhry', '0Qfg-uRVkYhjs7h6IufgZbXWDpa6ZUYk', 1456216722, NULL, NULL, '127.0.0.1', 1456216722, 1456216722, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_lang`
--
ALTER TABLE `banner_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `bannerlang_ibfk_1` (`banner_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_lang`
--
ALTER TABLE `category_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `category_lang_ibfk_1` (`category_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_lang`
--
ALTER TABLE `client_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `clientlang_ibfk_1` (`client_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
 ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_lang`
--
ALTER TABLE `department_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `departmentlang_ibfk_1` (`department_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
 ADD PRIMARY KEY (`order_item_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_lang`
--
ALTER TABLE `page_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `pagelang_ibfk_1` (`page_id`);

--
-- Indexes for table `phrase`
--
ALTER TABLE `phrase`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phrase_translate`
--
ALTER TABLE `phrase_translate`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popup`
--
ALTER TABLE `popup`
 ADD PRIMARY KEY (`popup_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_lang`
--
ALTER TABLE `post_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `postlang_ibfk_1` (`post_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `product_lang`
--
ALTER TABLE `product_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `productlang_ibfk_1` (`product_id`);

--
-- Indexes for table `recruitment`
--
ALTER TABLE `recruitment`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recruitment_lang`
--
ALTER TABLE `recruitment_lang`
 ADD PRIMARY KEY (`id`), ADD KEY `recruitmentlang_ibfk_1` (`recruitment_id`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
 ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
 ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `showroom`
--
ALTER TABLE `showroom`
 ADD PRIMARY KEY (`showroom_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_unique_email` (`email`), ADD UNIQUE KEY `user_unique_username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `banner_lang`
--
ALTER TABLE `banner_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `category_lang`
--
ALTER TABLE `category_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client_lang`
--
ALTER TABLE `client_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department_lang`
--
ALTER TABLE `department_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_lang`
--
ALTER TABLE `page_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `phrase`
--
ALTER TABLE `phrase`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=294;
--
-- AUTO_INCREMENT for table `phrase_translate`
--
ALTER TABLE `phrase_translate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=447;
--
-- AUTO_INCREMENT for table `popup`
--
ALTER TABLE `popup`
MODIFY `popup_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `post_lang`
--
ALTER TABLE `post_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_lang`
--
ALTER TABLE `product_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recruitment`
--
ALTER TABLE `recruitment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recruitment_lang`
--
ALTER TABLE `recruitment_lang`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `showroom`
--
ALTER TABLE `showroom`
MODIFY `showroom_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `banner_lang`
--
ALTER TABLE `banner_lang`
ADD CONSTRAINT `bannerlang_ibfk_1` FOREIGN KEY (`banner_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_lang`
--
ALTER TABLE `category_lang`
ADD CONSTRAINT `category_lang_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `client_lang`
--
ALTER TABLE `client_lang`
ADD CONSTRAINT `clientlang_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `department_lang`
--
ALTER TABLE `department_lang`
ADD CONSTRAINT `departmentlang_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_lang`
--
ALTER TABLE `page_lang`
ADD CONSTRAINT `pagelang_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `post_lang`
--
ALTER TABLE `post_lang`
ADD CONSTRAINT `postlang_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_lang`
--
ALTER TABLE `product_lang`
ADD CONSTRAINT `productlang_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `recruitment_lang`
--
ALTER TABLE `recruitment_lang`
ADD CONSTRAINT `recruitmentlang_ibfk_1` FOREIGN KEY (`recruitment_id`) REFERENCES `recruitment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
