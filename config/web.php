<?php
$params  = require(__DIR__ . '/params.php');
$baseUrl = str_replace('/web', '', (new \yii\web\Request)->getBaseUrl());
$config  = [
	'id'         => 'basic',
	'basePath'   => dirname(__DIR__),
	'bootstrap'  => ['log'],
	'language'   => 'vi',
	'timezone'   => 'Asia/Ho_Chi_Minh',
	'components' => [
		'request'      => [
			'baseUrl'             => $baseUrl,
			'cookieValidationKey' => 'aXbVOydPsTbPSA76HhPHyQJc7QgLqUU5',
		],
		'cache'        => [
			'class' => 'yii\caching\FileCache',
		],
		'user'         => [
			'identityClass'   => 'app\models\User',
			'enableAutoLogin' => true,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'warning',
					],
				],
			],
		],
		'db'           => require(__DIR__ . '/db.php'),
		'urlManager'   => [
//			'class'               => '\navatech\localeurls\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'rules'           => require(__DIR__ . '/route.php'),
		],
		'view'         => [
			'class' => '\app\components\View',
			'theme' => [
				'basePath' => '@app/web/themes/frontend',
				'baseUrl'  => '@web/themes/frontend',
			],
		],
		'setting'      => [
			'class' => 'navatech\setting\Setting',
		],
	],
	'modules'    => [
		'user'     => [
			'class'    => 'dektrium\user\Module',
			'modelMap' => [
				'User'      => 'app\models\User',
				'LoginForm' => 'navatech\role\models\LoginForm',
			],
		],
		'admin'    => [
			'class'    => 'app\modules\admin\Module',
			'viewPath' => '@app/web/themes/backend/views',
		],
		'gridview' => [
			'class' => '\kartik\grid\Module',
		],
		'language' => [
			'class'  => '\navatech\language\Module',
			'suffix' => 'lang',
		],
		'role'     => [
			'class'       => 'navatech\role\Module',
			'controllers' => [
				'app\modules\admin\controllers',
				'navatech\role\controllers',
			],
		],
		'roxymce'  => [
			'class' => '\navatech\roxymce\Module',
		],
	],
	'params'     => $params,
];
if (YII_ENV_DEV) {
	$config['bootstrap'][]      = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
	];
	$config['bootstrap'][]      = 'gii';
	$config['modules']['gii']   = [
		'class' => 'yii\gii\Module',
	];
}
return $config;
