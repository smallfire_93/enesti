<?php
/**
 * Created by Navatech.
 * @project enesti-com-vn
 * @author  Phuong
 * @email   phuong17889[at]gmail.com
 * @date    6/13/2016
 * @time    12:06 PM
 */
return [
	'<language:vi>/danh-muc/<slug>-<id>/trang-<page>'         => 'product/category',
	'<language:en>/category/<slug>-<id>/page-<page>'          => 'product/category',
	'<language:vi>/danh-muc-tin-tuc/<slug>-<id>/trang-<page>' => 'post/category',
	'<language:en>/news-category/<slug>-<id>/page-<page>'     => 'post/category',
	'<language:vi>/tin-tuc/<slug>-<id>'                       => 'post/view',
	'<language:en>/news/<slug>-<id>'                          => 'post/view',
	'<language:vi>/trang/<slug>-<id>'                         => 'page/view',
	'<language:en>/page/<slug>-<id>'                          => 'page/view',
	'<language:vi>/san-pham/<slug>-<id>'                      => 'product/view',
	'<language:en>/product/<slug>-<id>'                       => 'product/view',
	'<language:vi>/danh-muc/<slug>-<id>'                      => 'product/category',
	'<language:en>/category/<slug>-<id>'                      => 'product/category',
	'<language:vi>/danh-muc-tin-tuc/<slug>-<id>'              => 'post/category',
	'<language:en>/news-category/<slug>-<id>'                 => 'post/category',
	'<language:vi>/nha-phan-phoi/don-hang'                    => 'order/index',
	'<language:en>/distributor/order'                         => 'order/index',
	'<language:vi>/nha-phan-phoi/tai-khoan'                   => 'distributor/profile',
	'<language:en>/distributor/account'                       => 'distributor/profile',
	'<language:vi>/tin-tuc'                                   => 'post/index',
	'<language:en>/news'                                      => 'post/index',
	'<language:vi>/lien-he'                                   => 'contact/index',
	'<language:en>/contact'                                   => 'contact/index',
	'<language:vi>/san-pham'                                  => 'product/index',
	'<language:en>/product'                                   => 'product/index',
	'<language:vi>/dang-nhap'                                 => 'user/login',
	'<language:en>/login'                                     => 'user/login',
	'<language:vi>/dat-hang'                                  => 'order/orderitem',
	'<language:en>/place-order'                               => 'order/orderitem',
	'<language:vi>/dang-xuat'                                 => 'site/logout',
	'<language:en>/logout'                                    => 'site/logout',
	'<language:vi>'                                           => 'site/index',
	'<language:en>'                                           => 'site/index',
	'/'                                                       => 'site/index',
	'<language:en>/site/maintain'                             => 'site/maintain',
	'<language:vi>/recruitment/<slug>-<id>'                   => 'recruitment/view',
	'<language:en>/tuyen-dung/<slug>-<id>'                    => 'recruitment/view',
	'<language:vi>/khuyen-mai/<slug>-<id>'                    => 'promotion/view',
	'<language:en>/promotion/<slug>-<id>'                     => 'promotion/view'
];