<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class AppAsset extends AssetBundle
{

    public function init()
    {
        parent::init();
        $this->basePath = '@app/themes/frontend/assets';
        $this->baseUrl = '@web/themes/frontend/assets';
        $this->css = [
            'css/idangerous.swiper.3dflow.css',
            'css/owl.carousel.css',
            'css/colorbox.css',
            'css/style.css',
            'css/responsive.css',
        ];
        $this->js = [
            //			'scripts/jquery-1.12.3.min.js',
            'scripts/modernizr.custom.53451.js',
            'scripts/goodshare.js',
            'scripts/owl.carousel.js',
            'scripts/jquery.colorbox-min.js',
            'scripts/custom.js',
            'scripts/order.js',
        ];
        $this->depends = [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapAsset',
            '\rmrevin\yii\fontawesome\AssetBundle',
        ];
        $this->jsOptions = ['position' => View::POS_HEAD];
    }
}
