<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class BackendAsset extends AssetBundle {

	public $basePath  = '@app/themes/backend/assets';

	public $baseUrl   = '@web/themes/backend/assets';

	public $css       = [
		'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all',
		'plugins/font-awesome/css/font-awesome.min.css',
		'plugins/font-awesome/css/font-awesome.css',
		'plugins/simple-line-icons/simple-line-icons.min.css',
		//		'plugins/uniform/css/uniform.default.css',
		'plugins/bootstrap-daterangepicker/daterangepicker.min.css',
		'plugins/morris/morris.css',
		'plugins/fullcalendar/fullcalendar.min.css',
		'css/components.min.css',
		'css/plugins.min.css',
		'css/layout.min.css',
		'css/darkblue.min.css',
		'css/custom.min.css',
		'css/site.css',
		'css/custom.css',
	];

	public $js        = [
		'plugins/js.cookie.min.js',
		'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		'plugins/jquery.blockui.min.js',
		'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
		'plugins/moment.min.js',
		'plugins/bootstrap-daterangepicker/daterangepicker.min.js',
		'plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
		'plugins/morris/morris.min.js',
		'plugins/morris/raphael-min.js',
		'plugins/counterup/jquery.waypoints.min.js',
		'plugins/counterup/jquery.counterup.min.js',
		'plugins/fullcalendar/fullcalendar.min.js',
		'plugins/js.cookie.min.js',
		'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		'plugins/jquery.blockui.min.js',
		'scripts/app.min.js',
		'pages/scripts/dashboard.min.js',
		'scripts/layout.min.js',
		'scripts/demo.min.js',
		'scripts/quick-sidebar.min.js',
		'pages/scripts/components-date-time-pickers.min.js',

	];

	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

	public $depends   = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
		'yii\bootstrap\BootstrapPluginAsset',
		'\kartik\switchinput\SwitchInputAsset',
	];
}
