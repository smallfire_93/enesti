<?php
/**
 * Created by Navatech.
 * @project go_restaurant
 * @author  Thuc
 * @email   thuchm92[at]gmail.com
 * @date    23/02/2016
 * @time    12:10 CH
 */
namespace app\assets;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle {
	public $basePath = '@app/themes/backend/assets';
	public $baseUrl  = '@web/themes/backend/assets';

	public $css     = [
		'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all',
		'plugins/font-awesome/css/font-awesome.min.css',
		'plugins/simple-line-icons/simple-line-icons.min.css',
		'plugins/bootstrap/css/bootstrap.min.css',
		'plugins/uniform/css/uniform.default.css',
		'plugins/bootstrap-switch/css/bootstrap-switch.min.css',
		'plugins/select2/css/select2.min.css',
		'plugins/select2/css/select2-bootstrap.min.css',
		'css/components.min.css',
		'css/plugins.min.css',
		'css/login.min.css',
	];

	public $js      = [
		'plugins/jquery.min.js',
		'plugins/bootstrap/js/bootstrap.min.js',
		'plugins/js.cookie.min.js',
		'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
		'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
		'plugins/jquery.blockui.min.js',
		'plugins/uniform/jquery.uniform.min.js',
		'plugins/bootstrap-switch/js/bootstrap-switch.min.js',
		'plugins/jquery-validation/js/jquery.validate.min.js',
		'plugins/jquery-validation/js/additional-methods.min.js',
		'plugins/select2/js/select2.full.min.js',
		'scripts/app.min.js',
		'pages/scripts/login.min.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
