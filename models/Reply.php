<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "reply".
 *
 * @property integer $id
 * @property integer $contact_id
 * @property string $from
 * @property string $to
 * @property string $fullname
 * @property string $subject
 * @property string $content
 * @property string $created_date
 */
class Reply extends ActiveRecord
{
    public $detail;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'from', 'to', 'fullname', 'subject', 'content'], 'required'],
            [['contact_id'], 'integer'],
            [['content'], 'string'],
            [['created_date'], 'safe'],
            [['from', 'to', 'fullname', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Reply ID',
            'contact_id' => 'Contact ID',
            'from' => 'From',
            'to' => 'To',
            'fullname' => 'Fullname',
            'subject' => 'Subject',
            'content' => 'Content',
            'created_date' => 'Created Date',
        ];
    }
}
