<?php
namespace app\models\search;

use app\models\Department;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DepartmentSearch represents the model behind the search form about `app\models\Department`.
 */
class DepartmentSearch extends Department {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'status',
					'show_home',
					'order',
				],
				'integer',
			],
			[
				[
					'email',
					'phone',
					'created_date',
					'business_room',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Department::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['order' => SORT_ASC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'           => $this->id,
			'status'       => $this->status,
			'show_home'    => $this->show_home,
			'order'        => $this->order,
			'created_date' => $this->created_date,
		]);
		$query->andFilterWhere([
			'like',
			'email',
			$this->email,
		])->andFilterWhere([
			'like',
			'phone',
			$this->phone,
		]);
		return $dataProvider;
	}
}
