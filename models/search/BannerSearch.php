<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Banner;

/**
 * BannerSearch represents the model behind the search form about `app\models\Banner`.
 */
class BannerSearch extends Banner {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'order',
					'status',
				],
				'integer',
			],
			[
				[
					'image',
					'position',
					'url',
					'created_date',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query        = Banner::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		$query->andFilterWhere([
			'id'           => $this->id,
			'order'        => $this->order,
			'status'       => $this->status,
			'created_date' => $this->created_date,
		]);
		$query->andFilterWhere([
			'like',
			'image',
			$this->image,
		])->andFilterWhere([
			'like',
			'position',
			$this->position,
		])->andFilterWhere([
			'like',
			'url',
			$this->url,
		]);
		return $dataProvider;
	}

	
}
