<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reply;

/**
 * ReplySearch represents the model behind the search form about `app\models\Reply`.
 */
class ReplySearch extends Reply {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'contact_id',
				],
				'integer',
			],
			[
				[
					'from',
					'to',
					'fullname',
					'subject',
					'content',
					'created_date',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $id) {
		$query = Reply::find()->where(['contact_id' => $id]);
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'           => $this->id,
			'contact_id'   => $this->contact_id,
			'created_date' => $this->created_date,
		]);
		$query->andFilterWhere([
			'like',
			'from',
			$this->from,
		])->andFilterWhere([
			'like',
			'to',
			$this->to,
		])->andFilterWhere([
			'like',
			'fullname',
			$this->fullname,
		])->andFilterWhere([
			'like',
			'subject',
			$this->subject,
		])->andFilterWhere([
			'like',
			'content',
			$this->content,
		]);
		return $dataProvider;
	}
}
