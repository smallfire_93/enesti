<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Showroom;

/**
 * ShowroomSearch represents the model behind the search form about `app\models\Showroom`.
 */
class ShowroomSearch extends Showroom {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'showroom_id',
					'status',
					'order',
				],
				'integer',
			],
			[
				[
					'email',
					'phone',
					'fax',
					'map',
					'created_date',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Showroom::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'showroom_id'  => $this->showroom_id,
			'status'       => $this->status,
			'order'        => $this->order,
			'created_date' => $this->created_date,
		]);
		$query->andFilterWhere([
			'like',
			'email',
			$this->email,
		])->andFilterWhere([
			'like',
			'phone',
			$this->phone,
		])->andFilterWhere([
			'like',
			'fax',
			$this->fax,
		])->andFilterWhere([
			'like',
			'map',
			$this->map,
		]);
		return $dataProvider;
	}
	
}
