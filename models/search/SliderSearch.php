<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Slider;

/**
 * SliderSearch represents the model behind the search form about `app\models\Slider`.
 */
class SliderSearch extends Slider {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'sort',
					'status',
				],
				'integer',
			],
			[
				[
					'image',
					'url',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Slider::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'order'     => $this->sort,
			'status'    => $this->status,
		]);
		$query->andFilterWhere([
			'like',
			'image',
			$this->image,
		])->andFilterWhere([
			'like',
			'url',
			$this->url,
		]);
		return $dataProvider;
	}

	
}
