<?php
namespace app\models\search;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Distributor;

/**
 * DistributorSearch represents the model behind the search form about `app\models\Distributor`.
 */
class DistributorSearch extends Distributor {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'status',
					'show_home',
					'area',
				],
				'integer',
			],
			[
				[
					'logo',
					'website',
					'area',
					'payment',
					'type',
					'username',
					'password_hash',
					'email',
					'show_home',
					'transport_mode',
					'birthday',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Distributor::find()->where(['role_id' => 2])->orWhere(['role_id' => 3]);
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'       => $this->id,
			'status'   => $this->status,
			'show_home' => $this->show_home,
			'type'     => $this->type,
			'area'     => $this->area,
			'birthday' => $this->birthday,
		]);
		$query->andFilterWhere([
			'like',
			'website',
			$this->website,
			$this->transport_mode,
			$this->username,
			$this->email,
		]);
		return $dataProvider;
	}
}
