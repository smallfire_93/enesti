<?php
namespace app\models\search;

use app\models\Category;
use navatech\language\components\MultiLanguageBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class CategorySearch extends Category {

	public $name;

	public $description;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'parent_id',
					'type',
					'order',
					'status',
				],
				'integer',
			],
			[
				[
					'name',
					'description',
				],
				'safe',
			],
			[
				[
					'image',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * {@inheritDoc}
	 */
	public function behaviors($attributes = null) {
		if ($attributes == null) {
			$attributes = [
				'name_' . Yii::$app->language,
				'description_' . Yii::$app->language,
			];
		}
		return [
			'ml' => [
				'class'      => MultiLanguageBehavior::className(),
				'attributes' => $attributes,
			],
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query        = Category::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		$query->andWhere(['type' => $params['type']]);
		$query->andFilterWhere([
			//						'name'      => $this->name,
			'id'        => $this->id,
			'parent_id' => $this->parent_id,
			'type'      => $this->type,
			'order'     => $this->order,
			'status'    => $this->status,
		]);
		$query->andFilterWhere([
			'like',
			'image',
			$this->image,
		]);
		$query->andFilterWhere([
			'like',
			'name',
			$this->name,
		])->andFilterWhere([
			'like',
			'description',
			$this->description,
		]);
		return $dataProvider;
	}
}
