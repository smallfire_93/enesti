<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product {

	public $productLangs;

	public function attributes() {
		$attributes = parent::attributes();
		return ArrayHelper::merge($attributes, ['title']);
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'category_id',
					'feature',
					'status',
					'in_stock',
				],
				'integer',
			],
			[
				[
					'code',
					'origin',
					'created_date',
					'title',
				],
				'safe',
			],
			[
				['price'],
				'number',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query        = Product::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		$query->joinWith(['productLangs']);
		$query->andFilterWhere([
			'id'           => $this->id,
			'category_id'  => $this->category_id,
			'created_date' => $this->created_date,
			'feature'      => $this->feature,
			'status'       => $this->status,
			'price'        => $this->price,
			'in_stock'     => $this->in_stock,
		]);
		$query->andFilterWhere([
			'like',
			'code',
			$this->code,
		])->andFilterWhere([
			'like',
			'origin',
			$this->origin,
		])->andFilterWhere([
			'like',
			'title',
			$this->title,
		]);
		return $dataProvider;
	}

	/**
	 * {@inheritDoc}
	 */
	public function behaviors($attribute = null) {
		return [];
	}
}
