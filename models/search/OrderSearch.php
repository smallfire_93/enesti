<?php
namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order {


	public $createdFrom;

	public $createdTo;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['id'],
				'integer',
			],
			[
				[
					'phone_number',
					'shipping_address',
					'created_date',
					'createdFrom',
					'createdTo'
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Order::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$this->created_date = date('Y-m-d 00:00:00',$this->created_date);
		$this->createdFrom = date('Y-m-d 00:00:00',strtotime($this->createdFrom));
		$this->createdTo = date('Y-m-d 23:59:59',strtotime($this->createdTo));
		$query->andFilterWhere([
			'id'             => $this->id,
			'distributor_id' => $this->distributor_id,
		]);
		$query->andFilterWhere(['>=', 'created_date', $this->createdFrom])
		      ->andFilterWhere(['<=', 'created_date', $this->createdTo]);
		$query->andFilterWhere([
			'like',
			'phone_number',
			$this->phone_number,
		])->andFilterWhere([
			'like',
			'shipping_address',
			$this->shipping_address,
		]);
		return $dataProvider;
	}
}
