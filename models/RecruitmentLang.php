<?php

namespace app\models;

use app\models\Recruitment;
use Yii;

/**
 * This is the model class for table "recruitment_lang".
 *
 * @property integer $id
 * @property integer $recruitment_id
 * @property string $language
 * @property string $title
 * @property string $content
 *
 * @property Recruitment $recruitment
 */
class RecruitmentLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recruitment_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recruitment_id', 'language', 'title', 'content'], 'required'],
            [['recruitment_id'], 'integer'],
            [['content'], 'string'],
            [['language'], 'string', 'max' => 11],
            [['title'], 'string', 'max' => 255],
            [['recruitment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recruitment::className(), 'targetAttribute' => ['recruitment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recruitment_id' => 'Recruitment ID',
            'language' => 'Language',
            'title' => 'Title',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecruitment()
    {
        return $this->hasOne(Recruitment::className(), ['id' => 'recruitment_id']);
    }
}
