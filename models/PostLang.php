<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_lang".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $language
 * @property string $title
 * @property string $description
 * @property string $content
 */
class PostLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'language', 'title', 'description', 'content'], 'required'],
            [['post_id'], 'integer'],
            [['description','language','content'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'language' => 'Language',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Category::className(), ['id' => 'post_id']);
    }

    public function behaviors() {
        $attributes = [
            'title',
            'description',
            'content'
        ];
        return parent::behaviors($attributes);
    }
}
