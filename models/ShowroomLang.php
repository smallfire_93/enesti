<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "showroom_lang".
 *
 * @property integer $id
 * @property integer $showroom_id
 * @property string $name
 * @property string $address
 */
class ShowroomLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'showroom_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['showroom_id', 'name', 'address'], 'required'],
            [['showroom_id'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'showroom_id' => 'Showroom ID',
            'name' => 'Name',
            'address' => 'Address',
        ];
    }
}
