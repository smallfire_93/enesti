<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $image
 * @property string  $position
 * @property string  $url
 * @property integer $view
 * @property integer $order
 * @property integer $status
 * @property string  $created_date
 * @property string  $title
 */
class Banner extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'banner';
	}

	public $img;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'position',
					'url',
				],
				'required',
			],
			[
				[
					'user_id',
					'order',
					'status',
				],
				'integer',
			],
			[
				[
					'title',
					'created_date',
				],
				'safe',
			],
			[
				['img'],
				'file',
				'extensions' => 'jpg, gif, png',
			],
			[
				[
					'image',
					'position',
					'url',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'NO',
			'title'        => Translate::title(),
			'user_id'      => Translate::user(),
			'image'        => Translate::image(),
			'position'     => Translate::position(),
			'url'          => Translate::url(),
			'order'        => Translate::sort(),
			'status'       => Translate::status(),
			'created_date' => Translate::create_date(),
		];
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'title',
		];
		return parent::behaviors($attributes);
	}

	/**
	 * @param null $position
	 *
	 * @return array|string
	 */
	public function getPosition($position = null) {
		if ($position != null) {
			return $position == 1 ? Translate::right() : Translate::home();
		} else {
			return array(
				0 => Translate::home(),
				1 => Translate::right(),
			);
		}
	}

	/**
	 * @param $position int = 0 trái, 1 phải
	 *
	 * @return Banner
	 */
	public function getBanner($position) {
		return self::findOne(['position' => $position]);
	}
}
