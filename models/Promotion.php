<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "promotion".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string  $image
 * @property integer $view
 * @property string  $created_date
 * @property integer $feature
 * @property integer $status
 * @property string  $content
 * @property string  $title
 * @property string  $description
 */
class Promotion extends Model {
	public $pagelink;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'promotion';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'category_id',
				],
				'required',
			],
			[
				[
					'category_id',
					'feature',
					'status',
				],
				'integer',
			],
			[
				[
					'created_date',
					'title',
					'content',
					'description',
				],
				'safe',
			],
			[
				['image'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'category_id'  => Translate::category(),
			'image'        => Translate::image(),
			'created_date' => 'Created Date',
			'feature'      => Translate::feature(),
			'status'       => Translate::status(),
			'title'        => Translate::title(),
			'content'      => Translate::content(),
			'description'  => Translate::description(),
		];
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'title',
			'content',
			'description',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}

	public function getPromotionLangs() {
		return $this->hasMany(PromotionLang::className(), ['promotion_id' => 'id']);
	}

	public function getCategory() {
		return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}

	public function getCategoryName($id) {
		$name = Category::findOneTranslated($id)->name;
		return $name;
	}
}
