<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string  $logo
 * @property string  $website
 * @property string  $created_date
 * @property integer $status
 * @property integer $name
 *
 */
class Client extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'client';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'website',
				],
				'required',
			],
			[
				[
					'created_date',
					'name',
				],
				'safe',
			],
			[
				['status'],
				'integer',
			],
			[
				[
					'logo',
					'website',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'logo'         => 'Logo',
			'website'      => 'Website',
			'created_date' => 'Created Date',
			'status'       => Translate::status(),
		];
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'name',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}
}
