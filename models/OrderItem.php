<?php
namespace app\models;

use navatech\language\Translate;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $price
 */
class OrderItem extends ActiveRecord {

	public $category_id;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'order_item';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'order_id',
					'product_id',
					'price',
				],
				'required',
			],
			[
				[
					'order_id',
					'product_id',
					'quantity',
					'price',
				],
				'integer',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'         => 'Order Item ID',
			'order_id'   => 'Order ID',
			'product_id' => Translate::product(),
			'quantity'   => Translate::quantity(),
			'price'      => Translate::price(),
		];
	}
	//
	//	public function getOrder() {
	//		$this->hasOne(Order::className(), ['id' => 'order_id']);
	//	}
	public function getProductById($id = null) {
		if ($id !== null) {
			/**@var Product $product */
			$product = Product::findOneTranslated($id);
			return $product;
		} else {
			return '';
		}
	}

	public function getProductImage($id = null) {
		if ($id !== null) {
			/**@var Product $product */
			$product = Product::findOneTranslated($id);
			return $product->getPictureUrl('image');
		} else {
			return '';
		}
	}

	public function getProductArray($id) {
		$item     = OrderItem::findOne($id);
		$category = $this->getCategoryByProduct($item->product_id);
		$products = Product::find()->where(['category_id' => $category->id])->all();
		$array    = [];
		foreach ($products as $product) {
			$array[$product->id] = $product->title;
		}
		return $array;
	}
	

	public function getCategoryIdItem($id) {
		$item     = OrderItem::findOne($id);
		$product  = Product::findOne($item->product_id);
		$category = Category::findOne($product->category_id);
		return $category->id;
	}

	public function getCategoryByProduct($id) {
		$product  = Product::findOne($id);
		$category = Category::findOne($product->category_id);
		return $category;
	}
}
