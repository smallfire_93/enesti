<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "distributor_lang".
 *
 * @property integer     $id
 * @property integer     $distributor_id
 * @property string      $language
 * @property string      $address
 *
 * @property Distributor $distributor
 */
class DistributorLang extends ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'distributor_lang';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
//					'distributor_id',
					'language',
					'address',
				],
				'required',
			],
			[
				['distributor_id'],
				'integer',
			],
			[
				['language'],
				'string',
				'max' => 11,
			],
			[
				['distributor_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Distributor::className(),
				'targetAttribute' => ['distributor_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'             => 'ID',
			'distributor_id' => 'Distributor ID',
			'language'       => 'Language',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDistributor() {
		return $this->hasOne(Distributor::className(), ['id' => 'distributor_id']);
	}
}
