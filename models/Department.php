<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "department".
 *
 * @property integer          $id
 * @property string           $name
 * @property string           $address
 * @property string           $email
 * @property string           $phone
 * @property string           $website
 * @property integer          $status
 * @property integer          $show_home
 * @property integer          $order
 * @property string           $created_date
 * @property string           $company_code
 * @property string           $schedule
 * @property string           $google_plus
 * @property string           $business_room
 *
 * @property DepartmentLang[] $departmentLangs
 */
class Department extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'department';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'name',
				],
				'required',
			],
			[
				[
					'address',
					'facebook',
					'google_plus',
				],
				'string',
			],
			[
				[
					'status',
					'show_home',
					'order',
					'business_room',
				],
				'integer',
			],
			[
				[
					'created_date',
					'business_room',
					'company_code',
					'schedule',
				],
				'safe',
			],
			[
				[
					'name',
					'email',
					'phone',
					'website',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'name'          => Translate::name(),
			'address'       => Translate::address(),
			'email'         => Translate::email(),
			'phone'         => Translate::phone(),
			'show_home'     => Translate::show_home(),
			'status'        => Translate::status(),
			'order'         => Translate::sort(),
			'created_date'  => Translate::create_date(),
			'business_room' => Translate::business_room(),
			'facebook'      => Translate::facebook_link(),
			'company_code'  => Translate::company_code(),
			'google_plus'   => 'Google plus',
			'schedule'      => Translate::company_schedule(),
			'website'       => 'Website',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDepartmentLangs() {
		return $this->hasMany(DepartmentLang::className(), ['department_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'name',
			'address',
			'company_code',
			'schedule',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}
}
