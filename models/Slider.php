<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string  $title
 * @property string  $image
 * @property string  $url
 * @property integer $sort
 * @property integer $status
 */
class Slider extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'slider';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'sort',
					'status',
				],
				'integer',
			],
			[
				[
					'title',
					'image',
					'url',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'     => ' ID',
			'title'  => Translate::title(),
			'image'  => Translate::image(),
			'url'    => 'Url',
			'sort'  => Translate::sort(),
			'status' => Translate::status(),
		];
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'title',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}
}
