<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "department_lang".
 *
 * @property integer    $id
 * @property integer    $department_id
 * @property string     $language
 * @property string     $name
 * @property string     $company_code
 * @property string     $schedule
 *
 * @property Department $department
 */
class DepartmentLang extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'department_lang';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'company_code',
					'address',
					'name',
					'schedule',
				],
				'safe',
			],
			[
				['department_id'],
				'integer',
			],
			[
				['language'],
				'string',
				'max' => 11,
			],
			[
				['name'],
				'string',
				'max' => 255,
			],
			[
				['department_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Department::className(),
				'targetAttribute' => ['department_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'department_id' => 'Department ID',
			'language'      => 'Language',
			'name'          => 'Name',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDepartment() {
		return $this->hasOne(Department::className(), ['id' => 'department_id']);
	}
}
