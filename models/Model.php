<?php
namespace app\models;

use DateTime;
use navatech\language;
use navatech\language\components\MultiLanguageBehavior;
use navatech\language\components\MultiLanguageQuery;
use navatech\language\Translate;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * @method string getTranslateAttribute(string $attribute, string|null $language_code = null)
 * @method boolean hasTranslateAttribute(string $attribute_translation)
 * @method array getTranslateAttributes(string $attribute = null)
 */
class Model extends ActiveRecord {

	public $abc = [
		'no',
		'yes',
	];

	public $img;

	public static function format($current_date, $source_format = ' Y-m-d ', $destination_format = 'd-m-Y') {
		$date = DateTime::createFromFormat($source_format, $current_date);
		if (!$date) {
			return $current_date;
		} else {
			return $date->format($destination_format);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function behaviors($attributes = null) {
		if ($attributes == null) {
			$attributes = [
				'title',
				'content',
			];
		}
		return [
			'ml' => [
				'class'      => MultiLanguageBehavior::className(),
				'attributes' => $attributes,
			],
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public static function find() {
		return new MultiLanguageQuery(get_called_class());
	}

	/**
	 * @param $condition
	 *
	 * @return array|null|ActiveRecord
	 */
	public static function findOneTranslated($condition) {
		return is_array($condition) ? self::find()->where($condition)->translate()->one() : self::find()->where(['id' => $condition])->translate()->one();
	}

	/**
	 * fetch stored image file name with complete path
	 * @return string
	 */
	public function getImageFile() {
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		return isset($this->image) ? $dir . $this->image : null;
	}

	public function getPictureFile($picture = '') {
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		return isset($this->$picture) ? $dir . $this->$picture : null;
	}

	/**
	 * fetch stored image url
	 * @return string
	 */
	public function getImageUrl() {
		// return a default image placeholder if your source image is not found
		Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/uploads/' . $this->tableName() . '/';
		$image                         = !empty($this->image) ? $this->image : Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif';
		return Yii::$app->params['uploadUrl'] . $image;
	}

	public function getPictureUrl($picture = '') {
		Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/uploads/' . $this->tableName() . '/';
		$image                         = !empty($this->$picture) ? $this->$picture : Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif';
		clearstatcache();
		if (is_file(Yii::getAlias("@app/web") . '/uploads/' . $this->tableName() . '/' . $image)) {
			return Yii::$app->params['uploadUrl'] . $image;
		} else {
			return $image;
		}
	}

	/**
	 * Process upload of image
	 *
	 * @return mixed the uploaded image instance
	 */
	public function uploadImage() {
		// get the uploaded file instance. for multiple file uploads
		// the following data will return an array (you may need to use
		// getInstances method)
		$img = UploadedFile::getInstance($this, 'img');
		// if no image was uploaded abort the upload
		if (empty($img)) {
			return false;
		}
		// generate a unique file name
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		if (!is_dir($dir)) {
			@mkdir($dir, 0777, true);
		}
		$ext         = $img->getExtension();
		$this->image = $this->getPrimaryKey() . '_image' . ".{$ext}";
		// the uploaded image instance
		return $img;
	}

	public function uploadPicture($picture = '') {
		// get the uploaded file instance. for multiple file uploads
		// the following data will return an array (you may need to use
		// getInstances method)
		$img = UploadedFile::getInstance($this, 'img');
		// if no image was uploaded abort the upload
		if (empty($img)) {
			return false;
		}
		// generate a unique file name
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		if (!is_dir($dir)) {
			@mkdir($dir, 0777, true);
		}
		$ext            = $img->getExtension();
		$this->$picture = $this->getPrimaryKey() . '_image' . ".{$ext}";
		// the uploaded image instance
		return $img;
	}

	/**
	 * Process deletion of image
	 *
	 * @return boolean the status of deletion
	 */
	public function deleteImage() {
		$file = $this->getImageFile();
		// check if file exists on server
		if (empty($file) || !file_exists($file)) {
			return false;
		}
		// check if uploaded file can be deleted on server
		if (!unlink($file)) {
			return false;
		}
		// if deletion successful, reset your file attributes
		$this->image = null;
		return true;
	}

	/**
	 * Hàm trả về giá trị của status
	 *
	 * @param string $status
	 *
	 * @return array|bool trả về mảng status nếu không truyền gì vào, trả về bool true or false nếu truyền vào giá trị
	 */
	public function getStatus($status = null) {
		if ($status !== null) {
			return $status == 1 ? Translate::yes() : Translate::no();
		} else {
			return array(
				0 => Translate::no(),
				1 => Translate::yes(),
			);
		}
	}

	public function getShowhome($status = null) {
		if ($status !== null) {
			return $status == 1 ? Translate::yes() : Translate::no();
		} else {
			return array(
				0 => Translate::no(),
				1 => Translate::yes(),
			);
		}
	}

	public function getFeature($feature = null) {
		if ($feature !== null) {
			return $feature == 1 ? Translate::active() : Translate::not_active();
		} else {
			return array(
				0 => Translate::not_active(),
				1 => Translate::active(),
			);
		}
	}

	public function getCategoryById($id) {
		$category = Category::find()->where(['id' => $id])->one();
		if ($category != null) {
			return $category->name;
		} else {
			return Translate::no();
		}
	}

	public function isJson($string) {
		if (is_array($string) || empty($string)) {
			return false;
		}
		json_decode($string);
		return json_last_error() == JSON_ERROR_NONE;
	}

	public static function getLanguages(array $attributes = []) {
		if ($attributes === null) {
			$attributes = ['status' => 1];
		}
		$arrays = language\models\Language::find()->asArray()->where($attributes)->all();
		return $result = ArrayHelper::map($arrays, 'id', 'name');
	}

	//	public function requireModel($attribute) {
	//		if ($this->$attribute == null) {
	//			$this->addError($attribute, 'Không được để trống' . $attribute);
	//		}
	//	}
	public static function removeSign($convert, $char = "-") {
		$vietnameseChar  = "à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|ì|í|ị|ỉ|ĩ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ỳ|ý|ỵ|ỷ|ỹ|đ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|Ì|Í|Ị|Ỉ|Ĩ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|Ỳ|Ý|Ỵ|Ỷ|Ỹ|Đ";
		$unicodeChar     = "a|a|a|a|a|a|a|a|a|a|a|a|a|a|a|a|a|e|e|e|e|e|e|e|e|e|e|e|i|i|i|i|i|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|o|u|u|u|u|u|u|u|u|u|u|u|y|y|y|y|y|d|A|A|A|A|A|A|A|A|A|A|A|A|A|A|A|A|A|E|E|E|E|E|E|E|E|E|E|E|I|I|I|I|I|O|O|O|O|O|O|O|O|O|O|O|O|O|O|O|O|O|U|U|U|U|U|U|U|U|U|U|U|Y|Y|Y|Y|Y|D";
		$vietnameseChars = explode("|", $vietnameseChar);
		$unicodeChars    = explode("|", $unicodeChar);
		$str             = strtolower(str_replace($vietnameseChars, $unicodeChars, $convert));
		$str             = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
		$str             = preg_replace("/[\/_|+ -]+/", $char, $str);
		return $str;
	}

	public static function getVideoEmbed($url) {
		if(preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match) && isset($match[1])) {
			$id = $match[1];
			return "<iframe width='560' height='315' src='https://www.youtube.com/embed/" . $id . "' frameborder='0' allowfullscreen></iframe>";
		}
	}
}