<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "promotion_lang".
 *
 * @property integer $id
 * @property integer $promotion_id
 * @property string  $language
 * @property string  $title
 * @property string  $description
 * @property string  $content
 */
class PromotionLang extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'promotion_lang';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'promotion_id',
					'language',
					'title',
					'description',
					'content',
				],
				'required',
			],
			[
				['promotion_id'],
				'integer',
			],
			[
				[
					'description',
					'content',
				],
				'string',
			],
			[
				['language'],
				'string',
				'max' => 11,
			],
			[
				['title'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'promotion_id' => 'Promotion ID',
			'language'     => 'Language',
			'title'        => 'Title',
			'description'  => 'Description',
			'content'      => 'Content',
		];
	}

	public function getPromotion() {
		return $this->hasOne(Promotion::className(), ['id' => 'promotion_id']);
	}
}
