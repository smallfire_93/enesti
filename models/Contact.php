<?php
namespace app\models;

use navatech\language\Translate;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property string  $fullname
 * @property string  $email
 * @property integer $phone
 * @property string  $title
 * @property string  $address
 * @property string  $created_date
 * @property integer $status
 */
class Contact extends ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'contact';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'fullname',
					'email',
					'phone',
					'title',
					'address',
				],
				'required',
			],
			[
				[
					'status',
				],
				'integer',
			],
			[
				['created_date'],
				'safe',
			],
			[
				[
					'phone',
					'fullname',
					'email',
					'title',
					'address',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'Contact ID',
			'fullname'     => Translate::full_name(),
			'email'        => 'Email',
			'phone'        => Translate::phone(),
			'title'        => Translate::title(),
			'address'      => Translate::address(),
			'created_date' => 'Created Date',
			'status'       => Translate::status(),
		];
	}

	public function getContactStatus($status = null) {
		if ($status !== null) {
			return $status == 1 ? Translate::read() : Translate::unread();
		} else {
			return array(
				0 => Translate::unread(),
				1 => Translate::read(),
			);
		}
	}
}
