<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider_lang".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property string $language
 * @property string $title
 */
class SliderLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id'], 'integer'],
            [['language', 'title'], 'required'],
            [['language', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slider_id' => 'Slider ID',
            'language' => 'Language',
            'title' => 'Title',
        ];
    }
}
