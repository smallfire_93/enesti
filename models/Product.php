<?php
namespace app\models;

use navatech\language\Translate;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property integer       $id
 * @property integer       $category_id
 * @property integer       $user_id
 * @property string        $code
 * @property string        $origin
 * @property string        $image
 * @property integer       $view
 * @property string        $created_date
 * @property integer       $feature
 * @property integer       $status
 * @property double        $price
 * @property integer       $in_stock
 * @property               $excel
 * @property string        $instructions
 * @property  string       $capability
 * @property   double      $sale_off
 * @property  string       $title
 * @property  string       $unit
 *
 * @property ProductLang[] $productLangs
 */
class Product extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'product';
	}

	public $img;

	public $picture;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'category_id',
					//					'code',
					'price',
				],
				'required',
			],
			[
				[
					'sale_off',
				],
				'number',
			],
			[
				[
					'category_id',
					'feature',
					'status',
					'in_stock',
				],
				'integer',
			],
			[
				[
					'created_date',
					'title',
					'content',
					'sale_off',
					'code',
					'price',
					'picture',
				],
				'safe',
			],
			[
				['img'],
				'file',
				'extensions' => 'jpg, gif, png',
			],
			[
				[
					'code',
					'origin',
					'image',
					'instructions',
					'capability',
					'unit',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'category_id'  => Translate::category(),
			'user_id'      => Translate::user(),
			'code'         => Translate::code(),
			'origin'       => Translate::origin(),
			'image'        => Translate::image(),
			'view'         => Translate::view(),
			'created_date' => Translate::create_date(),
			'feature'      => Translate::feature(),
			'status'       => Translate::status(),
			'price'        => Translate::price(),
			'in_stock'     => Translate::in_stock(),
			'picture'      => Translate::image(),
			'title'        => Translate::title(),
			'sale_off'     => Translate::promotion(),
			'excel'        => Translate::import_excel(),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductLangs() {
		return $this->hasMany(ProductLang::className(), ['product_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'title',
			'content',
		];
		return parent::behaviors($attributes);
	}

	public function getStock($stock = null) {
		if ($stock !== null) {
			return $stock !== 0 ? $this->in_stock : Translate::out_of_stock();
		} else {
			return Translate::out_of_stock();
		}
	}

	public function downloadImages($url, $product_code) {
		$headers = get_headers($url);
		$status  = substr($headers[0], 9, 3);
		if (ini_get("allow_url_fopen") && $status == "200") {
			$ext = substr($headers[10], 20);
			$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
			if (!is_dir($dir)) {
				@mkdir($dir, 0777, true);
			}
			$path = $dir . $product_code . '.' . $ext;
			if ($ext == 'jpg' || $ext == 'png' || 'gif') {
				file_put_contents($path, file_get_contents($url));
				return $product_code . '.' . $ext;
			} else {
				return $url;
			}
		} else {
			return $url;
		}
	}

	public static function getProductById($id) {
		return $title = Product::findOne($id);
	}
}
