<?php
namespace app\models;

use navatech\language\Translate;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer        $id
 * @property integer        $parent_id
 * @property integer        $type
 * @property integer        $order
 * @property integer        $status
 * @property string         $image
 * @property string         $name
 *
 * @property CategoryLang[] $categoryLangs
 */
class Category extends Model {

	public $img;

	public $description;

	const TYPE = [
		1 => 'Danh mục Sản phẩm',
		2 => 'Danh mục tin tức',
	];

	public function getCategoryType() {
		return self::TYPE[$this->type];
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'category';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'parent_id',
				],
				'required',
			],
			[
				[
					'name',
					'description',
				],
				'safe',
			],
			[
				[
					'parent_id',
					'type',
					'order',
					'status',
				],
				'integer',
			],
			[
				['img'],
				'file',
				'extensions' => 'jpg, gif, png',
			],
		];
	}

	//TODO không client validate đc
	public function requireModel($attribute) {
		//		/*$name = explode("\\",self::className());
		//		$attribute       = $attribute . '_v.i';
		$defaultLanguage = Yii::$app->language;
		if (isset($_POST['Category'][$attribute . '_' . $defaultLanguage]) && $_POST['Category'][$attribute . '_' . $defaultLanguage] == null) {
			$this->addError($attribute . '_' . $defaultLanguage, 'This field cannot be empty');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'parent_id'   => Translate::parent_category(),
			'name'        => Translate::name(),
			'description' => Translate::description(),
			'type'        => Translate::type(),
			'order'       => Translate::sort(),
			'status'      => Translate::status(),
			'image'       => Translate::image(),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategoryLangs() {
		return $this->hasMany(CategoryLang::className(), ['category_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'name',
			'description',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}

	public function getParentCategory($type) {
		$parent = self::getCategoryText($type);
		$none   = [0 => Translate::no()];
		$result = ArrayHelper::merge($none, $parent);
		return $result;
	}

	public static function getCategoryText($type) {
		$parent = self::find()->where([
			'type'   => $type,
			'status' => 1,
		])->all();
		$result = ArrayHelper::map($parent, 'id', 'name');
		return $result;
	}

	public static function getCategoryOrder() {
		$cats     = Category::find()->where([
			'parent_id' => 0,
			'type'      => 1,
		])->all();
		$response = [];
		foreach ($cats as $cat) {
			$response[$cat->id] = $cat->name;
			$children           = $cat->find()->where([
				'type'      => 1,
				'parent_id' => $cat->id,
				'status'    => 1,
			])->all();
			if (count($children) > 0) {
				$response = self::getChildrenCat($children, $response, 1);
			}
		}
		return $response;
	}

	public static function getChildrenCat($models, $response, $level) {
		$prefix = '';
		for ($i = 0; $i < $level; $i ++) {
			$prefix .= "-";
		}
		foreach ($models as $model) {
			$response[$model->id] = $prefix . $model->name;
			$children            = $model->find()->where([
				'parent_id' => $model->id,
				'status'    => 1,
				'type'      => 1,
			])->all();
			if (count($children) > 0) {
				$response = self::getChildrenCat($children, $response, $level +1);
			}
		}
		return $response;
	}
	public static function getSubcategory($id){
		$model = Category::find()->where([
			'parent_id' => $id,
			'status'    => 1,
			'type'      => 1,
		])->all();
		return $model;
	}
}
