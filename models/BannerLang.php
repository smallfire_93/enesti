<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "banner_lang".
 *
 * @property integer $id
 * @property integer $banner_id
 * @property string  $language
 * @property string  $title
 *
 * @property Banner  $banner
 */
class BannerLang extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'banner_lang';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'banner_id',
					'language',
					'title',
				],
				'required',
			],
			[
				['banner_id'],
				'integer',
			],
			[
				['language'],
				'string',
				'max' => 11,
			],
			[
				['title'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'        => 'ID',
			'banner_id' => 'Banner ID',
			'language'  => 'Language',
			'title'     => 'Title',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBanner() {
		return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
	}
}
