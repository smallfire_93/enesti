<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "popup".
 *
 * @property integer $id
 * @property string  $background
 * @property integer $status
 * @property string  $url
 */
class Popup extends Model {

	public $img;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'popup';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'title',
					'content',
				],
				'required',
			],
			[
				['status'],
				'integer',
			],
			[
				[
					'background',
					'url',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'background' => Translate::background(),
			'status'     => Translate::status(),
			'url'        => 'Url',
			'title'      => Translate::title(),
			'content'    => Translate::content(),
		];
	}

	public function getPopupLangs() {
		return $this->hasMany(PopupLang::className(), ['popup_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'title',
			'content',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}
}
