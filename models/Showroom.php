<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "showroom".
 *
 * @property integer $showroom_id
 * @property string  $name
 * @property string  $address
 * @property string  $email
 * @property string  $phone
 * @property string  $fax
 * @property string  $map
 * @property integer $status
 * @property integer $order
 * @property string  $created_date
 */
class Showroom extends Model {

	public $img;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'showroom';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'address',
					'email',
					'phone',
				],
				'required',
			],
			[
				['address'],
				'string',
			],
			[
				[
					'status',
					'order',
				],
				'integer',
			],
			[
				['created_date'],
				'safe',
			],
			[
				[
					'name',
					'email',
					'phone',
					'fax',
					'map',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'showroom_id'  => 'Showroom ID',
			'name'         => Translate::name(),
			'address'      => Translate::address(),
			'email'        => 'Email',
			'phone'        => Translate::phone(),
			'fax'          => 'Fax',
			'map'          => Translate::map(),
			'status'       => Translate::status(),
			'order'        => Translate::sort(),
			'created_date' => 'Created Date',
		];
	}

	public function getShowroomLang() {
		return $this->hasMany(ShowroomLang::className(), ['showroom_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'name',
			'address',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}
}
