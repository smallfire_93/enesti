<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $permissions
 * @property integer $is_backend_login
 */
class Role extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'role';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'permissions',
				],
				'required',
			],
			[
				['permissions'],
				'string',
			],
			[
				['is_backend_login'],
				'integer',
			],
			[
				['name'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'               => 'ID',
			'name'             => 'Name',
			'permissions'      => 'Permissions',
			'is_backend_login' => 'Is Backend Login',
		];
	}

	public function getRole($role = null) {
		if ($role != null) {
			if ($role == 1) {
				return Translate::admin();
			} elseif ($role == 2) {
				return Translate::distributor();
			} else {
				return Translate::agent();
			}
		} else {
			return array(
				1 => Translate::admin(),
				2 => Translate::distributor(),
				3 => Translate::agent(),
			);
		}
	}
}
