<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string  $image
 * @property integer $view
 * @property string  $created_date
 * @property integer $feature
 * @property integer $status
 * @property string  $title
 */
class Post extends Model {

	public $img;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'post';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['category_id'],
				'required',
			],
			[
				[
					'category_id',
					'feature',
					'status',
				],
				'integer',
			],
			[
				[
					'title',
					'description',
					'content',
					'created_date',
				],
				'safe',
			],
			[
				['img'],
				'file',
				'extensions' => 'jpg, gif, png',
			],
			[
				['image'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'NO',
			'category_id'  => Translate::category(),
			'image'        => Translate::image(),
			'created_date' => Translate::create_date(),
			'feature'      => Translate::feature(),
			'status'       => Translate::status(),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPostLangs() {
		return $this->hasMany(PostLang::className(), ['post_id' => 'id']);
	}

	/**
	 * {@inheritDoc}
	 */
	public function behaviors($attributes = null) {
		$attributes = [
			'title',
			'description',
			'content',
		];
		return parent::behaviors($attributes);
	}

	public static function getPostByCategory($id, $limit) {
		$posts = Post::find()->where([
			'status'      => 1,
			'category_id' => $id,
		])->limit($limit)->orderBy(['created_date' => SORT_ASC])->all();
		return $posts;
	}
}
