<?php
namespace app\models;

use navatech\language\Translate;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "recruitment".
 *
 * @property integer           $id
 * @property integer           $user_id
 * @property string            $image
 * @property integer           $view
 * @property string            $created_date
 * @property integer           $feature
 * @property integer           $status
 *
 * @property RecruitmentLang[] $recruitmentLangs
 */
class Recruitment extends Model {

	public $img;
	public $pagelink;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'recruitment';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'feature',
					'status',
				],
				'integer',
			],
			[
				[
					'title',
					'content',
					'created_date',
				],
				'safe',
			],
			[
				['img'],
				'file',
				'extensions' => 'jpg, gif, png',
			],
			[
				['image'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'           => 'ID',
			'user_id'      => 'User ID',
			'image'        => Translate::image(),
			'created_date' => Translate::create_date(),
			'feature'      => Translate::feature(),
			'status'       => Translate::status(),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getRecruitmentLangs() {
		return $this->hasMany(RecruitmentLang::className(), ['recruitment_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = null;
		$behaviors = parent::behaviors($attributes);
		return $behaviors;
	}
}
