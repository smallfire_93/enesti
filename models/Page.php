<?php
namespace app\models;

use navatech\language\Translate;
use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer    $id
 * @property integer    $order
 * @property integer    $status
 * @property integer    $intro
 * @property integer    $content
 * @property integer    $title
 *
 * @property PageLang[] $pageLangs
 */
class Page extends Model {

	public $pagelink;
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'page';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'content',
					'intro',
				],
				'required',
			],
			[
				[
					'title',
					'intro',
					'content',
				],
				'safe',
			],
			[
				[
					'order',
					'status',
				],
				'integer',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'      => 'ID',
			'order'   => Translate::sort(),
			'status'  => Translate::status(),
			'title'   => Translate::title(),
			'intro'   => Translate::intro(),
			'content' => Translate::content(),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPageLangs() {
		return $this->hasMany(PageLang::className(), ['page_id' => 'id']);
	}

	public function behaviors($attributes = null) {
		$attributes = [
			'title',
			'content',
			'intro',
		];
		$behaviors  = parent::behaviors($attributes);
		return $behaviors;
	}
}
