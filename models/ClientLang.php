<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_lang".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $language
 * @property string $name
 */
class ClientLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'language', 'name'], 'required'],
            [['client_id'], 'integer'],
            [['language'], 'string', 'max' => 11],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }
}
