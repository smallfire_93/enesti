<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "popup_lang".
 *
 * @property integer $id
 * @property integer $popup_id
 * @property string  $language
 * @property string  $title
 * @property string  $content
 */
class PopupLang extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'popup_lang';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'popup_id',
					'language',
					'title',
					'content',
				],
				'required',
			],
			[
				['popup_id'],
				'integer',
			],
			[
				['content'],
				'string',
			],
			[
				['language'],
				'string',
				'max' => 11,
			],
			[
				['title'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'       => 'ID',
			'popup_id' => 'Popup ID',
			'language' => 'Language',
			'title'    => 'Title',
			'content'  => 'Content',
		];
	}

	public function getPopup() {
		return $this->hasOne(Popup::className(), ['id' => 'popup_id']);
	}
}
