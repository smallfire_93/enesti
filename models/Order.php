<?php
namespace app\models;

use navatech\language\Translate;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string  $distributor_id
 * @property string  $phone_number
 * @property string  $shipping_address
 * @property string  $created_date
 * @property integer $status
 */
class Order extends ActiveRecord {

	public $quantity;

	public $total_price;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'order';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'distributor_id',
				],
				'required',
			],
			[
				[
					'status',
				],
				'integer',
			],
			[
				[
					'created_date',
					'status',
					'distributor_id',
					'shipping_address',
					'phone_number',
				],
				'safe',
			],
			[
				['phone_number'],
				'string',
				'max' => 16,
			],
			[
				['shipping_address'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'               => 'Order ID',
			'phone_number'     => Translate::phone(),
			'shipping_address' => Translate::shipping_address(),
			'created_date'     => Translate::created_date(),
			'total_price'      => Translate::total_amount(),
			'distributor_id'   => Translate::distributor(),
			'quantity'         => Translate::quantity(),
			'status'           => Translate::status(),
		];
	}

	public function getOrderItem() {
		return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
	}

	public function getOrderItemById($id) {
		$items = OrderItem::find()->where(['order_id' => $id])->all();
		return $items;
	}

	public function getTotalAmount($id) {
		/**@var OrderItem[] $items */
		$items = OrderItem::find()->where(['order_id' => $id])->all();
		$order = Order::findOne($id);
		$total = 0;
		$gross = 0;
		if ($items != null) {
			foreach ($items as $item) {
				$product = Product::findOne($item->product_id);
				$gross += (($item->price) - ($product->sale_off) * ($item->price)) * ($item->quantity);
			}
			$distributor = Distributor::findOne($order->distributor_id);
			$total       = $gross - $gross * ($distributor->discount);
		}
		return round($total);
	}

	public function getTotalQuantity($id) {
		/**@var OrderItem[] $items */
		$items = OrderItem::find()->where(['order_id' => $id])->all();
		$total = 0;
		if ($items != null) {
			foreach ($items as $item) {
				$total += ($item->quantity);
			}
		}
		return $total;
	}

	public function getOrderStatus($status = null) {
		if ($status !== null) {
			if ($status == 1) {
				return Translate::paid();
			} else {
				return Translate::unpaid();
			}
		} else {
			return array(
				0 => Translate::unpaid(),
				1 => Translate::paid(),
			);
		}
	}

	public function getFeature($feature = null) {
		if ($feature !== null) {
			return $feature == 1 ? Translate::active() : Translate::not_active();
		} else {
			return array(
				0 => Translate::not_active(),
				1 => Translate::active(),
			);
		}
	}
}
