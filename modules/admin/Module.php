<?php
namespace app\modules\admin;

use app\modules\admin\controllers\SecurityController;
use Yii;
use yii\web\User;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\admin\controllers';

	public $basePath            = '@app/web/themes/backend';

	public function init() {
		parent::init();
		if (Yii::$app->user->isGuest) {
			Yii::$app->errorHandler->errorAction = 'admin/user/login';
		}
		Yii::configure($this, require(__DIR__ . '/config/web.php'));
		$baseUrl = str_replace('/web', '', (new \yii\web\Request)->getBaseUrl());
		Yii::$app->setComponents([
			'user'    => [
				'class'           => User::className(),
				'identityClass'   => 'app\models\User',
				'enableAutoLogin' => true,
				'loginUrl'        => ['admin/user/login'],
			],
			'request' => [
				'class'               => '\yii\web\Request',
				'baseUrl'             => $baseUrl,
				// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
				'cookieValidationKey' => 'aXbVOydPsTbPSA76HhPHyQJc7QgLqUU5',
			],
			'view'    => [
				'class' => 'yii\web\View',
				'theme' => [
					'basePath' => '@app/web/themes/backend',
					'baseUrl'  => '@web/themes/backend',
				],
			],
			'setting' => [
				'class' => 'navatech\setting\Setting',
			],
		]);
		//		Yii::$app->setComponents([
		//			'errorHandler' => [
		//				'class'       => 'yii\web\ErrorHandler',
		//				'errorAction' => 'admin/user/login',
		//			],
		//		]);
		$this->setComponents([
			'mailers' => [
				'class'            => 'yii\swiftmailer\Mailer',
				'useFileTransport' => false,
				'viewPath'         => '@app/web/themes/backend/views/contact',
				'htmlLayout'       => 'mail',
				//set this property to false to send mails to real email addresses
				//comment the following array to send mail using php's mail function
				//],
				'transport'        => [
					'class'      => 'Swift_SmtpTransport',
					'host'       => Yii::$app->setting->get('email_host'),
					'username'   => Yii::$app->setting->get('email_field'),
					'password'   => Yii::$app->setting->get('password_mail'),
					'port'       => Yii::$app->setting->get('email_port'),
					'encryption' => Yii::$app->setting->get('email_secure'),
				],
			],
		]);
		$this->setModules([
			'language' => [
				'class'  => '\navatech\language\Module',
				'suffix' => 'lang',
			],
			'user'     => [
				'class'    => 'dektrium\user\Module',
				'modelMap' => [
					'User'      => 'app\models\Users',
					'LoginForm' => 'navatech\role\models\LoginForm',
				],
			],
			'role'     => [
				'class'       => 'navatech\role\Module',
				'controllers' => [
					'app\modules\admin\controllers',
					'navatech\role\controllers',
				],
			],
			'setting'  => [
				'class'               => 'navatech\setting\Module',
				'controllerNamespace' => 'navatech\setting\controllers',
			],
			'debug'    => [
				'class' => 'yii\debug\Module',
			],
		]);
		$this->controllerMap = [
			'user' => SecurityController::className(),
		];
	}
}
