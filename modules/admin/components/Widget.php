<?php

namespace app\modules\admin\components;
class Widget extends \yii\bootstrap\Widget {

	public function init() {
		parent::init();
	}

	public function getViewPath() {
		$name = explode('\\', self::className());
		return \Yii::$app->view->theme->basePath . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . end($name);
	}
}