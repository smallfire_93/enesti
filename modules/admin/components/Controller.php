<?php
/**
 * Created by Navatech.
 * @project enesti-com-vn
 * @author  Phuong
 * @email   phuong17889[at]gmail.com
 * @date    12/03/2016
 * @time    2:07 SA
 */
namespace app\modules\admin\components;

use navatech\language\components\MultiLanguageController;
use Yii;
use yii\helpers\Url;

class Controller extends MultiLanguageController {

	public function init() {
		parent::init();
	}

	public function beforeAction($action) {
		if (Yii::$app->user->isGuest) {
			$this->redirect(Url::to('/admin/user/login'));
		}
		return parent::beforeAction($action);
	}
}