<?php
namespace app\modules\admin\widgets;

use app\modules\admin\components\Widget;
use Yii;

class SidebarWidget extends Widget {

	public function run() {
		return $this->render('sidebarWidget');
	}

	/**
	 * @param array|string      $controller
	 * @param null|array|string $action
	 * @param null|array|string $params
	 *
	 *
	 * @return string
	 */
	public static function isActive($controller, $action = null, $params = null) {
		$string = '';
		if (!is_array($controller)) {
			$string = Yii::$app->controller->uniqueId;
			$controller = [$controller];
		}
		if ($action !== null && !is_array($action)) {
			$action = [$action];
		}
		if ($params !== null && !is_array($params)) {
			$params = [$params];
		}
		if (in_array(Yii::$app->controller->id, $controller, true)||in_array(Yii::$app->controller->uniqueId, $controller, true)) {
			if ($action === null || ($action != null && in_array(Yii::$app->controller->action->id, $action, true))) {
				if ($params === null || in_array($params, array_chunk(Yii::$app->controller->actionParams, 1, true), true)) {
					$string = 'active';
				}
			}
		}
		return $string;
	}
}