<?php
namespace app\modules\admin\widgets;

use app\modules\admin\components\Widget;

class NavbarWidget extends Widget {

	public function run() {
		return $this->render('navbarWidget');
	}
}