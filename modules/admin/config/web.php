<?php
$config = [
	'components'       => [
		'errorHandler' => [
			'class'       => 'yii\web\ErrorHandler',
			'errorAction' => 'admin/user/login',
		],
	],
	'as beforeRequest' => [
		'class'        => 'yii\filters\AccessControl',
		'rules'        => [
			[
				'allow'   => true,
				'actions' => ['login'],
			],
			[
				'allow' => true,
				'roles' => ['@'],
			],
		],
		'denyCallback' => function() {
			return Yii::$app->response->redirect([
				'/admin/user/login',
			]);
		},
	],
];
return $config;
