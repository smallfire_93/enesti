<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use navatech\language\Translate;
use navatech\role\filters\RoleFilter;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class DefaultController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'login',
							'error',
						],
						'allow'   => true,
					],
					[
						'actions' => [
							'logout',
							'index',
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
			'role'   => [
				'class'   => RoleFilter::className(),
				'name'    => Translate::home(),
				'actions' => [
					'index' => 'Trang quản trị',
				],
			],
		];
	}

	public function actionIndex() {
		return $this->render('index');
	}

	//	public function actionLogin() {
	//		if (!\Yii::$app->user->isGuest) {
	//			return $this->goHome();
	//		}
	//		$model = new LoginForm();
	//		if ($model->load(Yii::$app->request->post()) && $model->login()) {
	//			return $this->goBack();
	//		} else {
	//			return $this->render('login', [
	//				'model' => $model,
	//			]);
	//		}
	//	}
	//	public function actionLogout() {
	//		Yii::$app->user->logout();
	//		return $this->redirect('/user/login');
	//	}
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
}
