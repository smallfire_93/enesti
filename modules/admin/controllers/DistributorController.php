<?php
namespace app\modules\admin\controllers;

use app\models\Distributor;
use app\models\search\DistributorSearch;
use app\modules\admin\components\Controller;
use navatech\language\Translate;
use navatech\role\filters\RoleFilter;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DistributorController implements the CRUD actions for Distributor model.
 */
class DistributorController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'role'  => [
				'class'   => RoleFilter::className(),
				'name'    => Translate::manage_x(Translate::distributor()),
				'actions' => [
					'index'  => 'Danh sách',
					'view'   => 'Chi tiết',
					'create' => 'Thêm mới',
					'update' => 'Cập nhật',
					'delete' => 'Xóa',
				],
			],
		];
	}

	/**
	 * Lists all Distributor models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new DistributorSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Distributor model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Distributor model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model               = new Distributor();
		$model->scenario     = 'check';
		$model->role_id      = 2;
		$model->confirmed_at = time();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			// if no image was uploaded abort the upload
			$model->updateAttributes(['discount' => ($model->discount) / 100]);
			$img = $model->uploadPicture('logo');
			if ($model->save()) {
				if ($img !== false) {
					$path = $model->getPictureFile('logo');
					$img->saveAs($path);
				}
				return $this->redirect(['index']);
			}
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Distributor model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		//		$model->scenario = 'update';
		if ($model->load(Yii::$app->request->post())) {
			//			if ($_POST['Distributor']['password_hash'] != null) {
			//				$model->scenario      = 'updatePassword';
			//				$model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
			//			}
			$model->updateAttributes(['discount' => ($model->discount) / 100]);
			if ($model->save()) {
				$model->img = UploadedFile::getInstance($model, 'img');
				if ($model->img !== null) {
					$path = Yii::getAlias('@app/web') . '/uploads/' . $model->tableName() . '/';
					if (!is_dir($path)) {
						BaseFileHelper::createDirectory($path, 0777);
					}
					$img = $path . '/' . $model->getPrimaryKey() . '_image' . ".{$model->img->extension}";
					if ($model->img->saveAs($img)) {
						$model->updateAttributes(['logo' => $model->getPrimaryKey() . '_image' . ".{$model->img->extension}"]);
					}
				}
				return $this->redirect([
					'index',
				]);
			}
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Distributor model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Distributor model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Distributor the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Distributor::findOneTranslated($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
