<?php
namespace app\modules\admin\controllers;

use app\models\Distributor;
use app\models\Order;
use app\models\OrderItem;
use app\models\Product;
use app\models\search\OrderItemSearch;
use app\models\search\OrderSearch;
use app\modules\admin\components\Controller;
use navatech\language\Translate;
use navatech\role\filters\RoleFilter;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			//			'verbs' => [
			//				'class'   => VerbFilter::className(),
			//				'actions' => [
			//					'delete' => ['POST'],
			//				],
			//			],
			'role' => [
				'class'   => RoleFilter::className(),
				'name'    => Translate::manage_x(Translate::order()),
				'actions' => [
					'index'  => 'Danh sách',
					'view'   => 'Chi tiết',
					'create' => 'Thêm mới',
					'report' => 'Báo cáo',
					'update' => 'Cập nhật',
					'delete' => 'Xóa',
					'check'  => 'Duyệt đơn hàng',
				],
			],
		];
	}

	/**
	 * Lists all Order models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new OrderSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$total_order = $dataProvider->getTotalCount();
		$order_cancel = 0;
		$order_paid = 0;
		$order_unpaid = 0;
		$total_revenue = 0;
		foreach($dataProvider->getModels() as $model){
			if($model->status == 0 ){
				$order_unpaid += 1 ;
			}else if($model->status == 1 ){
				$order_paid += 1;
				$total_revenue += $model->getTotalAmount($model->id);
			}
		}
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'total_order' => $total_order,
			'order_cancel' => $order_cancel,
			'order_paid' => $order_paid,
			'order_unpaid' => $order_unpaid,
			'total_revenue' => $total_revenue,

		]);
	}

	/**
	 * Displays a single Order model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		$searchModel  = new OrderItemSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
		$order        = $this->findModel($id);
		$orderItem    = OrderItem::find()->where(['order_id' => $id])->all();
		$model        = $this->findModel($id);
		$distributor  = Distributor::findOne($model->distributor_id);
		return $this->render('view', [
			'model'        => $model,
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'order'        => $order,
			'orderItem'    => $orderItem,
			'distributor'  => $distributor,
		]);
	}

	/**
	 * Creates a new Order model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Order();
		if (Yii::$app->request->post()) {
			echo '<pre>';
			print_r(Yii::$app->request->post());
			die;
		}
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Order model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$orderItem = OrderItem::find()->where(['order_id' => $id])->all();
		$model     = $this->findModel($id);
		if (isset($_POST['OrderItem'])) {
			$item = new OrderItem();
			$item->deleteAll(['order_id' => $id]);
			foreach ($_POST['OrderItem'] as $attribute) {
				$item = new OrderItem();
				$item->setAttributes($attribute);
				$product = Product::findOne($item['product_id']);
				if ($product) {
					$item->price = $product->price;
				}
				if (($item->quantity) <= 0) {
					$item->quantity = 0;
					$item->price    = 0;
				}
				$item->order_id = $id;
				$item->save();
			}
			return $this->redirect([
				'update',
				'id' => $model->id,
			]);
		}
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model'     => $model,
				'orderItem' => $orderItem,
			]);
		}
	}

	/**
	 * Deletes an existing Order model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}


	/**
	 * Finds the Order model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Order the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Order::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionCheck($id) {
		if ($id !== null) {
			$model = $this->findModel($id);
			$model->updateAttributes(['status' => 1]);
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		return $this->redirect(['index']);
	}
}
