<?php
namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\CategoryLang;
use app\models\Product;
use app\models\search\ProductSearch;
use app\models\UploadExcel;
use app\modules\admin\components\Controller;
use navatech\language\Translate;
use navatech\role\filters\RoleFilter;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['post'],
				],
			],
			'role'  => [
				'class'   => RoleFilter::className(),
				'name'    => Translate::manage_x(Translate::product()),
				'actions' => [
					'index'  => 'Danh sách',
					'view'   => 'Chi tiết',
					'create' => 'Thêm mới',
					'update' => 'Cập nhật',
					'delete' => 'Xóa',
				],
			],
		];
	}

	/**
	 * Lists all Product models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new ProductSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$file         = new UploadExcel();
		if (isset($_POST['UploadExcel']['excel'])) {
			$file->excel = UploadedFile::getInstance($file, 'excel');
			$input       = $file->uploadExcel();
			if ($input !== null) {
				Yii::$app->session->setFlash('success', 'Dữ liệu đã được lưu thành công');
				try {
					$inputFileType  = \PHPExcel_IOFactory::identify($input);
					$objectReader   = \PHPExcel_IOFactory::createReader($inputFileType);
					$objectPHPExcel = $objectReader->load($input);
				} catch (Exception $e) {
					die('Error');
				}$sheet         = $objectPHPExcel->getSheet(0);
				
				$highestRow    = $sheet->getHighestRow();
				$highestColumn = $sheet->getHighestColumn();
				for ($row = 1; $row <= $highestRow; $row ++) {
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, false, false);
					if ($row == 1) {
						continue;
					}
					if ($rowData[0][0] !== null) {
						$category     = new Category();
						$name         = CategoryLang::find()->where(['name' => $rowData[0][0]])->one();
						$product_code = Product::find()->where(['code' => $rowData[0][1]])->one();
						if ($product_code !== null) {
							$product = $this->findModel($product_code->id);
						} else {
							$product = new Product();
						}
						if ($name !== null) {
							$product->category_id = $name->category_id;
						} else {
							$attribute            = 'name_' . Yii::$app->language;
							$category->$attribute = $rowData[0][0];
							$category->status     = 1;
							$category->type       = 1;
							$category->parent_id  = 0;
							$category->order      = 100;
							$category->save();
							if ($category->save()) {
								$product->category_id = $category->getPrimaryKey();
							}
						}
						$product->code          = $rowData[0][1] . '';
						$title_lang             = 'title_' . Yii::$app->language;
						$content_lang           = 'content_' . Yii::$app->language;
						$product->$title_lang   = $rowData[0][2];
						$product->$content_lang = '';
						if ($rowData[0][7] != null) {
							$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
							$charactersLength = strlen($characters);
							$randomString     = '';
							for ($i = 0; $i < 5; $i ++) {
								$randomString .= $characters[rand(0, $charactersLength - 1)];
							}
							if ($product->code == null) {
								$name = $randomString;
							} else {
								$name = $product->code;
							}
							$product->image = $product->downloadImages($rowData[0][7], $name);
						}
						$product->status       = 1;
						$product->feature      = 0;
						$product->price        = $rowData[0][3];
						$product->sale_off     = 0;
						$product->capability   = $rowData[0][5];
						$product->origin       = $rowData[0][6];
						$product->instructions = '';
						$product->unit         = $rowData[0][4];
						$product->save();
					}
				}
			} else {
				Yii::$app->session->setFlash('failed', 'Có lỗi trong quá trình lưu dữ liệu');
			}
		}
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'file'         => $file,
		]);
	}

	/**
	 * Displays a single Product model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Product();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$img = $model->uploadPicture('image');
			if ($model->save()) {
				if ($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
				return $this->redirect(['index']);
			}
			return $this->redirect([
				'index',
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model    = $this->findModel($id);
		$oldImage = $model->image;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$img = $model->uploadImage();
			if ($img === false) {
				$model->image = $oldImage;
			}
			if ($model->save()) {
				if ($img !== false) {
					$path = $model->getImageFile();
					$img->saveAs($path);
				}
				return $this->redirect([
					'index',
				]);
			}
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Product::findOneTranslated($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
