<?php
/**
 * @project PhpStorm
 * @created Navatech.
 * @author  LocPX
 * @email loc.xuanphama1t1@gmail.com
 * @date    19/05/2016
 * @time    10:13 SA
 */
namespace app\modules\admin\controllers;

use dektrium\user\models\LoginForm;
use Yii;

class SecurityController extends \dektrium\user\controllers\SecurityController {

	public function actionLogout() {
		$event = $this->getUserEvent(Yii::$app->user->identity);
		$this->trigger(self::EVENT_BEFORE_LOGOUT, $event);
		Yii::$app->getUser()->logout();
		$this->trigger(self::EVENT_AFTER_LOGOUT, $event);
		return $this->redirect(['/admin/user/login']);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		/** @var LoginForm $model */
		$model = Yii::createObject(LoginForm::className());
		$event = $this->getFormEvent($model);
		$this->performAjaxValidation($model);
		$this->trigger(self::EVENT_BEFORE_LOGIN, $event);
		if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
			$this->trigger(self::EVENT_AFTER_LOGIN, $event);
			return $this->goBack(['/admin']);
		}
		return $this->render('login', [
			'model'  => $model,
			'module' => $this->module,
		]);
	}
}