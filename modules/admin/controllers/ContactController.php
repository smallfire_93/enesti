<?php
namespace app\modules\admin\controllers;

use app\models\Contact;
use app\models\Reply;
use app\models\search\ContactSearch;
use app\models\search\ReplySearch;
use app\modules\admin\components\Controller;
use navatech\language\Translate;
use navatech\role\filters\RoleFilter;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'role'  => [
				'class'   => RoleFilter::className(),
				'name'    => Translate::manage_x(Translate::contact()),
				'actions' => [
					'index'  => 'Danh sách',
					'view'   => 'Chi tiết',
					'create' => 'Thêm mới',
					'update' => 'Cập nhật',
					'delete' => 'Xóa',
				],
			],
		];
	}

	/**
	 * Lists all Contact models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new ContactSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Contact model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		$reply        = new Reply();
		$searchModel  = new ReplySearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
		$model        = $this->findModel($id);
		if ($model->status == 0) {
			$model->status = 1;
			$model->save();
		}
		$reply->contact_id = $id;
		$reply->fullname   = $model->fullname;
		$reply->from       = Yii::$app->setting->get('email_field');
		$reply->to         = $model->fullname;
		if ($reply->load(Yii::$app->request->post()) && $reply->save()) {
			$this->actionMailer($model->email, $reply->content, $reply->subject);
			return $this->redirect([
				'index',
			]);
		} else {
			return $this->render('view', [
				'model'        => $model,
				'reply'        => $reply,
				'searchModel'  => $searchModel,
				'dataProvider' => $dataProvider,
			]);
		}
	}

	/**
	 * Creates a new Contact model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Contact();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Contact model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Contact model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Contact model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Contact the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Contact::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionMailer($email = null, $content = null, $title = null) {
		//		Yii::$app->mailer->set
		if ($this->module->mailers->compose('mail', [])->setFrom(Yii::$app->setting->get('from_email'))->setTo($email)->setSubject($title)->setHtmlBody($content)->send()) {
			return true;
		} else {
			return false;
		}
	}
}
