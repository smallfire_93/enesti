<?php
namespace app\widgets;

use app\components\Widget;
use Yii;

class SidebarWidget extends Widget {

	public function run() {
		return $this->render('sidebarWidget');
	}
}