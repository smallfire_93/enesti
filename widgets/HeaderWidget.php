<?php
namespace app\widgets;

use app\models\Category;
use app\models\Slider;
use navatech\language\widgets\LanguageWidget;
use Yii;

class HeaderWidget extends LanguageWidget {

	public function run() {
		$sliders    = Slider::find()->where(['status' => 1])->limit(6)->all();
		$categories = Category::find()->where([
			'status'    => 1,
			'type'      => 1,
			'parent_id' => 0,
		])->orderBy(['order' => SORT_ASC])->all();
		return $this->render('headerWidget', [
			'sliders'    => $sliders,
			'languages'  => $this->getData(),
			'categories' => $categories,
		]);
	}

	public function getViewPath() {
		$name = explode('\\', self::className());
		return \Yii::$app->view->theme->basePath . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . end($name);
	}

	public static function isActive($controller, $action = null, $params = null) {
		$string = '';
		if (!is_array($controller)) {
			$controller = [$controller];
		}
		if ($action !== null && !is_array($action)) {
			$action = [$action];
		}
		if ($params !== null && !is_array($params)) {
			$params = [$params];
		}
		if (in_array(Yii::$app->controller->id, $controller, true)) {
			if ($action === null || ($action != null && in_array(Yii::$app->controller->action->id, $action, true))) {
				if ($params === null || in_array($params, array_chunk(Yii::$app->controller->actionParams, 1, true), true)) {
					$string = 'active';
				}
			}
		}
		return $string;
	}
}