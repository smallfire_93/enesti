<?php
namespace app\widgets;

use app\components\Widget;
use app\models\Department;
use Yii;

class FooterWidget extends Widget {

	public function run() {
		$department = Department::find()->where(['status' => 1])->limit(2)->all();
		return $this->render('footerWidget', ['department' => $department]);
	}
}