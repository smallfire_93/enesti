<?php
use app\controllers\ContactController;
use app\modules\admin\controllers\AccountController;
use app\modules\admin\controllers\BannerController;
use app\modules\admin\controllers\CategoryController;
use app\modules\admin\controllers\ClientController;
use app\modules\admin\controllers\DepartmentController;
use app\modules\admin\controllers\DistributorController;
use app\modules\admin\controllers\OrderController;
use app\modules\admin\controllers\PageController;
use app\modules\admin\controllers\PopupController;
use app\modules\admin\controllers\PostController;
use app\modules\admin\controllers\ProductController;
use app\modules\admin\controllers\PromotionController;
use app\modules\admin\controllers\RecruitmentController;
use app\modules\admin\controllers\SliderController;
use app\modules\admin\widgets\sidebarWidget;
use navatech\language\controllers\IndexController;
use navatech\language\Translate;
use navatech\role\controllers\DefaultController;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Url;

?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper hide">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler"></div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			<li class="nav-item start <?= SidebarWidget::isActive('site', 'index') ?>">
				<a href="<?= Url::to(['/admin/default/index']) ?>" class="nav-link nav-toggle">
					<i class="fa fa-tachometer"></i>
					<span class="title"><?php echo Translate::dashboard() ?></span>
					<span class="selected"></span>
				</a>
			</li>

			<?php if(RoleChecker::isAuth(CategoryController::className())): ?>
				<li class="nav-item <?= SidebarWidget::isActive(['category']) ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-archive"></i>
						<span class="title"><?php echo Translate::category() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(CategoryController::className(), 'index')): ?>
							<li class="nav-item  <?= SidebarWidget::isActive('category', 'index', ['type' => 1]) ?>">
								<a href="<?= Url::to([
									'/admin/category/index',
									'type' => 1,
								]) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::x_category(Translate::product()); ?></span>
								</a>
							</li>

							<li class="nav-item  <?= SidebarWidget::isActive('category', 'index', ['type' => 2]) ?>">

								<a href="<?= Url::to([
									'/admin/category/index',
									'type' => 2,
								]) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::x_category(Translate::news()); ?></span>
								</a>
							</li>
						<?php endif ?>

					</ul>
				</li>                    <?php endif ?>

			<?php if(RoleChecker::isAuth(PostController::className())): ?>
				<li class="nav-item <?= SidebarWidget::isActive('post') ?> ">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-pencil-square-o"></i>
						<span class="title"><?php echo Translate::post() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(PostController::className(), 'create')): ?>

							<li class="nav-item <?= SidebarWidget::isActive('post', 'create') ?> ">
								<a href="<?= Url::to(['/admin/post/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::post()) ?></span>
								</a>
							</li>
						<?php endif ?>

						<?php if(RoleChecker::isAuth(PostController::className(), 'index')): ?>

							<li class="nav-item <?= SidebarWidget::isActive('post', 'index') ?> ">
								<a href="<?= Url::to(['/admin/post/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::list_x(Translate::post()) ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(ProductController::className())): ?>
				<li class="nav-item <?= SidebarWidget::isActive('product') ?> ">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-product-hunt"></i>
						<span class="title"><?php echo Translate::product() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(ProductController::className(), 'create')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('product', 'create') ?> ">
								<a href="<?= Url::to(['/admin/product/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::product()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(ProductController::className(), 'index')): ?>

							<li class="nav-item  <?= SidebarWidget::isActive('product', 'index') ?>">
								<a href="<?= Url::to(['/admin/product/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::list_x(Translate::product()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>
			<?php if(RoleChecker::isAuth(OrderController::className())): ?>
				<li class="nav-item  <?= SidebarWidget::isActive('order') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-cart-plus"></i>
						<span class="title"><?php echo Translate::order() ?> </span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(OrderController::className(), 'index')): ?>
							<li class="nav-item  <?= SidebarWidget::isActive('order', 'index') ?>">
								<a href="<?= Url::to(['/admin/order/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::list_x(Translate::order()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(PromotionController::className())): ?>
				<li class="nav-item <?= SidebarWidget::isActive('promotion') ?> ">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-cart-arrow-down"></i>
						<span class="title"><?php echo Translate::promotion() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(PromotionController::className(), 'create')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('promotion', 'create') ?> ">
								<a href="<?= Url::to(['/admin/promotion/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::promotion()); ?></span>
								</a>
							</li>
						<?php endif ?>

						<?php if(RoleChecker::isAuth(PromotionController::className(), 'index')): ?>

							<li class="nav-item  <?= SidebarWidget::isActive('promotion', 'index') ?>">
								<a href="<?= Url::to(['/admin/promotion/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::list_x(Translate::promotion()); ?></span>
								</a>
							</li>
						<?php endif ?>

					</ul>
				</li>
			<?php endif ?>

			<?php if(RoleChecker::isAuth(DistributorController::className())): ?>
				<li class="nav-item <?= SidebarWidget::isActive('distributor') ?> ">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-sitemap "></i>
						<span class="title"><?php echo Translate::distributor() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(DistributorController::className(), 'create')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('distributor', 'create') ?> ">
								<a href="<?= Url::to(['/admin/distributor/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::distributor()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(DistributorController::className(), 'index')): ?>
							<li class="nav-item  <?= SidebarWidget::isActive('distributor', 'index') ?>">
								<a href="<?= Url::to(['/admin/distributor/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::list_x(Translate::distributor()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(BannerController::className())): ?>
				<li class="nav-item  <?= SidebarWidget::isActive('banner') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-area-chart"></i>
						<span class="title"><?php echo Translate::banner() ?> </span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(BannerController::className(), 'create')): ?>
							<li class="nav-item   <?= SidebarWidget::isActive('banner', 'create') ?>">
								<a href="<?= Url::to(['/admin/banner/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::banner()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(BannerController::className(), 'index')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('banner', 'index') ?> ">
								<a href="<?= Url::to(['/admin/banner/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::banner()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(PopupController::className())): ?>

				<li class="nav-item  <?= SidebarWidget::isActive('popup') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-gift"></i>
						<span class="title"><?php echo Translate::popup() ?> </span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(PopupController::className(), 'create')): ?>
							<li class="nav-item   <?= SidebarWidget::isActive('popup', 'create') ?>">
								<a href="<?= Url::to(['/admin/popup/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::popup()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(PopupController::className(), 'index')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('popup', 'index') ?> ">
								<a href="<?= Url::to(['/admin/popup/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::popup()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(SliderController::className())): ?>

				<li class="nav-item  <?= SidebarWidget::isActive('slider') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-file-image-o"></i>
						<span class="title"><?php echo Translate::slider() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(SliderController::className(), 'create')): ?>
							<li class="nav-item   <?= SidebarWidget::isActive('slider', 'create') ?>">
								<a href="<?= Url::to(['/admin/slider/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::slider()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(SliderController::className(), 'index')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('slider', 'index') ?> ">
								<a href="<?= Url::to(['/admin/slider/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::slider()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(ClientController::className())): ?>

				<li class="nav-item  <?= SidebarWidget::isActive('client') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-user"></i>
						<span class="title"><?php echo Translate::client() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(ClientController::className(), 'create')): ?>

							<li class="nav-item   <?= SidebarWidget::isActive('client', 'create') ?>">
								<a href="<?= Url::to(['/admin/client/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::client()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(ClientController::className(), 'index')): ?>

							<li class="nav-item <?= SidebarWidget::isActive('client', 'index') ?> ">
								<a href="<?= Url::to(['/admin/client/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::client()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(RecruitmentController::className())): ?>

				<li class="nav-item   <?= SidebarWidget::isActive('recruitment') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-black-tie"></i>
						<span class="title"><?php echo Translate::recruitment() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(RecruitmentController::className(), 'create')): ?>
							<li class="nav-item   <?= SidebarWidget::isActive('recruitment') ?>">
								<a href="<?= Url::to(['/admin/recruitment/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::recruitment()); ?></span>
								</a>
							</li>
							<?php if(RoleChecker::isAuth(RecruitmentController::className(), 'index')): ?>
							<?php endif ?>
							<li class="nav-item <?= SidebarWidget::isActive('recruitment') ?> ">
								<a href="<?= Url::to(['/admin/recruitment/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::recruitment()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(DepartmentController::className())): ?>

				<li class="nav-item   <?= SidebarWidget::isActive('department') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-building"></i>
						<span class="title"><?php echo Translate::office() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(DepartmentController::className(), 'create')): ?>
							<li class="nav-item   <?= SidebarWidget::isActive('department', 'create') ?>">
								<a href="<?= Url::to(['/admin/department/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::office()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(DepartmentController::className(), 'index')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('department', 'index') ?> ">
								<a href="<?= Url::to(['/admin/department/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::office()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(PageController::className())): ?>

				<li class="nav-item   <?= SidebarWidget::isActive('page') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-file-o"></i>
						<span class="title"><?php echo Translate::page() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(PageController::className(), 'create')): ?>
							<li class="nav-item   <?= SidebarWidget::isActive('page', 'create') ?>">
								<a href="<?= Url::to(['/admin/page/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::page()); ?></span>
								</a>
							</li>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(PageController::className(), 'index')): ?>
							<li class="nav-item <?= SidebarWidget::isActive('page', 'index') ?> ">
								<a href="<?= Url::to(['/admin/page/index']) ?>" class="nav-link ">
									<span class="title"> <?php echo Translate::list_x(Translate::page()); ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li> <?php endif ?>

			<?php if(RoleChecker::isAuth(DefaultController::className())): ?>

				<li class="nav-item  <?= SidebarWidget::isActive('admin/role/default') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-users"></i>
						<span class="title"><?php echo Translate::user_role() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(DefaultController::className(), 'index')): ?>
							<li class="nav-item  <?= SidebarWidget::isActive('admin/role/default', ['index',]) ?>">
								<a href="<?= Url::to(['/admin/role/default/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::x_management(Translate::user_role()); ?> </span>
								</a>
							</li>
						<?php endif ?>

						<?php if(RoleChecker::isAuth(DefaultController::className(), 'create')): ?>
							<li class="nav-item  <?= SidebarWidget::isActive('admin/role/default', ['create',]) ?>">
								<a href="<?= Url::to(['/admin/role/default/create']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::create_x(Translate::user_role()); ?> </span>
								</a>
							</li>
						<?php endif ?>

					</ul>
				</li>
			<?php endif ?>

			<?php if(RoleChecker::isAuth(AccountController::className())): ?>

				<li class="nav-item  <?= SidebarWidget::isActive('account') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-users"></i>
						<span class="title"><?php echo Translate::admin() ?></span>
						<span class="arrow"></span>
					</a>

					<?php if(RoleChecker::isAuth(AccountController::className(), 'index')): ?>
					<ul class="sub-menu">
						<li class="nav-item  <?= SidebarWidget::isActive(DefaultController::className(), ['index',]) ?>">
							<a href="<?= Url::to(['/admin/account/index']) ?>" class="nav-link ">
								<span class="title"><?php echo Translate::list_x(Translate::admin()); ?> </span>
							</a>
						</li>
						<?php endif ?>

					</ul>
				</li>
			<?php endif ?>

			<?php if(RoleChecker::isAuth(ContactController::className())): ?>

				<li class="nav-item   <?= SidebarWidget::isActive('contact') ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-comments"></i>
						<span class="title"><?php echo Translate::contact() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(ContactController::className(), 'index')): ?>

							<li class="nav-item   <?= SidebarWidget::isActive('contact', 'index') ?>">
								<a href="<?= Url::to(['/admin/contact/index']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::list_x(Translate::contact()); ?></span>
								</a>
							</li>
						<?php endif ?>

					</ul>
				</li>
			<?php endif ?>

			<?php if(RoleChecker::isAuth(IndexController::className())): ?>

				<li class="nav-item  <?= SidebarWidget::isActive('admin/language/' . Yii::$app->controller->id) ?>">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-flag"></i>
						<span class="title"><?php echo Translate::language() ?> </span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(YII_ENV_DEV): ?>
							<?php if(RoleChecker::isAuth(IndexController::className(), 'index')): ?>

								<li class="nav-item  <?= SidebarWidget::isActive('admin/language/index', 'index') ?>">
									<a href="<?= Url::to(['/admin/language/index']) ?>" class="nav-link ">
										<span class="title"><?php echo Translate::language() ?> </span>
									</a>
								</li>
							<?php endif ?>
						<?php endif ?>
						<?php if(RoleChecker::isAuth(IndexController::className(), 'phrase')): ?>

							<li class="nav-item  <?= SidebarWidget::isActive('admin/language/phrase', 'phrase') ?>">
								<a href="<?= Url::to(['/admin/language/phrase']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::phrase() ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li>
			<?php endif ?>

			<?php if(RoleChecker::isAuth(\navatech\setting\controllers\DefaultController::className())): ?>

				<li class="nav-item   <?= SidebarWidget::isActive('admin/setting/default') ?> ">
					<a href="javascript:;" class="nav-link nav-toggle">
						<i class="fa fa-cogs"></i>
						<span class="title"><?php echo Translate::setting() ?></span>
						<span class="arrow"></span>
					</a>
					<ul class="sub-menu">
						<?php if(RoleChecker::isAuth(\navatech\setting\controllers\DefaultController::className(), 'setting')): ?>
							<li class="nav-item  ">
								<a href="<?= Url::to(['/admin/setting']) ?>" class="nav-link ">
									<span class="title"><?php echo Translate::setting() ?></span>
								</a>
							</li>
						<?php endif ?>
					</ul>
				</li>
			<?php endif ?>

		</ul>
		<!-- END SIDEBAR MENU -->
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->