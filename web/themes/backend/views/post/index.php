<?php
use app\models\Post;
use app\modules\admin\controllers\PostController;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::post();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::create_x(Translate::post()), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?php
	$gridColumns = [
		['class' => 'yii\grid\SerialColumn'],
		[
			'attribute' => 'category_id',
			'value'     => function(Post $data) {
				return $data->getCategoryById($data->category_id);
			},
		],
		'title',
		[
			'attribute' => 'image',
			'filter'    => false,
			'format'    => 'raw',
			'value'     => function(Post $data) {
				return Html::img($data->getPictureUrl('image'), [
					'class' => 'img-thumbnail',
				]);
			},
		],
		[
			'attribute' => 'feature',
			'value'     => function(Post $data) {
				return $data->getFeature($data->feature);
			},
			'filter'    => $searchModel->getFeature(),
		],
		[
			'attribute' => 'status',
			'value'     => function(Post $data) {
				return $data->getStatus($data->status);
			},
			'filter'    => $searchModel->getStatus(),
		],
		[
			'class'          => 'yii\grid\ActionColumn',
			'visibleButtons' => [
				'view'   => RoleChecker::isAuth(PostController::className(), 'view'),
				'update' => RoleChecker::isAuth(PostController::className(), 'update'),
				'delete' => RoleChecker::isAuth(PostController::className(), 'delete'),
			],
		],
	];
	?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => $gridColumns,
	]); ?>
</div>
