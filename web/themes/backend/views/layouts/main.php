<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\assets\BackendAsset;
use app\modules\admin\widgets\NavbarWidget;
use app\modules\admin\widgets\SidebarWidget;
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

BackendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<link rel="shortcut icon" href="<?= Yii::$app->setting->get('general_favicon') ?>" type="image/x-icon"/>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<?php $this->beginBody() ?>

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?= NavbarWidget::widget() ?>
	<?= SidebarWidget::widget() ?>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content clearfix">
			<?= Breadcrumbs::widget([
				'homeLink' => [
					'label' => Yii::t('yii', 'Bảng điều khiển'),
					'url'   => Yii::$app->homeUrl,
				],
				'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<!-- BEGIN PAGE HEADER-->
			<?= $content ?>
		</div>
	</div>

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner"> 2016 &copy; Enesti by Navatech.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
