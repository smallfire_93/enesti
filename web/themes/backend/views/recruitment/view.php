<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recruitment */
?>
<div class="recruitment-view">

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'title',
			[
				'attribute' => 'content',
				'format'    => 'raw',
			],
			[
				'attribute' => 'image',
				'format'    => 'raw',
				'value'     => Html::img($model->getPictureUrl('image'), ['class' => 'img-thumbnail']),
			],
			[
				'attribute' => 'feature',
				'value'     => $model->getFeature($model->feature),
			],
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
		],
	]) ?>

</div>
