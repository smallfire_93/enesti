<?php
use app\models\Recruitment;
use app\modules\admin\controllers\RecruitmentController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel \app\models\search\RecruitmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::recruitment();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recruitment-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::create_x(Translate::recruitment()), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'image',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Recruitment $data) {
					return Html::img($data->getImageUrl(), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			'title',
			[
				'attribute' => 'pagelink',
				'value'     => function(Recruitment $data) {
					return Url::base(true).'/'.Yii::$app->language.'/tuyen-dung/'.$data->removeSign($data->title).'-'.$data->id;
				},

			],
			[
				'attribute' => 'feature',
				'filter'    => $searchModel->getFeature(),
				'value'     => function(Recruitment $data) {
					return $data->getFeature($data->feature);
				},
			],
			[
				'attribute' => 'status',
				'value'     => function(Recruitment $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(RecruitmentController::className(), 'view'),
					'update' => RoleChecker::isAuth(RecruitmentController::className(), 'update'),
					'delete' => RoleChecker::isAuth(RecruitmentController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
