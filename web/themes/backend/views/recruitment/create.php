<?php
use navatech\language\Translate;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recruitment */
$this->title                   = Translate::create_x(Translate::recruitment());
$this->params['breadcrumbs'][] = [
	'label' => Translate::recruitment(),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

	<div class="recruitment-create">
		<?= $this->render('_form', [
			'model' => $model,
		]) ?>
	</div>
</div>
