<?php
use navatech\language\Translate;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Popup */

$this->title = Translate::update_x(Translate::recruitment()).': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Translate::recruitment(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Translate::update();
?>
<div class="recruitment-update">
	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model'     => $model,
	]) ?>

</div>
