<?php
/**
 * @var yii\web\View                   $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module           $module
 */
use app\assets\LoginAsset;
use navatech\language\Translate;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

Yii::$app->layout = false;
LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php
$this->title                   = Translate::login();
$this->params['breadcrumbs'][] = $this->title; ?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<!-- END HEAD -->

<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
	<!--	<a href="index-2.html">-->
	<img style=" max-height: 70px;" src="<?= Yii::$app->setting->get('general_logo') ?>" alt=""/> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<?php $form = ActiveForm::begin([
		'id'                     => 'login-form',
		'enableAjaxValidation'   => true,
		'enableClientValidation' => false,
		'validateOnBlur'         => false,
		'validateOnType'         => false,
		'validateOnChange'       => false,
	]) ?>
	<h3 class="form-title font-green"><?php echo Translate::login() ?></h3>

	<div class="form-group">
		<?= $form->field($model, 'login', ['labelOptions' => ['class' => 'control-label visible-ie8 visible-ie9']])->textInput([
			'autofocus'   => 'autofocus',
			'class'       => 'form-control form-control-solid placeholder-no-fix',
			'placeholder' => Translate::username(),
		]) ?>
	</div>
	<div class="form-group">
		<?= $form->field($model, 'password', ['labelOptions' => ['class' => 'control-label visible-ie8 visible-ie9']])->passwordInput([
			'class'       => 'form-control form-control-solid placeholder-no-fix',
			'placeholder' => Translate::password(),
		]) ?>
	</div>
	<div class="form-group">
		<?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
	</div>
	<div class="form-actions">
		<?= Html::submitButton(Yii::t('user', Translate::login()), [
			'class'    => 'btn green uppercase',
			'tabindex' => '3',
		]) ?>
	</div>
	<?php ActiveForm::end(); ?>
	<!-- END LOGIN FORM -->
</div>
<div class="copyright"> 2016 © Navatech. Admin Dashboard Template.</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

