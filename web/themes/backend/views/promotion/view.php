<?php
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Promotion */
$this->title                   = $model->title;
$this->params['breadcrumbs'][] = [
	'label' => 'Promotions',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotion-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::update(), [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Translate::delete(), [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			[
				'attribute' => 'category_id',
				'value'     => $model->getCategoryName($model->category_id),
			],
			[
				'attribute' => 'image',
				'format'    => 'raw',
				'value'     => Html::img($model->getPictureUrl('image'), ['class' => 'img-thumbnail']),
			],
			[
				'attribute' => 'feature',
				'value'     => $model->getFeature($model->feature),
			],
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
		],
	]) ?>

</div>
