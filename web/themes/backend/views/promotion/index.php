<?php
use app\models\Promotion;
use app\modules\admin\controllers\PromotionController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PromotionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::promotion();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotion-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::create_x(Translate::promotion()), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'image',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Promotion $data) {
					return Html::img($data->getPictureUrl('image'), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			'title',
			[
				'attribute' => 'pagelink',
				'value'     => function(Promotion $data) {
					return Url::base(true).'/'.Yii::$app->language.'/khuyen-mai/'.$data->removeSign($data->title).'-'.$data->id;
				},

			],
			[
				'attribute' => 'feature',
				'value'     => function(Promotion $data) {
					return $data->getFeature($data->feature);
				},
				'filter'    => $searchModel->getFeature(),
			],
			[
				'attribute' => 'status',
				'value'     => function(Promotion $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(PromotionController::className(), 'view'),
					'update' => RoleChecker::isAuth(PromotionController::className(), 'update'),
					'delete' => RoleChecker::isAuth(PromotionController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
