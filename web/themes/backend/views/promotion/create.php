<?php
use navatech\language\Translate;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Promotion */
$this->title                   = Translate::create_x(Translate::promotion());
$this->params['breadcrumbs'][] = [
	'label' => Translate::create_x(Translate::promotion()),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotion-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
