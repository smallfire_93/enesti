<?php
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
$this->title                   = $model->title;
$this->params['breadcrumbs'][] = [
	'label' => 'Sản phẩm',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::update(), [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Translate::delete(), [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			[
				'attribute' => 'category_id',
				'value'     => $model->getCategoryById($model->category_id),
			],
			'title',
			[
				'attribute' => 'content',
				'format'    => 'raw',
			],
			'code',
			'origin',
			[
				'attribute' => 'image',
				'value'     => Html::img($model->getPictureUrl('image'), ['class' => 'img-thumbnail']),
				'format'    => 'raw',
			],
			[
				'attribute' => 'feature',
				'value'     => $model->getFeature($model->feature),
			],
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
			[
				'attribute' => 'price',
				'format'    => [
					'decimal',
					0,
				],
			],
			[
				'attribute' => 'in_stock',
				'value'     => $model->getStock($model->in_stock),
			],
		],
	]) ?>

</div>
