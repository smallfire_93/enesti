<?php
use navatech\language\Translate;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title =  Translate::create_x(Translate::product());
$this->params['breadcrumbs'][] = ['label' => Translate::product(), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
