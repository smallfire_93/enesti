<?php
use app\models\Category;
use app\models\Product;
use app\modules\admin\controllers\ProductController;
use kartik\export\ExportMenu;
use kartik\file\FileInput;
use kartik\grid\GridView;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::product();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index clearfix">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::create_x(Translate::product()), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<!--	--><?php
	//	if (Yii::$app->session->hasFlash('failed')) {
	//		echo Yii::$app->session->getFlash('failed');
	//	} else {
	//		echo 'bbbb';
	//	}
	//	?>
	<?php $form = ActiveForm::begin([
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>
	<?= $form->field($file, 'excel', [
	])->widget(FileInput::className(), [
		'pluginOptions' => [
			'allowedFileExtensions' => [
				'xlsx',
			],
			'showUpload'            => false,
		],
	]) ?>
	<!--	<button style="float: right;clear: right">--><?php //echo  ?><!--</button>-->
	<?= Html::submitButton(Translate::import_excel(), [
		'class'    => 'btn green uppercase',
		'tabindex' => '3',
		'style'    => 'float: right;clear: right',
	]) ?>
	<?php ActiveForm::end(); ?>
	<?php
	$gridColumns = [
		['class' => 'kartik\grid\SerialColumn'],
		[
			'attribute' => 'picture',
			'filter'    => false,
			'format'    => 'raw',
			'value'     => function(Product $data) {
				return Html::img($data->getPictureUrl('image'), ['class' => 'img-thumbnail',]);
			},
		],
		[
			'attribute' => 'category_id',
			'value'     => function(Product $data) {
				return $data->getCategoryById($data->category_id);
			},
			'filter'    => Category::getCategoryOrder(),
		],
		'code',
		[
			'attribute' => 'title',
		],
		[
			'attribute' => 'price',
			'format'    => [
				'decimal',
				0,
			],
			'filter'    => (int) str_replace(",", "", $searchModel->price),
		],
//		[
//			'attribute' => 'sale_off',
//			'value'     => function(Product $data) {
//				return (($data->sale_off) * 100) . '%';
//			},
//		],
//		[
//			'attribute' => 'in_stock',
//			'value'     => function(Product $data) {
//				return $data->getStock($data->in_stock);
//			},
//		],
		[
			'attribute' => 'feature',
			'value'     => function(Product $data) {
				return $data->getFeature($data->feature);
			},
			'filter'    => $searchModel->getFeature(),
		],
		[
			'attribute'      => 'image',
			'headerOptions'  => ['style' => 'display:none'],
			'contentOptions' => ['style' => 'display:none'],
			'filterOptions'  => ['style' => 'display:none'],
		],
		[
			'class'          => 'kartik\grid\ActionColumn',
			'visibleButtons' => [
				'view'   => RoleChecker::isAuth(ProductController::className(), 'view'),
				'update' => RoleChecker::isAuth(ProductController::className(), 'update'),
				'delete' => RoleChecker::isAuth(ProductController::className(), 'delete'),
			],
		],
	];
	echo ExportMenu::widget([
		'dataProvider'    => $dataProvider,
		'columns'         => $gridColumns,
		'fontAwesome'     => true,
		'filename'        => 'product',
		'disabledColumns' => [
			0,
			1,
			10,
		],
		'selectedColumns' => [
			2,
			3,
			4,
			5,
			6,
			9,
		],
		'dropdownOptions' => [
			'label' => 'Export Excel',
			'class' => 'btn btn-default',
		],
		'exportConfig'    => [
			ExportMenu::FORMAT_TEXT  => false,
			ExportMenu::FORMAT_PDF   => false,
			ExportMenu::FORMAT_EXCEL => false,
			ExportMenu::FORMAT_CSV   => false,
			ExportMenu::FORMAT_HTML  => false,
		],
	]);
	?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'toolbar'      => [
			'{export}',
			'{toggleData}',
		],
		'columns'      => $gridColumns,
	]); ?>
</div>
<script>
	<?php
	if (Yii::$app->session->hasFlash('success')) { ?>
	alert('<?= Yii::$app->session->getFlash('success')?>');
	<?php } elseif (Yii::$app->session->hasFlash('failed')) { ?>
	alert('<?= Yii::$app->session->getFlash('failed')?>');
	<?php } ?>
</script>