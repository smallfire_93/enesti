<?php
use app\models\Category;
use kartik\file\FileInput;
use navatech\language\models\Language;
use navatech\language\Translate;
use navatech\roxymce\widgets\RoxyMceWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form clearfix">

	<?php $form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>
	<ul class="nav nav-tabs" role="tablist">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<li class="<?= ($key == 0) ? 'active' : '' ?>">
				<a href="#tab_<?= $language->code ?>" role="tab" data-toggle="tab"><?= $language->name ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<div class="<?= ($key == 0) ? 'active in' : '' ?> tab-pane fade" id="tab_<?= $language->code ?>">
				<?php
				echo $form->field($model, 'title_' . $language->code, ['template' => '{label}<div class="col-sm-8">{input}</div>',])->textInput([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('title', $language->code),
				])->label(Translate::title());
				?>
				<?php
				echo $form->field($model, 'content_' . $language->code, ['template' => '{label}<div class="col-sm-8">{input}</div>',])->widget(RoxyMceWidget::className(), [
					'model'       => $model,
					//your Model, REQUIRED
					'attribute'   => 'content_' . $language->code,
					//attribute name of your model, REQUIRED if using 'model' section
					'name'        => 'Post[content_' . $language->code . ']',
					//default name of textarea which will be auto generated, NOT REQUIRED if using 'model' section
					'value'       => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('content', $language->code),
					'action'      => Url::to(['/roxymce/default']),
					//default value of current textarea, NOT REQUIRED
					'options'     => [//TinyMce options, NOT REQUIRED, see https://www.tinymce.com/docs/
						'title' => 'RoxyMCE',
						//title of roxymce dialog, NOT REQUIRED
						'min_height' => 250,
					],
					'htmlOptions' => [
						'rows' => 6,
					],
				])->label(Translate::content());
				?>
			</div>
		<?php endforeach; ?>

	</div>
	<?= $form->field($model, 'category_id')->dropDownList(Category::getCategoryText(1), ['prompt' => Translate::category()]) ?>

	<?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'origin')->textInput(['maxlength' => true]) ?>

	<?php
	echo $form->field($model, 'img')->widget(FileInput::className(), [
		'options'       => ['accept' => 'image/*'],
		'pluginOptions' => [
			'allowedFileExtensions'                          => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'                                     => false,
			$model->getIsNewRecord() ? '' : 'initialPreview' => [
				Html::img($model->getPictureUrl('image'), ['class' => 'file-preview-image']),
			],
		],
	]);
	?>

	<?= $form->field($model, 'status')->checkbox([
		'class'    => 'make-switch',
		'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
	]) ?>
	<?= $form->field($model, 'feature')->checkbox([
		'class'    => 'make-switch',
		'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
	]) ?>
	<?= $form->field($model, 'price', [
		'template' => '{label}<div class="col-sm-3">{input}</div>',
	])->textInput([
		'class' => 'form-control col-sm-2',
	]) ?>
	<?= $form->field($model, 'in_stock')->textInput() ?>

	<div class="form-group col-sm-4">
		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
