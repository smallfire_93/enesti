<?php
use navatech\language\models\Language;
use navatech\language\Translate;
use navatech\roxymce\widgets\RoxyMceWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Department */
?>

	<div class="department-form">

<?php $form = ActiveForm::begin([
	'layout' => 'horizontal',
	//	'options' => [
	//		'role'  => 'form',
	//	],
]); ?>
	<!-- TAB NAVIGATION -->
	<ul class="nav nav-tabs" role="tablist">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<li class="<?= ($key == 0) ? 'active' : '' ?>">
				<a href="#tab_<?= $language->code ?>" role="tab" data-toggle="tab"><?= $language->name ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<div class="<?= ($key == 0) ? 'active in' : '' ?> tab-pane fade" id="tab_<?= $language->code ?>">
				<?php
				echo $form->field($model, 'name_' . $language->code, [
					'labelOptions' => [
						'class' => 'control-label col-sm-3',
					],
				])->textInput([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('name', $language->code),
				])->label(Translate::name());
				echo $form->field($model, 'address_' . $language->code, [
					'labelOptions' => ['class' => 'control-label col-sm-3'],
				])->textInput([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('address', $language->code),
				])->label(Translate::address());
				echo $form->field($model, 'company_code_' . $language->code, [
					'labelOptions' => ['class' => 'control-label col-sm-3'],
				])->textarea([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('company_code', $language->code),
				])->label(Translate::company_code());
				echo $form->field($model, 'schedule_' . $language->code, [
					'template' => '{label}<div class="col-sm-8">{input}</div>',
				])->widget(RoxyMceWidget::className(), [
					'model'       => $model,
					//your Model, REQUIRED
					'attribute'   => 'schedule_' . $language->code,
					//attribute name of your model, REQUIRED if using 'model' section
					'name'        => 'Department[schedule_' . $language->code . ']',
					//default name of textarea which will be auto generated, NOT REQUIRED if using 'model' section
					'value'       => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('schedule', $language->code),
					'action'      => Url::to(['/roxymce/default']),
					//default value of current textarea, NOT REQUIRED
					'options'     => [//TinyMce options, NOT REQUIRED, see https://www.tinymce.com/docs/
						'title'      => 'RoxyMCE',
						//title of roxymce dialog, NOT REQUIRED
						'min_height' => 250,
					],
					'htmlOptions' => [
						'rows' => 6,
					],
				])->label(Translate::company_schedule());
				?>

			</div>
		<?php endforeach; ?>
	</div>

<?= $form->field($model, 'email', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
<?= $form->field($model, 'business_room', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['placeholder' => Translate::phone()]) ?>
<?= $form->field($model, 'facebook', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['placeholder' => Translate::facebook_link()]) ?>

<?= $form->field($model, 'phone', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>

<?= $form->field($model, 'website', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
<?= $form->field($model, 'google_plus', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>

<?= $form->field($model, 'order', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
<?= $form->field($model, 'show_home')->checkbox([
	'class'    => 'make-switch',
	'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
]) ?>
<?= $form->field($model, 'status')->checkbox([
	'class'    => 'make-switch',
	'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
]) ?>
	<div class="col-sm-4">

		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	</div>
<?php ActiveForm::end() ?>