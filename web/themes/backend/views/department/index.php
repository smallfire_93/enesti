<?php
use app\models\Department;
use app\modules\admin\controllers\DepartmentController;
use kartik\grid\GridView;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::office();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::create_x(Translate::office()), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'kartik\grid\SerialColumn'],
			'name',
			'phone',
			[
				'attribute' => 'show_home',
				'value'     => function(Department $data) {
					return $data->getShowhome($data->show_home);
				},
				'filter'    => $searchModel->getShowhome(),
			],
			[
				'attribute' => 'status',
				'value'     => function(Department $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			'order',
			// 'created_date',
			[
				'class'          => 'kartik\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(DepartmentController::className(), 'view'),
					'update' => RoleChecker::isAuth(DepartmentController::className(), 'update'),
					'delete' => RoleChecker::isAuth(DepartmentController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
