<?php
use app\models\Distributor;
use app\models\Model;
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $distributor app\models\Distributor */
/* @var $dataProvider app\models\Order */
/* @var $searchModel app\models\Order */
$this->title                   = Translate::order() . ' #000' . $model->id;
$this->params['breadcrumbs'][] = [
	'label' => Translate::order(),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">
	<h1><?= Html::encode($this->title) ?></h1>

	<div class="row">
		<div class="col-md-6 col-sm-12">
			<div class="portlet yellow-crusta box">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i><?= Translate::order() . ' #000' . $model->id ?>
					</div>
					<div class="actions">
					</div>
				</div>
				<div class="portlet-body">
					<div class="row static-info">
						<div class="col-md-5 name"> Order #:</div>
						<div class="col-md-7 value"> <?= '000' . $model->id ?>
						</div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::created_date() ?>:</div>
						<div class="col-md-7 value"> <?= Model::format($model->created_date, 'Y-m-d H:i:s', 'H:i:s d-m-Y ') ?></div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::status() ?>:</div>
						<div class="col-md-7 value">
							<?php if ($model->status = 1) { ?>
								<span class="label label-success"> <?= $model->getOrderStatus($model->status) ?> </span>
							<?php } else { ?>
								<span class="label label-danger"> <?= $model->getOrderStatus($model->status) ?> </span>
							<?php } ?>
						</div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::total_amount() ?>:</div>
						<div class="col-md-7 value"> <?= number_format($model->getTotalAmount($model->id)) ?> VNĐ</div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::quantity() ?>:</div>
						<div class="col-md-7 value"> <?= $model->getTotalQuantity($model->id) ?></div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::shipping_address() ?>:</div>
						<div class="col-md-7 value"> <?= $model->shipping_address ?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="portlet blue-hoki box">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i><?= Translate::distributor() ?>
					</div>
					<div class="actions">
						<a href="javascript:;" class="btn btn-default btn-sm">
							<i class="fa fa-pencil"></i> Edit </a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::name() . ' ' . Translate::distributor() ?>:</div>
						<div class="col-md-7 value"> <?= $distributor->full_name ?></div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> Email:</div>
						<div class="col-md-7 value"><?= $distributor->email ?> </div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::username() ?>:</div>
						<div class="col-md-7 value"><?= $distributor->username ?> </div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::address() ?>:</div>
						<div class="col-md-7 value"> <?= $distributor->address ?></div>
					</div>
					<div class="row static-info">
						<div class="col-md-5 name"> <?= Translate::phone() ?>:</div>
						<div class="col-md-7 value"> <?= $model->phone_number ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="portlet grey-cascade box">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-cogs"></i> <?= Translate::list_x(Translate::product()) ?>

					</div>
					<div class="actions">
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-responsive">
						<table class="table table-hover table-bordered table-striped">
							<thead>
							<tr>
								<th> <?= Translate::product() ?></th>
								<th> <?= Translate::code() ?></th>
								<th> <?= Translate::x_category(Translate::product()) ?></th>
								<th> <?= Translate::price() ?></th>
								<th> <?= Translate::discount() ?></th>
								<th> <?= Translate::distributor_discount() ?></th>
								<th> <?= Translate::quantity() ?></th>
								<th> <?= Translate::total_amount() ?></th>
							</tr>
							</thead>
							<tbody>
							<?php /** @var app\models\OrderItem $item */
							/** @var app\models\OrderItem $orderItem */
							foreach ($orderItem as $item) {
								$total       = (($item->quantity * $item->price) - ($item->price * $item->getProductById($item)->sale_off));
								$final_total = $total - ($total * Distributor::findOne($model->distributor_id)->discount);
								?>

								<tr>
									<td> <?= $item->getProductById($item->product_id)->title ?></td>
									<td> <?= $item->getProductById($item->product_id)->code ?></td>
									<td> <?= $item->getCategoryByProduct($item->product_id)->name ?></td>
									<td><?= number_format($item->price) ?>VNĐ</td>
									<td> <?= $item->getProductById($item->product_id)->sale_off * 100 ?>%</td>
									<td> <?= Distributor::findOne($model->distributor_id)->discount * 100 ?>%</td>
									<td> <?= $item->quantity ?></td>
									<td> <?= number_format($final_total) ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6">
			<div class="well">
				<div class="row static-info align-reverse">
					<div class="col-md-8 name"> <?= Translate::total_amount() ?>:</div>
					<div class="col-md-3 value"> <?= number_format($model->getTotalAmount($model->id)) ?> VNĐ</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

