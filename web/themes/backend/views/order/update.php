<?php
use navatech\language\Translate;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
$this->title                   = Translate::update_x(Translate::order()) . ': #000' . $model->id;
$this->params['breadcrumbs'][] = [
	'label' => Translate::order(),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = [
	'label' => '#000' . $model->id,
	'url'   => [
		'view',
		'id' => $model->id,
	],
];
$this->params['breadcrumbs'][] = Translate::update();
?>
<div class="order-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model'     => $model,
		'orderItem' => $orderItem,
	]) ?>

</div>
