<?php
use app\models\Distributor;
use app\models\Model;
use app\models\Order;
use app\modules\admin\controllers\OrderController;
use kartik\grid\GridView;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::order();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="clearfix"></div>

	<div class="row widget-row">
		<div class="col-md-3 count-total">
			<!-- BEGIN WIDGET THUMB -->
			<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
				<h4 class="widget-thumb-heading"><?= Translate::total_x(Translate::order())?></h4>
				<div class="widget-thumb-wrap">
					<i class="widget-thumb-icon bg-green icon-bulb"></i>
					<div class="widget-thumb-body">
						<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?= $total_order ?>">0</span>
					</div>
				</div>
			</div>
			<!-- END WIDGET THUMB -->
		</div>
		<div class="col-md-3 count-total">
			<!-- BEGIN WIDGET THUMB -->
			<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
				<h4 class="widget-thumb-heading"><?= Translate::order_x(Translate::paid())?></h4>
				<div class="widget-thumb-wrap">
					<i class="widget-thumb-icon bg-red icon-layers"></i>
					<div class="widget-thumb-body">
						<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?= $order_paid ?>">0</span>
					</div>
				</div>
			</div>
			<!-- END WIDGET THUMB -->
		</div>
		<div class="col-md-3 count-total">
			<!-- BEGIN WIDGET THUMB -->
			<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
				<h4 class="widget-thumb-heading"><?= Translate::order_x(Translate::unpaid())?></h4>
				<div class="widget-thumb-wrap">
					<i class="widget-thumb-icon bg-purple icon-screen-desktop"></i>
					<div class="widget-thumb-body">
						<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?= $order_unpaid ?>">0</span>
					</div>
				</div>
			</div>
			<!-- END WIDGET THUMB -->
		</div>
		<div class="col-md-3 count-total">
			<!-- BEGIN WIDGET THUMB -->
			<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
				<h4 class="widget-thumb-heading"><?= Translate::order_x(Translate::cancel())?></h4>
				<div class="widget-thumb-wrap">
					<i class="widget-thumb-icon bg-blue icon-bar-chart"></i>
					<div class="widget-thumb-body">
						<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?= $order_cancel ?>">0</span>
					</div>
				</div>
			</div>
			<!-- END WIDGET THUMB -->
		</div>
		<div class="col-md-3 count-total">
			<!-- BEGIN WIDGET THUMB -->
			<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
				<h4 class="widget-thumb-heading"><?= Translate::total_x(Translate::revenue())?></h4>
				<div class="widget-thumb-wrap">
					<i class="widget-thumb-icon bg-green fa fa-money"></i>
					<div class="widget-thumb-body">
						<span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?= number_format($total_revenue) ?>">0</span>
					</div>
				</div>
			</div>
			<!-- END WIDGET THUMB -->
		</div>
	</div>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		//		'showPageSummary' => true,
		'columns'      => [
			['class' => 'kartik\grid\SerialColumn'],
			[
				'attribute' => 'created_date',
				'filter'    => false,
				'value'     => function(Order $data) {
					return Model::format($data->created_date, 'Y-m-d H:i:s', 'H:i:s d-m-Y');
				},
			],
			[
				'attribute' => 'distributor_id',
				'value'     => function(Order $data) {
					return Distributor::getDistributorById($data->distributor_id)->full_name;
				},
			],
			[
				'attribute' => 'quantity',
				'value'     => function(Order $data) {
					return $data->getTotalQuantity($data->id);
				},
			],
			[
				'attribute' => 'total_price',
				'value'     => function(Order $data) {
					return number_format($data->getTotalAmount($data->id));
				},
				'footer'    => true,
			],
			[
				'attribute' => 'status',
				'value'     => function(Order $data) {
					return $data->getOrderStatus($data->status);
				},
			],
			[
				'class'          => 'kartik\grid\ActionColumn',
				'template'       => '{check} {view} {update} {delete}',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(OrderController::className(), 'view'),
					'update' => RoleChecker::isAuth(OrderController::className(), 'update'),
					'delete' => RoleChecker::isAuth(OrderController::className(), 'delete'),
					'check'  => RoleChecker::isAuth(OrderController::className(), 'check'),
				],
				'buttons'        => [
					'check'  => function($url, $model, $key) {
						return Html::a('<i class="fa fa-check-square-o"></i>', Url::to([
							'order/check',
							'id' => $model->id,
						]), [
							'data-confirm' => Yii::t('yii', Translate::check_order_confirm()),
						]);
					},
					'delete' => function($url, $model, $key) {
						return Html::a('<i class="fa fa-trash-o"></i>', Url::to([
							'order/delete',
							'id' => $model->id,
						]), [
							'data-confirm' => Yii::t('yii', 'Bạn có chắc muốn xóa đơn hàng này không?'),
						]);
					},
				],
			],
		],
	]); ?>
</div>
