<?php
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="form-group">
        <label class="control-label search-input"><?= Translate::choose_date()?></label>
            <div class="input-group input-large date-picker input-daterange search-input" data-date="<?= date("d/m/Y") ?>" data-date-format="yyyy-mm-dd">
                <?= $form->field($model, 'createdFrom')->textInput(['class'=>'form-control'])->label(null,['class'=>'hidden']); ?>
                <span class="input-group-addon"> Đến ngày </span>
                <?= $form->field($model, 'createdTo')->textInput(['class'=>'form-control'])->label(null,['class'=>'hidden']); ?>

            </div>
        <?= Html::submitButton(Translate::search(), ['class' => 'search-input btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
