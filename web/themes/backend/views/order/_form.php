<?php
use app\models\Category;
use app\models\Distributor;
use navatech\language\Translate;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
	<?php $form = ActiveForm::begin([
		'layout' => 'horizontal',
	]); ?>
	<?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'shipping_address')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'status')->dropDownList($model->getOrderStatus()) ?>
	<?= $form->field($model, 'distributor_id')->textInput([
		'value'    => $name = Distributor::getDistributorById($model->distributor_id)->full_name != null ? $name : Distributor::getDistributorById($model->distributor_id)->username,
		'disabled' => true,
	]) ?>
	<?= $form->field($model, 'total_price')->textInput([
		'disabled' => true,
		'value'    => $model->isNewRecord ? '' : number_format($model->getTotalAmount($model->id)),
	]) ?>
	<?= $form->field($model, 'quantity')->textInput([
		'disabled' => true,
		'value'    => $model->isNewRecord ? '' : $model->getTotalQuantity($model->id),
	]) ?>
	<div class="form-group col-sm-4">
		<?= Html::submitButton($model->isNewRecord ? Translate::create() : Translate::update(), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>
	<div class="wrapper-order">
		<div class="item-header">
			<div class="id grid-display"><p style="text-transform: uppercase"><?= Translate::sort() ?></p></div>
			<div class="code grid-display"><p style="text-transform: uppercase"><?= Translate::code() ?></p></div>
			<div class="category-select grid-display">
				<p style="text-transform: uppercase"><?= Translate::x_category(Translate::product()) ?></p>
			</div>
			<div class="product-select grid-display">
				<p style="text-transform: uppercase"><?= Translate::product() ?></p>
			</div>
			<div class="quantity grid-display"><p style="text-transform: uppercase"><?= Translate::quantity() ?></p>
			</div>
			<div class="price-show grid-display">
				<p style="text-transform: uppercase"><?= Translate::total_amount() ?></p>
			</div>
		</div>
		<?php $form = ActiveForm::begin(); ?>
		<div class="items">
			<?php if (isset($_GET['id'])) {
				$i     = 1;
				$total = 0;
				/** @var app\models\Order $orderItem */
				foreach ($orderItem as $item) { ?>

					<div class="item-detail">
						<div class="id grid-display">
							<div class="overflow"><?= Html::input('text', '', $i, [
									'class'    => 'ordinal form-control form-height form-boder',
									"disabled" => true,
								]) ?></div>
						</div>
						<div class="code grid-display">
							<div class="overflow"><?= Html::input('text', 'code', $item->getProductById($item->product_id)->code, [
									'class'    => 'form-control form-height form-boder',
									"disabled" => true,
								]) ?></div>
						</div>
						<div class="category-select grid-display">
							<div class="overflow"><?= Html::dropDownList('', $item->getCategoryIdItem($item->id), Category::getCategoryOrder(), [
									'class'  => 'form-control form-height form-boder ',
									'prompt' => 'Chọn danh mục',
								]) ?>
							</div>
						</div>
						<?php $product = $item->getProductById($item->product_id);
						$sale_off = (($product->price) - ($product->price * $product->sale_off)) ?>
						<div class="product-select grid-display">
							<div class="overflow"><?= Html::dropDownList('OrderItem[' . $i . '][product_id]', $item->product_id, $item->getProductArray($item->id), [
									'class'      => 'form-control form-height form-boder',
									'prompt'     => 'Chọn sản phẩm',
									'id'         => 'orderitem-product_id',
									'data-price' => $product->price,
									'data-sale'  => $product->sale_off,
								]) ?></div>
						</div>
						<div class="quantity grid-display">
							<div class="overflow"><?= Html::activeTextInput($item, 'quantity', [
									'class' => 'form-control form-height form-boder',
									'type'  => 'number',
									'min'   => 0,
									'name'  => 'OrderItem[' . $i . '][quantity]',
								]) ?></div>
						</div>

						<div class="price-show grid-display">

							<div class="overflow"><?= Html::activeTextInput($item, 'price', [
									'class'    => 'form-control form-height form-boder',
									'disabled' => true,
									'value'    => number_format($sale_off * $item->quantity),
								]) ?></div>
						</div>
					</div>

					<?php $i ++;
					$total += ($sale_off * $item->quantity);
				} ?>
			<?php } ?>
		</div>
		<div class="row action-pager">
			<div class="col-sm-6 action-item add-item">
				<a class="fleft add-form" href=""><?= Translate::add_a_new_x(Translate::product()) ?></a>
			</div>
		</div>
	</div>
	<div class=" row final-total">
		<div class="col-sm-6 total">
			<p><?= Translate::total() ?></p>
		</div>
		<div class="detail-total col-sm-6 ">
			<div class="col-sm-6 label-item">
				<p>Tổng giá trị đơn hàng:</p>
				<p><?= Translate::discount() ?>: </p>
				<p><?= Translate::total_amount() ?>:</p>
			</div>
			<div class="col-sm-6 value-item">
				<p><?= number_format($total) ?></p>
				<p><?= Yii::$app->getUser()->getIdentity()->discount_percent ?>%</p>
				<p><?= number_format($model->getTotalAmount($model->id)) ?></p>
			</div>

		</div>
	</div>
	<div class="row action-pager">
		<div class="col-sm-6 action-item order-accept">
			<?= Html::submitButton(Translate::update_x(Translate::order()), ['class' => 'fleft']) ?>
		</div>
	</div>
	<?php ActiveForm::end() ?>
</div>
<script>
	$(".add-form").click(function() {
		var number  = $(".items").find(".item-detail").length;
		var context = $("<div class='item-detail' >" + $(".item-detail").html() + "</div>");
		context.find("input,select").val("");
		context.find(".product-select select").removeAttr("data-price");
		context.appendTo(".items").find('input:first').val(parseInt(number) + 1);
		$('.items .item-detail:last div:nth-child(4) select:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][product_id]');
		$('.items .item-detail:last div:nth-child(5) input:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][quantity]');
		return false;
	});
	$(document).on("change", ".category-select .form-control", function() {
		var context           = $(this).closest(".item-detail");
		var dependentDropdown = context.find("select[id='orderitem-product_id']");
		dependentDropdown.hide();
		dependentDropdown.parent().append('<i class="fa fa-spin fa-spinner"></i>');
		$.ajax({
			url    : "/order/orderitem",
			type   : "post",
			data   : {
				category: $(this).val()
			},
			success: function(data) {
				setTimeout(function() {
					dependentDropdown.parent().find('.fa').remove();
					dependentDropdown.show();
					dependentDropdown.html(data);
				}, 500);
			}
		});
	});
	function numberFormat(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function originFormat(x) {
		return x.toString().replace(/,/g, "");
	}
	var sum = 0;
	$(document).on("change", ".product-select .form-control", function() {
		var context  = $(this).closest(".item-detail");
		var codeGen  = context.find("input[name='code']");
		var quantity = context.find("input[id='orderitem-quantity']");
		var price    = context.find("input[name='OrderItem[price]']");
		$.ajax({
			url     : "/order/orderitem",
			type    : "post",
			data    : {
				product: $(this).val()
			},
			dataType: "json",
			success : function(data) {
				context.find(".product-select .form-control option:selected").attr("data-price", data.product_price);
				context.find(".product-select .form-control option:selected").attr("data-sale", data.sale_off);
				codeGen.val(data.product_code);
				quantity.val(1);
				sub_total_price_event(context.find('.quantity input'));
			}
		})
	});
	$(document).on("change", ".quantity .form-control", function() {
		sub_total_price_event($(this));
	});
	$(document).on("keyup", ".quantity .form-control", function() {
		sub_total_price_event($(this));
	});
	function grand_total_price_event() {
		var sub_total = 0;
		$("input[name='OrderItem[price]']").each(function() {
			sub_total += Number(originFormat($(this).val()));
		});
		var discount_value = parseFloat($(".value-item p:nth-child(2)").text());
		var grand_total    = sub_total - (sub_total * discount_value / 100);
		$(".value-item p:nth-child(1)").html(numberFormat(sub_total) + " vnđ");
		$(".value-item p:nth-child(3)").html(numberFormat(Math.round(grand_total) + " vnđ"));
	}
	function sub_total_price_event(selector) {
		var context                   = selector.closest(".item-detail");
		var origin_price_value        = context.find(".product-select select option:selected").data("price");
		var sale_off_by_product_value = context.find(".product-select select option:selected").data("sale");

		if(origin_price_value == null) {
			origin_price_value        = context.find(".product-select select").data("price");
			sale_off_by_product_value = context.find(".product-select select").data("sale")
		}
		if(typeof sale_off_by_product_value == typeof undefined) {
			sale_off_by_product_value = 0;
		}
		if(origin_price_value != '' && origin_price_value != undefined) {
			var quantity        = selector.val();
			var sub_total       = context.find("input[name='OrderItem[price]']");
			var sub_total_value = (origin_price_value - (origin_price_value * sale_off_by_product_value)) * quantity;
			sub_total.val(numberFormat(sub_total_value));
			grand_total_price_event();
		}
	}
	<?php
	if (Yii::$app->session->hasFlash('success')) { ?>
	alert('<?= Yii::$app->session->getFlash('success')?>');
	<?php } elseif (Yii::$app->session->hasFlash('failed')) { ?>
	alert('<?= Yii::$app->session->getFlash('failed')?>');
	<?php } ?>
</script>