<?php
use app\models\Category;
use navatech\language\Translate;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel yii\data\ActiveDataProvider */
/* @var $model app\models\Category */
$this->title                   = Translate::category();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::create_x(Translate::category()), [
			'create',
			'type' => $_GET['type'],
		], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			[
				'class'  => 'yii\grid\SerialColumn',
				'header' => 'STT',
			],
			[
				'attribute' => 'parent_id',
				'value'     => function(Category $data) {
					return $data->getCategoryById($data->parent_id);
				},
				'filter'    => $model->getParentCategory($_GET['type']),
			],
			'name',
			[
				'attribute' => 'status',
				'value'     => function(Category $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			[
				'attribute' => 'image',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Category $data) {
					return Html::img($data->getPictureUrl('image'), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			[
				'class'  => 'yii\grid\ActionColumn',
				
				'header' => Translate::Actions(),
			],
		],
	]); ?>
</div>
