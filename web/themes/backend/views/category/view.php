<?php
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
$this->title                   = $model->name;
$this->params['breadcrumbs'][] = [
	'label' => Translate::category(),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::update(), [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Translate::delete(), [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			[
				'attribute' => 'parent_id',
				'value'     => $model->getCategoryById($model->parent_id),
			],
			'name',
			'description',
			[
				'attribute' => 'type',
				'value'     => $model->getCategoryType(),
			],
			'order',
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
			[
				'attribute' => 'image',
				'format'    => 'raw',
				'value'     => Html::img($model->getPictureUrl('image'), ['class' => 'img-thumbnail']),
			],
		],
	]) ?>

</div>
