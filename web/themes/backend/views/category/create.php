<?php
use navatech\language\Translate;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
$this->title                   = Translate::create_x(Translate::category());
$this->params['breadcrumbs'][] = [
	'label' => Translate::category(),
	'url'   => [
		'index',
		'type' => $type,
	],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
		'type'  => $type,
	]) ?>

</div>
