<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
$this->title                   = $model->title;
$this->params['breadcrumbs'][] = [
	'label' => 'Sliders',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Cập nhật', [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Xóa', [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'title',
			[
				'attribute' => 'image',
				'value'     => Html::img($model->getPictureUrl('image'), ['class' => 'img-thumbnail']),
				'format'    => 'raw',
			],
			'url:url',
			'sort',
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
		],
	]) ?>

</div>
