<?php
use app\models\Slider;
use app\modules\admin\controllers\ReplyController;
use app\modules\admin\controllers\SliderController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::slider();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::create_x('Slider'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'image',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Slider $data) {
					return Html::img($data->getPictureUrl('image'), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			'title',
			'url:url',
			'sort',
			[
				'attribute' => 'status',
				'value'     => function(Slider $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(SliderController::className(), 'view'),
					'update' => RoleChecker::isAuth(SliderController::className(), 'update'),
					'delete' => RoleChecker::isAuth(SliderController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
