<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(['layout' => 'horizontal',]); ?>

	<?= $form->field($model, 'username', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'password', ['labelOptions' => ['class' => 'control-label col-sm-3']])->passwordInput() ?>

	<?= $form->field($model, 'email', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?php if (isset($_GET['id'])) {
		if ((Yii::$app->user->identity) !== null) {
			if (Yii::$app->getUser()->getIdentity()->id != $_GET['id']) { ?>
				<?= $form->field($model, 'role_id', ['labelOptions' => ['class' => 'control-label col-sm-3']])->dropDownList($model->getAllRole($id = null)) ?>
			<?php }
		}
	} else { ?>
		<?= $form->field($model, 'role_id', ['labelOptions' => ['class' => 'control-label col-sm-3']])->dropDownList($model->getAllRole($id = null)) ?>

	<?php } ?>
	<div class="form-group  col-sm-4">
		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
