<?php
use navatech\language\Translate;
use navatech\roxymce\widgets\RoxyMceWidget;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */
/* @var $reply app\models\Contact */
/* @var $dataProvider app\models\Contact */
$this->title                   = Translate::contact() . ' #' . $model->id;
$this->params['breadcrumbs'][] = [
	'label' => Translate::contact(),
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-view">

	<h1><?= Html::encode($this->title) ?></h1>
	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'fullname',
			'email:email',
			'phone',
			'title',
			[
				'attribute' => 'address',
				'format'    => 'raw',
			],
			[
				'attribute' => 'status',
				'value'     => $model->getContactStatus($model->status),
			],
		],
	]) ?>

</div>
<div class="reply-index">
	<!--	 GridView::widget([-->
	<!--		'dataProvider' => $dataProvider,-->
	<!--		'columns'      => [-->
	<!--			['class' => 'yii\grid\SerialColumn'],-->
	<!--			'from',-->
	<!--			'to',-->
	<!--			'fullname',-->
	<!--			'subject',-->
	<!--			// 'content:ntext',-->
	<!--			'created_date',-->
	<!--			[-->
	<!--				'attribute' => 'detail',-->
	<!--				'value'     => function(\app\models\Reply $data) {-->
	<!--					return Html::a(Translate::details(), Url::to('/admin/reply/view?id=' . $data->id));-->
	<!--				},-->
	<!--				'format'    => 'raw',-->
	<!--			],-->
	<!--		],-->
	<!--	]); -->
</div>
<div class="reply-form">
	<h3><?php echo Translate::reply() ?></h3>
	<?php $form = ActiveForm::begin(); ?>
	<?= $form->field($reply, 'subject')->textInput(['maxlength' => true])->label(Translate::subject()) ?>

	<?= $form->field($reply, 'content')->widget(RoxyMceWidget::className(), [
		'model'       => $reply,
		//default name of textarea which will be auto generated, NOT REQUIRED if using 'model' section
		'action'      => Url::to(['/roxymce/default']),
		//default value of current textarea, NOT REQUIRED
		'options'     => [//TinyMce options, NOT REQUIRED, see https://www.tinymce.com/docs/
			'title' => 'RoxyMCE',
			//title of roxymce dialog, NOT REQUIRED
		],
		'htmlOptions' => [
			'rows' => 6,
		],
	])->label(Translate::content()); ?>

	<div class="form-group">
		<?= Html::submitButton('Gửi', ['class' => $reply->isNewRecord ? 'btn btn-success ' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>