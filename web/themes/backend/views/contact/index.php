<?php
use app\models\Contact;
use app\modules\admin\controllers\ContactController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::list_x(Translate::contact());
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'fullname',
			'phone',
			'email',
			'address',
			// 'content',
			// 'created_date',
			[
				'attribute' => 'status',
				'value'     => function(Contact $data) {
					return $data->getContactStatus($data->status);
				},
				'filter'    => $searchModel->getContactStatus(),
			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'template'       => '{view}{delete}',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(ContactController::className(), 'view'),
					'update' => RoleChecker::isAuth(ContactController::className(), 'update'),
					'delete' => RoleChecker::isAuth(ContactController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
