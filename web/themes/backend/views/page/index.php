<?php
use app\models\Page;
use app\modules\admin\controllers\PageController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::page();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Thêm mới Trang', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'title',
			[
				'attribute' => 'pagelink',
				'value'     => function(Page $data) {
					return Url::base(true).'/'.Yii::$app->language.'/trang/'.$data->removeSign($data->title).'-'.$data->id;
				},

			],
			'order',
			[
				'attribute' => 'status',
				'value'     => function(Page $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(PageController::className(), 'view'),
					'update' => RoleChecker::isAuth(PageController::className(), 'update'),
					'delete' => RoleChecker::isAuth(PageController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
