<?php
use app\modules\admin\controllers\BannerController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Translate::create_x('banner'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'image',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::img($data->getImageUrl(), [
                        'class'=>'img-thumbnail',
                    ]);
                },
            ],
            'title',
            [
                'attribute'=>'position',
                'value' => function ($data) {
                    return $data->getPosition($data->position);
                },
            ],

            'url:url',
            'order',
            [
                'attribute'=>'status',
                'value' => function ($data) {
                    return $data->getStatus($data->status);
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view'   => RoleChecker::isAuth(BannerController::className(), 'view'),
                    'update' => RoleChecker::isAuth(BannerController::className(), 'update'),
                    'delete' => RoleChecker::isAuth(BannerController::className(), 'delete'),
                ],
            ],
        ],
    ]); ?>

</div>
