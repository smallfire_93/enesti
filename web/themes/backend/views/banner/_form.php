<?php
use navatech\language\models\Language;
use yii\helpers\Html;
use \navatech\language\Translate;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

	<?php $form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>

	<ul class="nav nav-tabs" role="tablist">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<li class="<?= ($key == 0) ? 'active' : '' ?>">
				<a href="#tab_<?= $language->code ?>" role="tab" data-toggle="tab"><?= $language->name ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<div class="<?= ($key == 0) ? 'active in' : '' ?> tab-pane fade" id="tab_<?= $language->code ?>">
				<?php
				echo $form->field($model, 'title_' . $language->code, ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('name', $language->code),
				])->label(Translate::title());
				?>

			</div>
		<?php endforeach; ?>

	</div>
	<?php
	echo $form->field($model, 'img')->widget(FileInput::className(), [
		'options'       => ['accept' => 'image/*'],
		'pluginOptions' => [
			'allowedFileExtensions'                          => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'                                     => false,
			$model->getIsNewRecord() ? '' : 'initialPreview' => [
				Html::img($model->getImageUrl(), ['class' => 'file-preview-image']),
			],
		],
	])->label(Translate::image());
	?>
	<?php
	$positions = $model->getPosition();
	echo $form->field($model, 'position')->dropDownList($positions, ['prompt' => Translate::position()]);
	?>

	<?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'order')->textInput() ?>

	<?= $form->field($model, 'status')->checkbox([
		'class'    => 'make-switch',
		'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
	]); ?>

	<div class="form-group col-sm-4">
		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
