<?php
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */
$this->title                   = $model->title;
$this->params['breadcrumbs'][] = [
	'label' => 'Banners',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::update(), [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Translate::delete(), [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			[
				'attribute' => 'image',
				'value'     => Html::img($model->getPictureUrl('image')),
				'format'    => 'raw',
			],
			'position',
			'url:url',
			'order',
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
		],
	]) ?>

</div>
