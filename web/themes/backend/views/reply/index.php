<?php
use app\modules\admin\controllers\ReplyController;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReplySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Replies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reply-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Reply', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'contact_id',
			'from',
			'to',
			'fullname',
			// 'subject',
			// 'content:ntext',
			// 'created_date',
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(ReplyController::className(), 'view'),
					'update' => RoleChecker::isAuth(ReplyController::className(), 'update'),
					'delete' => RoleChecker::isAuth(ReplyController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
