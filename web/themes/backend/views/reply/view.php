<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reply */
$this->title                   = $model->subject;
$this->params['breadcrumbs'][] = [
	'label' => 'Replies',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reply-view">

	<h1><?= Html::encode($this->title) ?></h1>
	
	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'from',
			'to',
			'fullname',
			'subject',
			[
				'attribute' => 'content',
				'format'    => 'raw',
			],
			'created_date',
		],
	]) ?>

</div>
