<?php
use app\modules\admin\controllers\OrderItemController;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Order Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-item-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Order Item', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'order_item_id',
			'order_id',
			'product_id',
			'quantity',
			'price',
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(OrderItemController::className(), 'view'),
					'update' => RoleChecker::isAuth(OrderItemController::className(), 'update'),
					'delete' => RoleChecker::isAuth(OrderItemController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
