<?php
use app\models\Client;
use app\modules\admin\controllers\ClientController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::client();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::create_x(Translate::client()), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'name',
			[
				'attribute' => 'logo',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Client $data) {
					return Html::img($data->getPictureUrl('logo'), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			'website',
			[
				'attribute' => 'status',
				'value'     => function(Client $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(ClientController::className(), 'view'),
					'update' => RoleChecker::isAuth(ClientController::className(), 'update'),
					'delete' => RoleChecker::isAuth(ClientController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
