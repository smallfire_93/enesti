<?php
use kartik\file\FileInput;
use navatech\language\models\Language;
use navatech\language\Translate;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Popup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="popup-form">

	<?php $form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>
	<ul class="nav nav-tabs" role="tablist">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<li class="<?= ($key == 0) ? 'active' : '' ?>">
				<a href="#tab_<?= $language->code ?>" role="tab" data-toggle="tab"><?= $language->name ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<div class="<?= ($key == 0) ? 'active in' : '' ?> tab-pane fade" id="tab_<?= $language->code ?>">
				<?php
				echo $form->field($model, 'title_' . $language->code)->textInput([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('title', $language->code),
				])->label(Translate::title());
				echo $form->field($model, 'content_' . $language->code)->textarea([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('content', $language->code),
				])->label(Translate::content());
				?>
			</div>
		<?php endforeach; ?>

	</div>

	<?php
	echo $form->field($model, 'img')->widget(FileInput::className(), [
		'options'       => ['accept' => 'image/*'],
		'pluginOptions' => [
			'allowedFileExtensions'                          => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'                                     => false,
			$model->getIsNewRecord() ? '' : 'initialPreview' => [
				Html::img($model->getPictureUrl('background'), ['class' => 'file-preview-image']),
			],
		],
	]);
	?>
	<?= $form->field($model, 'status')->checkbox([
		'class'    => 'make-switch',
		'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
	]) ?>
	<?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

	<div class="form-group col-sm-4 ">
		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
