<?php
use app\models\Popup;
use app\modules\admin\controllers\PopupController;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PopupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Popups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="popup-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(\navatech\language\Translate::create_x('popups'), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'title',
			'content:ntext',
			[
				'attribute' => 'background',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Popup $data) {
					return Html::img($data->getPictureUrl('background'), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			[
				'attribute' => 'status',
				'value'     => function(Popup $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			'url:url',
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(PopupController::className(), 'view'),
					'update' => RoleChecker::isAuth(PopupController::className(), 'update'),
					'delete' => RoleChecker::isAuth(PopupController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
