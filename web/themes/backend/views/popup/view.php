<?php
use navatech\language\Translate;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Popup */
$this->title                   = $model->title;
$this->params['breadcrumbs'][] = [
	'label' => 'Popups',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="popup-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a(Translate::update(), [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Translate::delete(), [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'title',
			'content:ntext',
			[
				'attribute' => 'background',
				'format'    => 'raw',
				'value'     => Html::img($model->getPictureUrl('background'), ['class' => 'img-thumbnail']),
			],
			[
				'attribute' => 'status',
				'value'     => $model->getStatus($model->status),
			],
			'url:url',
		],
	]) ?>

</div>
