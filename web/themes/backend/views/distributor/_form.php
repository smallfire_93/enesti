<?php
use kartik\file\FileInput;
use kartik\widgets\DatePicker;
use navatech\language\models\Language;
use navatech\language\Translate;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

\yii\validators\ValidationAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Distributor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distributor-form">

	<?php $form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>
	<ul class="nav nav-tabs" role="tablist">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<li class="<?= ($key == 0) ? 'active' : '' ?>">
				<a href="#tab_<?= $language->code ?>" role="tab" data-toggle="tab"><?= $language->name ?></a></li>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<?php foreach (Language::getAllLanguages() as $key => $language): ?>
			<div class="<?= ($key == 0) ? 'active in' : '' ?> tab-pane fade" id="tab_<?= $language->code ?>">
				<?php
				echo $form->field($model, 'address_' . $language->code, ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput([
					'value' => $model->getIsNewRecord() ? '' : $model->getTranslateAttribute('address', $language->code),
				])->label(Translate::address());
				?>
			</div>
		<?php endforeach; ?>
	</div>
	<?= $form->field($model, 'full_name', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
	<?php
	echo $form->field($model, 'img')->widget(FileInput::className(), [
		'options'       => ['accept' => 'image/*'],
		'pluginOptions' => [
			'allowedFileExtensions'                          => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'                                     => false,
			$model->getIsNewRecord() ? '' : 'initialPreview' => [
				Html::img($model->getPictureUrl('logo'), ['class' => 'file-preview-image']),
			],
		],
	]);
	?>
	<?= $form->field($model, 'website', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'area', ['labelOptions' => ['class' => 'control-label col-sm-3']])->dropDownList($model->getArea()) ?>
	<?= $form->field($model, 'payment', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'phone_number', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'transport_mode', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
	<?= $form->field($model, 'city', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'shipping_address', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'type', ['labelOptions' => ['class' => 'control-label col-sm-3']])->dropDownList($model->getType()) ?>
	<h3><?= Translate::information() . ' ' . Translate::account() . ' ' . Translate::distributor() ?></h3>
	<?= $form->field($model, 'username', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
	<?= $form->field($model, 'password_hash', ['labelOptions' => ['class' => 'control-label col-sm-3']])->passwordInput(['value' => '']) ?>
	<?= $form->field($model, 'fullname', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>
	<?= $form->field($model, 'email', ['labelOptions' => ['class' => 'control-label col-sm-3']])->textInput() ?>

	<?= $form->field($model, 'birthday')->widget(DatePicker::className(), [
		'options'       => ['placeholder' => Translate::birthday()],
		'pluginOptions' => [
			'autoclose' => true,
			'format'    => 'yyyy-mm-dd',
		],
	]) ?>
	<?= $form->field($model, 'discount', [
		'labelOptions' => [
			'class' => 'control-label col-sm-3',
		],
	])->textInput([
		'type'        => 'number',
		'placeholder' => 'Đơn vị %',
		'value'       => $model->isNewRecord ? '' : $model->discount * 100,
	]) ?>
	<?= $form->field($model, 'show_home')->checkbox([
		'class'    => 'make-switch',
		'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
	]) ?>
	<?= $form->field($model, 'status')->checkbox([
		'class'    => 'make-switch',
		'template' => '<label class="control-label col-sm-3">{label}</label><div class="col-sm-8">{input}</div>',
	]) ?>
	<div class="form-group col-sm-4">
		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
