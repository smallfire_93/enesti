<?php
use app\models\Distributor;
use app\modules\admin\controllers\DistributorController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DistributorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = Translate::distributor();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distributor-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a(Translate::distributor(), ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'logo',
				'filter'    => false,
				'format'    => 'raw',
				'value'     => function(Distributor $data) {
					return Html::img($data->getLogoUrl(), [
						'class' => 'img-thumbnail',
					]);
				},
			],
			'full_name',
			[
				'attribute' => 'type',
				'value'     => function(Distributor $data) {
					return $data->getType($data->type);
				},
				'filter'    => $searchModel->getType(),
			],
			[
				'attribute' => 'area',
				'value'     => function(Distributor $data) {
					return $data->getArea($data->area);
				},
				'filter'    => $searchModel->getArea(),
			],
			[
				'attribute' => 'show_home',
				'value'     => function(Distributor $data) {
					return $data->getShowhome($data->show_home);
				},
				'filter'    => $searchModel->getShowhome(),
			],
			[
				'attribute' => 'status',
				'value'     => function(Distributor $data) {
					return $data->getStatus($data->status);
				},
				'filter'    => $searchModel->getStatus(),
			],
			//			[
			//				'attribute' => 'type',
			//				'value'     => function(Distributor $data) {
			//					return $data->getType($data->type);
			//				},
			//				'filter'    => $searchModel->getType(),
			//			],
			[
				'class'          => 'yii\grid\ActionColumn',
				'visibleButtons' => [
					'view'   => RoleChecker::isAuth(DistributorController::className(), 'view'),
					'update' => RoleChecker::isAuth(DistributorController::className(), 'update'),
					'delete' => RoleChecker::isAuth(DistributorController::className(), 'delete'),
				],
			],
		],
	]); ?>
</div>
