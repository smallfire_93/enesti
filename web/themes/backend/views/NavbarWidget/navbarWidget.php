<?php
use navatech\language\Translate;
use navatech\language\widgets\LanguageWidget;
use yii\helpers\Url;

?>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top ">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner  ">
		<!-- BEGIN LOGO -->
		<div class="col-sm-3 logo ">
			<a href="<?= Url::to(['/admin/default/index']) ?>">

				<img src="<?= Yii::$app->setting->get('general_logo') ?>" alt="logo" style="max-height: 46px;background: #fff; padding: 5px;" class="logo-default"/>
			</a>
		</div>
		<div class="menu-toggler sidebar-toggler"></div>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN RESPONSIVE MENU TOGGLER -->
	<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
	<!-- END RESPONSIVE MENU TOGGLER -->
	<!-- BEGIN TOP NAVIGATION MENU -->
	<div class="top-menu ">
		<div style="clear: both; display: inline-block;padding-top: 5px;" class="pull-right">
			<?php echo LanguageWidget::widget([
				'type' => 'selector',
			]); ?>
		</div>
		<ul class="nav navbar-nav pull-right">

			<!-- BEGIN USER LOGIN DROPDOWN -->
			<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
			<li class="dropdown dropdown-user">
				<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="username username-hide-on-mobile"> <?php if ((Yii::$app->user->identity) !== null) {
							echo Yii::$app->getUser()->getIdentity()->username;
						} ?> </span>
					<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-default">
					<li>
						<a href="<?php if ((Yii::$app->user->identity) !== null) {
							echo Url::to([
								'/admin/account/view',
								'id' => Yii::$app->getUser()->getIdentity()->id,
							]);
						} ?>">
							<i class="icon-user"></i> <?php echo Translate::profile() ?>
						</a>
					</li>
					<li>
						<a href="<?= Url::to(['/admin/user/logout']) ?>" data-method="POST">
							<i class="icon-key"></i> <?php echo Translate::logout() ?></a>
					</li>
				</ul>

			</li>
		</ul>

	</div>
</div>
<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->


