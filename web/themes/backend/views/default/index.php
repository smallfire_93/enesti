<?php
use \navatech\language\Translate;

$this->title                   = Yii::$app->setting->get('general_title');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="index-2.html"><?= Translate::home(); ?></a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span><?= Translate::dashboard() ?></span>
		</li>
	</ul>
</div>
<!-- END PAGE BAR -->
<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">
				<i class="fa fa-close"></i>
			</button>
			<i class="icon-ok green"></i> <?= Translate::welcome_text(); ?>
		</div>
		<div class="space-6"></div>
	</div>
</div>
