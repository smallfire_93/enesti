<?php
use kartik\alert\Alert;
use kartik\widgets\DatePicker;
use navatech\language\Translate;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Translate::account_management();
echo Alert::widget([
	'options' => [
		'class' => 'alert-info',
	],
	'body'    => Translate::profile_alert(),
]);
?>
<h2 class="profile-title"><?= Translate::account_management(); ?></h2>
<div class="">
	<?= /** @var \app\models\Distributor $model */
	Translate::hello() . ' ' . $model->username ?>
	<?= Translate::profile_instruction() ?>
</div>
<div id="profile">
	<div class="logout-profile"><?= Html::a(Translate::logout(), Url::to([
			'/user/logout',
			'language' => Yii::$app->language,
		]), [
			'data-method' => 'post',
		]) ?></div>
	<?php ActiveForm::begin() ?>

	<div class="info-group">
		<h3><?= Translate::full_name() ?> <?= Translate::customer() ?>:
		</h3>
		<div class="info-detail">
			<?= Html::activeTextInput($model, 'fullname', [
				'value'       => $model->fullname,
				'class'       => 'form-control',
				'style'       => 'max-width:280px',
				'placeholder' => Translate::full_name(),
				'readOnly'    => true,
			]) ?><br>
			<?= DatePicker::widget([
				'model'         => $model,
				'attribute'     => 'birthday',
				'removeButton'  => false,
				'readonly'      => true,
				'pickerButton'  => [
					'icon' => '',
				],
				'pluginOptions' => [
					'format'    => 'yyyy-mm-dd',
					'autoclose' => true,
				],
				'options'       => [
					'style'       => 'max-width:253px;',
					'placeholder' => Translate::birthday(),
				],
			]) ?>
			<br>
			<?= Html::activeTextInput($model, 'address_' . Yii::$app->language, [
				'value'       => $model->getTranslateAttribute('address', Yii::$app->language),
				'class'       => 'form-control',
				'style'       => 'max-width:280px',
				'placeholder' => Translate::address(),
				'readOnly'    => true,
			]) ?>
			<br>
			<?= Html::activeTextInput($model, 'phone_number', [
				'value'       => $model->phone_number,
				'class'       => 'form-control',
				'style'       => 'max-width:280px',
				'placeholder' => Translate::phone(),
				'readOnly'    => true,
			]) ?>
			<br>
		</div>
		<a href="javascript:void(0)"><?= Translate::update_info() ?></a>
	</div>

	<div class="info-group">
		<h3><?= Translate::promotion_info() ?>:</h3>
		<div class="info-detail">

			Qua email: <?= Html::activeTextInput($model, 'email', [
				'value'    => $model->email,
				'class'    => 'form-control',
				'style'    => 'max-width:280px',
				'readOnly' => true,
			]) ?>

		</div>
		<a href="javascript:void(0)"><?= Translate::update_info() ?></a>
	</div>

	<div class="info-group">
		<h3><?= Translate::payment() ?>:</h3>
		<div class="info-detail">

			<?= Html::activeTextInput($model, 'payment', [
				'value'       => $model->payment,
				'class'       => 'form-control',
				'style'       => 'max-width:280px',
				'placeholder' => Translate::payment(),
				'readOnly'    => true,
			]) ?>

		</div>
		<a href="javascript:void(0)"><?= Translate::update_info() ?></a>
	</div>

	<div class="info-group">
		<h3><?= Translate::transport_mode() ?> - <?= Translate::shipping_address() ?>:</h3>
		<div class="info-detail">

			<?= Html::activeTextInput($model, 'transport_mode', [
				'value'       => $model->transport_mode,
				'class'       => 'form-control',
				'style'       => 'max-width:280px',
				'placeholder' => Translate::transport_mode(),
				'readOnly'    => true,
			]) ?>
			<br>
			<?= Html::activeTextInput($model, 'shipping_address', [
				'value'       => $model->shipping_address,
				'class'       => 'form-control',
				'style'       => 'max-width:280px',
				'placeholder' => Translate::shipping_address(),
				'readOnly'    => true,
			]) ?>

		</div>
		<div class="row action-pager">
			<div class="col-sm-6 col-xs-6 action-item order-accept" id="submit">

			</div>
		</div>
		<a href="javascript:void(0)"><?= Translate::update_info() ?></a>
	</div>

	<?php ActiveForm::end() ?>
</div>

<script>
	$(".info-group a").click(function() {
		$(this).parent().find(".info-detail input").removeAttr("readonly");
		$(this).closest("#profile").find(".order-accept").empty();
		$(this).closest("#profile").find(".order-accept").append('<?= Html::submitButton(Translate::update(), [
			'style' => "position: absolute; right: -76px; top: -15px ",
		]) ?>');
	});
	<?php if (Yii::$app->session->hasFlash("success")) {?>
	alert('<?php echo Yii::$app->session->getFlash("success")?>');
	<?php    } elseif(Yii::$app->session->hasFlash("failed")) {?>
	alert('<?php    echo Yii::$app->session->getFlash("failed")?>');
	<?php } ?>
</script>
