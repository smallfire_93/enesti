<?php
use navatech\language\Translate;

/** @var \app\models\OrderItem $orderItem */
/** @var \app\models\Order $order */
/** @var \app\models\Order $data_order */
$this->title = Translate::place_order();
?>

<?php
echo $this->render('_form', [
	'orderItem'  => $orderItem,
	'order'      => $order,
	'data_order' => $data_order,
]);
?>

