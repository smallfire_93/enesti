<?php /** @var \app\models\Order $order */
use app\models\Model;
use app\models\OrderItem;
use app\models\Product;
use navatech\language\Translate;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = Translate::order();
foreach ($orders as $order) { ?>
	<div class="order-toggle">
		<div class="order-info row">
			<div class="col-sm-4 order">
				<p class="order-header"><?= Translate::order() ?> #000<?= $order->id ?></p>
				<p><?= Translate::place_order(Translate::date()) ?> <?= Model::format($order->created_date, 'Y-m-d H:i:s', 'd-m-Y H:i:s') ?></p>
			</div>
			<div class="col-sm-3 order">
				<p class="order-header"><?= Translate::total_amount() ?></p>
				<p><?= number_format($order->getTotalAmount($order->id)) ?> VNĐ</p>
			</div>
			<div class="col-sm-3 order">
				<p class="order-header"><?= Translate::payment() ?></p>
				<p><?= $order->getOrderStatus($order->status) ?></p>
			</div>
			<div class="col-sm-2 order order-open">
				<p class="order-header">Mở</p>
				<p><?= Translate::view_detail() ?></p>
				<div class="arrow-right "></div>
			</div>
		</div>
		<div class="order-items">
			<div class="list-header row">
				<div class="col-sm-1 text-center">
					<p>STT</p>
				</div>
				<div class="col-sm-1 text-center">
				</div>
				<div class="col-sm-3 text-center">
					<p><?= Translate::product_name() ?></p>
				</div>
				<div class="col-sm-1 text-center">
					<p>Dung tích</p>
				</div>
				<div class="col-sm-2 text-center"><p><?= Translate::price() ?></p></div>
				<div class="col-sm-2 text-center"><p><?= Translate::quantity() ?></p></div>
				<div class="col-sm-2 text-center"><p><?= Translate::total_amount() ?></p></div>
			</div>
			<?php $items = $order->getOrderItemById($order->id);
			$i           = 0;
			/** @var OrderItem $item */
			foreach ($items as $item) {
				$product = Product::getProductById($item->product_id);
				$sale    = ($item->price) * ($product->sale_off);
				$i ++ ?>

				<div class="grid-border row">
					<div class="col-sm-1 text-center stt">
						<p><?= $i ?></p>
					</div>
					<div class="col-sm-1">
						<?= Html::img(Url::to($item->getProductImage($item->product_id)), ['class' => 'order-thumbnail']); ?>
					</div>
					<div class="col-sm-3 product-title">
						<p><?= $product->title ?></p>
					</div>
					<div class="col-sm-1 product-detail text-center">
						<p><?= $product->capability ?></p>
					</div>
					<div class="col-sm-2 product-detail text-center">
						<p><?= number_format($real_price = $item->price - $sale) ?> VNĐ</p>
					</div>
					<div class="col-sm-2 product-detail text-center"><p><?= $item->quantity ?></p></div>
					<div class="col-sm-2 product-detail text-center">
						<p><?= number_format($real_price * ($item->quantity)) ?> VND</p></div>
				</div>
			<?php } ?>
		</div>
	</div>
<?php } ?>
<?php
/** @var app\models\Order $pagination */
echo LinkPager::widget([
	'pagination' => $pagination,
]);
?>