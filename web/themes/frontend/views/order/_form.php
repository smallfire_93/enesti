<?php
use app\models\Category;
use app\modules\admin\controllers\OrderItemController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\helpers\Html;

/** @var \app\models\OrderItem $orderItem */
?>
<div class="wrapper-order">
	<?php
	if (!Yii::$app->user->isGuest){ ?>
	<?php if (RoleChecker::isAuth(OrderItemController::className(), 'create', Yii::$app->getUser()->getIdentity()->role_id)){ ?>
	<div class="order-title text-center ">
		<h3>ĐƠN HÀNG</h3>
		<p>(Sử dụng chức năng bên dưới để lên đơn đặt hàng)</p>
	</div>
	<div class="item-header">
		<div class="id grid-display"><p style="text-transform: uppercase"><?= Translate::sort() ?></p></div>
		<div class="code grid-display"><p style="text-transform: uppercase"><?= Translate::code() ?></p></div>
		<div class="category-select grid-display">
			<p style="text-transform: uppercase"><?= Translate::x_category(Translate::product()) ?></p>
		</div>
		<div class="product-select grid-display"><p style="text-transform: uppercase"><?= Translate::product() ?></p>
		</div>
		<div class="quantity grid-display"><p style="text-transform: uppercase"><?= Translate::quantity() ?></p></div>
		<div class="price-show grid-display"><p style="text-transform: uppercase"><?= Translate::total_amount() ?></p>
		</div>
	</div>
	<?php \yii\widgets\ActiveForm::begin() ?>
	<div class="order-form" style="display: none">
		<?= /** @var app\models\Order $order */
		/** @var app\models\Order $data_order */
		Html::activeTextInput($order, 'distributor_id', [
			'style' => 'display:none',
			'value' => $data_order['distributor_id'],
		]) ?>
		<?= Html::activeTextInput($order, 'status', [
			'style' => 'display:none',
			'value' => 0,
		]) ?>
		<?= Html::activeTextInput($order, 'shipping_address', [
			'style' => 'display:none',
			'value' => $data_order['shipping_address'],
		]) ?>
		<?= Html::activeTextInput($order, 'phone_number', [
			'style' => 'display:none',
			'value' => $data_order['phone_number'],
		]) ?>
	</div>
	<div class="items">
		<?php if ($order->isNewRecord) { ?>

			<?php for (
				$i = 1; $i < 6; $i ++
			) { ?>
				<div class="item-detail">
					<div class="id grid-display"><?= Html::input('text', '', $i, [
							'class'    => 'ordinal form-control form-height form-boder',
							"disabled" => true,
						]) ?></div>
					<div class="code grid-display"><?= Html::input('text', 'code', $orderItem->isNewRecord ? '' : $orderItem->getProductById($orderItem->product_id)->code, [
							'class'    => 'form-control form-height form-boder',
							"disabled" => true,
						]) ?></div>
					<div class="category-select grid-display"><?= Html::dropDownList('', $orderItem->isNewRecord ? '' : $orderItem->getCategoryIdItem($orderItem->id), Category::getCategoryOrder(), [
							'class'  => 'form-control form-height form-boder ',
							'prompt' => 'Chọn danh mục',
							'style'  => 'float:left',
						]) ?>
					</div>
					<div class="product-select grid-display">
						<div class="overflow"><?= Html::activeDropDownList($orderItem, 'product_id', [
							], [
								'name'   => 'OrderItem[' . $i . '][product_id]',
								'class'  => 'form-control form-height form-boder',
								'prompt' => 'Chọn sản phẩm',
								'style'  => 'float:left',
							]) ?></div>
					</div>
					<div class="quantity grid-display"><?= Html::activeTextInput($orderItem, 'quantity', [
							'class' => 'form-control form-height form-boder',
							'type'  => 'number',
							'min'   => 0,
							'name'  => 'OrderItem[' . $i . '][quantity]',
						]) ?></div>
					<div class="price-show grid-display"><?= Html::activeTextInput($orderItem, 'price', [
							'class'    => 'form-control form-height form-boder',
							'disabled' => true,
						]) ?></div>
				</div>
			<?php } ?>
		<?php } else {
			if (isset($_GET['id'])) {
				foreach ($orderItem as $item) { ?>

					<div class="item-detail">
						<div class="id grid-display">
							<div class="overflow"><?= Html::input('text', '', $item->id, [
									'class'    => 'ordinal form-control form-height form-boder',
									"disabled" => true,
								]) ?></div>
						</div>
						<div class="code grid-display">
							<div class="overflow"><?= Html::input('text', 'code', $item->getProductById($item->product_id)->code, [
									'class'    => 'form-control form-height form-boder',
									"disabled" => true,
								]) ?></div>
						</div>
						<div class="category-select grid-display">
							<div class="overflow"><?= Html::dropDownList('', $item->getCategoryIdItem($item->id), Category::getCategoryOrder(), [
									'class'  => 'form-control form-height form-boder ',
									'prompt' => 'Chọn danh mục',
								]) ?>
							</div>
						</div>
						<div class="product-select grid-display">
							<div class="overflow"><?= Html::activeDropDownList($item, 'product_id', [
								], [
									'name'   => 'OrderItem[' . $item->id . '][product_id]',
									'class'  => 'form-control form-height form-boder',
									'prompt' => 'Chọn sản phẩm',
								]) ?></div>
						</div>
						<div class="quantity grid-display">
							<div class="overflow"><?= Html::activeTextInput($item, 'quantity', [
									'class' => 'form-control form-height form-boder',
									'type'  => 'number',
									'name'  => 'OrderItem[' . $item->id . '][quantity]',
								]) ?></div>
						</div>
						<div class="price-show grid-display">
							<div class="overflow"><?= Html::activeTextInput($item, 'price', [
									'class'    => 'form-control form-height form-boder',
									'type'     => 'number',
									'disabled' => true,
								]) ?></div>
						</div>
					</div>
				<?php } ?>
			<?php }
		}
		?>
	</div>
	<div class="row action-pager ">
		<div class="col-sm-6 action-item add-item">
			<a class="fleft add-form" href=""><?= Translate::add_a_new_x(Translate::product()) ?></a>
		</div>
	</div>
</div>
	<div class=" row final-total">
		<div class="col-sm-6 total">
			<p><?= Translate::total() ?></p>
		</div>
		<div class="detail-total col-sm-6 ">
			<div class="col-sm-6 label-item">
				<p>Tổng giá trị đơn hàng:</p>
				<p><?= Translate::discount() ?>: </p>
				<p><?= Translate::total_amount() ?>:</p>
			</div>
			<div class="col-sm-6 value-item">
				<p>0</p>
				<p><?= Yii::$app->getUser()->getIdentity()->discount_percent ?>%</p>
				<p>0</p>
			</div>

		</div>
	</div>
	<div class="row action-pager">
		<div class="col-sm-6 action-item order-accept">
			<?= Html::submitButton(Translate::create_x(Translate::order()), ['class' => 'fleft']) ?>
		</div>
		<div class="col-sm-6 action-item order-cancel">
			<a class="fright" href=""><?= Translate::cancel(Translate::product()) ?></a>

		</div>
	</div>
<?php \yii\widgets\ActiveForm::end() ?>
<?php } else { ?>
	<p>Vui lòng
		<span style="color: #00a69c;"><a data-toggle="modal" data-target="#w0" style="text-decoration: none;  cursor: pointer;">đăng nhập</a></span> bằng tài khoản nhà phân phối hoặc đại lý để đặt hàng
	</p>
<?php } ?>
<?php } else { ?>
	<p style="font-size: 25px" class="login ">Vui lòng
		<span style="color: #00a69c;"><a data-toggle="modal" data-target="#w0" style="text-decoration: none;  cursor: pointer;">đăng nhập</a></span> để đặt hàng
	</p>
<?php } ?>

<script>
	$(".add-form").click(function() {
		var number  = $(".items").find(".item-detail").length;
		var context = $("<div class='item-detail' >" + $(".item-detail").html() + "</div>");
		context.find("input,select").val("");
		context.appendTo(".items").find('input:first').val(parseInt(number) + 1);
		$('.items .item-detail:last div:nth-child(4) select:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][product_id]');
		$('.items .item-detail:last div:nth-child(5) input:first').attr('name', 'OrderItem[' + parseInt(number + 1) + '][quantity]');
		return false;
	});
	$(document).on("change", ".category-select .form-control", function() {
		var context           = $(this).closest(".item-detail");
		var dependentDropdown = context.find("select[id='orderitem-product_id']");
		dependentDropdown.hide();
		dependentDropdown.parent().append('<i class="fa fa-spin fa-spinner"></i>');
		$.ajax({
			url    : "/order/orderitem",
			type   : "post",
			data   : {
				category: $(this).val()
			},
			success: function(data) {
				setTimeout(function() {
					dependentDropdown.parent().find('.fa').remove();
					dependentDropdown.show();
					dependentDropdown.html(data);
				}, 500);
			}
		});
	});
	function numberFormat(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	function originFormat(x) {
		return x.toString().replace(/,/g, "");
	}
	var sum = 0;
	$(document).on("change", ".product-select .form-control", function() {
		var context  = $(this).closest(".item-detail");
		var codeGen  = context.find("input[name='code']");
		var quantity = context.find("input[id='orderitem-quantity']");
		var price    = context.find("input[name='OrderItem[price]']");
		$.ajax({
			url     : "/order/orderitem",
			type    : "post",
			data    : {
				product: $(this).val()
			},
			dataType: "json",
			success : function(data) {
				context.find(".product-select .form-control option:selected").attr("data-price", data.product_price);
				context.find(".product-select .form-control option:selected").attr("data-sale", data.sale_off);
				codeGen.val(data.product_code);
				quantity.val(1);
				sub_total_price_event(context.find('.quantity input'));
			}
		})
	});
	$(document).on("change", ".quantity .form-control", function() {
		sub_total_price_event($(this));
	});
	$(document).on("keyup", ".quantity .form-control", function() {
		sub_total_price_event($(this));
	});
	function grand_total_price_event() {
		var sub_total = 0;
		$("input[name='OrderItem[price]']").each(function() {
			sub_total += Number(originFormat($(this).val()));
		});
		var discount_value = parseFloat($(".value-item p:nth-child(2)").text());
		var grand_total    = sub_total - (sub_total * discount_value / 100);
		$(".value-item p:nth-child(1)").html(numberFormat(sub_total) + " vnđ");
		$(".value-item p:nth-child(3)").html(numberFormat(Math.round(grand_total) + " vnđ"));
	}
	function sub_total_price_event(selector) {
		var context            = selector.closest(".item-detail");
		var origin_price_value = context.find(".product-select select option:selected").data("price");
		if(origin_price_value != '' && origin_price_value != undefined) {
			var quantity                  = selector.val();
			var sub_total                 = context.find("input[name='OrderItem[price]']");
			var sale_off_by_product_value = context.find(".product-select select option:selected").data("sale");
			if(typeof sale_off_by_product_value == typeof undefined) {
				sale_off_by_product_value = 0;
			}
			var sub_total_value = (origin_price_value - (origin_price_value * sale_off_by_product_value)) * quantity;
			sub_total.val(numberFormat(sub_total_value));
			grand_total_price_event();
		}
	}
	<?php
	if (Yii::$app->session->hasFlash('success')) { ?>
	alert('<?= Yii::$app->session->getFlash('success')?>');
	<?php } elseif (Yii::$app->session->hasFlash('failed')) { ?>
	alert('<?= Yii::$app->session->getFlash('failed')?>');
	<?php } ?>
</script>