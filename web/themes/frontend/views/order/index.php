<?php
use app\modules\admin\controllers\OrderController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use yii\bootstrap\Alert;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 */
$this->title = Translate::list_x(Translate::order());
?>
<?php if (RoleChecker::isAuth(OrderController::className(), 'index')) { ?>
	<div class="wrapper">
		<div class="row">
			<?= Alert::widget([
				'options' => [
					'class' => "alert alert-dismissible top-alert",
				],
				'body'    => Translate::order_alert(),
			]) ?>
			<h3 class="title-order"><?= Translate::my_order() ?></h3>
			<div class="order-link">
				<a href="<?= Url::to([
					'/order/orderitem',
					'language' => Yii::$app->language,
				]) ?>"><?= Translate::place_order() ?></a>
			</div>
			<div class="filters">
				<p style="display: inline-block;margin-right: 5px;"><?= Translate::have() . ' ' . $pagination->totalCount . ' ' . Translate::order_number() ?></p>
				<?= Html::dropDownList('Order', '', [
					'15'  => '15 ' . Translate::day_ago(),
					'180' => '6 ' . Translate::month_ago(),
				], ['class' => 'filter-order']) ?>
			</div>
		</div>
		<div class="order-wrapper">
			<?= /** @var \app\models\Order $orders */
			/** @var \app\models\Order $pagination */
			$this->render('_index', [
				'orders'     => $orders,
				'pagination' => $pagination,
			]) ?>
		</div>
	</div>
<?php } ?>
<div class="row action-pager">
	<div class="col-sm-6 action-item go-home">
		<a class="fleft" href="<?= Url::home(); ?>"><?= Translate::go_home() ?></a>
	</div>
	<div class="col-sm-6 action-item go-top">
		<a class="fright" href="javascript:void(0)"><?= Translate::go_top() ?></a>
	</div>
</div>
<script>
	$(document).on("change", ".filter-order", function() {
		$(this).closest('.filters').closest('.wrapper').find('.order-wrapper').empty();
		$.ajax({
			type   : "POST",
			data   : {
				'data': $(this).find("option:selected").val()
			},
			cache  : false,
			success: function(response) {
				$(".wrapper").find('.order-wrapper').html(response);
			}
		})
	});
	<?php
	if (Yii::$app->session->hasFlash('success')) { ?>
	alert('<?= Yii::$app->session->getFlash('success')?>');
	<?php } elseif (Yii::$app->session->hasFlash('failed')) { ?>
	alert('<?= Yii::$app->session->getFlash('failed')?>');
	<?php } ?>
</script>