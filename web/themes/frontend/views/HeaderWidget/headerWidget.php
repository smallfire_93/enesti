<?php
/**
 * @var array     $languages
 * @var array     $categories
 * @var View      $this
 * @var LoginForm $model
 */
use app\components\View;
use app\models\Category;
use app\models\Model;
use app\models\Slider;
use app\widgets\HeaderWidget;
use navatech\language\Translate;
use navatech\role\models\LoginForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="header">
	<div class="container">
		<div class="top-header">
			<?php Yii::$app->user->isGuest ? [
				'label' => Translate::login(),
				'url'   => ['/user/security/login'],
			] : [
				'label'       => Translate::logout() . '(' . $this->user->username . ')',
				'url'         => ['/user/security/logout'],
				'linkOptions' => ['data-method' => 'post'],
			] ?>
			<div class="row">
				<div class="col-sm-3 col-xs-6 logo">
					<a href="<?= Yii::$app->homeUrl; ?>">
						<img src="<?= Yii::$app->setting->get('general_logo') ?>"/>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6 fright language">
					<div class="dropdown">
						<button class="fright dropdown-toggle" type="button" id="multiLanguage" data-toggle="dropdown">
							<span><?= Yii::$app->language ?></span>
							<?= Translate::language(); ?>
						</button>
						<ul class="dropdown-menu" role="menu" aria-labelledby="multiLanguage">
							<?php foreach ($languages as $key => $language) : ?>
								<li role="presentation">
									<?php if ($key === 0): ?>
										<a class="active" role="menuitem" tabindex="-1" href="<?= $language['url'] ?>">
											<?= $language['code'] ?>
										</a>
									<?php else : ?>
										<a role="menuitem" tabindex="-1" href="<?= $language['url'] ?>">
											<?= $language['code'] ?>
										</a>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
							<li>
								<a href="http://enesti.co.kr" target="_blank">
									Kr
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>
<div class="main-menu">
	<div class="container">
		<div class="row">
			<div class="menu-main col-sm-offset-3 col-sm-6">
				<ul>
					<li class="<?= HeaderWidget::isActive('site', 'index') ?>">
						<a href="<?= Yii::$app->homeUrl ?>"><?= Translate::home() ?></a>
					</li>
					<li class="<?= HeaderWidget::isActive('post', 'index') ?>">
						<a href="<?= Url::to([
							'/page/view',
							'id'       => 10,
							'slug'     => Model::removeSign(Translate::intro()),
							'language' => Yii::$app->language,
						]) ?>"><?= Translate::intro() ?></a>
					</li>
					<li class="<?= HeaderWidget::isActive('post', 'index') ?>">
						<a href="<?= Url::to([
							'/post/index',
							'language' => Yii::$app->language,
						]) ?>"><?= Translate::news() ?></a>
					</li>
					<li class="<?= HeaderWidget::isActive('product', 'index') ?>">
						<a href="<?= Url::to([
							'/product/index',
							'language' => Yii::$app->language,
						]) ?>"><?= Translate::product() ?></a>
						<ul class="sub-menu">
							<?php foreach ($categories as $category): ?>
								<li class="<?= HeaderWidget::isActive('category', 'view') ?>">
									<a href="<?= Url::to([
										'product/category',
										'id'       => $category->id,
										'slug'     => Model::removeSign($category->name),
										'language' => Yii::$app->language,
									]) ?>"><?= $category->name; ?></a>
									<?php if (count(Category::getSubcategory($category->id)) >= 1): ?>
										<ul class="sub-menu">
											<?php foreach (Category::getSubcategory($category->id) as $sub_cat): ?>
												<li class="<?= HeaderWidget::isActive('category', 'view') ?>">
													<a href="<?= Url::to([
														'product/category',
														'id'       => $sub_cat->id,
														'slug'     => Model::removeSign($sub_cat->name),
														'language' => Yii::$app->language,
													]) ?>"><?= $sub_cat->name; ?></a>
												</li>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li class="<?= HeaderWidget::isActive('contact', 'index') ?>">
						<a href="<?= Url::to([
							'/contact/index',
							'language' => Yii::$app->language,
						]) ?>"><?= Translate::contact() ?></a>
					</li>
				</ul>
				<div class="button-menu">
					<div class="icon_menu"><span></span></div>
				</div>
			</div>
			<div class="fright login text-center col-sm-3">
				<ul class="fright">
					<?php if ((Yii::$app->user->identity) !== null): ?>
						<li>
							<a href="javascript:void(0)">
								<?= Translate::account(); ?>
							</a>

							<ul class="sub-menu">
								<li><a href="<?= Url::to([
										'/distributor/profile',
										'language' => Yii::$app->language,
									]); ?>"><?= Translate::manage_x(Translate::account()) ?></a></li>
								<li>
									<a href="<?= Url::to([
										'/order/index',
										'language' => Yii::$app->language,
									]) ?>"><?= Translate::manage_x(Translate::order()) ?></a>
								</li>
								<li>
									<a href="<?= Url::to([
										'/site/logout',
										'language' => Yii::$app->language,
									]) ?>" data-method="POST"><?= Translate::logout() ?></a>
								</li>
							</ul>
						</li>
					<?php else: ?>
						<li>
							<?php
							Modal::begin([
								'header'       => '<h2>Enesti  <b>' . Translate::login() . '</b></h2><h3>(' . Translate::for_department() . ')</h3>',
								'toggleButton' => [
									'label' => Translate::login(),
									'tag'   => 'a',
								],
							]);
							?>
							<div class="login-form">

								<?php
								$model = Yii::createObject(LoginForm::className());
								$form = ActiveForm::begin([
									'id'                     => 'login-form',
									'action'                 => Url::to(['/user/security/login']),
									'options'                => ['class' => 'form-horizontal'],
									'fieldConfig'            => [
										'template'     => "{label}\n<div class=\"col-sm-9\">{input}</div>\n{error}",
										'labelOptions' => ['class' => 'control-label col-sm-3'],
									],
									'enableAjaxValidation'   => true,
									'enableClientValidation' => false,
									'validateOnBlur'         => false,
									'validateOnType'         => false,
									'validateOnChange'       => false,
								]) ?>
								<h3 class="form-title font-green"><?php echo Translate::login() ?></h3>

								<div class="form-group">
									<?= $form->field($model, 'login')->textInput([
										'autofocus'   => 'autofocus',
										'placeholder' => Translate::username(),
									]) ?>
								</div>
								<div class="form-group">
									<?= $form->field($model, 'password')->passwordInput([
										'placeholder' => Translate::password(),
									]) ?>
								</div>
								<div class="form-group">
									<?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
								</div>
								<div class="form-actions">
									<?= Html::submitButton(Yii::t('user', Translate::login()), [
										'class'    => 'btn green uppercase',
										'tabindex' => '3',
									]) ?>
								</div>
								<?php ActiveForm::end(); ?>
								<div class="forgot-pass">
									<div class="fright">
										<a href="#"><span><?= Translate::support() ?></span> <?= Translate::forgot_password() ?>?</a>
									</div>
								</div>
							</div>

							<?php
							Modal::end();
							?>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>

	</div>
</div>
<div class="header-slider">
	<div class="container">
		<?php if (count($sliders) >= 2): ?>
			<div class="mobile-slider-item owl-theme">
				<?php /** @var Slider[] $sliders */
				foreach ($sliders as $slider) {
					?>
					<a class="item" href="<?= $slider->url ?>">
						<img class="img-responsive" src="<?= $slider->getPictureUrl('image') ?>">
					</a>
				<?php } ?>
			</div>
		<?php else: ?>
			<a class="item" href="<?= $sliders[0]->url ?>">
				<img class="img-responsive" src="<?= $sliders[0]->getPictureUrl('image') ?>">
			</a>
		<?php endif; ?>
	</div>
</div>