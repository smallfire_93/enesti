<?php
/**
 * @project PhpStorm
 * @created Navatech.
 * @author  LocPX
 * @email loc.xuanphama1t1@gmail.com
 * @date    09/06/2016
 * @time    6:39 CH
 */
use yii\helpers\Html;

echo Html::tag('div', Html::button('aaaa', ['class' => 'btn-info']), ['class' => 'new-content']);
?>
<script>
	function new_ajax() {
		$.ajax({
			type   : "POST",
			cache  : false,
			data   : "id=1",
			success: function(response) {
				$(".new-content").html(response);
			}
		});
	}
	$(".new-content").on("click", ".btn-info", function() {
		new_ajax();
	});
	//		$(".btn-info").click(function() {
	//			new_ajax();
	//		});
	//	$(".btn-info").on("click",function() {
	//		new_ajax();
	//	})
</script>
