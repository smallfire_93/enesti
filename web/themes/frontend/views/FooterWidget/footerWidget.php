<?php
use navatech\language\Translate;

?>
<footer>
	<div class="row footer">
		<div class="col-sm-4 footer-item representative-office">
			<h3>
				<img src="<?= Yii::$app->view->theme->baseUrl; ?>/assets/images/home.png">
				<?= $department[0]->name ?>
			</h3>
			<p><?= Translate::address() ?>: <b><?= $department[0]->address ?></b></p>
			<p class="phone"><span><?= Translate::phone() ?>: </span><?= $department[0]->phone ?></p>
			<div class="map">
				<div id="map"></div>
				<script>
					var lat  = <?= Yii::$app->setting->get('general_latitude') ?>;
					var long = <?= Yii::$app->setting->get('general_longitude') ?>;
					function initMap() {
						var map         = new google.maps.Map(document.getElementById('map'), {
							zoom       : 15,
							scrollwheel: false,
							center     : {
								lat: lat,
								lng: long
							}
						});
						var image       = '<?= Yii::$app->view->theme->baseUrl; ?>/assets/images/map.png';
						var beachMarker = new google.maps.Marker({
							position: {
								lat: lat,
								lng: long
							},
							map     : map,
							icon    : image
						});
					}
				</script>
				<script async defer
				        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDe81mjX7hM7fNSNfUrnKgazOLkc5WC44E&callback=initMap">
				</script>
			</div>
		</div>
		<div class="col-sm-4 footer-item about-company">
			<h3>
				<img src="<?= Yii::$app->view->theme->baseUrl; ?>/assets/images/home.png">
				<?= $department[1]->name ?>
			</h3>
			<p><?= Translate::address() ?>: <b><?= $department[1]->address ?></b></p>
			<p><?= $department[1]->company_code; ?></p>
			<p class="phone"><span><?= Translate::phone() ?>:</span> <?= $department[1]->phone ?></p>
			<p class="phone"><span><?= Translate::sale_department() ?>:</span><?= $department[1]->business_room ?></p>
			<div class="social-footer">
				<p><?= Translate::website(); ?>: <?= $department[1]->website; ?></p>
				<p><?= Translate::email(); ?>: <?= $department[1]->email; ?></p>
				<p class="social-icon">
					<a class="icon-fb" target="_blank" href="<?= $department[1]->facebook; ?>">
						<i class="fa fa-facebook"></i>
					</a>
					<a target="_blank" href="<?= $department[1]->google_plus; ?>">
						<i class="fa fa-google-plus"></i>
					</a>
				</p>
			</div>
			<p> <?= Translate::terms_of_use() ?></p>

		</div>
		<div class="col-sm-4 footer-item hotline-worktime">
			<div class="hotline">
				<h2><?= Translate::hotline() ?>:</h2>
				<h3><?= Yii::$app->setting->get('general_hotline') ?></h3>
			</div>
			<div class="work-time">
				<?= $department[1]->schedule; ?>
			</div>
		</div>
	</div>
</footer>