<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\assets\AppAsset;
use app\widgets\FooterWidget;
use app\widgets\HeaderWidget;
use yii\helpers\Html;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?= Yii::$app->setting->get('general_favicon') ?>"/>
	<?= Html::csrfMetaTags() ?>
	<title><?php echo $this->title . ' - ' . Yii::$app->setting->get('general_title') ?></title>
	<meta name="description" content="<?php echo Yii::$app->setting->get('website_description') ?>">
	<meta name="keywords" content="<?php echo Yii::$app->setting->get('website_keyword') ?>">
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= HeaderWidget::widget() ?>
<div class="container">
	<div class="main-content-wrap">
		<div class="main-content">
			<?= $content ?>
		</div>
		<?= FooterWidget::widget() ?>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
