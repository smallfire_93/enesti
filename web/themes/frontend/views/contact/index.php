<?php
use navatech\language\Translate;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Translate::contact();
?>
<div class="contact-form text-center">
	<h2>ĐĂNG KÝ LÀM NHÀ PHÂN PHỐI VÀ ĐẠI LÝ</h2>
	<h3>(Xin gửi thông tin cho Enesti, hoặc có thể gọi trực tiếp cho chúng tôi)</h3>
	<?php
	$form = ActiveForm::begin([
		'layout'      => 'horizontal',
		'fieldConfig' => [
			'template'     => "{label}\n<div class=\"col-sm-9\">{input}</div>\n<div class=\"col-sm-9 col-sm-offset-3\">{error}</div>",
			'labelOptions' => ['class' => 'control-label col-sm-3'],
		],
	]); ?>

	<?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'phone')->textInput() ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'address')->textarea(['maxlength' => true]) ?>

	<div class="form-group">
		<div class="col-sm-12">
			<p class="fright">(Enesti sẽ liên hệ sau khi nhận được thông tin từ quý khách)</p>
			<?= Html::submitButton(Translate::send(), ['class' => 'btn btn-success fright']) ?>
		</div>

	</div>

	<?php ActiveForm::end(); ?>
</div>
<script>
	<?php
	if (Yii::$app->session->hasFlash('success')) { ?>
	alert('<?= Yii::$app->session->getFlash('success')?>');
	<?php } elseif (Yii::$app->session->hasFlash('failed')) { ?>
	alert('<?= Yii::$app->session->getFlash('failed')?>');
	<?php } ?>
</script>

