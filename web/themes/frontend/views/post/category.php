<?php
use yii\widgets\ListView;

$this->title = \navatech\language\Translate::category();
?>
<div class="row category-page list-post-item">
	<?= ListView::widget([
		'dataProvider' => $posts,
		'options'      => [
			'tag'   => 'div',
			'class' => 'list-wrapper',
			'id'    => 'list-wrapper',
		],
		'layout'       => "{items}\n{pager}",
		'itemView'     => '_post_category_item',
	]); ?>
</div>
