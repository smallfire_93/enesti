<?php
use app\models\Model;
use app\models\Post;
use navatech\language\Translate;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Url;

$this->title = Translate::news();
?>

<div class="row">
	<div class="col-sm-8">
		<div class="list-cate-post">
			<?php foreach ($category_post as $category): ?>
				<div class="category-post-item">
					<div class="category-name">
						<h2><a href="<?= Url::to([
								'post/category',
								'id'       => $category->id,
								'slug'     => $category->name,
								'language' => Yii::$app->language,
							]) ?>"><?= $category->name ?></h2>
					</div>
					<div class="list-post-item">
						<?php foreach (Post::getPostByCategory($category->id, 2) as $post): ?>
							<div class="row post-item">
								<div class="col-xs-4 img-post">
									<a href="<?= Url::to([
										'post/view',
										'id'       => $post->id,
										'slug'     => Model::removeSign($category->name),
										'language' => Yii::$app->language,
									]); ?>">
										<img src="<?= $post->getPictureUrl('image') ?>">
									</a>
								</div>
								<div class="col-xs-8 content-post">
									<h3 class="title-post"><a href="<?= Url::to([
											'post/view',
											'id'       => $post->id,
											'slug'     => Model::removeSign($post->title),
											'language' => Yii::$app->language,
										]) ?>"><?= $post->title ?></a></h3>
									<div class="description-post">
										<p><?= $post->description ?></p>
										<a class="read-more" href="<?= Url::to([
											'post/view',
											'id'       => $post->id,
											'slug'     => Model::removeSign($post->title),
											'language' => Yii::$app->language,
										]) ?>">Xem chi tiết <?= FA::icon('angle-double-right'); ?></a>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="top-sidebar relative">
			<div class="title-top-sidebar">
				<h3><?= 'Enesti - ' . Translate::vietnamese(); ?></h3>
			</div>
			<div class="title-icon-sidebar">
				<h4><?= Translate::news() ?> <span><?= Translate::newest(); ?></span></h4>
				<span class="sidebar-title-icon">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
				     viewBox="0 0 20 14" style="enable-background:new 0 0 20 14;" xml:space="preserve">
					<g id="XMLID_15_">
						<path id="XMLID_18_" class="title-icon" d="M19,1.8c-0.4,0.9-1.2,1.6-2.1,2c-1.6,0.7-3.3,1.1-4.9,1.9c-1,0.5-1.6,1.2-2.3,2.1
							c-0.5,0.7-0.8,1.5-1,2.4c-0.1,0.7-0.2,1.3,0,1.9c0,0.1,0.2,0.4,0.2,0.4c0,0,0,0,0-0.1c0.4-4.5,3.9-5.4,5.9-6.2
							c1.3-0.5,2.1-1,2.4-1.3c0.1-0.1,0.2-0.2,0.2-0.3c-0.2,0.4-0.7,0.9-0.9,1c-0.3,0.2-0.5,0.4-0.8,0.6c0,0,0,0,0,0
							c-1.8,1.3-3.8,2-5.2,3.8C9.7,11.3,9,12.6,8.6,14c0,0,0.2-0.5,2-1.1c1.9-0.5,4.9-1.6,6.7-3.7c0.9-1.1,1.5-2.3,1.7-3.6
							C19.3,4.3,19.5,2.9,19,1.8z"/>
						<path id="XMLID_17_" class="title-icon" d="M3.1,8C3.1,8,3,8,3.1,8C2.8,7.9,2.6,7.7,2.4,7.6C2.3,7.4,1.9,7,1.7,6.7
							c0.1,0.1,0.1,0.1,0.2,0.2c0.3,0.3,0.8,0.7,1.8,1.2c0.6,0.3,3.8,1.2,4.7,4.6c0,0,0,0,0,0c0,0,0.1-0.2,0.2-0.3c0.1-0.5,0.1-1,0-1.5
							c-0.1-0.7-0.3-1.3-0.8-1.9C7.3,8.5,6.8,7.9,6,7.5C4.8,6.9,3.5,6.6,2.2,6.1C1.5,5.8,1,5.2,0.6,4.5c-0.4,0.9-0.2,2-0.1,2.9
							c0.2,1.1,0.6,2,1.3,2.8c1.4,1.6,3.8,2.5,5.2,2.9C8.5,13.5,8.6,14,8.6,14c-0.3-1-0.9-2.1-1.5-2.9C6,9.6,4.4,9,3.1,8z"/>
						<circle id="XMLID_16_" class="title-icon" cx="7.6" cy="2.9" r="2.6"/>
					</g>
				</svg>
			</span>
			</div>
			<div class="list-post-sidebar">
				<?php foreach ($newest as $new) { ?>
					<div class="row relative post-sidebar-item">
						<div class="col-xs-5 img-post">
							<a href="<?= Url::to([
								'post/view',
								'id'       => $new->id,
								'language' => Yii::$app->language,
							]) ?>">
								<img src="<?php echo $new->getPictureUrl('image') ?>">
							</a>
						</div>
						<div class="col-xs-7 content-post">
							<h3 class="title-post"><a href="<?= Url::to([
									'post/view',
									'id'       => $new->id,
									'language' => Yii::$app->language,
								]) ?>"><?= $new->title ?></a></h3>
							<div class="description-post">
								<p><?= $new->description ?></p>
								<a class="read-more" href="<?= Url::to([
									'post/view',
									'id'       => $new->id,
									'language' => Yii::$app->language,
								]) ?>">Xem chi tiết <?= FA::icon('angle-double-right'); ?></a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="list-category">
				<?php /** @var array $categories */
				foreach ($categories as $category) { ?>
					<div class="col-sm-12 category-item text-center">
						<div class="category-title">
							<h3>
								<a href="<?= Url::to([
									'product/category/view',
									'id'       => $category->id,
									'language' => Yii::$app->language,
								]) ?>"><?php echo $category->name ?>
									<span class="fright"><?= FA::icon('angle-right') ?></span></a>
							</h3>
						</div>
						<div class="category-image">
							<a href="<?= Url::to([
								'product/category/view',
								'id'       => $category->id,
								'language' => Yii::$app->language,
							]) ?>">
								<img src="<?= $category->getPictureUrl('image') ?>">
							</a>
						</div>
					</div>
				<?php } ?>
			</div>

		</div>
	</div>
</div>