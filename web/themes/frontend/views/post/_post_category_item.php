<?php
use app\models\Model;
use yii\helpers\Url;

?>
<div class="col-sm-6 post-item">
	<div class="img-post">
		<a href="<?= Url::to([
			'post/view',
			'id'       => $model->id,
			'slug'     => Model::removeSign($model->title),
			'language' => Yii::$app->language,
		]); ?>">
			<img src="<?= $model->getPictureUrl('image') ?>">
		</a>
	</div>
	<div class="content-post">
		<h3>
			<a href="<?= Url::to([
				'post/view',
				'id'       => $model->id,
				'slug'     => Model::removeSign($model->title),
				'language' => Yii::$app->language,
			]); ?>">
				<?= $model->title ?>
			</a>
		</h3>
		<div class="description-post">
			<p><?= $model->description ?></p>
		</div>
	</div>
</div>