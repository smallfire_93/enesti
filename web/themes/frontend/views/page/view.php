<?php
use navatech\language\Translate;
use yii\helpers\Url;

/** @var app\models\Page $model */
$this->title = $model->title;
?>
<div class="row single-post-description">
	<div class="col-sm-12">
		<h1><?= $model->title; ?></h1>
<!--		<div class="single-post-des">-->
<!--			<h1>--><?//= $model->intro; ?><!--</h1>-->
<!--		</div>-->
	</div>

</div>
<div class="single-post-content">
	<?= $model->content; ?>
</div>
<div class="row action-pager">
	<div class="col-sm-6 action-item go-home">
		<a class="fleft" href="<?= Url::home(); ?>"><?= Translate::go_home() ?></a>
	</div>
	<div class="col-sm-6 action-item go-top">
		<a class="fright" href="javascript:void(0)"><?= Translate::go_top() ?></a>
	</div>
</div>