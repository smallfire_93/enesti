<?php
/**
 * @var Post      $hotnews
 * @var Product[] $hot_product
 */
use app\models\Distributor;
use app\models\Model;
use app\models\Post;
use app\models\Product;
use navatech\language\Translate;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Url;

$this->title = Translate::home();
?>
<?php if (!empty($popup)): ?>
	<div class="popup">
		<div class="popup-content  text-center" id="popup-content">
			<img src="<?php echo $popup->getPictureUrl('background'); ?>">
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$.colorbox({
				width : "70%",
				inline: true,
				href  : "#popup-content"
			});
		});
	</script>
<?php endif; ?>
<div class="row">
	<div class="col-sm-8">
		<div class="list-feature-product">
			<?php foreach ($hot_product as $hot) { ?>

				<div class="row feature-product">
					<div class="col-xs-4 img-product">
						<a href="<?= Url::to([
							'/product/view',
							'id'       => $hot->id,
							'slug'     => Model::removeSign($hot->title),
							'language' => Yii::$app->language,
						]) ?>">
							<img src="<?= $hot->getPictureUrl('image') ?> ">
						</a>
					</div>

					<div class="col-xs-8 content-product">
						<h2 class="title-block">
							<span class="icon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
							     viewBox="0 0 20 14" style="enable-background:new 0 0 20 14;" xml:space="preserve">
								<g id="XMLID_15_">
									<path id="XMLID_18_" class="title-icon" d="M19,1.8c-0.4,0.9-1.2,1.6-2.1,2c-1.6,0.7-3.3,1.1-4.9,1.9c-1,0.5-1.6,1.2-2.3,2.1
										c-0.5,0.7-0.8,1.5-1,2.4c-0.1,0.7-0.2,1.3,0,1.9c0,0.1,0.2,0.4,0.2,0.4c0,0,0,0,0-0.1c0.4-4.5,3.9-5.4,5.9-6.2
										c1.3-0.5,2.1-1,2.4-1.3c0.1-0.1,0.2-0.2,0.2-0.3c-0.2,0.4-0.7,0.9-0.9,1c-0.3,0.2-0.5,0.4-0.8,0.6c0,0,0,0,0,0
										c-1.8,1.3-3.8,2-5.2,3.8C9.7,11.3,9,12.6,8.6,14c0,0,0.2-0.5,2-1.1c1.9-0.5,4.9-1.6,6.7-3.7c0.9-1.1,1.5-2.3,1.7-3.6
										C19.3,4.3,19.5,2.9,19,1.8z"/>
									<path id="XMLID_17_" class="title-icon" d="M3.1,8C3.1,8,3,8,3.1,8C2.8,7.9,2.6,7.7,2.4,7.6C2.3,7.4,1.9,7,1.7,6.7
										c0.1,0.1,0.1,0.1,0.2,0.2c0.3,0.3,0.8,0.7,1.8,1.2c0.6,0.3,3.8,1.2,4.7,4.6c0,0,0,0,0,0c0,0,0.1-0.2,0.2-0.3c0.1-0.5,0.1-1,0-1.5
										c-0.1-0.7-0.3-1.3-0.8-1.9C7.3,8.5,6.8,7.9,6,7.5C4.8,6.9,3.5,6.6,2.2,6.1C1.5,5.8,1,5.2,0.6,4.5c-0.4,0.9-0.2,2-0.1,2.9
										c0.2,1.1,0.6,2,1.3,2.8c1.4,1.6,3.8,2.5,5.2,2.9C8.5,13.5,8.6,14,8.6,14c-0.3-1-0.9-2.1-1.5-2.9C6,9.6,4.4,9,3.1,8z"/>
									<circle id="XMLID_16_" class="title-icon" cx="7.6" cy="2.9" r="2.6"/>
								</g>
							</svg>
						</span>
							<?= 'ENESTI - ' . Translate::feature_product(); ?>
						</h2>
						<h3 class="title-product">
							<a href="<?= Url::to([
								'/product/view/',
								'id'       => $hot->id,
								'slug'     => Model::removeSign($hot->title),
								'language' => Yii::$app->language,
							]) ?>"><?= $hot->title ?></a></h3>
						<div class="product-info">
							<p><?= Translate::capability(); ?>: <span class="vol"><?= $hot->capability ?></span></p>
							<p><?= Translate::code(); ?>: <span class="sku"><?= $hot->code ?></span></p>
							<p><?= Translate::origin(); ?>: <span class="madein"><?= $hot->origin ?></span></p>
						</div>
						<div class="description-product">
							<p><?= $hot->content ?></p>
						</div>
					</div>
				</div>
			<?php } ?>

		</div>
	</div>
	<div class="col-sm-4">
		<div class="top-sidebar relative">
			<div class="title-top-sidebar">
				<h3><?= 'ENESTI - ' . Translate::vietnamese() ?></h3>
			</div>
			<div class="title-icon-sidebar">
				<h4><?= Translate::news() ?> <span><?= Translate::newest(); ?></span></h4>
				<span class="sidebar-title-icon">
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
				     viewBox="0 0 20 14" style="enable-background:new 0 0 20 14;" xml:space="preserve">
					<g id="XMLID_15_">
						<path id="XMLID_18_" class="title-icon" d="M19,1.8c-0.4,0.9-1.2,1.6-2.1,2c-1.6,0.7-3.3,1.1-4.9,1.9c-1,0.5-1.6,1.2-2.3,2.1
							c-0.5,0.7-0.8,1.5-1,2.4c-0.1,0.7-0.2,1.3,0,1.9c0,0.1,0.2,0.4,0.2,0.4c0,0,0,0,0-0.1c0.4-4.5,3.9-5.4,5.9-6.2
							c1.3-0.5,2.1-1,2.4-1.3c0.1-0.1,0.2-0.2,0.2-0.3c-0.2,0.4-0.7,0.9-0.9,1c-0.3,0.2-0.5,0.4-0.8,0.6c0,0,0,0,0,0
							c-1.8,1.3-3.8,2-5.2,3.8C9.7,11.3,9,12.6,8.6,14c0,0,0.2-0.5,2-1.1c1.9-0.5,4.9-1.6,6.7-3.7c0.9-1.1,1.5-2.3,1.7-3.6
							C19.3,4.3,19.5,2.9,19,1.8z"/>
						<path id="XMLID_17_" class="title-icon" d="M3.1,8C3.1,8,3,8,3.1,8C2.8,7.9,2.6,7.7,2.4,7.6C2.3,7.4,1.9,7,1.7,6.7
							c0.1,0.1,0.1,0.1,0.2,0.2c0.3,0.3,0.8,0.7,1.8,1.2c0.6,0.3,3.8,1.2,4.7,4.6c0,0,0,0,0,0c0,0,0.1-0.2,0.2-0.3c0.1-0.5,0.1-1,0-1.5
							c-0.1-0.7-0.3-1.3-0.8-1.9C7.3,8.5,6.8,7.9,6,7.5C4.8,6.9,3.5,6.6,2.2,6.1C1.5,5.8,1,5.2,0.6,4.5c-0.4,0.9-0.2,2-0.1,2.9
							c0.2,1.1,0.6,2,1.3,2.8c1.4,1.6,3.8,2.5,5.2,2.9C8.5,13.5,8.6,14,8.6,14c-0.3-1-0.9-2.1-1.5-2.9C6,9.6,4.4,9,3.1,8z"/>
						<circle id="XMLID_16_" class="title-icon" cx="7.6" cy="2.9" r="2.6"/>
					</g>
				</svg>
			</span>
			</div>
			<div class="list-post-sidebar">
				<?php foreach ($newest as $new) { ?>
					<div class="row relative post-sidebar-item">
						<div class="col-xs-5 img-post">
							<a href="<?= Url::to([
								'/post/view',
								'id'       => $new->id,
								'slug'     => Model::removeSign($new->title),
								'language' => Yii::$app->language,
							]) ?>">
								<img src="<?php echo $new->getPictureUrl('image') ?>">
							</a>
						</div>
						<div class="col-xs-7 content-post">
							<h3 class="title-post">
								<a href="<?= Url::to([
									'/post/view',
									'id'       => $new->id,
									'slug'     => Model::removeSign($new->title),
									'language' => Yii::$app->language,
								]) ?>"><?= $new->title ?></a></h3>
							<div class="description-post">
								<p><?= $new->description ?></p>
								<a class="read-more" href="<?= Url::to([
									'/post/view',
									'id'       => $new->id,
									'slug'     => Model::removeSign($new->title),
									'language' => Yii::$app->language,
								]) ?>"><?= Translate::view_detail() ?> <?= FA::icon('angle-double-right'); ?></a>
							</div>
						</div>
					</div>
				<?php } ?>

			</div>
			<div class="video-sidebar">
				<div class="video-detail">
					<?= Model::getVideoEmbed(Yii::$app->setting->get('general_video'))?>
				</div>
				<h4><?= Translate::title_video() ?></h4>
			</div>

		</div>
	</div>
</div>
<div class="list-category">
	<?php /** @var array $categories */
	foreach ($categories as $category) { ?>
		<div class="col-sm-4 category-item text-center">
			<div class="category-title">
				<h3><a href="<?= Url::to([
						'/product/category',
						'id'       => $category->id,
						'slug'     => Model::removeSign($category->name),
						'language' => Yii::$app->language,
					]) ?>"><?php echo $category->name ?>
						<span class="fright"><?= FA::icon('angle-right') ?></span></a>
				</h3>
			</div>
			<div class="category-image">
				<a href="<?= Url::to([
					'/product/category',
					'id'       => $category->id,
					'slug'     => Model::removeSign($category->name),
					'language' => Yii::$app->language,
				]) ?>">
					<img src="<?= $category->getPictureUrl('image') ?>">
				</a>
			</div>
		</div>
	<?php } ?>
</div>
<?php if ($banner != null) : ?>
	<div class="row list-ads">
		<div class="col-sm-12 ads-item">
			<a href="<?= $banner->url ?>">
				<img src="<?= $banner->getPictureUrl('image') ?>">
			</a>
		</div>
	</div>
<?php endif; ?>
<div class="distributor-agency">
	<div class="title-distribute-agency fleft">
		<h3 class="fleft">
			<span class="fleft relative"><img src="<?= Yii::$app->view->theme->baseUrl; ?>/assets/images/basket.png"></span>
			<?= Translate::system_x_and_x([
				Translate::distributor(),
				Translate::agent(),
			]); ?>
		</h3>
		<h4 class="fleft"><?= 'Enesti - ' . Translate::vietnamese(); ?></h4>
		<a class="fright active" href="javescript:void(0)">
			<img src="<?= Yii::$app->view->theme->baseUrl; ?>/assets/images/arrow.png">
		</a>
	</div>
	<div class="distribute-agency-content">
		<div class="row">
			<?php
			/** @var Distributor $distributors */
			foreach ($distributors->getType() as $type => $value) { ?>
				<div class="col-sm-6 distribute">
					<div class="title-agency-content text-center">
						<?= $value ?>
					</div>
					<div class="content-group">
						<?php
						foreach ($distributors->getArea() as $keyArea => $valueArea) {
							?>
							<div class="content-group-item">
								<h3>
									<img src="<?= Yii::$app->view->theme->baseUrl; ?>/assets/images/cart.png">
									<?= $valueArea . ":" ?>
								</h3>
								<div class="ad-address">
									<?php foreach (Distributor::getDistributor($keyArea, $type) as $get) { ?>
										<b><?= $get->city ?>: </b>
										<p>Đc:<?= $get->address ?></p>
									<?php } ?>
								</div>
							</div>
						<?php }
						?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
