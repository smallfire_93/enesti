<?php
use app\models\Model;
use navatech\language\Translate;
use yii\helpers\Url;

?>
<div class="col-sm-3 col-xs-6 product-item">
	<div class="product-item-wrap">
		<div class="img-product">
			<a href="<?= Url::to([
				'product/view',
				'id'       => $model->id,
				'slug'     => Model::removeSign($model->title),
				'language' => Yii::$app->language,
			]) ?>">
				<img src="<?= $model->getPictureUrl('image') ?>">
			</a>
		</div>
		<div class="product-info">
			<h3><a href="<?= Url::to([
					'product/view',
					'id'       => $model->id,
					'slug'     => Model::removeSign($model->title),
					'language' => Yii::$app->language,
				]) ?>"><?= $model->title ?></a></h3>
			<p class="product-sku">
				<span class="fleft"><?= Translate::code() ?>:</span> <span class="fright"><?= $model->code ?></span>
			</p>
			<p class="product-price">
				<span class="fleft"><?= Translate::price() ?>:</span>
				<span class="fright"><?= number_format($model->price) ?> ₫</span>
			</p>
			<a class="view-detail" href="<?= Url::to([
				'product/view',
				'id'       => $model->id,
				'slug'     => Model::removeSign($model->title),
				'language' => Yii::$app->language,
			]) ?>"><?= Translate::view_detail(); ?></a>
		</div>
	</div>

</div>