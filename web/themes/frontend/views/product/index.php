<?php use navatech\language\Translate;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = Translate::product();
?>
<div class="title-page">
	<h1><?= Translate::product() ?></h1>
</div>
<div class="row list-product">
	<?= ListView::widget([
		'dataProvider' => $products,
		'options'      => [
			'tag'   => 'div',
			'class' => 'list-wrapper',
			'id'    => 'list-wrapper',
		],
		'layout'       => "{items}\n{pager}",
		'itemView'     => '_product_item',
	]); ?>
</div>
<div class="row action-pager">
	<div class="col-sm-6 action-item go-home">
		<a class="fleft" href="<?= Url::home(); ?>"><?= Translate::go_home() ?></a>
	</div>
	<div class="col-sm-6 action-item go-top">
		<a class="fright" href="javascript:void(0)"><?= Translate::go_top() ?></a>
	</div>
</div>