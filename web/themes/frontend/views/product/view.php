<?php
use navatech\language\Translate;
use yii\widgets\ListView;

$this->title = $model->title;
?>
<div class="row">
	<div class="col-sm-3 single-image-product">
		<img src="<?= $model->getPictureUrl('image') ?>">
	</div>
	<div class="col-sm-9">
		<h1 class="title-product-page"><?= $model->title ?></h1>
		<div class="product-des">
			<p><?= Translate::code()?>: <span><?= $model->code ?></span></p>
			<p><?= Translate::price()?>: <span><?= number_format($model->price) ?> VND </span></p>
			<p><?= Translate::origin()?>: <?= $model->origin ?></p>
			<p><?= Translate::capability()?>: <?= $model->capability ?></p>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="product-share">
			<a class="fleft text-center" data-toggle="modal" href="#lager-image">(<?= Translate::view_large_image() ?>)</a>
			<div class="modal fade" id="lager-image">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								<i class="fa fa-close"></i></button>
							<h4 class="modal-title"><?= $model->title ?></h4>
						</div>
						<div class="modal-body text-center">
							<div class="product-lager-image">
								<img src="<?= $model->getPictureUrl('image') ?>">
							</div>

						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<p class="fleft">Chia sẻ:</p>
			<div class="goodshare-color fleft">
				<a href="#" class="goodshare fb" data-type="fb">
					<i class="fa fa-facebook"></i>
				</a>
				<a href="#" class="goodshare tw" data-type="tw">
					<i class="fa fa-twitter"></i>
				</a>
				<a href="#" class="goodshare gp" data-type="gp">
					<i class="fa fa-google-plus"></i>
				</a>

			</div>
		</div>
	</div>
	<div class="col-sm-12 product-information">
		<?= $model->content ?>
	</div>
</div>
<div class="row list-product">
	<?= ListView::widget([
		'dataProvider' => $related,
		'options'      => [
			'tag'   => 'div',
			'class' => 'list-wrapper',
			'id'    => 'list-wrapper',
		],
		'layout'       => "{items}\n{pager}",
		'itemView'     => '_product_item',
	]); ?>
</div>