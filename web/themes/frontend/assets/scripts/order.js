$(document).ready(function() {
	$('.order-toggle .order-open').click(function() {
		$(this).closest(".order-toggle").find(".order-items").toggle('slow');
		if($(this).children().hasClass("arrow-right")) {
			$(this).children().removeClass("arrow-right");
			$(this).find("div").addClass("arrow-down");

		} else if($(this).children().hasClass("arrow-down")) {
			$(this).children().removeClass("arrow-down");
			$(this).children("div").addClass("arrow-right");
		}
		return false;
	});
});