$(document).ready(function() {
	$('.icon_menu').click(function() {
		if($(this).hasClass('open')) {
			$('.menu-main ul').removeClass('open');
			$('.menu-main .arrow-menu').removeClass('open');
			$(this).removeClass('open');
			$(this).clearQueue();
		} else {
			$(this).addClass('open');
			$('.menu-main > ul').addClass('open');
			$(this).clearQueue();
		}
	});
	if($(window).width() < 768){
		$('.menu-main li').has('ul.sub-menu').append('<span class="arrow-menu"><i class="fa fa-chevron-right"></i></span>');
		$(document).on('click','.arrow-menu',function() {
			if($(this).hasClass('open')) {
				$(this).parent().find('ul.sub-menu').removeClass('open');
				$(this).removeClass('open');
				$(this).clearQueue();
			} else {
				$(this).addClass('open');
				$(this).parent().find('ul.sub-menu').addClass('open');
				$(this).clearQueue();
			}
		});
	}
	$('.title-distribute-agency a').click(function() {
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.distribute-agency-content').slideDown(500);
		} else {
			$(this).addClass('active');
			$('.distribute-agency-content').slideUp(500);
		}
		return false;
	});
	$('.mobile-slider-item').owlCarousel({
		autoplay: true,
		center: true,
		items : 1,
		loop:true,
		margin:0,
	});

	$('.go-top a').on('click', function() {
		$('html,body').animate({
			scrollTop: 0
		}, 700);
		return false;
	});
	$(".list-our-client").owlCarousel({
		itemsCustom: [
			[0, 3],
		],
		autoPlay   : 5000,
		navigation : true,
	});


})
