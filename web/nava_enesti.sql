/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : nava_enesti

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-02-24 18:14:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('4', '1', '4_image.png', '0', '123', '1', '1', '2016-05-09 15:06:03');

-- ----------------------------
-- Table structure for banner_lang
-- ----------------------------
DROP TABLE IF EXISTS `banner_lang`;
CREATE TABLE `banner_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bannerlang_ibfk_1` (`banner_id`),
  CONSTRAINT `bannerlang_ibfk_1` FOREIGN KEY (`banner_id`) REFERENCES `banner` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner_lang
-- ----------------------------
INSERT INTO `banner_lang` VALUES ('3', '4', 'vi', '123');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('60', '0', '1', '1', '1', '60_image.jpg');
INSERT INTO `category` VALUES ('61', '0', '1', '2', '1', '61_image.jpg');
INSERT INTO `category` VALUES ('62', '0', '1', '3', '1', '62_image.jpg');
INSERT INTO `category` VALUES ('63', '0', '1', '4', '1', '63_image.jpg');
INSERT INTO `category` VALUES ('64', '0', '1', '5', '1', '64_image.jpg');
INSERT INTO `category` VALUES ('65', '0', '1', '6', '1', '65_image.jpg');
INSERT INTO `category` VALUES ('66', '61', '1', '7', '1', '');
INSERT INTO `category` VALUES ('67', '61', '1', '8', '1', '');
INSERT INTO `category` VALUES ('68', '61', '1', '9', '1', '');
INSERT INTO `category` VALUES ('69', '61', '1', '10', '1', '');
INSERT INTO `category` VALUES ('70', '61', '1', '11', '1', '');
INSERT INTO `category` VALUES ('71', '61', '1', '12', '1', '');
INSERT INTO `category` VALUES ('72', '0', '2', null, '1', '72_image.jpg');
INSERT INTO `category` VALUES ('73', '0', '1', '100', '1', '');

-- ----------------------------
-- Table structure for category_lang
-- ----------------------------
DROP TABLE IF EXISTS `category_lang`;
CREATE TABLE `category_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_lang_ibfk_1` (`category_id`),
  CONSTRAINT `category_lang_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category_lang
-- ----------------------------
INSERT INTO `category_lang` VALUES ('1', '60', 'vi', 'Sản Phẩm Mới', 'Sản Phẩm Mới');
INSERT INTO `category_lang` VALUES ('2', '61', 'vi', 'Chăm sóc da', 'Chăm sóc da');
INSERT INTO `category_lang` VALUES ('3', '62', 'vi', 'Mặt nạ & Kem tẩy trang', 'Mặt nạ & Kem tẩy trang');
INSERT INTO `category_lang` VALUES ('4', '63', 'vi', 'Trang điểm', 'Trang điểm');
INSERT INTO `category_lang` VALUES ('5', '64', 'vi', 'Chăm sóc tóc', 'Chăm sóc tóc');
INSERT INTO `category_lang` VALUES ('6', '65', 'vi', 'Nước hoa', 'Nước hoa');
INSERT INTO `category_lang` VALUES ('7', '66', 'vi', 'Bộ sản phẩm Remine', 'Bộ sản phẩm Remine');
INSERT INTO `category_lang` VALUES ('8', '67', 'vi', 'Bộ sản phẩm SEOYOON', 'Bộ sản phẩm SEOYOON');
INSERT INTO `category_lang` VALUES ('9', '68', 'vi', 'Bộ sản phẩm SUANSU', 'Bộ sản phẩm SUANSU');
INSERT INTO `category_lang` VALUES ('10', '69', 'vi', 'Xịt khoáng', 'Xịt khoáng');
INSERT INTO `category_lang` VALUES ('11', '70', 'vi', 'Mặt nạ', 'Mặt nạ');
INSERT INTO `category_lang` VALUES ('12', '71', 'vi', 'Kem dưỡng', 'Kem dưỡng');
INSERT INTO `category_lang` VALUES ('13', '72', 'vi', 'enesti', '123');
INSERT INTO `category_lang` VALUES ('14', '73', 'vi', 'Tóc', '');

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', '1_image.jpg', 'ádf', '2016-04-23 11:10:23', '1');

-- ----------------------------
-- Table structure for client_lang
-- ----------------------------
DROP TABLE IF EXISTS `client_lang`;
CREATE TABLE `client_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clientlang_ibfk_1` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of client_lang
-- ----------------------------
INSERT INTO `client_lang` VALUES ('1', '1', 'vi', 'ádf');
INSERT INTO `client_lang` VALUES ('2', '1', 'en', 'sd');

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contact
-- ----------------------------
INSERT INTO `contact` VALUES ('9', 'abc', 'loc.xuanphama1t1@gmail.com', '111', '111', '111', '2016-05-18 15:06:35', '1');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', '123', '123', '123', '1', '123', '2016-04-21 17:28:17');
INSERT INTO `department` VALUES ('2', '12312', '123', '123', '0', '123', '2016-04-25 09:58:36');
INSERT INTO `department` VALUES ('3', '123', '1231', '123', '1', '123', '2016-04-25 10:09:39');

-- ----------------------------
-- Table structure for department_lang
-- ----------------------------
DROP TABLE IF EXISTS `department_lang`;
CREATE TABLE `department_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text,
  PRIMARY KEY (`id`),
  KEY `departmentlang_ibfk_1` (`department_id`),
  CONSTRAINT `departmentlang_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department_lang
-- ----------------------------
INSERT INTO `department_lang` VALUES ('1', '1', 'vi', 'abc', '12312312');
INSERT INTO `department_lang` VALUES ('2', '1', 'en', '123123', '123123123');
INSERT INTO `department_lang` VALUES ('3', '2', 'vi', '123', '333');
INSERT INTO `department_lang` VALUES ('4', '3', 'vi', '123', '1111');
INSERT INTO `department_lang` VALUES ('5', '3', 'en', '123', '123');

-- ----------------------------
-- Table structure for distributor
-- ----------------------------
DROP TABLE IF EXISTS `distributor`;
CREATE TABLE `distributor` (
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `area` int(11) NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of distributor
-- ----------------------------

-- ----------------------------
-- Table structure for distributor_lang
-- ----------------------------
DROP TABLE IF EXISTS `distributor_lang`;
CREATE TABLE `distributor_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributor_id` int(11) NOT NULL,
  `language` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of distributor_lang
-- ----------------------------
INSERT INTO `distributor_lang` VALUES ('2', '2', 'vi', 'Đại lý 1', '');
INSERT INTO `distributor_lang` VALUES ('3', '3', 'vi', 'Nhà phân phối 2', '');
INSERT INTO `distributor_lang` VALUES ('4', '4', 'vi', 'dl2', '');
INSERT INTO `distributor_lang` VALUES ('5', '5', 'vi', 'Nhà phân phối 1', '');
INSERT INTO `distributor_lang` VALUES ('6', '6', 'vi', 'npp2', '');
INSERT INTO `distributor_lang` VALUES ('7', '7', 'vi', 'npp3', '');
INSERT INTO `distributor_lang` VALUES ('8', '8', 'vi', 'dl1', '');

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of language
-- ----------------------------
INSERT INTO `language` VALUES ('1', 'Việt Nam', 'vi', 'vn', '1');
INSERT INTO `language` VALUES ('2', 'United States', 'en', 'us', '1');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m160421_043307_delete_column_department', '1463477349');
INSERT INTO `migration` VALUES ('m160421_083357_dropcolumns_address_department', '1463477369');
INSERT INTO `migration` VALUES ('m160421_084144_add_columns_address', '1463477384');
INSERT INTO `migration` VALUES ('m160423_045131_add_sliderLang', '1463477424');
INSERT INTO `migration` VALUES ('m160425_113020_add_showroom_lang', '1463477462');
INSERT INTO `migration` VALUES ('m160426_050300_delete_type_add_language', '1463477499');
INSERT INTO `migration` VALUES ('m160426_094418_alter_columns_slider', '1463477536');
INSERT INTO `migration` VALUES ('m160426_095630_create_table_distributor', '1463477536');
INSERT INTO `migration` VALUES ('m160426_101221_distributor_lang', '1463477561');
INSERT INTO `migration` VALUES ('m160429_041822_alter_column_distributor', '1463477561');
INSERT INTO `migration` VALUES ('m160429_083047_add_column_to_distributor', '1463477576');
INSERT INTO `migration` VALUES ('m160506_045854_add_columns_to_distributor', '1463477576');
INSERT INTO `migration` VALUES ('m160507_075934_add_profile', '1463477595');
INSERT INTO `migration` VALUES ('m160507_092130_alter_columns_product', '1463477622');
INSERT INTO `migration` VALUES ('m160509_032032_alter_columns_contact_reply', '1463477647');
INSERT INTO `migration` VALUES ('m160510_113814_alter_column_category', '1463477647');
INSERT INTO `migration` VALUES ('m160511_114630_move_distributor_to_user', '1463477730');
INSERT INTO `migration` VALUES ('m160512_030645_fk_to_user', '1463477753');
INSERT INTO `migration` VALUES ('m160512_043524_dropcolumns_user', '1463477753');
INSERT INTO `migration` VALUES ('m160512_043549_rename_column_order', '1463477755');
INSERT INTO `migration` VALUES ('m160516_022051_droptable_product_image', '1463477770');
INSERT INTO `migration` VALUES ('m160516_025421_add_columns_to_product', '1463477773');
INSERT INTO `migration` VALUES ('m160516_040746_drop_column_user_id', '1463477774');
INSERT INTO `migration` VALUES ('m160516_041323_alter_column_product', '1463477775');
INSERT INTO `migration` VALUES ('m160517_034742_delete_view_column', '1463477775');
INSERT INTO `migration` VALUES ('m160517_040137_delete_view_banner', '1463477776');
INSERT INTO `migration` VALUES ('m160517_075240_addcolumn_status', '1463477777');
INSERT INTO `migration` VALUES ('m160517_083254_alter_column_order', '1463477778');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` int(11) NOT NULL,
  `phone_number` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `shipping_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_item
-- ----------------------------

-- ----------------------------
-- Table structure for page
-- ----------------------------
DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of page
-- ----------------------------
INSERT INTO `page` VALUES ('1', '123', '1');
INSERT INTO `page` VALUES ('2', '123', '1');
INSERT INTO `page` VALUES ('3', '123', '0');
INSERT INTO `page` VALUES ('4', '123', '0');
INSERT INTO `page` VALUES ('5', '123123', '0');
INSERT INTO `page` VALUES ('6', '123', '0');

-- ----------------------------
-- Table structure for page_lang
-- ----------------------------
DROP TABLE IF EXISTS `page_lang`;
CREATE TABLE `page_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `intro` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pagelang_ibfk_1` (`page_id`),
  CONSTRAINT `pagelang_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of page_lang
-- ----------------------------
INSERT INTO `page_lang` VALUES ('1', '3', 'vi', '123', '123', '123');
INSERT INTO `page_lang` VALUES ('2', '4', 'vi', 'My name\'s Phuong', '123', '123');
INSERT INTO `page_lang` VALUES ('3', '6', 'vi', '123123', '1231', '123');
INSERT INTO `page_lang` VALUES ('4', '1', 'vi', '123', '123', '123');

-- ----------------------------
-- Table structure for phrase
-- ----------------------------
DROP TABLE IF EXISTS `phrase`;
CREATE TABLE `phrase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=404 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phrase
-- ----------------------------
INSERT INTO `phrase` VALUES ('1', 'language');
INSERT INTO `phrase` VALUES ('2', 'list_of_available_x');
INSERT INTO `phrase` VALUES ('3', 'add_a_new');
INSERT INTO `phrase` VALUES ('4', 'action');
INSERT INTO `phrase` VALUES ('5', 'delete_warning');
INSERT INTO `phrase` VALUES ('6', 'update');
INSERT INTO `phrase` VALUES ('7', 'delete');
INSERT INTO `phrase` VALUES ('8', 'language_code');
INSERT INTO `phrase` VALUES ('9', 'country_code');
INSERT INTO `phrase` VALUES ('10', 'status');
INSERT INTO `phrase` VALUES ('11', 'phrase');
INSERT INTO `phrase` VALUES ('12', 'double_click_to_edit');
INSERT INTO `phrase` VALUES ('13', 'success_add_update_x_successfully');
INSERT INTO `phrase` VALUES ('14', 'warning_x_existed');
INSERT INTO `phrase` VALUES ('15', 'error_mark_required');
INSERT INTO `phrase` VALUES ('16', 'ex_x_welcome_to_my_site');
INSERT INTO `phrase` VALUES ('17', 'support');
INSERT INTO `phrase` VALUES ('18', 'support_max_5_params_in_a_sentence');
INSERT INTO `phrase` VALUES ('19', 'params_is_started_from_1_and_ended_with_5');
INSERT INTO `phrase` VALUES ('20', 'create');
INSERT INTO `phrase` VALUES ('21', 'close');
INSERT INTO `phrase` VALUES ('22', 'category');
INSERT INTO `phrase` VALUES ('23', 'update_x');
INSERT INTO `phrase` VALUES ('24', 'back');
INSERT INTO `phrase` VALUES ('25', 'details');
INSERT INTO `phrase` VALUES ('26', 'view_x');
INSERT INTO `phrase` VALUES ('27', 'list_x');
INSERT INTO `phrase` VALUES ('28', 'Actions');
INSERT INTO `phrase` VALUES ('29', 'page');
INSERT INTO `phrase` VALUES ('30', 'create_x');
INSERT INTO `phrase` VALUES ('33', 'manage_x');
INSERT INTO `phrase` VALUES ('36', 'dashboard');
INSERT INTO `phrase` VALUES ('37', 'product');
INSERT INTO `phrase` VALUES ('38', 'x_category');
INSERT INTO `phrase` VALUES ('39', 'news');
INSERT INTO `phrase` VALUES ('40', 'project');
INSERT INTO `phrase` VALUES ('42', 'media');
INSERT INTO `phrase` VALUES ('43', 'picture');
INSERT INTO `phrase` VALUES ('44', 'audio');
INSERT INTO `phrase` VALUES ('45', 'video');
INSERT INTO `phrase` VALUES ('46', 'banner');
INSERT INTO `phrase` VALUES ('47', 'popup');
INSERT INTO `phrase` VALUES ('48', 'client');
INSERT INTO `phrase` VALUES ('49', 'recruitment');
INSERT INTO `phrase` VALUES ('50', 'user');
INSERT INTO `phrase` VALUES ('51', 'x_management');
INSERT INTO `phrase` VALUES ('52', 'user_role');
INSERT INTO `phrase` VALUES ('53', 'contact');
INSERT INTO `phrase` VALUES ('54', 'setting');
INSERT INTO `phrase` VALUES ('57', 'hello');
INSERT INTO `phrase` VALUES ('59', 'logout');
INSERT INTO `phrase` VALUES ('60', 'information');
INSERT INTO `phrase` VALUES ('61', 'account');
INSERT INTO `phrase` VALUES ('62', 'password');
INSERT INTO `phrase` VALUES ('64', 'login');
INSERT INTO `phrase` VALUES ('65', 'general_setting');
INSERT INTO `phrase` VALUES ('68', 'no');
INSERT INTO `phrase` VALUES ('69', 'yes');
INSERT INTO `phrase` VALUES ('72', 'slug');
INSERT INTO `phrase` VALUES ('73', 'type');
INSERT INTO `phrase` VALUES ('77', 'configuration');
INSERT INTO `phrase` VALUES ('78', 'save');
INSERT INTO `phrase` VALUES ('79', 'edit_x');
INSERT INTO `phrase` VALUES ('80', 'aboutus');
INSERT INTO `phrase` VALUES ('85', 'title');
INSERT INTO `phrase` VALUES ('86', 'image');
INSERT INTO `phrase` VALUES ('87', 'position');
INSERT INTO `phrase` VALUES ('89', 'user_id');
INSERT INTO `phrase` VALUES ('90', 'created_date');
INSERT INTO `phrase` VALUES ('92', 'translate');
INSERT INTO `phrase` VALUES ('99', 'home');
INSERT INTO `phrase` VALUES ('100', 'welcome_text');
INSERT INTO `phrase` VALUES ('101', 'remember_password');
INSERT INTO `phrase` VALUES ('102', 'change_password');
INSERT INTO `phrase` VALUES ('108', 'unread');
INSERT INTO `phrase` VALUES ('109', 'read');
INSERT INTO `phrase` VALUES ('111', 'full_name');
INSERT INTO `phrase` VALUES ('112', 'phone');
INSERT INTO `phrase` VALUES ('113', 'content');
INSERT INTO `phrase` VALUES ('115', 'sort');
INSERT INTO `phrase` VALUES ('116', 'feature');
INSERT INTO `phrase` VALUES ('117', 'completed');
INSERT INTO `phrase` VALUES ('118', 'uncompleted');
INSERT INTO `phrase` VALUES ('119', 'existed');
INSERT INTO `phrase` VALUES ('120', 'icon');
INSERT INTO `phrase` VALUES ('121', 'parent_category');
INSERT INTO `phrase` VALUES ('122', 'home_page');
INSERT INTO `phrase` VALUES ('123', 'about');
INSERT INTO `phrase` VALUES ('124', 'name');
INSERT INTO `phrase` VALUES ('125', 'view');
INSERT INTO `phrase` VALUES ('126', 'post');
INSERT INTO `phrase` VALUES ('127', 'list');
INSERT INTO `phrase` VALUES ('130', 'success');
INSERT INTO `phrase` VALUES ('132', 'permission');
INSERT INTO `phrase` VALUES ('133', 'is_backend_login');
INSERT INTO `phrase` VALUES ('134', 'background');
INSERT INTO `phrase` VALUES ('141', 'email');
INSERT INTO `phrase` VALUES ('142', 'username');
INSERT INTO `phrase` VALUES ('143', 'first_name');
INSERT INTO `phrase` VALUES ('144', 'last_name');
INSERT INTO `phrase` VALUES ('145', 'gender');
INSERT INTO `phrase` VALUES ('146', 'birthday');
INSERT INTO `phrase` VALUES ('147', 'update_date');
INSERT INTO `phrase` VALUES ('148', 'old_password');
INSERT INTO `phrase` VALUES ('149', 'new_password');
INSERT INTO `phrase` VALUES ('150', 'confirm_password');
INSERT INTO `phrase` VALUES ('153', 'executing');
INSERT INTO `phrase` VALUES ('154', 'executed');
INSERT INTO `phrase` VALUES ('155', 'price');
INSERT INTO `phrase` VALUES ('156', 'left');
INSERT INTO `phrase` VALUES ('157', 'right');
INSERT INTO `phrase` VALUES ('158', 'url');
INSERT INTO `phrase` VALUES ('159', 'description');
INSERT INTO `phrase` VALUES ('160', 'hot_news');
INSERT INTO `phrase` VALUES ('161', 'minutes_ago');
INSERT INTO `phrase` VALUES ('163', 'hours_ago');
INSERT INTO `phrase` VALUES ('164', 'contact_for_price');
INSERT INTO `phrase` VALUES ('165', 'view_all');
INSERT INTO `phrase` VALUES ('166', 'fullname');
INSERT INTO `phrase` VALUES ('167', 'send');
INSERT INTO `phrase` VALUES ('168', 'user_role_id');
INSERT INTO `phrase` VALUES ('169', 'online_support');
INSERT INTO `phrase` VALUES ('170', 'our_client');
INSERT INTO `phrase` VALUES ('171', 'our_clients');
INSERT INTO `phrase` VALUES ('172', 'search');
INSERT INTO `phrase` VALUES ('173', 'parent_id');
INSERT INTO `phrase` VALUES ('174', 'previous');
INSERT INTO `phrase` VALUES ('175', 'next');
INSERT INTO `phrase` VALUES ('176', 'order_by');
INSERT INTO `phrase` VALUES ('177', 'product_name');
INSERT INTO `phrase` VALUES ('178', 'asc');
INSERT INTO `phrase` VALUES ('179', 'desc');
INSERT INTO `phrase` VALUES ('180', 'display');
INSERT INTO `phrase` VALUES ('181', 'product_per_page');
INSERT INTO `phrase` VALUES ('184', 'company_information');
INSERT INTO `phrase` VALUES ('185', 'become_our_client');
INSERT INTO `phrase` VALUES ('186', 'content_our_client');
INSERT INTO `phrase` VALUES ('187', 'become_our_partners');
INSERT INTO `phrase` VALUES ('188', 'about_general_director');
INSERT INTO `phrase` VALUES ('189', 'about_company');
INSERT INTO `phrase` VALUES ('190', 'development_process');
INSERT INTO `phrase` VALUES ('191', 'award');
INSERT INTO `phrase` VALUES ('192', 'typical_project');
INSERT INTO `phrase` VALUES ('193', 'order');
INSERT INTO `phrase` VALUES ('194', 'total_amount');
INSERT INTO `phrase` VALUES ('195', 'shipping_address');
INSERT INTO `phrase` VALUES ('196', 'message');
INSERT INTO `phrase` VALUES ('197', 'invoice');
INSERT INTO `phrase` VALUES ('198', 'invoice_information');
INSERT INTO `phrase` VALUES ('199', 'order_no');
INSERT INTO `phrase` VALUES ('200', 'customer_information');
INSERT INTO `phrase` VALUES ('201', 'quantity');
INSERT INTO `phrase` VALUES ('203', 'export_to_excel');
INSERT INTO `phrase` VALUES ('204', 'add_image');
INSERT INTO `phrase` VALUES ('205', 'caption');
INSERT INTO `phrase` VALUES ('206', 'is_main_image');
INSERT INTO `phrase` VALUES ('209', 'list_categories');
INSERT INTO `phrase` VALUES ('210', 'headquarter_address');
INSERT INTO `phrase` VALUES ('211', 'factory_address');
INSERT INTO `phrase` VALUES ('212', 'office_address');
INSERT INTO `phrase` VALUES ('213', 'hotline');
INSERT INTO `phrase` VALUES ('214', 'connect_with_us');
INSERT INTO `phrase` VALUES ('215', 'sales_consultant');
INSERT INTO `phrase` VALUES ('217', 'technology_production');
INSERT INTO `phrase` VALUES ('218', 'slider');
INSERT INTO `phrase` VALUES ('219', 'search_product');
INSERT INTO `phrase` VALUES ('220', 'x_products');
INSERT INTO `phrase` VALUES ('221', 'product_newest');
INSERT INTO `phrase` VALUES ('222', 'price_asc');
INSERT INTO `phrase` VALUES ('223', 'price_desc');
INSERT INTO `phrase` VALUES ('224', 'feature_product');
INSERT INTO `phrase` VALUES ('225', 'add_to_cart');
INSERT INTO `phrase` VALUES ('226', 'product_information');
INSERT INTO `phrase` VALUES ('227', 'customer_comment');
INSERT INTO `phrase` VALUES ('228', 'related_product');
INSERT INTO `phrase` VALUES ('229', 'caption_in_x');
INSERT INTO `phrase` VALUES ('231', 'communication_news');
INSERT INTO `phrase` VALUES ('232', 'read_more');
INSERT INTO `phrase` VALUES ('233', 'your_cart');
INSERT INTO `phrase` VALUES ('234', 'product_price');
INSERT INTO `phrase` VALUES ('235', 'total');
INSERT INTO `phrase` VALUES ('236', 'cancel');
INSERT INTO `phrase` VALUES ('237', 'continue_shopping');
INSERT INTO `phrase` VALUES ('238', 'place_order');
INSERT INTO `phrase` VALUES ('239', 'cart_empty');
INSERT INTO `phrase` VALUES ('240', 'go_to_cart');
INSERT INTO `phrase` VALUES ('241', 'technical');
INSERT INTO `phrase` VALUES ('242', 'order_information');
INSERT INTO `phrase` VALUES ('243', 'personal_information');
INSERT INTO `phrase` VALUES ('244', 'thankyou_for_your_order');
INSERT INTO `phrase` VALUES ('245', 'thankyou');
INSERT INTO `phrase` VALUES ('246', 'company_name');
INSERT INTO `phrase` VALUES ('247', 'address');
INSERT INTO `phrase` VALUES ('249', 'showroom');
INSERT INTO `phrase` VALUES ('250', 'related_post');
INSERT INTO `phrase` VALUES ('251', 'prev_news');
INSERT INTO `phrase` VALUES ('252', 'next_news');
INSERT INTO `phrase` VALUES ('253', 'in_stock');
INSERT INTO `phrase` VALUES ('254', 'product_not_in_stock');
INSERT INTO `phrase` VALUES ('255', 'not_in_stock');
INSERT INTO `phrase` VALUES ('256', 'perform_indepth_development_goals');
INSERT INTO `phrase` VALUES ('257', 'technology');
INSERT INTO `phrase` VALUES ('258', 'equipment');
INSERT INTO `phrase` VALUES ('259', 'send_success');
INSERT INTO `phrase` VALUES ('260', 'send_false');
INSERT INTO `phrase` VALUES ('261', 'from');
INSERT INTO `phrase` VALUES ('262', 'to');
INSERT INTO `phrase` VALUES ('263', 'subject');
INSERT INTO `phrase` VALUES ('264', 'reply');
INSERT INTO `phrase` VALUES ('265', 'create_date');
INSERT INTO `phrase` VALUES ('266', 'project_intro');
INSERT INTO `phrase` VALUES ('267', 'cancelled');
INSERT INTO `phrase` VALUES ('268', 'new');
INSERT INTO `phrase` VALUES ('269', 'received');
INSERT INTO `phrase` VALUES ('270', 'cancel_order');
INSERT INTO `phrase` VALUES ('271', 'receive_order');
INSERT INTO `phrase` VALUES ('272', 'are_you_sure');
INSERT INTO `phrase` VALUES ('273', 'office');
INSERT INTO `phrase` VALUES ('274', 'factory');
INSERT INTO `phrase` VALUES ('275', 'map');
INSERT INTO `phrase` VALUES ('276', 'homepage_lager_text');
INSERT INTO `phrase` VALUES ('277', 'code');
INSERT INTO `phrase` VALUES ('278', 'origin');
INSERT INTO `phrase` VALUES ('279', 'group_product');
INSERT INTO `phrase` VALUES ('280', 'videos_gallery');
INSERT INTO `phrase` VALUES ('281', 'select_from_computer');
INSERT INTO `phrase` VALUES ('282', 'select_from_url');
INSERT INTO `phrase` VALUES ('283', 'only_youtube_supported');
INSERT INTO `phrase` VALUES ('284', 'search_by_x');
INSERT INTO `phrase` VALUES ('285', 'customer');
INSERT INTO `phrase` VALUES ('286', 'general');
INSERT INTO `phrase` VALUES ('287', 'payment');
INSERT INTO `phrase` VALUES ('288', 'lists');
INSERT INTO `phrase` VALUES ('289', 'country');
INSERT INTO `phrase` VALUES ('290', 'languages');
INSERT INTO `phrase` VALUES ('291', 'add_a_new_x');
INSERT INTO `phrase` VALUES ('292', 'in_use');
INSERT INTO `phrase` VALUES ('293', 'not_in_use');
INSERT INTO `phrase` VALUES ('294', 'fax');
INSERT INTO `phrase` VALUES ('295', 'intro');
INSERT INTO `phrase` VALUES ('296', 'distributor');
INSERT INTO `phrase` VALUES ('297', 'promotion');
INSERT INTO `phrase` VALUES ('298', 'city');
INSERT INTO `phrase` VALUES ('299', 'area');
INSERT INTO `phrase` VALUES ('300', 'agent');
INSERT INTO `phrase` VALUES ('301', 'not_read');
INSERT INTO `phrase` VALUES ('302', 'value');
INSERT INTO `phrase` VALUES ('303', 'key');
INSERT INTO `phrase` VALUES ('304', 'index');
INSERT INTO `phrase` VALUES ('305', 'desc_general');
INSERT INTO `phrase` VALUES ('306', 'site_name');
INSERT INTO `phrase` VALUES ('307', 'desc_site_name');
INSERT INTO `phrase` VALUES ('308', 'site_title');
INSERT INTO `phrase` VALUES ('309', 'desc_site_title');
INSERT INTO `phrase` VALUES ('310', 'site_keywords');
INSERT INTO `phrase` VALUES ('311', 'desc_site_keywords');
INSERT INTO `phrase` VALUES ('312', 'Bảo trì website?');
INSERT INTO `phrase` VALUES ('313', 'desc_Bảo trì website?');
INSERT INTO `phrase` VALUES ('314', 'general_active');
INSERT INTO `phrase` VALUES ('315', 'desc_general_active');
INSERT INTO `phrase` VALUES ('316', 'title_language');
INSERT INTO `phrase` VALUES ('317', 'desc_title_language');
INSERT INTO `phrase` VALUES ('318', 'north');
INSERT INTO `phrase` VALUES ('319', 'middle');
INSERT INTO `phrase` VALUES ('320', 'south');
INSERT INTO `phrase` VALUES ('321', 'not_active');
INSERT INTO `phrase` VALUES ('322', 'active');
INSERT INTO `phrase` VALUES ('323', 'isset_setting');
INSERT INTO `phrase` VALUES ('324', 'desc_isset_setting');
INSERT INTO `phrase` VALUES ('325', 'test_abc');
INSERT INTO `phrase` VALUES ('326', 'desc_test_abc');
INSERT INTO `phrase` VALUES ('327', 'test');
INSERT INTO `phrase` VALUES ('328', 'desc_test');
INSERT INTO `phrase` VALUES ('329', 'text_field');
INSERT INTO `phrase` VALUES ('330', 'desc_text_field');
INSERT INTO `phrase` VALUES ('331', 'email_field');
INSERT INTO `phrase` VALUES ('332', 'desc_email_field');
INSERT INTO `phrase` VALUES ('333', 'number_field');
INSERT INTO `phrase` VALUES ('334', 'desc_number_field');
INSERT INTO `phrase` VALUES ('335', 'textarea_field');
INSERT INTO `phrase` VALUES ('336', 'desc_textarea_field');
INSERT INTO `phrase` VALUES ('337', 'color_field');
INSERT INTO `phrase` VALUES ('338', 'desc_color_field');
INSERT INTO `phrase` VALUES ('339', 'date_field');
INSERT INTO `phrase` VALUES ('340', 'desc_date_field');
INSERT INTO `phrase` VALUES ('341', 'time_field');
INSERT INTO `phrase` VALUES ('342', 'desc_time_field');
INSERT INTO `phrase` VALUES ('343', 'password_field');
INSERT INTO `phrase` VALUES ('344', 'desc_password_field');
INSERT INTO `phrase` VALUES ('345', 'roxymce_field');
INSERT INTO `phrase` VALUES ('346', 'desc_roxymce_field');
INSERT INTO `phrase` VALUES ('347', 'select_field');
INSERT INTO `phrase` VALUES ('348', 'desc_select_field');
INSERT INTO `phrase` VALUES ('349', 'additional');
INSERT INTO `phrase` VALUES ('350', 'multiselect_field');
INSERT INTO `phrase` VALUES ('351', 'desc_multiselect_field');
INSERT INTO `phrase` VALUES ('352', 'file_field');
INSERT INTO `phrase` VALUES ('353', 'desc_file_field');
INSERT INTO `phrase` VALUES ('354', 'url_field');
INSERT INTO `phrase` VALUES ('355', 'desc_url_field');
INSERT INTO `phrase` VALUES ('356', 'percent_field');
INSERT INTO `phrase` VALUES ('357', 'desc_percent_field');
INSERT INTO `phrase` VALUES ('358', 'switch_field');
INSERT INTO `phrase` VALUES ('359', 'desc_switch_field');
INSERT INTO `phrase` VALUES ('360', 'checkbox_option');
INSERT INTO `phrase` VALUES ('361', 'desc_checkbox_option');
INSERT INTO `phrase` VALUES ('362', 'radio_option');
INSERT INTO `phrase` VALUES ('363', 'desc_radio_option');
INSERT INTO `phrase` VALUES ('364', 'department');
INSERT INTO `phrase` VALUES ('365', 'password_mail');
INSERT INTO `phrase` VALUES ('366', 'desc_password_mail');
INSERT INTO `phrase` VALUES ('367', 'go_home');
INSERT INTO `phrase` VALUES ('368', 'go_top');
INSERT INTO `phrase` VALUES ('369', 'admin');
INSERT INTO `phrase` VALUES ('370', 'import_to_excel');
INSERT INTO `phrase` VALUES ('371', 'Bảo trì website');
INSERT INTO `phrase` VALUES ('372', 'desc_Bảo trì website');
INSERT INTO `phrase` VALUES ('373', 'Ngôn ngữ mặc định');
INSERT INTO `phrase` VALUES ('374', 'desc_Ngôn ngữ mặc định');
INSERT INTO `phrase` VALUES ('375', 'general_language');
INSERT INTO `phrase` VALUES ('376', 'desc_general_language');
INSERT INTO `phrase` VALUES ('377', 'general_url');
INSERT INTO `phrase` VALUES ('378', 'desc_general_url');
INSERT INTO `phrase` VALUES ('379', 'general_title');
INSERT INTO `phrase` VALUES ('380', 'desc_general_title');
INSERT INTO `phrase` VALUES ('381', 'general_logo');
INSERT INTO `phrase` VALUES ('382', 'desc_general_logo');
INSERT INTO `phrase` VALUES ('383', 'general_hotline');
INSERT INTO `phrase` VALUES ('384', 'desc_general_hotline');
INSERT INTO `phrase` VALUES ('385', 'general_showroom');
INSERT INTO `phrase` VALUES ('386', 'desc_general_showroom');
INSERT INTO `phrase` VALUES ('387', 'note');
INSERT INTO `phrase` VALUES ('388', 'email_type');
INSERT INTO `phrase` VALUES ('389', 'desc_email_type');
INSERT INTO `phrase` VALUES ('390', 'email_secure');
INSERT INTO `phrase` VALUES ('391', 'desc_email_secure');
INSERT INTO `phrase` VALUES ('392', 'email_port');
INSERT INTO `phrase` VALUES ('393', 'desc_email_port');
INSERT INTO `phrase` VALUES ('394', 'email_host');
INSERT INTO `phrase` VALUES ('395', 'desc_email_host');
INSERT INTO `phrase` VALUES ('396', 'general_favicon');
INSERT INTO `phrase` VALUES ('397', 'desc_general_favicon');
INSERT INTO `phrase` VALUES ('398', 'import_excel');
INSERT INTO `phrase` VALUES ('399', 'view_detail');
INSERT INTO `phrase` VALUES ('400', 'newest');
INSERT INTO `phrase` VALUES ('401', 'for_deapartment');
INSERT INTO `phrase` VALUES ('402', 'forgot_password');
INSERT INTO `phrase` VALUES ('403', 'business_room');

-- ----------------------------
-- Table structure for phrase_translate
-- ----------------------------
DROP TABLE IF EXISTS `phrase_translate`;
CREATE TABLE `phrase_translate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=576 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phrase_translate
-- ----------------------------
INSERT INTO `phrase_translate` VALUES ('1', '1', '1', 'Ngôn ngữ');
INSERT INTO `phrase_translate` VALUES ('2', '2', '1', 'Danh sách {1}');
INSERT INTO `phrase_translate` VALUES ('3', '3', '1', 'Thêm mới');
INSERT INTO `phrase_translate` VALUES ('4', '4', '1', 'Hành động');
INSERT INTO `phrase_translate` VALUES ('5', '5', '1', 'Nếu bạn xóa mục này, toàn bộ dữ liệu đi kèm cũng sẽ bị xóa. Bạn có chắc chắn muốn xóa không?');
INSERT INTO `phrase_translate` VALUES ('6', '6', '1', 'Cập nhật');
INSERT INTO `phrase_translate` VALUES ('7', '7', '1', 'Xóa');
INSERT INTO `phrase_translate` VALUES ('8', '8', '1', 'Mã ngôn ngữ');
INSERT INTO `phrase_translate` VALUES ('9', '9', '1', 'Mã quốc gia');
INSERT INTO `phrase_translate` VALUES ('10', '10', '1', 'Trạng thái');
INSERT INTO `phrase_translate` VALUES ('11', '11', '1', 'Từ ngữ');
INSERT INTO `phrase_translate` VALUES ('12', '12', '1', 'Clip đúp chuột để sửa');
INSERT INTO `phrase_translate` VALUES ('13', '13', '1', '<strong>Chúc mừng!</strong> {1} {2} hoàn tất!');
INSERT INTO `phrase_translate` VALUES ('14', '14', '1', '<strong>Cảnh báo!</strong> Những {1} này đã có trong dữ liệu, vui lòng tìm và cập nhật chúng!');
INSERT INTO `phrase_translate` VALUES ('15', '15', '1', '<strong>Có lỗi!</strong> Các trường có dấu (*) là bắt buộc!');
INSERT INTO `phrase_translate` VALUES ('16', '16', '1', 'VD(VI): Chào mừng ghé thăm website');
INSERT INTO `phrase_translate` VALUES ('17', '17', '1', 'Hỗ trợ');
INSERT INTO `phrase_translate` VALUES ('18', '18', '1', 'Hỗ trợ tối đa 5 tham số');
INSERT INTO `phrase_translate` VALUES ('19', '19', '1', 'Tham số bắt đầu từ 1 đến 5');
INSERT INTO `phrase_translate` VALUES ('20', '20', '1', 'Thêm mới');
INSERT INTO `phrase_translate` VALUES ('21', '21', '1', 'Đóng');
INSERT INTO `phrase_translate` VALUES ('22', '21', '2', 'Close');
INSERT INTO `phrase_translate` VALUES ('23', '20', '2', 'Create');
INSERT INTO `phrase_translate` VALUES ('24', '19', '2', 'Params is started from 1 and ended with 5');
INSERT INTO `phrase_translate` VALUES ('25', '18', '2', 'System supports max 5 params in a sentence');
INSERT INTO `phrase_translate` VALUES ('26', '17', '2', 'Support');
INSERT INTO `phrase_translate` VALUES ('27', '16', '2', 'Ex(EN): Welcome to my site');
INSERT INTO `phrase_translate` VALUES ('28', '15', '2', '<strong>Error!</strong> Field (*) is required!');
INSERT INTO `phrase_translate` VALUES ('29', '14', '2', '<strong>Warning!</strong> This {1} has been existed, please find and update it!');
INSERT INTO `phrase_translate` VALUES ('30', '13', '2', '<strong>Success!</strong> {1} {2} successfully!');
INSERT INTO `phrase_translate` VALUES ('31', '12', '2', 'Double click to edit');
INSERT INTO `phrase_translate` VALUES ('32', '11', '2', 'Phrase');
INSERT INTO `phrase_translate` VALUES ('33', '10', '2', 'Status');
INSERT INTO `phrase_translate` VALUES ('34', '9', '2', 'Country Code');
INSERT INTO `phrase_translate` VALUES ('35', '8', '2', 'Language Code');
INSERT INTO `phrase_translate` VALUES ('36', '7', '2', 'Delete');
INSERT INTO `phrase_translate` VALUES ('37', '6', '2', 'Update');
INSERT INTO `phrase_translate` VALUES ('38', '5', '2', 'If you delete this item, all related data is deleted. Are you sure ?');
INSERT INTO `phrase_translate` VALUES ('39', '4', '2', 'Action');
INSERT INTO `phrase_translate` VALUES ('40', '3', '2', 'Add a new');
INSERT INTO `phrase_translate` VALUES ('41', '2', '2', 'List of available {1}');
INSERT INTO `phrase_translate` VALUES ('42', '1', '2', 'Language');
INSERT INTO `phrase_translate` VALUES ('43', '22', '1', 'Danh mục');
INSERT INTO `phrase_translate` VALUES ('44', '23', '1', 'Cập nhật {1}');
INSERT INTO `phrase_translate` VALUES ('45', '24', '1', 'Quay lại');
INSERT INTO `phrase_translate` VALUES ('46', '25', '1', 'Chi tiết');
INSERT INTO `phrase_translate` VALUES ('47', '26', '1', 'Xem {1}');
INSERT INTO `phrase_translate` VALUES ('48', '27', '1', 'Danh sách {1}');
INSERT INTO `phrase_translate` VALUES ('49', '28', '1', 'Thao tác');
INSERT INTO `phrase_translate` VALUES ('50', '22', '2', 'Categories');
INSERT INTO `phrase_translate` VALUES ('51', '23', '2', 'Update {1}');
INSERT INTO `phrase_translate` VALUES ('52', '25', '2', 'Details');
INSERT INTO `phrase_translate` VALUES ('53', '26', '2', 'View {1}');
INSERT INTO `phrase_translate` VALUES ('54', '24', '2', 'Back');
INSERT INTO `phrase_translate` VALUES ('55', '27', '2', 'List of {1}');
INSERT INTO `phrase_translate` VALUES ('56', '28', '2', 'Actions');
INSERT INTO `phrase_translate` VALUES ('57', '29', '1', 'Trang');
INSERT INTO `phrase_translate` VALUES ('58', '30', '1', 'Thêm mới {1}');
INSERT INTO `phrase_translate` VALUES ('61', '33', '1', 'Quản lý {1}');
INSERT INTO `phrase_translate` VALUES ('62', '29', '2', 'Page');
INSERT INTO `phrase_translate` VALUES ('63', '30', '2', 'Create a new {1}');
INSERT INTO `phrase_translate` VALUES ('64', '33', '2', '{1} Management');
INSERT INTO `phrase_translate` VALUES ('67', '36', '1', 'Bảng điều khiển');
INSERT INTO `phrase_translate` VALUES ('68', '37', '1', 'Sản phẩm');
INSERT INTO `phrase_translate` VALUES ('69', '38', '1', 'Danh mục {1}');
INSERT INTO `phrase_translate` VALUES ('70', '39', '1', 'Tin tức');
INSERT INTO `phrase_translate` VALUES ('71', '40', '1', 'Dự án');
INSERT INTO `phrase_translate` VALUES ('73', '42', '1', 'Thư viện');
INSERT INTO `phrase_translate` VALUES ('74', '43', '1', 'Hình ảnh');
INSERT INTO `phrase_translate` VALUES ('75', '44', '1', 'Bài hát về Quang Trung');
INSERT INTO `phrase_translate` VALUES ('76', '45', '1', 'Video');
INSERT INTO `phrase_translate` VALUES ('77', '46', '1', 'Banner');
INSERT INTO `phrase_translate` VALUES ('78', '47', '1', 'Popup');
INSERT INTO `phrase_translate` VALUES ('79', '48', '1', 'Đối tác');
INSERT INTO `phrase_translate` VALUES ('80', '49', '1', 'Tuyển dụng');
INSERT INTO `phrase_translate` VALUES ('81', '50', '1', 'Người dùng');
INSERT INTO `phrase_translate` VALUES ('82', '51', '1', 'Quản lý {1}');
INSERT INTO `phrase_translate` VALUES ('83', '52', '1', 'Phân quyền');
INSERT INTO `phrase_translate` VALUES ('84', '53', '1', 'Liên hệ');
INSERT INTO `phrase_translate` VALUES ('85', '54', '1', 'Cấu hình hệ thống');
INSERT INTO `phrase_translate` VALUES ('88', '57', '1', 'Xin chào');
INSERT INTO `phrase_translate` VALUES ('90', '59', '1', 'Đăng xuất');
INSERT INTO `phrase_translate` VALUES ('91', '36', '2', 'Dashboard');
INSERT INTO `phrase_translate` VALUES ('92', '37', '2', 'Product');
INSERT INTO `phrase_translate` VALUES ('93', '38', '2', '{1} category');
INSERT INTO `phrase_translate` VALUES ('94', '39', '2', 'News');
INSERT INTO `phrase_translate` VALUES ('95', '40', '2', 'Project');
INSERT INTO `phrase_translate` VALUES ('96', '42', '2', 'Library');
INSERT INTO `phrase_translate` VALUES ('97', '43', '2', 'Images');
INSERT INTO `phrase_translate` VALUES ('98', '44', '2', 'The Song of Quang Trung');
INSERT INTO `phrase_translate` VALUES ('99', '45', '2', 'Video');
INSERT INTO `phrase_translate` VALUES ('100', '46', '2', 'Banner');
INSERT INTO `phrase_translate` VALUES ('101', '47', '2', 'Popup');
INSERT INTO `phrase_translate` VALUES ('102', '48', '2', 'Partners');
INSERT INTO `phrase_translate` VALUES ('103', '49', '2', 'Recruitment');
INSERT INTO `phrase_translate` VALUES ('104', '50', '2', 'User');
INSERT INTO `phrase_translate` VALUES ('105', '51', '2', '{1} Management');
INSERT INTO `phrase_translate` VALUES ('106', '52', '2', 'User Role');
INSERT INTO `phrase_translate` VALUES ('107', '53', '2', 'Contact');
INSERT INTO `phrase_translate` VALUES ('108', '54', '2', 'Settings');
INSERT INTO `phrase_translate` VALUES ('110', '59', '2', 'Logout');
INSERT INTO `phrase_translate` VALUES ('111', '60', '1', 'Thông tin');
INSERT INTO `phrase_translate` VALUES ('112', '61', '1', 'Tài khoản');
INSERT INTO `phrase_translate` VALUES ('113', '62', '1', 'Mật khẩu');
INSERT INTO `phrase_translate` VALUES ('115', '64', '1', 'Đăng nhập');
INSERT INTO `phrase_translate` VALUES ('116', '57', '2', 'Hello');
INSERT INTO `phrase_translate` VALUES ('117', '65', '1', 'Cấu hình chung');
INSERT INTO `phrase_translate` VALUES ('119', '60', '2', 'Information');
INSERT INTO `phrase_translate` VALUES ('120', '61', '2', 'Account');
INSERT INTO `phrase_translate` VALUES ('121', '62', '2', 'Password');
INSERT INTO `phrase_translate` VALUES ('123', '65', '2', 'General settings');
INSERT INTO `phrase_translate` VALUES ('124', '64', '2', 'Login');
INSERT INTO `phrase_translate` VALUES ('125', '68', '1', 'Không');
INSERT INTO `phrase_translate` VALUES ('126', '69', '1', 'Có');
INSERT INTO `phrase_translate` VALUES ('129', '72', '1', 'Đường dẫn tĩnh');
INSERT INTO `phrase_translate` VALUES ('130', '73', '1', 'Loại');
INSERT INTO `phrase_translate` VALUES ('134', '69', '2', 'Yes');
INSERT INTO `phrase_translate` VALUES ('135', '68', '2', 'No');
INSERT INTO `phrase_translate` VALUES ('136', '72', '2', 'Slug ');
INSERT INTO `phrase_translate` VALUES ('137', '73', '2', 'Type ');
INSERT INTO `phrase_translate` VALUES ('138', '77', '1', 'Cấu hình');
INSERT INTO `phrase_translate` VALUES ('139', '78', '1', 'Lưu');
INSERT INTO `phrase_translate` VALUES ('140', '77', '2', 'Configuration ');
INSERT INTO `phrase_translate` VALUES ('141', '78', '2', 'Save ');
INSERT INTO `phrase_translate` VALUES ('142', '79', '1', 'Thay đổi {1}');
INSERT INTO `phrase_translate` VALUES ('143', '79', '2', 'Update {1} ');
INSERT INTO `phrase_translate` VALUES ('144', '80', '1', 'Về chúng tôi');
INSERT INTO `phrase_translate` VALUES ('149', '85', '1', 'Tiêu đề');
INSERT INTO `phrase_translate` VALUES ('150', '86', '1', 'Hình ảnh');
INSERT INTO `phrase_translate` VALUES ('151', '87', '1', 'Vị trí');
INSERT INTO `phrase_translate` VALUES ('153', '89', '1', 'ID');
INSERT INTO `phrase_translate` VALUES ('154', '90', '1', 'Ngày tạo');
INSERT INTO `phrase_translate` VALUES ('155', '90', '2', 'Created date ');
INSERT INTO `phrase_translate` VALUES ('156', '89', '2', 'ID ');
INSERT INTO `phrase_translate` VALUES ('157', '85', '2', 'Title ');
INSERT INTO `phrase_translate` VALUES ('158', '87', '2', 'Position ');
INSERT INTO `phrase_translate` VALUES ('159', '86', '2', 'Image');
INSERT INTO `phrase_translate` VALUES ('160', '80', '2', 'About us');
INSERT INTO `phrase_translate` VALUES ('162', '99', '2', 'Home');
INSERT INTO `phrase_translate` VALUES ('163', '100', '2', 'Welcome to Website Management page');
INSERT INTO `phrase_translate` VALUES ('164', '101', '2', 'Remember password?  ');
INSERT INTO `phrase_translate` VALUES ('165', '102', '2', 'Change password');
INSERT INTO `phrase_translate` VALUES ('166', '99', '1', 'Home');
INSERT INTO `phrase_translate` VALUES ('169', '92', '1', 'Dịch ');
INSERT INTO `phrase_translate` VALUES ('170', '92', '2', 'Translate ');
INSERT INTO `phrase_translate` VALUES ('171', '100', '1', 'Xin chào. Bạn đang truy cập vào trang Quản trị hệ thống ');
INSERT INTO `phrase_translate` VALUES ('172', '101', '1', 'Ghi nhớ mật khẩu? ');
INSERT INTO `phrase_translate` VALUES ('173', '102', '1', 'Đổi mật khẩu ');
INSERT INTO `phrase_translate` VALUES ('179', '108', '1', 'Chưa đọc');
INSERT INTO `phrase_translate` VALUES ('180', '109', '1', 'Đã đọc');
INSERT INTO `phrase_translate` VALUES ('182', '111', '1', 'Họ tên');
INSERT INTO `phrase_translate` VALUES ('183', '112', '1', 'Số điện thoại');
INSERT INTO `phrase_translate` VALUES ('184', '113', '1', 'Nội dung');
INSERT INTO `phrase_translate` VALUES ('186', '115', '1', 'Thứ tự');
INSERT INTO `phrase_translate` VALUES ('187', '116', '1', 'Nổi bật');
INSERT INTO `phrase_translate` VALUES ('188', '117', '1', 'Hoàn thành');
INSERT INTO `phrase_translate` VALUES ('189', '118', '1', 'Chưa hoàn thành');
INSERT INTO `phrase_translate` VALUES ('190', '119', '1', 'Đã tồn tại');
INSERT INTO `phrase_translate` VALUES ('191', '120', '1', 'Biểu tượng');
INSERT INTO `phrase_translate` VALUES ('193', '108', '2', 'Unread ');
INSERT INTO `phrase_translate` VALUES ('194', '109', '2', 'Read ');
INSERT INTO `phrase_translate` VALUES ('195', '111', '2', 'Full name ');
INSERT INTO `phrase_translate` VALUES ('196', '112', '2', 'Phone ');
INSERT INTO `phrase_translate` VALUES ('197', '113', '2', 'Content ');
INSERT INTO `phrase_translate` VALUES ('198', '115', '2', 'Order ');
INSERT INTO `phrase_translate` VALUES ('199', '116', '2', 'Featured ');
INSERT INTO `phrase_translate` VALUES ('200', '117', '2', 'Completed ');
INSERT INTO `phrase_translate` VALUES ('201', '118', '2', 'Uncompleted ');
INSERT INTO `phrase_translate` VALUES ('202', '119', '2', 'Existed ');
INSERT INTO `phrase_translate` VALUES ('203', '120', '2', 'icon');
INSERT INTO `phrase_translate` VALUES ('204', '121', '1', 'Danh mục cha');
INSERT INTO `phrase_translate` VALUES ('205', '121', '2', 'Parent category  ');
INSERT INTO `phrase_translate` VALUES ('206', '122', '1', 'Trang chủ');
INSERT INTO `phrase_translate` VALUES ('207', '123', '1', 'Giới thiệu');
INSERT INTO `phrase_translate` VALUES ('208', '122', '2', 'Home ');
INSERT INTO `phrase_translate` VALUES ('209', '123', '2', 'About us ');
INSERT INTO `phrase_translate` VALUES ('210', '124', '1', 'Tên');
INSERT INTO `phrase_translate` VALUES ('211', '125', '1', 'Lượt xem');
INSERT INTO `phrase_translate` VALUES ('212', '126', '1', 'Bài viết');
INSERT INTO `phrase_translate` VALUES ('213', '127', '1', 'Danh sách');
INSERT INTO `phrase_translate` VALUES ('216', '130', '1', 'Thành công');
INSERT INTO `phrase_translate` VALUES ('218', '132', '1', 'Quyền hạn');
INSERT INTO `phrase_translate` VALUES ('219', '133', '1', 'Được phép đăng nhập Quản lý');
INSERT INTO `phrase_translate` VALUES ('220', '134', '1', 'Hình nền');
INSERT INTO `phrase_translate` VALUES ('225', '125', '2', 'View ');
INSERT INTO `phrase_translate` VALUES ('228', '141', '1', 'E-mail');
INSERT INTO `phrase_translate` VALUES ('229', '142', '1', 'Tài khoản');
INSERT INTO `phrase_translate` VALUES ('230', '143', '1', 'Họ');
INSERT INTO `phrase_translate` VALUES ('231', '144', '1', 'Tên');
INSERT INTO `phrase_translate` VALUES ('232', '145', '1', 'Giới tính');
INSERT INTO `phrase_translate` VALUES ('233', '146', '1', 'Ngày sinh');
INSERT INTO `phrase_translate` VALUES ('234', '147', '1', 'Ngày cập nhật');
INSERT INTO `phrase_translate` VALUES ('235', '148', '1', 'Mật khẩu cũ');
INSERT INTO `phrase_translate` VALUES ('236', '149', '1', 'Mật khẩu mới');
INSERT INTO `phrase_translate` VALUES ('237', '150', '1', 'Xác nhận mật khẩu');
INSERT INTO `phrase_translate` VALUES ('238', '141', '2', 'E-mail ');
INSERT INTO `phrase_translate` VALUES ('239', '143', '2', 'First name ');
INSERT INTO `phrase_translate` VALUES ('240', '144', '2', 'Last name');
INSERT INTO `phrase_translate` VALUES ('241', '142', '2', 'Username ');
INSERT INTO `phrase_translate` VALUES ('245', '150', '2', 'Confirm password ');
INSERT INTO `phrase_translate` VALUES ('246', '149', '2', 'New password ');
INSERT INTO `phrase_translate` VALUES ('247', '148', '2', 'Old password ');
INSERT INTO `phrase_translate` VALUES ('248', '147', '2', 'Updated date ');
INSERT INTO `phrase_translate` VALUES ('249', '146', '2', 'Birthday ');
INSERT INTO `phrase_translate` VALUES ('250', '145', '2', 'Gender ');
INSERT INTO `phrase_translate` VALUES ('251', '153', '1', 'Đang thực hiện');
INSERT INTO `phrase_translate` VALUES ('252', '154', '1', 'Đã thực hiện');
INSERT INTO `phrase_translate` VALUES ('253', '155', '1', 'Giá');
INSERT INTO `phrase_translate` VALUES ('254', '156', '1', 'Bên trái');
INSERT INTO `phrase_translate` VALUES ('255', '157', '1', 'Bên phải');
INSERT INTO `phrase_translate` VALUES ('256', '158', '1', 'Liên kết');
INSERT INTO `phrase_translate` VALUES ('257', '159', '1', 'Mô tả');
INSERT INTO `phrase_translate` VALUES ('258', '160', '1', 'Tin nóng');
INSERT INTO `phrase_translate` VALUES ('259', '161', '1', 'Phút trước');
INSERT INTO `phrase_translate` VALUES ('261', '163', '1', 'giờ trước');
INSERT INTO `phrase_translate` VALUES ('262', '156', '2', 'Bên trái');
INSERT INTO `phrase_translate` VALUES ('263', '155', '2', 'Price ');
INSERT INTO `phrase_translate` VALUES ('264', '158', '2', 'URL ');
INSERT INTO `phrase_translate` VALUES ('265', '163', '2', 'hours ago ');
INSERT INTO `phrase_translate` VALUES ('266', '161', '2', 'minutes ago ');
INSERT INTO `phrase_translate` VALUES ('267', '160', '2', 'Hot news ');
INSERT INTO `phrase_translate` VALUES ('268', '159', '2', 'Description ');
INSERT INTO `phrase_translate` VALUES ('269', '157', '2', 'Bên phải  ');
INSERT INTO `phrase_translate` VALUES ('270', '154', '2', 'Completed ');
INSERT INTO `phrase_translate` VALUES ('271', '153', '2', 'Developing ');
INSERT INTO `phrase_translate` VALUES ('272', '124', '2', 'Name ');
INSERT INTO `phrase_translate` VALUES ('273', '126', '2', 'Post ');
INSERT INTO `phrase_translate` VALUES ('274', '127', '2', 'List ');
INSERT INTO `phrase_translate` VALUES ('275', '130', '2', 'Success ');
INSERT INTO `phrase_translate` VALUES ('276', '132', '2', 'Permission ');
INSERT INTO `phrase_translate` VALUES ('277', '133', '2', 'Login to backend ');
INSERT INTO `phrase_translate` VALUES ('278', '134', '2', 'Background ');
INSERT INTO `phrase_translate` VALUES ('279', '164', '1', 'Để trống nếu muốn hiển thị giá LIÊN HỆ');
INSERT INTO `phrase_translate` VALUES ('280', '165', '1', 'Xem tất cả');
INSERT INTO `phrase_translate` VALUES ('281', '165', '2', 'View all');
INSERT INTO `phrase_translate` VALUES ('282', '164', '2', 'Leave blank for AGREEMENT price ');
INSERT INTO `phrase_translate` VALUES ('283', '166', '1', 'Tên đầy đủ');
INSERT INTO `phrase_translate` VALUES ('284', '167', '1', 'Gửi');
INSERT INTO `phrase_translate` VALUES ('285', '168', '1', 'Phân quyền');
INSERT INTO `phrase_translate` VALUES ('286', '168', '2', 'Select permission ');
INSERT INTO `phrase_translate` VALUES ('287', '167', '2', 'Send ');
INSERT INTO `phrase_translate` VALUES ('288', '166', '2', 'Full name ');
INSERT INTO `phrase_translate` VALUES ('289', '169', '1', 'Hỗ trợ trực tuyến ');
INSERT INTO `phrase_translate` VALUES ('290', '169', '2', 'Support Online');
INSERT INTO `phrase_translate` VALUES ('291', '170', '1', 'Đối tác của chúng tôi');
INSERT INTO `phrase_translate` VALUES ('292', '170', '2', 'Our Partners');
INSERT INTO `phrase_translate` VALUES ('293', '171', '1', 'Đối tác của chúng tôi');
INSERT INTO `phrase_translate` VALUES ('294', '171', '2', 'Our Partners ');
INSERT INTO `phrase_translate` VALUES ('295', '172', '1', 'Tìm kiếm');
INSERT INTO `phrase_translate` VALUES ('296', '172', '2', 'Search ');
INSERT INTO `phrase_translate` VALUES ('297', '173', '1', 'Danh mục cha');
INSERT INTO `phrase_translate` VALUES ('298', '173', '2', 'Parent category ');
INSERT INTO `phrase_translate` VALUES ('299', '174', '1', 'Trước');
INSERT INTO `phrase_translate` VALUES ('300', '175', '1', 'Sau');
INSERT INTO `phrase_translate` VALUES ('301', '176', '1', 'Sắp xếp theo');
INSERT INTO `phrase_translate` VALUES ('302', '177', '1', 'Tên sản phẩm');
INSERT INTO `phrase_translate` VALUES ('303', '178', '1', 'Tăng dần');
INSERT INTO `phrase_translate` VALUES ('304', '179', '1', 'Giảm dần');
INSERT INTO `phrase_translate` VALUES ('305', '180', '1', 'Hiển thị');
INSERT INTO `phrase_translate` VALUES ('306', '181', '1', 'Sản phẩm/Trang');
INSERT INTO `phrase_translate` VALUES ('307', '175', '2', 'Next ');
INSERT INTO `phrase_translate` VALUES ('308', '174', '2', 'Previous ');
INSERT INTO `phrase_translate` VALUES ('309', '176', '2', 'Order by ');
INSERT INTO `phrase_translate` VALUES ('310', '177', '2', 'Product name ');
INSERT INTO `phrase_translate` VALUES ('311', '178', '2', 'Low to high ');
INSERT INTO `phrase_translate` VALUES ('312', '179', '2', 'High to low ');
INSERT INTO `phrase_translate` VALUES ('313', '180', '2', 'Display ');
INSERT INTO `phrase_translate` VALUES ('314', '181', '2', 'Product/Page ');
INSERT INTO `phrase_translate` VALUES ('315', '182', '1', 'error: phrase ');
INSERT INTO `phrase_translate` VALUES ('317', '184', '1', 'Thông tin Công ty');
INSERT INTO `phrase_translate` VALUES ('318', '185', '1', 'Hãy trở thành Đối tác của chúng tôi');
INSERT INTO `phrase_translate` VALUES ('319', '186', '1', 'Xí nghiệp Quang Trung hiện tại có hơn 100 đối tác trên Toàn quốc.....');
INSERT INTO `phrase_translate` VALUES ('320', '187', '1', 'Liên hệ hoặc góp ý với Xí nghiệp Quang Trung');
INSERT INTO `phrase_translate` VALUES ('321', '184', '2', 'Company info ');
INSERT INTO `phrase_translate` VALUES ('322', '185', '2', 'Become our Partners ');
INSERT INTO `phrase_translate` VALUES ('323', '188', '1', 'Giới thiệu về tổng giám đốc');
INSERT INTO `phrase_translate` VALUES ('324', '189', '1', 'Giới thiệu về công ty');
INSERT INTO `phrase_translate` VALUES ('325', '190', '1', 'Dự án đang triển khai');
INSERT INTO `phrase_translate` VALUES ('326', '191', '1', 'Thành tích');
INSERT INTO `phrase_translate` VALUES ('327', '192', '1', 'Dự án tiêu biểu');
INSERT INTO `phrase_translate` VALUES ('328', '193', '1', 'Đơn hàng');
INSERT INTO `phrase_translate` VALUES ('329', '194', '1', 'Tổng tiền hàng');
INSERT INTO `phrase_translate` VALUES ('330', '195', '1', 'Địa chỉ chuyển hàng');
INSERT INTO `phrase_translate` VALUES ('331', '196', '1', 'Tin nhắn');
INSERT INTO `phrase_translate` VALUES ('332', '197', '1', 'Hóa đơn');
INSERT INTO `phrase_translate` VALUES ('333', '198', '1', 'Thông tin đơn hàng');
INSERT INTO `phrase_translate` VALUES ('334', '199', '1', 'Mã đơn hàng');
INSERT INTO `phrase_translate` VALUES ('335', '200', '1', 'Thông tin khách hàng');
INSERT INTO `phrase_translate` VALUES ('336', '201', '1', 'Số lượng mua');
INSERT INTO `phrase_translate` VALUES ('338', '203', '1', 'Xuất ra excel');
INSERT INTO `phrase_translate` VALUES ('339', '204', '1', 'Thêm ảnh');
INSERT INTO `phrase_translate` VALUES ('340', '205', '1', 'Phụ đề');
INSERT INTO `phrase_translate` VALUES ('341', '206', '1', 'Ảnh chính?');
INSERT INTO `phrase_translate` VALUES ('344', '209', '1', 'Danh mục sản phẩm');
INSERT INTO `phrase_translate` VALUES ('345', '210', '1', 'Trụ sở');
INSERT INTO `phrase_translate` VALUES ('346', '211', '1', 'Nhà máy');
INSERT INTO `phrase_translate` VALUES ('347', '212', '1', 'Văn phòng');
INSERT INTO `phrase_translate` VALUES ('348', '213', '1', 'Hotline');
INSERT INTO `phrase_translate` VALUES ('349', '214', '1', 'Kết nối với chúng tôi');
INSERT INTO `phrase_translate` VALUES ('350', '215', '2', 'Sales consultant');
INSERT INTO `phrase_translate` VALUES ('351', '215', '1', 'Tư vấn bán hàng ');
INSERT INTO `phrase_translate` VALUES ('353', '217', '1', 'Công nghệ & thiết bị sản xuất');
INSERT INTO `phrase_translate` VALUES ('354', '217', '2', 'Technology & production equipment');
INSERT INTO `phrase_translate` VALUES ('355', '218', '1', 'Slider');
INSERT INTO `phrase_translate` VALUES ('356', '219', '1', 'Tìm kiếm sản phẩm');
INSERT INTO `phrase_translate` VALUES ('357', '220', '1', 'Có {1} sản phẩm');
INSERT INTO `phrase_translate` VALUES ('358', '221', '1', 'Sản phẩm mới nhất');
INSERT INTO `phrase_translate` VALUES ('359', '222', '1', 'Giá: Từ thấp đến cao');
INSERT INTO `phrase_translate` VALUES ('360', '223', '1', 'Giá: Từ cao xuống thấp');
INSERT INTO `phrase_translate` VALUES ('361', '224', '1', 'Sản phẩm nổi bật');
INSERT INTO `phrase_translate` VALUES ('362', '225', '1', 'Thêm vào giỏ hàng');
INSERT INTO `phrase_translate` VALUES ('363', '226', '1', 'Thông tin sản phẩm');
INSERT INTO `phrase_translate` VALUES ('364', '227', '1', 'Nhận xét của khách hàng');
INSERT INTO `phrase_translate` VALUES ('365', '228', '1', 'Sản phẩm tương tự');
INSERT INTO `phrase_translate` VALUES ('366', '229', '1', 'Chú thích {1}');
INSERT INTO `phrase_translate` VALUES ('368', '231', '1', 'Tin tức');
INSERT INTO `phrase_translate` VALUES ('369', '232', '1', 'Đọc tiếp');
INSERT INTO `phrase_translate` VALUES ('370', '233', '1', 'Giỏ hàng của bạn');
INSERT INTO `phrase_translate` VALUES ('371', '234', '1', 'Đơn giá');
INSERT INTO `phrase_translate` VALUES ('372', '235', '1', 'Tổng tiền');
INSERT INTO `phrase_translate` VALUES ('373', '236', '1', 'Hủy');
INSERT INTO `phrase_translate` VALUES ('374', '237', '1', 'Tiếp tục mua sắm');
INSERT INTO `phrase_translate` VALUES ('375', '238', '1', 'Đặt hàng');
INSERT INTO `phrase_translate` VALUES ('376', '239', '1', 'Chưa có sản phẩm');
INSERT INTO `phrase_translate` VALUES ('377', '240', '1', 'Đến giỏ hàng');
INSERT INTO `phrase_translate` VALUES ('378', '241', '1', 'Công nghệ');
INSERT INTO `phrase_translate` VALUES ('379', '242', '1', 'Thông tin đơn hàng');
INSERT INTO `phrase_translate` VALUES ('380', '243', '1', 'Thông tin cá nhân');
INSERT INTO `phrase_translate` VALUES ('381', '244', '1', 'Quý khách đã đặt hàng thành công! Chúng tôi sẽ liên hệ lại với Quý khách trong thời gian sớm nhất');
INSERT INTO `phrase_translate` VALUES ('382', '245', '1', 'Cảm ơn Quý khách!');
INSERT INTO `phrase_translate` VALUES ('383', '246', '1', 'Thông tin công ty');
INSERT INTO `phrase_translate` VALUES ('384', '247', '1', 'Địa chỉ');
INSERT INTO `phrase_translate` VALUES ('386', '249', '1', 'Showroom');
INSERT INTO `phrase_translate` VALUES ('387', '250', '1', 'Tin tức liên quan');
INSERT INTO `phrase_translate` VALUES ('388', '251', '1', 'Tin trước');
INSERT INTO `phrase_translate` VALUES ('389', '252', '1', 'Tin sau');
INSERT INTO `phrase_translate` VALUES ('390', '253', '1', 'Còn hàng');
INSERT INTO `phrase_translate` VALUES ('391', '254', '1', 'Sản phẩm hiện tại đang hết hàng! Chúng tôi sẽ cập nhật trong thời gian sớm nhất');
INSERT INTO `phrase_translate` VALUES ('392', '255', '1', 'Hết hàng');
INSERT INTO `phrase_translate` VALUES ('393', '255', '2', 'Out of stock');
INSERT INTO `phrase_translate` VALUES ('394', '256', '1', 'Thực hiện mục tiêu phát triển chiều sâu...');
INSERT INTO `phrase_translate` VALUES ('395', '256', '2', 'Perform in-depth development goals');
INSERT INTO `phrase_translate` VALUES ('396', '257', '1', 'Công nghệ');
INSERT INTO `phrase_translate` VALUES ('397', '258', '1', 'Thiết bị');
INSERT INTO `phrase_translate` VALUES ('398', '250', '2', 'Related Posts ');
INSERT INTO `phrase_translate` VALUES ('399', '253', '2', 'In stock ');
INSERT INTO `phrase_translate` VALUES ('400', '252', '2', 'Next ');
INSERT INTO `phrase_translate` VALUES ('401', '251', '2', 'Previous ');
INSERT INTO `phrase_translate` VALUES ('402', '249', '2', 'Showroom ');
INSERT INTO `phrase_translate` VALUES ('403', '259', '1', 'Xin cảm ơn. Tin nhắn của Quý vị đã được gửi thành công! Quang Trung sẽ liên hệ với Quý vị trong thời gian sớm nhất.');
INSERT INTO `phrase_translate` VALUES ('404', '260', '1', 'Gửi không thành công! Vui lòng kiểm tra lại thông tin.');
INSERT INTO `phrase_translate` VALUES ('405', '260', '2', 'Failed. Please check your information.  ');
INSERT INTO `phrase_translate` VALUES ('406', '259', '2', 'Thank you! Your message was sent successfully. Quang Trung will contact you as soon as possible.  ');
INSERT INTO `phrase_translate` VALUES ('407', '261', '1', 'Gửi từ');
INSERT INTO `phrase_translate` VALUES ('408', '262', '1', 'Gửi đến');
INSERT INTO `phrase_translate` VALUES ('409', '263', '1', 'Tiêu đề');
INSERT INTO `phrase_translate` VALUES ('410', '264', '1', 'Trả lời');
INSERT INTO `phrase_translate` VALUES ('411', '265', '1', 'Ngày gửi');
INSERT INTO `phrase_translate` VALUES ('412', '265', '2', 'Sent date ');
INSERT INTO `phrase_translate` VALUES ('413', '264', '2', 'Reply ');
INSERT INTO `phrase_translate` VALUES ('414', '263', '2', 'Subject ');
INSERT INTO `phrase_translate` VALUES ('415', '262', '2', 'To ');
INSERT INTO `phrase_translate` VALUES ('416', '261', '2', 'From ');
INSERT INTO `phrase_translate` VALUES ('417', '266', '1', 'Các dự án tiêu biểu mà Tập đoàn Công nghiệp Quang Trung đã thực hiện');
INSERT INTO `phrase_translate` VALUES ('418', '267', '1', 'Đã hủy');
INSERT INTO `phrase_translate` VALUES ('419', '268', '1', 'Đơn hàng mới');
INSERT INTO `phrase_translate` VALUES ('420', '269', '1', 'Đã xử lý');
INSERT INTO `phrase_translate` VALUES ('421', '270', '1', 'Hủy đơn hàng');
INSERT INTO `phrase_translate` VALUES ('422', '271', '1', 'Tiếp nhận đơn hàng');
INSERT INTO `phrase_translate` VALUES ('423', '272', '1', 'Bạn có chắc không?');
INSERT INTO `phrase_translate` VALUES ('424', '209', '2', 'Categories ');
INSERT INTO `phrase_translate` VALUES ('425', '273', '1', 'Văn phòng đại diện');
INSERT INTO `phrase_translate` VALUES ('426', '274', '1', 'Nhà máy');
INSERT INTO `phrase_translate` VALUES ('427', '275', '1', 'Bản đồ');
INSERT INTO `phrase_translate` VALUES ('428', '276', '1', 'Những điểm mạnh của Sản phẩm Đèn Led Quang Trung');
INSERT INTO `phrase_translate` VALUES ('429', '277', '1', 'Mã sản phẩm');
INSERT INTO `phrase_translate` VALUES ('430', '278', '1', 'Xuất xứ');
INSERT INTO `phrase_translate` VALUES ('431', '279', '1', 'Nhóm sản phẩm');
INSERT INTO `phrase_translate` VALUES ('432', '280', '1', 'Thư viện Video');
INSERT INTO `phrase_translate` VALUES ('433', '281', '1', 'Chọn từ máy tính');
INSERT INTO `phrase_translate` VALUES ('434', '282', '1', 'Chọn từ URL');
INSERT INTO `phrase_translate` VALUES ('435', '283', '1', 'Chỉ hỗ trợ link YouTube');
INSERT INTO `phrase_translate` VALUES ('436', '284', '1', 'Kết quả tìm kiếm cho từ khóa: {1}');
INSERT INTO `phrase_translate` VALUES ('437', '284', '2', 'Search results for keyword: {1} ');
INSERT INTO `phrase_translate` VALUES ('438', '285', '2', 'error: phrase [customer] not found');
INSERT INTO `phrase_translate` VALUES ('439', '286', '2', 'error: phrase [general] not found');
INSERT INTO `phrase_translate` VALUES ('440', '287', '2', 'error: phrase [payment] not found');
INSERT INTO `phrase_translate` VALUES ('441', '288', '1', 'error: phrase [lists] not found');
INSERT INTO `phrase_translate` VALUES ('442', '289', '1', 'error: phrase [country] not found');
INSERT INTO `phrase_translate` VALUES ('443', '290', '1', 'error: phrase [languages] not found');
INSERT INTO `phrase_translate` VALUES ('444', '291', '1', 'error: phrase [add_a_new_x] not found');
INSERT INTO `phrase_translate` VALUES ('445', '292', '1', 'error: phrase [in_use] not found');
INSERT INTO `phrase_translate` VALUES ('446', '293', '1', 'error: phrase [not_in_use] not found');
INSERT INTO `phrase_translate` VALUES ('447', '302', '1', 'error: phrase [value] not found');
INSERT INTO `phrase_translate` VALUES ('448', '303', '1', 'error: phrase [key] not found');
INSERT INTO `phrase_translate` VALUES ('449', '304', '1', 'error: phrase [index] not found');
INSERT INTO `phrase_translate` VALUES ('450', '305', '1', 'error: phrase [desc_general] not found');
INSERT INTO `phrase_translate` VALUES ('451', '306', '1', 'error: phrase [site_name] not found');
INSERT INTO `phrase_translate` VALUES ('452', '307', '1', 'error: phrase [desc_site_name] not found');
INSERT INTO `phrase_translate` VALUES ('453', '308', '1', 'error: phrase [site_title] not found');
INSERT INTO `phrase_translate` VALUES ('454', '309', '1', 'error: phrase [desc_site_title] not found');
INSERT INTO `phrase_translate` VALUES ('455', '310', '1', 'error: phrase [site_keywords] not found');
INSERT INTO `phrase_translate` VALUES ('456', '311', '1', 'error: phrase [desc_site_keywords] not found');
INSERT INTO `phrase_translate` VALUES ('457', '312', '1', 'error: phrase [Bảo trì website?] not found');
INSERT INTO `phrase_translate` VALUES ('458', '313', '1', 'error: phrase [desc_Bảo trì website?] not found');
INSERT INTO `phrase_translate` VALUES ('459', '314', '1', 'error: phrase [general_active] not found');
INSERT INTO `phrase_translate` VALUES ('460', '315', '1', 'error: phrase [desc_general_active] not found');
INSERT INTO `phrase_translate` VALUES ('461', '316', '1', 'error: phrase [title_language] not found');
INSERT INTO `phrase_translate` VALUES ('462', '317', '1', 'error: phrase [desc_title_language] not found');
INSERT INTO `phrase_translate` VALUES ('463', '318', '1', 'error: phrase [north] not found');
INSERT INTO `phrase_translate` VALUES ('464', '319', '1', 'error: phrase [middle] not found');
INSERT INTO `phrase_translate` VALUES ('465', '320', '1', 'error: phrase [south] not found');
INSERT INTO `phrase_translate` VALUES ('466', '321', '1', 'error: phrase [not_active] not found');
INSERT INTO `phrase_translate` VALUES ('467', '322', '1', 'error: phrase [active] not found');
INSERT INTO `phrase_translate` VALUES ('468', '323', '1', 'error: phrase [isset_setting] not found');
INSERT INTO `phrase_translate` VALUES ('469', '324', '1', 'error: phrase [desc_isset_setting] not found');
INSERT INTO `phrase_translate` VALUES ('470', '325', '1', 'error: phrase [test_abc] not found');
INSERT INTO `phrase_translate` VALUES ('471', '326', '1', 'error: phrase [desc_test_abc] not found');
INSERT INTO `phrase_translate` VALUES ('472', '327', '1', 'error: phrase [test] not found');
INSERT INTO `phrase_translate` VALUES ('473', '328', '1', 'error: phrase [desc_test] not found');
INSERT INTO `phrase_translate` VALUES ('474', '329', '1', 'error: phrase [text_field] not found');
INSERT INTO `phrase_translate` VALUES ('475', '330', '1', 'error: phrase [desc_text_field] not found');
INSERT INTO `phrase_translate` VALUES ('476', '331', '1', 'error: phrase [email_field] not found');
INSERT INTO `phrase_translate` VALUES ('477', '332', '1', 'error: phrase [desc_email_field] not found');
INSERT INTO `phrase_translate` VALUES ('478', '333', '1', 'error: phrase [number_field] not found');
INSERT INTO `phrase_translate` VALUES ('479', '334', '1', 'error: phrase [desc_number_field] not found');
INSERT INTO `phrase_translate` VALUES ('480', '335', '1', 'error: phrase [textarea_field] not found');
INSERT INTO `phrase_translate` VALUES ('481', '336', '1', 'error: phrase [desc_textarea_field] not found');
INSERT INTO `phrase_translate` VALUES ('482', '337', '1', 'error: phrase [color_field] not found');
INSERT INTO `phrase_translate` VALUES ('483', '338', '1', 'error: phrase [desc_color_field] not found');
INSERT INTO `phrase_translate` VALUES ('484', '339', '1', 'error: phrase [date_field] not found');
INSERT INTO `phrase_translate` VALUES ('485', '340', '1', 'error: phrase [desc_date_field] not found');
INSERT INTO `phrase_translate` VALUES ('486', '341', '1', 'error: phrase [time_field] not found');
INSERT INTO `phrase_translate` VALUES ('487', '342', '1', 'error: phrase [desc_time_field] not found');
INSERT INTO `phrase_translate` VALUES ('488', '343', '1', 'error: phrase [password_field] not found');
INSERT INTO `phrase_translate` VALUES ('489', '344', '1', 'error: phrase [desc_password_field] not found');
INSERT INTO `phrase_translate` VALUES ('490', '345', '1', 'error: phrase [roxymce_field] not found');
INSERT INTO `phrase_translate` VALUES ('491', '346', '1', 'error: phrase [desc_roxymce_field] not found');
INSERT INTO `phrase_translate` VALUES ('492', '347', '1', 'error: phrase [select_field] not found');
INSERT INTO `phrase_translate` VALUES ('493', '348', '1', 'error: phrase [desc_select_field] not found');
INSERT INTO `phrase_translate` VALUES ('494', '349', '1', 'error: phrase [additional] not found');
INSERT INTO `phrase_translate` VALUES ('495', '350', '1', 'error: phrase [multiselect_field] not found');
INSERT INTO `phrase_translate` VALUES ('496', '351', '1', 'error: phrase [desc_multiselect_field] not found');
INSERT INTO `phrase_translate` VALUES ('497', '352', '1', 'error: phrase [file_field] not found');
INSERT INTO `phrase_translate` VALUES ('498', '353', '1', 'error: phrase [desc_file_field] not found');
INSERT INTO `phrase_translate` VALUES ('499', '354', '1', 'error: phrase [url_field] not found');
INSERT INTO `phrase_translate` VALUES ('500', '355', '1', 'error: phrase [desc_url_field] not found');
INSERT INTO `phrase_translate` VALUES ('501', '356', '1', 'error: phrase [percent_field] not found');
INSERT INTO `phrase_translate` VALUES ('502', '357', '1', 'error: phrase [desc_percent_field] not found');
INSERT INTO `phrase_translate` VALUES ('503', '358', '1', 'error: phrase [switch_field] not found');
INSERT INTO `phrase_translate` VALUES ('504', '359', '1', 'error: phrase [desc_switch_field] not found');
INSERT INTO `phrase_translate` VALUES ('505', '360', '1', 'error: phrase [checkbox_option] not found');
INSERT INTO `phrase_translate` VALUES ('506', '361', '1', 'error: phrase [desc_checkbox_option] not found');
INSERT INTO `phrase_translate` VALUES ('507', '362', '1', 'error: phrase [radio_option] not found');
INSERT INTO `phrase_translate` VALUES ('508', '363', '1', 'error: phrase [desc_radio_option] not found');
INSERT INTO `phrase_translate` VALUES ('509', '364', '1', 'error: phrase [department] not found');
INSERT INTO `phrase_translate` VALUES ('510', '365', '1', 'error: phrase [password_mail] not found');
INSERT INTO `phrase_translate` VALUES ('511', '366', '1', 'error: phrase [desc_password_mail] not found');
INSERT INTO `phrase_translate` VALUES ('512', '367', '1', 'Về trang chủ');
INSERT INTO `phrase_translate` VALUES ('513', '368', '1', 'Lên đầu trang');
INSERT INTO `phrase_translate` VALUES ('514', '368', '2', 'Go to top');
INSERT INTO `phrase_translate` VALUES ('515', '367', '2', 'Go home');
INSERT INTO `phrase_translate` VALUES ('516', '297', '1', 'Khuyến Mãi');
INSERT INTO `phrase_translate` VALUES ('517', '297', '2', 'Promotion');
INSERT INTO `phrase_translate` VALUES ('518', '218', '2', 'Slider');
INSERT INTO `phrase_translate` VALUES ('519', '296', '1', 'Nhà phân phối');
INSERT INTO `phrase_translate` VALUES ('520', '296', '2', 'distributor');
INSERT INTO `phrase_translate` VALUES ('521', '193', '2', 'order');
INSERT INTO `phrase_translate` VALUES ('522', '273', '2', 'office');
INSERT INTO `phrase_translate` VALUES ('523', '369', '2', 'error: phrase [admin] not found');
INSERT INTO `phrase_translate` VALUES ('524', '370', '2', 'error: phrase [import_to_excel] not found');
INSERT INTO `phrase_translate` VALUES ('525', '371', '1', 'error: phrase [Bảo trì website] not found');
INSERT INTO `phrase_translate` VALUES ('526', '372', '1', 'error: phrase [desc_Bảo trì website] not found');
INSERT INTO `phrase_translate` VALUES ('527', '373', '1', 'error: phrase [Ngôn ngữ mặc định] not found');
INSERT INTO `phrase_translate` VALUES ('528', '374', '1', 'error: phrase [desc_Ngôn ngữ mặc định] not found');
INSERT INTO `phrase_translate` VALUES ('529', '375', '1', 'error: phrase [general_language] not found');
INSERT INTO `phrase_translate` VALUES ('530', '376', '1', 'error: phrase [desc_general_language] not found');
INSERT INTO `phrase_translate` VALUES ('531', '377', '1', 'error: phrase [general_url] not found');
INSERT INTO `phrase_translate` VALUES ('532', '378', '1', 'error: phrase [desc_general_url] not found');
INSERT INTO `phrase_translate` VALUES ('533', '379', '1', 'Tiêu đề');
INSERT INTO `phrase_translate` VALUES ('534', '380', '1', 'Tiêu đề của website');
INSERT INTO `phrase_translate` VALUES ('535', '381', '1', 'Logo');
INSERT INTO `phrase_translate` VALUES ('536', '382', '1', 'Logo của website');
INSERT INTO `phrase_translate` VALUES ('537', '383', '1', 'Hotline');
INSERT INTO `phrase_translate` VALUES ('538', '384', '1', 'Số Hotline hiển thị trên website');
INSERT INTO `phrase_translate` VALUES ('539', '385', '1', 'Showroom');
INSERT INTO `phrase_translate` VALUES ('540', '386', '1', 'Showroom trên bản đồ');
INSERT INTO `phrase_translate` VALUES ('541', '387', '1', 'Ghi chú');
INSERT INTO `phrase_translate` VALUES ('542', '388', '1', 'error: phrase [email_type] not found');
INSERT INTO `phrase_translate` VALUES ('543', '389', '1', 'error: phrase [desc_email_type] not found');
INSERT INTO `phrase_translate` VALUES ('544', '390', '1', 'Loại bảo mật');
INSERT INTO `phrase_translate` VALUES ('545', '391', '1', 'Chọn loại bảo mật của máy chủ E-mail');
INSERT INTO `phrase_translate` VALUES ('546', '392', '1', 'Cổng');
INSERT INTO `phrase_translate` VALUES ('547', '393', '1', 'Cổng máy chủ E-mail');
INSERT INTO `phrase_translate` VALUES ('548', '394', '1', 'Máy chủ E-mail');
INSERT INTO `phrase_translate` VALUES ('549', '395', '1', 'Máy chủ gửi mail');
INSERT INTO `phrase_translate` VALUES ('550', '396', '1', 'Favicon');
INSERT INTO `phrase_translate` VALUES ('551', '397', '1', 'icon nhỏ trên thanh tiêu đề của trình duyệt');
INSERT INTO `phrase_translate` VALUES ('552', '398', '1', 'Nhập từ Excel');
INSERT INTO `phrase_translate` VALUES ('553', '398', '2', 'Import from Excel files');
INSERT INTO `phrase_translate` VALUES ('554', '396', '2', 'Favicon');
INSERT INTO `phrase_translate` VALUES ('555', '397', '2', 'Small icon on Title bar of your browser');
INSERT INTO `phrase_translate` VALUES ('556', '395', '2', 'E-mail sending hostname');
INSERT INTO `phrase_translate` VALUES ('557', '394', '2', 'E-mail server');
INSERT INTO `phrase_translate` VALUES ('558', '393', '2', 'E-mail server port');
INSERT INTO `phrase_translate` VALUES ('559', '392', '2', 'Port');
INSERT INTO `phrase_translate` VALUES ('560', '391', '2', 'Select a security type of your e-mail server');
INSERT INTO `phrase_translate` VALUES ('561', '390', '2', 'Secure');
INSERT INTO `phrase_translate` VALUES ('562', '387', '2', 'Note');
INSERT INTO `phrase_translate` VALUES ('563', '386', '2', 'Showroom on map');
INSERT INTO `phrase_translate` VALUES ('564', '385', '2', 'Showroom');
INSERT INTO `phrase_translate` VALUES ('565', '384', '2', 'Hotline on Homepages footer');
INSERT INTO `phrase_translate` VALUES ('566', '383', '2', 'Hotline');
INSERT INTO `phrase_translate` VALUES ('567', '382', '2', 'Logo');
INSERT INTO `phrase_translate` VALUES ('568', '381', '2', 'Logo');
INSERT INTO `phrase_translate` VALUES ('569', '380', '2', 'Website title');
INSERT INTO `phrase_translate` VALUES ('570', '379', '2', 'Title');
INSERT INTO `phrase_translate` VALUES ('571', '399', '2', 'error: phrase [view_detail] not found');
INSERT INTO `phrase_translate` VALUES ('572', '400', '2', 'error: phrase [newest] not found');
INSERT INTO `phrase_translate` VALUES ('573', '401', '2', 'error: phrase [for_deapartment] not found');
INSERT INTO `phrase_translate` VALUES ('574', '402', '2', 'error: phrase [forgot_password] not found');
INSERT INTO `phrase_translate` VALUES ('575', '403', '2', 'error: phrase [business_room] not found');

-- ----------------------------
-- Table structure for popup
-- ----------------------------
DROP TABLE IF EXISTS `popup`;
CREATE TABLE `popup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `background` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of popup
-- ----------------------------
INSERT INTO `popup` VALUES ('83', '83_image.jpg', '1', '1');

-- ----------------------------
-- Table structure for popup_lang
-- ----------------------------
DROP TABLE IF EXISTS `popup_lang`;
CREATE TABLE `popup_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `popup_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of popup_lang
-- ----------------------------
INSERT INTO `popup_lang` VALUES ('1', '1', 'vi', 'sdfasd', '444');
INSERT INTO `popup_lang` VALUES ('2', '1', 'en', 'ádfasdf', 'ádfasdf');
INSERT INTO `popup_lang` VALUES ('3', '2', 'vi', 'sdfasd', 'ádfasdf');
INSERT INTO `popup_lang` VALUES ('4', '2', 'en', 'ádfasdf', 'ádfasdf');
INSERT INTO `popup_lang` VALUES ('5', '3', 'vi', 'dfasd', 'ádfasd');
INSERT INTO `popup_lang` VALUES ('6', '4', 'vi', 'dfasd', 'ádfasd');
INSERT INTO `popup_lang` VALUES ('7', '5', 'vi', 'dfasd', 'ádfasd');
INSERT INTO `popup_lang` VALUES ('8', '6', 'vi', 'dfasd', 'ádfasd');
INSERT INTO `popup_lang` VALUES ('9', '7', 'vi', 'dfgd', 'ag');
INSERT INTO `popup_lang` VALUES ('10', '8', 'vi', 'dfgd', 'ag');
INSERT INTO `popup_lang` VALUES ('11', '9', 'vi', 'dfgd', 'ag');
INSERT INTO `popup_lang` VALUES ('12', '10', 'vi', 'dfgd', 'ag');
INSERT INTO `popup_lang` VALUES ('13', '11', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('14', '12', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('15', '13', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('16', '14', 'vi', 'ádA', 'ÁDF');
INSERT INTO `popup_lang` VALUES ('17', '15', 'vi', 'ádA', 'ÁDF');
INSERT INTO `popup_lang` VALUES ('18', '16', 'vi', 'ádA', 'ÁDF');
INSERT INTO `popup_lang` VALUES ('19', '17', 'vi', 'ádA', 'ÁDF');
INSERT INTO `popup_lang` VALUES ('20', '18', 'vi', 'ádA', 'ÁDF');
INSERT INTO `popup_lang` VALUES ('21', '19', 'vi', 'sdfd', 'sdf');
INSERT INTO `popup_lang` VALUES ('22', '20', 'vi', 'fasdf', 'áda');
INSERT INTO `popup_lang` VALUES ('23', '21', 'vi', 'dsaf', 'ádf');
INSERT INTO `popup_lang` VALUES ('24', '22', 'vi', 'sdf', 'ádf');
INSERT INTO `popup_lang` VALUES ('25', '23', 'vi', 'fdasf', 'sdf');
INSERT INTO `popup_lang` VALUES ('26', '24', 'vi', 'ád', 'ádf');
INSERT INTO `popup_lang` VALUES ('27', '25', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('28', '26', 'vi', 'èasd', 'sdf');
INSERT INTO `popup_lang` VALUES ('29', '27', 'vi', '1231', 'sdf');
INSERT INTO `popup_lang` VALUES ('30', '28', 'vi', 'sdf', 'ádf');
INSERT INTO `popup_lang` VALUES ('31', '29', 'vi', 'sdf', 'ádf');
INSERT INTO `popup_lang` VALUES ('32', '30', 'vi', 'sdf', 'ádf');
INSERT INTO `popup_lang` VALUES ('33', '31', 'vi', 'sdf', '123');
INSERT INTO `popup_lang` VALUES ('34', '32', 'vi', 'sdzf', 'ádf');
INSERT INTO `popup_lang` VALUES ('35', '33', 'vi', 'sdzf', 'ádf');
INSERT INTO `popup_lang` VALUES ('36', '34', 'vi', 'sdzf', 'ádf');
INSERT INTO `popup_lang` VALUES ('37', '35', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('38', '36', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('39', '37', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('40', '38', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('41', '39', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('42', '40', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('43', '41', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('44', '42', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('45', '43', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('46', '44', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('47', '45', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('48', '46', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('49', '47', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('50', '48', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('51', '49', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('52', '50', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('53', '51', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('54', '52', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('55', '53', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('56', '54', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('57', '55', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('58', '56', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('59', '57', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('60', '58', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('61', '59', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('62', '60', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('63', '61', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('64', '62', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('65', '63', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('66', '64', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('67', '65', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('68', '66', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('69', '67', 'vi', 'cvasdf', 'ádf');
INSERT INTO `popup_lang` VALUES ('70', '68', 'vi', 'cvasdf', 'ádf');
INSERT INTO `popup_lang` VALUES ('71', '69', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('72', '70', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('73', '71', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('74', '72', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('75', '73', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('76', '74', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('77', '75', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('78', '76', 'vi', '11', '11');
INSERT INTO `popup_lang` VALUES ('79', '77', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('80', '78', 'vi', 'sdf', 'sdf');
INSERT INTO `popup_lang` VALUES ('81', '79', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('82', '80', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('83', '81', 'vi', 'ádf', 'ádf');
INSERT INTO `popup_lang` VALUES ('84', '82', 'vi', 'ádf', 'ád');
INSERT INTO `popup_lang` VALUES ('85', '83', 'vi', '123', '123');

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES ('2', '30', '2_image.png', '5', '2016-03-21 09:54:48', '1', '1');
INSERT INTO `post` VALUES ('5', '30', '_image.png', '6', '2016-03-21 17:18:18', '1', '1');
INSERT INTO `post` VALUES ('6', '30', '6_image.jpg', '5', '2016-04-19 17:11:26', '1', '1');
INSERT INTO `post` VALUES ('7', '30', '', '123', '2016-04-20 11:47:27', '0', '0');
INSERT INTO `post` VALUES ('8', '30', '_image.jpg', '123', '2016-04-27 15:10:54', '1', '1');
INSERT INTO `post` VALUES ('9', '38', '9_image.jpg', '12', '2016-04-28 16:26:06', '1', '1');
INSERT INTO `post` VALUES ('10', '38', '_image.jpg', '222', '2016-04-28 16:46:45', '1', '1');
INSERT INTO `post` VALUES ('11', '38', '', '1', '2016-05-07 10:49:19', '0', '0');
INSERT INTO `post` VALUES ('12', '72', '12_image.jpg', '1', '2016-05-07 15:33:09', '1', '1');

-- ----------------------------
-- Table structure for post_lang
-- ----------------------------
DROP TABLE IF EXISTS `post_lang`;
CREATE TABLE `post_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postlang_ibfk_1` (`post_id`),
  CONSTRAINT `postlang_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_lang
-- ----------------------------
INSERT INTO `post_lang` VALUES ('1', '5', 'vi', 'tin bóng đá', 'tin bóng đá', 'tin bóng đá');
INSERT INTO `post_lang` VALUES ('2', '5', 'en', 'football news', 'football news', 'football news');
INSERT INTO `post_lang` VALUES ('3', '6', 'vi', 'post_vi', 'des_vi', 'content_vi');
INSERT INTO `post_lang` VALUES ('4', '6', 'en', 'post_en', 'des_en', 'content_en');
INSERT INTO `post_lang` VALUES ('5', '7', 'vi', 'vn', 'vn', 'vn');
INSERT INTO `post_lang` VALUES ('6', '7', 'en', 'en', 'en', 'en');
INSERT INTO `post_lang` VALUES ('7', '8', 'vi', '123', '123', '123');
INSERT INTO `post_lang` VALUES ('8', '8', 'en', '123', '123', '123');
INSERT INTO `post_lang` VALUES ('9', '9', 'vi', 'Tin tức việt nam', '1', '1');
INSERT INTO `post_lang` VALUES ('10', '10', 'vi', '1111', '2222', '2222');
INSERT INTO `post_lang` VALUES ('11', '12', 'vi', 'asdf', 'asdf', '<p>asd</p>');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `price` float NOT NULL,
  `in_stock` int(11) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `capability` varchar(255) DEFAULT NULL,
  `sale_off` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('12', '67', 'MS16', 'Hàn Quốc', '12_image.jpg', '2016-05-10 17:48:36', '0', '1', '776000', '1', null, null, null);
INSERT INTO `product` VALUES ('13', '67', 'MS17', 'Hàn Quốc', '13_image.jpg', '2016-05-10 17:50:07', '0', '1', '776000', '1', null, null, null);
INSERT INTO `product` VALUES ('14', '67', 'MS18', 'Hàn Quốc', '14_image.jpg', '2016-05-10 17:50:54', '0', '1', '848000', '1', null, null, null);
INSERT INTO `product` VALUES ('15', '61', '1', null, 'http://enesti.com.vn/uploads/shops/kem_tay/tra_xanh_146.jpg', '2016-05-18 17:30:57', '0', '1', '144000', '200', 'Test', '160g', '0.30');
INSERT INTO `product` VALUES ('16', '61', '2', null, 'http://enesti.com.vn/uploads/shops/kem_tay/gao_suatuoi_146.jpg', '2016-05-18 17:30:57', '0', '1', '144000', '200', 'Test', '160g', '0.30');
INSERT INTO `product` VALUES ('17', '61', '3', null, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/chanh_146.jpg', '2016-05-18 17:30:57', '0', '1', '138000', '200', 'Test', '160g', '0.20');
INSERT INTO `product` VALUES ('18', '61', '4', null, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/gao_146.jpg', '2016-05-18 17:30:57', '0', '1', '138000', '200', 'Test', '160g', '0.20');
INSERT INTO `product` VALUES ('19', '61', '5', null, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/lo_hoi_146.jpg', '2016-05-18 17:30:57', '0', '1', '138000', '200', 'Test', '160g', '0.20');
INSERT INTO `product` VALUES ('20', '61', '6', null, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/oliu_146.jpg', '2016-05-18 17:30:58', '0', '1', '138000', '200', 'Test', '160g', '0.20');
INSERT INTO `product` VALUES ('21', '61', '7', null, 'http://enesti.com.vn/uploads/shops/sua_rua_mat/luu_146.jpg', '2016-05-18 17:30:58', '0', '1', '138000', '200', 'Test', '160g', '0.20');
INSERT INTO `product` VALUES ('22', '63', '8', null, 'http://enesti.com.vn/uploads/shops/2016_01/1456217182195_4524013.jpg', '2016-05-18 17:30:58', '0', '1', '149000', '200', 'Test', '7ml', '0.00');
INSERT INTO `product` VALUES ('23', '63', '9', null, 'http://enesti.com.vn/uploads/shops/chuot_mi/damday-146.jpg', '2016-05-18 17:30:58', '0', '1', '149000', '200', 'Test', '7ml', '0.00');
INSERT INTO `product` VALUES ('24', '63', '10', null, 'http://enesti.com.vn/uploads/shops/chuot_mi/congvadai_146.jpg', '2016-05-18 17:30:58', '0', '1', '149000', '200', 'Test', '7ml', '0.00');
INSERT INTO `product` VALUES ('25', '63', '11', null, 'http://enesti.com.vn/uploads/shops/chuot_mi/chong_nuoc_146.jpg', '2016-05-18 17:30:58', '0', '1', '195000', '200', 'Test', '7ml', '0.00');
INSERT INTO `product` VALUES ('26', '61', '12', null, 'http://enesti.com.vn/uploads/shops/remine_collagen/nuoc_hoa_146.jpg', '2016-05-18 17:30:58', '0', '1', '250000', '200', 'Test', '150ml', '0.00');
INSERT INTO `product` VALUES ('27', '61', '13', null, 'http://enesti.com.vn/uploads/shops/remine_collagen/sua_duong_146.jpg', '2016-05-18 17:30:58', '0', '1', '250000', '200', 'Test', '150ml', '0.00');
INSERT INTO `product` VALUES ('28', '61', '14', null, 'http://enesti.com.vn/uploads/shops/remine_collagen/kem_duong_146.jpg', '2016-05-18 17:30:58', '0', '1', '270000', '200', 'Test', '50ml', '0.00');
INSERT INTO `product` VALUES ('29', '61', '15', null, 'http://enesti.com.vn/uploads/shops/2015_01/dd_remine_collagen_700_700.jpg', '2016-05-18 17:30:59', '0', '1', '240000', '200', 'Test', '65ml', '0.00');
INSERT INTO `product` VALUES ('30', '61', '16', null, 'http://enesti.com.vn/uploads/shops/xoyun/nuoc_hoa_hong_146.jpg', '2016-05-18 17:30:59', '0', '1', '776000', '200', 'Test', '130ml', '0.00');
INSERT INTO `product` VALUES ('31', '61', '17', null, 'http://enesti.com.vn/uploads/shops/xoyun/sua_duong_146.jpg', '2016-05-18 17:30:59', '0', '1', '776000', '200', 'Test', '130ml', '0.00');
INSERT INTO `product` VALUES ('32', '61', '18', null, 'http://enesti.com.vn/uploads/shops/xoyun/kem_duong_146.jpg', '2016-05-18 17:30:59', '0', '1', '848000', '200', 'Test', '50ml', '0.00');
INSERT INTO `product` VALUES ('33', '61', '19', null, 'http://enesti.com.vn/uploads/shops/crystal/nuoc_hoa_146.jpg', '2016-05-18 17:30:59', '0', '1', '240000', '200', 'Test', '150ml', '0.00');
INSERT INTO `product` VALUES ('34', '61', '20', null, 'http://enesti.com.vn/uploads/shops/crystal/sua_duong_146.jpg', '2016-05-18 17:30:59', '0', '1', '240000', '200', 'Test', '150ml', '0.00');
INSERT INTO `product` VALUES ('35', '61', '21', null, 'http://enesti.com.vn/uploads/shops/crystal/kem_duong_146.jpg', '2016-05-18 17:30:59', '0', '1', '264000', '200', 'Test', '50mg', '0.00');
INSERT INTO `product` VALUES ('36', '65', '22', null, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/freshcotton_146.jpg', '2016-05-18 17:30:59', '0', '1', '125000', '200', 'Test', '15ml', '0.40');
INSERT INTO `product` VALUES ('37', '65', '23', null, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/golden_rose_146.jpg', '2016-05-18 17:30:59', '0', '1', '125000', '200', 'Test', '15ml', '0.40');
INSERT INTO `product` VALUES ('38', '65', '24', null, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/babypowder_146.jpg', '2016-05-18 17:30:59', '0', '1', '125000', '200', 'Test', '15ml', '0.40');
INSERT INTO `product` VALUES ('39', '65', '25', null, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/lovelyday_146.jpg', '2016-05-18 17:31:00', '0', '1', '125000', '200', 'Test', '15ml', '0.40');
INSERT INTO `product` VALUES ('40', '65', '26', null, 'http://enesti.com.vn/uploads/shops/nuoc_hoa/mayme_146.jpg', '2016-05-18 17:31:00', '0', '1', '125000', '200', 'Test', '15ml', '0.40');
INSERT INTO `product` VALUES ('41', '63', '27', null, 'http://enesti.com.vn/uploads/shops/son_bong/lo01_146.jpg', '2016-05-18 17:31:00', '0', '1', '149000', '200', 'Test', '8ml', '0.00');
INSERT INTO `product` VALUES ('42', '63', '28', null, 'http://enesti.com.vn/uploads/shops/son_bong/lo02_146.jpg', '2016-05-18 17:31:00', '0', '1', '149000', '200', 'Test', '8ml', '0.00');
INSERT INTO `product` VALUES ('43', '63', '29', null, 'http://enesti.com.vn/uploads/shops/son_bong/lo03_146.jpg', '2016-05-18 17:31:00', '0', '1', '149000', '200', 'Test', '8ml', '0.00');
INSERT INTO `product` VALUES ('44', '63', '30', null, 'http://enesti.com.vn/uploads/shops/son_bong/lo04_146.jpg', '2016-05-18 17:31:00', '0', '1', '149000', '200', 'Test', '8ml', '0.00');
INSERT INTO `product` VALUES ('45', '63', '31', null, 'http://enesti.com.vn/uploads/shops/son_enesti/31_146.jpg', '2016-05-18 17:31:00', '0', '1', '165000', '200', 'Test', '3.4g', '0.00');
INSERT INTO `product` VALUES ('46', '63', '32', null, 'http://enesti.com.vn/uploads/shops/son_enesti/32_146.jpg', '2016-05-18 17:31:00', '0', '1', '165000', '200', 'Test', '3.4g', '0.00');
INSERT INTO `product` VALUES ('47', '63', '33', null, 'http://enesti.com.vn/uploads/shops/son_enesti/33_146.jpg', '2016-05-18 17:31:00', '0', '1', '165000', '200', 'Test', '3.4g', '0.00');
INSERT INTO `product` VALUES ('48', '63', '34', null, 'http://enesti.com.vn/uploads/shops/son_enesti/34_146.jpg', '2016-05-18 17:31:00', '0', '1', '165000', '200', 'Test', '3.4g', '0.00');
INSERT INTO `product` VALUES ('49', '63', '35', null, 'http://enesti.com.vn/uploads/shops/son_enesti/36_146.jpg', '2016-05-18 17:31:00', '0', '1', '165000', '200', 'Test', '3.4g', '0.00');
INSERT INTO `product` VALUES ('50', '63', '36', null, 'http://enesti.com.vn/uploads/shops/son_enesti/37_146.jpg', '2016-05-18 17:31:01', '0', '1', '165000', '200', 'Test', '3.4g', '0.00');
INSERT INTO `product` VALUES ('51', '73', '37', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/30_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('52', '73', '38', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/10_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('53', '73', '39', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/28_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('54', '73', '40', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/63_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('55', '73', '41', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/40_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('56', '73', '42', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/51_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('57', '73', '43', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/50_164.jpg', '2016-05-18 17:31:01', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('58', '73', '44', null, 'http://enesti.com.vn/uploads/shops/thuoc_nhuom/53_164.jpg', '2016-05-18 17:31:02', '0', '1', '125000', '200', 'Test', '60ml+10ml', '0.10');
INSERT INTO `product` VALUES ('59', '61', '45', null, 'http://enesti.com.vn/uploads/shops/mat_na/collagen_146.jpg', '2016-05-18 17:31:03', '0', '1', '20000', '200', 'Test', '25g', '0.50');
INSERT INTO `product` VALUES ('60', '61', '46', null, 'http://enesti.com.vn/uploads/shops/mat_na/ngoc_trai_146.jpg', '2016-05-18 17:31:03', '0', '1', '20000', '200', 'Test', '25g', '0.50');
INSERT INTO `product` VALUES ('61', '61', '47', null, 'http://enesti.com.vn/uploads/shops/mat_na/lo_hoi_146.jpg', '2016-05-18 17:31:04', '0', '1', '20000', '200', 'Test', '25g', '0.50');
INSERT INTO `product` VALUES ('62', '61', '48', null, 'http://enesti.com.vn/uploads/shops/mat_na/gao_146.jpg', '2016-05-18 17:31:04', '0', '1', '20000', '200', 'Test', '25g', '0.50');
INSERT INTO `product` VALUES ('63', '63', '49', null, 'http://enesti.com.vn/uploads/shops/2015_10/ghi-den.jpg', '2016-05-18 17:31:04', '0', '1', '75000', '200', 'Test', '5ml', '0.00');
INSERT INTO `product` VALUES ('64', '63', '50', null, 'http://enesti.com.vn/uploads/shops/2015_10/den.jpg', '2016-05-18 17:31:05', '0', '1', '75000', '200', 'Test', '5ml', '0.00');
INSERT INTO `product` VALUES ('65', '63', '51', null, 'http://enesti.com.vn/uploads/shops/2015_10/nau-ghi.jpg', '2016-05-18 17:31:05', '0', '1', '75000', '200', 'Test', '5ml', '0.00');
INSERT INTO `product` VALUES ('66', '63', '52', null, 'http://enesti.com.vn/uploads/shops/2015_10/nau.jpg', '2016-05-18 17:31:05', '0', '1', '75000', '200', 'Test', '5ml', '0.00');
INSERT INTO `product` VALUES ('67', '63', '53', null, 'http://enesti.com.vn/uploads/shops/2015_10/nau-den.jpg', '2016-05-18 17:31:06', '0', '1', '75000', '200', 'Test', '5ml', '0.00');
INSERT INTO `product` VALUES ('68', '61', '54', null, 'http://enesti.com.vn/uploads/shops/kem_chong_nang/chong_nang_146.jpg', '2016-05-18 17:31:06', '0', '1', '165000', '200', 'Test', '40ml', '0.00');
INSERT INTO `product` VALUES ('69', '63', '55', null, 'http://enesti.com.vn/uploads/shops/trang_diem/da_chucnang_146.jpg', '2016-05-18 17:31:07', '0', '1', '318000', '200', 'Test', '50g', '0.00');
INSERT INTO `product` VALUES ('70', '63', '56', null, 'http://enesti.com.vn/uploads/shops/trang_diem/3_chucnang_146.jpg', '2016-05-18 17:31:07', '0', '1', '318000', '200', 'Test', '45g', '0.00');
INSERT INTO `product` VALUES ('71', '63', '57', null, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_cc_146.png', '2016-05-18 17:31:07', '0', '1', '300000', '200', 'Test', '30g', '0.00');
INSERT INTO `product` VALUES ('72', '63', '58', null, 'http://enesti.com.vn/uploads/shops/trang_diem/kem_bb_xoyun_146.jpg', '2016-05-18 17:31:07', '0', '1', '354000', '200', 'Test', '40g', '0.00');
INSERT INTO `product` VALUES ('73', '63', '59', null, 'http://enesti.com.vn/uploads/shops/2015_10/phan-21.jpg', '2016-05-18 17:31:08', '0', '1', '360000', '200', 'Test', '12g', '0.00');
INSERT INTO `product` VALUES ('74', '63', '60', null, 'http://enesti.com.vn/uploads/shops/2015_10/phan-23.jpg', '2016-05-18 17:31:08', '0', '1', '360000', '200', 'Test', '12g', '0.00');
INSERT INTO `product` VALUES ('75', '73', '61', null, 'http://enesti.com.vn/uploads/shops/v_v/toc_oliu_146.jpg', '2016-05-18 17:31:08', '0', '1', '210000', '200', 'Test', '80ml', '0.00');
INSERT INTO `product` VALUES ('76', '61', '62', null, 'http://enesti.com.vn/uploads/shops/v_v/ocsen_146.jpg', '2016-05-18 17:31:08', '0', '1', '150000', '200', 'Test', '50ml', '0.00');
INSERT INTO `product` VALUES ('77', '63', '63', null, 'http://enesti.com.vn/uploads/shops/but/but-de-dang.jpg', '2016-05-18 17:31:09', '0', '1', '195000', '200', 'Test', '5ml', '0.00');
INSERT INTO `product` VALUES ('78', '63', '64', null, 'http://enesti.com.vn/uploads/shops/2016_01/1456217645593_8896302.jpg', '2016-05-18 17:31:09', '0', '1', '149000', '200', 'Test', '0.5g', '0.00');
INSERT INTO `product` VALUES ('79', '61', '65', null, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-am-suansu-water-bank-capsule.png', '2016-05-18 17:31:09', '0', '1', '268000', '200', 'Test', '50mg', '0.00');
INSERT INTO `product` VALUES ('80', '61', '66', null, 'http://enesti.com.vn/uploads/shops/2016_01/kem-duong-trang-crystal-whitening.png', '2016-05-18 17:31:09', '0', '1', '268000', '200', 'Test', '50mg', '0.00');
INSERT INTO `product` VALUES ('81', '61', '68', null, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-lo-hoi.png', '2016-05-18 17:31:10', '0', '1', '198000', '200', 'Test', '150ml+60ml', '0.00');
INSERT INTO `product` VALUES ('82', '61', '69', null, 'http://enesti.com.vn/uploads/shops/2016_01/xit-khoang-duong-am-suansu-oliu.png', '2016-05-18 17:31:10', '0', '1', '198000', '200', 'Test', '150ml+60ml', '0.00');
INSERT INTO `product` VALUES ('83', '61', '72', null, 'http://enesti.com.vn/uploads/shops/v_v/gel_lohoi_146.jpg', '2016-05-18 17:31:10', '0', '1', '85000', '200', 'Test', '120ml', '0.00');
INSERT INTO `product` VALUES ('84', '61', '74', '', '84_image.jpg', '2016-05-18 17:31:10', '0', '1', '150000', '200', 'Test', '300ml', '0.00');

-- ----------------------------
-- Table structure for product_lang
-- ----------------------------
DROP TABLE IF EXISTS `product_lang`;
CREATE TABLE `product_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `productlang_ibfk_1` (`product_id`),
  CONSTRAINT `productlang_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_lang
-- ----------------------------
INSERT INTO `product_lang` VALUES ('1', '12', 'vi', 'Sữa dưỡng thảo dược - SEOYOON', '<p>Sữa dưỡng thảo dược -<br />SEOYOON</p>');
INSERT INTO `product_lang` VALUES ('2', '13', 'vi', 'Nước hoa hồng thảo dược- SEOYOON', '<p>Nước hoa hồng thảo dược- SEOYOON</p>');
INSERT INTO `product_lang` VALUES ('3', '14', 'vi', 'Kem dưỡng thảo dược - SEOYOON', '<p>Kem dưỡng thảo dược -<br />SEOYOON</p>');
INSERT INTO `product_lang` VALUES ('4', '15', 'vi', 'Kem tẩy trang Day-to-day / Trà xanh', 'Kem tẩy trang Day-to-day / Trà xanh');
INSERT INTO `product_lang` VALUES ('5', '16', 'vi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi', 'Kem tẩy trang Day-to-day / Gạo và sữa tươi');
INSERT INTO `product_lang` VALUES ('6', '17', 'vi', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh', 'Sữa rửa mặt Day-to-day / Chanh và Trà Xanh');
INSERT INTO `product_lang` VALUES ('7', '18', 'vi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi', 'Sữa rửa mặt Day-to-day / Gạo và sữa tươi');
INSERT INTO `product_lang` VALUES ('8', '19', 'vi', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong', 'Sữa rửa mặt Day-to-day / Lô hội và mật ong');
INSERT INTO `product_lang` VALUES ('9', '20', 'vi', 'Sữa rửa mặt Day-to-day / Oliu', 'Sữa rửa mặt Day-to-day / Oliu');
INSERT INTO `product_lang` VALUES ('10', '21', 'vi', 'Sữa rửa mặt Day-to-Day / Lựu', 'Sữa rửa mặt Day-to-Day / Lựu');
INSERT INTO `product_lang` VALUES ('11', '22', 'vi', 'Chuốt mi / Siêu dài', 'Chuốt mi / Siêu dài');
INSERT INTO `product_lang` VALUES ('12', '23', 'vi', 'Chuốt mi / Đậm dày', 'Chuốt mi / Đậm dày');
INSERT INTO `product_lang` VALUES ('13', '24', 'vi', 'Chuốt mi / Cong và dài', 'Chuốt mi / Cong và dài');
INSERT INTO `product_lang` VALUES ('14', '25', 'vi', 'Chuốt mi chống nước / Cong và dài', 'Chuốt mi chống nước / Cong và dài');
INSERT INTO `product_lang` VALUES ('15', '26', 'vi', 'Nước hoa hồng Remine Collagen', 'Nước hoa hồng Remine Collagen');
INSERT INTO `product_lang` VALUES ('16', '27', 'vi', 'Sữa dưỡng da Remine Collagen', 'Sữa dưỡng da Remine Collagen');
INSERT INTO `product_lang` VALUES ('17', '28', 'vi', 'Kem dưỡng da Remine Collagen', 'Kem dưỡng da Remine Collagen');
INSERT INTO `product_lang` VALUES ('18', '29', 'vi', 'Dung dịch Remine collagen', 'Dung dịch Remine collagen');
INSERT INTO `product_lang` VALUES ('19', '30', 'vi', 'Nước hoa hồng thảo dược Xơ-yun', 'Nước hoa hồng thảo dược Xơ-yun');
INSERT INTO `product_lang` VALUES ('20', '31', 'vi', 'Sữa dưỡng thảo dược Xơ-yun', 'Sữa dưỡng thảo dược Xơ-yun');
INSERT INTO `product_lang` VALUES ('21', '32', 'vi', 'Kem dưỡng thảo dược Xơ-yun', 'Kem dưỡng thảo dược Xơ-yun');
INSERT INTO `product_lang` VALUES ('22', '33', 'vi', 'Nước hoa hồng Remine Crystal', 'Nước hoa hồng Remine Crystal');
INSERT INTO `product_lang` VALUES ('23', '34', 'vi', 'Sữa dưỡng ẩm Remine Crystal', 'Sữa dưỡng ẩm Remine Crystal');
INSERT INTO `product_lang` VALUES ('24', '35', 'vi', 'Kem dưỡng trắng Remine Crystal ', 'Kem dưỡng trắng Remine Crystal ');
INSERT INTO `product_lang` VALUES ('25', '36', 'vi', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton', 'Nước hoa Enesti Fresh Cotton / Hoa Cotton');
INSERT INTO `product_lang` VALUES ('26', '37', 'vi', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng', 'Nước hoa Enesti Golden Rose / Hoa hồng vàng');
INSERT INTO `product_lang` VALUES ('27', '38', 'vi', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ', 'Nước hoa Enesti Baby Powder / Hương trẻ thơ');
INSERT INTO `product_lang` VALUES ('28', '39', 'vi', 'Nước hoa Enesti Lovely Day / Ngày tình yêu', 'Nước hoa Enesti Lovely Day / Ngày tình yêu');
INSERT INTO `product_lang` VALUES ('29', '40', 'vi', 'Nước hoa Enesti May Me / Là chính tôi', 'Nước hoa Enesti May Me / Là chính tôi');
INSERT INTO `product_lang` VALUES ('30', '41', 'vi', 'Son bóng dưỡng môi / L01 Hồng đỏ', 'Son bóng dưỡng môi / L01 Hồng đỏ');
INSERT INTO `product_lang` VALUES ('31', '42', 'vi', 'Son bóng dưỡng môi / L02 Vàng Cam', 'Son bóng dưỡng môi / L02 Vàng Cam');
INSERT INTO `product_lang` VALUES ('32', '43', 'vi', 'Son bóng dưỡng môi / L03 Hồng', 'Son bóng dưỡng môi / L03 Hồng');
INSERT INTO `product_lang` VALUES ('33', '44', 'vi', 'Son bóng dưỡng môi / L04 Hồng tím', 'Son bóng dưỡng môi / L04 Hồng tím');
INSERT INTO `product_lang` VALUES ('34', '45', 'vi', 'Son enesti số 31', 'Son enesti số 31');
INSERT INTO `product_lang` VALUES ('35', '46', 'vi', 'Son enesti số 32', 'Son enesti số 32');
INSERT INTO `product_lang` VALUES ('36', '47', 'vi', 'Son enesti số 33', 'Son enesti số 33');
INSERT INTO `product_lang` VALUES ('37', '48', 'vi', 'Son enesti số 34', 'Son enesti số 34');
INSERT INTO `product_lang` VALUES ('38', '49', 'vi', 'Son enesti số 36', 'Son enesti số 36');
INSERT INTO `product_lang` VALUES ('39', '50', 'vi', 'Son enesti số 37', 'Son enesti số 37');
INSERT INTO `product_lang` VALUES ('40', '51', 'vi', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)', 'Thuốc nhuộm tóc Aroma / Đen tự nhiên (3/0)');
INSERT INTO `product_lang` VALUES ('41', '52', 'vi', 'Thuốc nhuộm tóc Aroma / Đen (1/0)', 'Thuốc nhuộm tóc Aroma / Đen (1/0)');
INSERT INTO `product_lang` VALUES ('42', '53', 'vi', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)', 'Thuốc nhuộm tóc Aroma / Xanh đen (2/8)');
INSERT INTO `product_lang` VALUES ('43', '54', 'vi', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)', 'Thuốc nhuộm tóc Aroma / Ánh đồng (6/3)');
INSERT INTO `product_lang` VALUES ('44', '55', 'vi', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)', 'Thuốc nhuộm tóc Aroma / Nâu đậm (4/0)');
INSERT INTO `product_lang` VALUES ('45', '56', 'vi', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)', 'Thuốc nhuộm tóc Aroma / Nâu sáng (5/1)');
INSERT INTO `product_lang` VALUES ('46', '57', 'vi', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)', 'Thuốc nhuộm tóc Aroma / Nâu tự nhiên (5/0)');
INSERT INTO `product_lang` VALUES ('47', '58', 'vi', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)', 'Thuốc nhuộm tóc Aroma / Vàng tự nhiên (5/3)');
INSERT INTO `product_lang` VALUES ('48', '59', 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Collagen', 'Mặt nạ nước ấm từ thiên nhiên / Collagen');
INSERT INTO `product_lang` VALUES ('49', '60', 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai', 'Mặt nạ nước ấm từ thiên nhiên / Ngọc trai');
INSERT INTO `product_lang` VALUES ('50', '61', 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội', 'Mặt nạ nước ấm từ thiên nhiên / Lô hội');
INSERT INTO `product_lang` VALUES ('51', '62', 'vi', 'Mặt nạ nước ấm từ thiên nhiên / Gạo', 'Mặt nạ nước ấm từ thiên nhiên / Gạo');
INSERT INTO `product_lang` VALUES ('52', '63', 'vi', 'Bút kẻ mày / ghi đen ', 'Bút kẻ mày / ghi đen ');
INSERT INTO `product_lang` VALUES ('53', '64', 'vi', 'Bút kẻ mày / đen', 'Bút kẻ mày / đen');
INSERT INTO `product_lang` VALUES ('54', '65', 'vi', 'Bút kẻ mày / Nâu ghi', 'Bút kẻ mày / Nâu ghi');
INSERT INTO `product_lang` VALUES ('55', '66', 'vi', 'Bút kẻ mày / Nâu', 'Bút kẻ mày / Nâu');
INSERT INTO `product_lang` VALUES ('56', '67', 'vi', 'Bút kẻ mày / Nâu đen', 'Bút kẻ mày / Nâu đen');
INSERT INTO `product_lang` VALUES ('57', '68', 'vi', 'Kem chống nắng 50', 'Kem chống nắng 50');
INSERT INTO `product_lang` VALUES ('58', '69', 'vi', 'Kem nền giữ ẩm đa chức năng', 'Kem nền giữ ẩm đa chức năng');
INSERT INTO `product_lang` VALUES ('59', '70', 'vi', 'Kem nền ngọc trai 3 chức năng', 'Kem nền ngọc trai 3 chức năng');
INSERT INTO `product_lang` VALUES ('60', '71', 'vi', 'Kem nền CC 3 chức năng', 'Kem nền CC 3 chức năng');
INSERT INTO `product_lang` VALUES ('61', '72', 'vi', 'Kem nền thảo dược Xơ - yun', 'Kem nền thảo dược Xơ - yun');
INSERT INTO `product_lang` VALUES ('62', '73', 'vi', 'Phấn nén chống nắng số 21', 'Phấn nén chống nắng số 21');
INSERT INTO `product_lang` VALUES ('63', '74', 'vi', 'Phấn nén chống nắng số 23', 'Phấn nén chống nắng số 23');
INSERT INTO `product_lang` VALUES ('64', '75', 'vi', 'Tinh chất dưỡng tóc Day-to-day / Oliu', 'Tinh chất dưỡng tóc Day-to-day / Oliu');
INSERT INTO `product_lang` VALUES ('65', '76', 'vi', 'Kem dưỡng da Silk Snail Ốc Sên', 'Kem dưỡng da Silk Snail Ốc Sên');
INSERT INTO `product_lang` VALUES ('66', '77', 'vi', 'Bút kẻ mắt chống nước', 'Bút kẻ mắt chống nước');
INSERT INTO `product_lang` VALUES ('67', '78', 'vi', 'Bút kẻ mắt nước', 'Bút kẻ mắt nước');
INSERT INTO `product_lang` VALUES ('68', '79', 'vi', 'Kem dưỡng ẩm SUANSU Water Bank Capsule', 'Kem dưỡng ẩm SUANSU Water Bank Capsule');
INSERT INTO `product_lang` VALUES ('69', '80', 'vi', 'Kem dưỡng trắng SUANSU Crystal Whitening', 'Kem dưỡng trắng SUANSU Crystal Whitening');
INSERT INTO `product_lang` VALUES ('70', '81', 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội', 'Xịt khoáng dưỡng ẩm SUANSU/ Lô hội');
INSERT INTO `product_lang` VALUES ('71', '82', 'vi', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu', 'Xịt khoáng dưỡng ẩm SUANSU/ Oliu');
INSERT INTO `product_lang` VALUES ('72', '83', 'vi', 'Gel dưỡng da lô hội', 'Gel dưỡng da lô hội');
INSERT INTO `product_lang` VALUES ('73', '84', 'vi', 'Gel dưỡng da lô hội SUANSU ( jar )', '<p>Gel dưỡng da l&ocirc; hội SUANSU ( jar )</p>');

-- ----------------------------
-- Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of profile
-- ----------------------------

-- ----------------------------
-- Table structure for promotion
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion
-- ----------------------------
INSERT INTO `promotion` VALUES ('8', '29', '8_image.jpg', '1', '2016-04-23 17:06:33', '1', '1');

-- ----------------------------
-- Table structure for promotion_lang
-- ----------------------------
DROP TABLE IF EXISTS `promotion_lang`;
CREATE TABLE `promotion_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postlang_ibfk_1` (`promotion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion_lang
-- ----------------------------
INSERT INTO `promotion_lang` VALUES ('5', '8', 'vi', '123', '123', '123');

-- ----------------------------
-- Table structure for recruitment
-- ----------------------------
DROP TABLE IF EXISTS `recruitment`;
CREATE TABLE `recruitment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `feature` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recruitment
-- ----------------------------
INSERT INTO `recruitment` VALUES ('2', '0', '2_image.jpg', '11', '2016-04-20 11:39:28', '0', '0');
INSERT INTO `recruitment` VALUES ('3', '0', '3_image.jpg', '123', '2016-04-20 12:09:24', '0', '0');
INSERT INTO `recruitment` VALUES ('4', '0', '', '111', '2016-04-20 16:04:34', '0', '0');
INSERT INTO `recruitment` VALUES ('5', '0', '', '111', '2016-04-20 16:05:01', '0', '0');
INSERT INTO `recruitment` VALUES ('6', '0', '6_image.jpg', '11', '2016-04-20 16:10:59', '0', '1');
INSERT INTO `recruitment` VALUES ('7', '0', '', '123', '2016-04-20 16:35:35', '0', '0');
INSERT INTO `recruitment` VALUES ('8', '0', '', '11', '2016-04-20 16:36:48', '0', '0');
INSERT INTO `recruitment` VALUES ('9', '0', '9_image.gif', '1', '2016-04-22 18:07:18', '0', '0');
INSERT INTO `recruitment` VALUES ('10', '0', '10_image.jpg', '1', '2016-04-22 18:10:07', '0', '0');

-- ----------------------------
-- Table structure for recruitment_lang
-- ----------------------------
DROP TABLE IF EXISTS `recruitment_lang`;
CREATE TABLE `recruitment_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recruitment_id` int(11) NOT NULL,
  `language` varchar(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recruitmentlang_ibfk_1` (`recruitment_id`),
  CONSTRAINT `recruitmentlang_ibfk_1` FOREIGN KEY (`recruitment_id`) REFERENCES `recruitment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recruitment_lang
-- ----------------------------
INSERT INTO `recruitment_lang` VALUES ('2', '2', 'vi', 'sssss', 'abcsdssss');
INSERT INTO `recruitment_lang` VALUES ('3', '2', 'en', 'asdfasdf', 'asdfasdfasdf');
INSERT INTO `recruitment_lang` VALUES ('4', '3', 'vi', '123sssss', '123');
INSERT INTO `recruitment_lang` VALUES ('5', '3', 'en', '321ssss', '321');
INSERT INTO `recruitment_lang` VALUES ('6', '5', 'vi', '12312355555', 'sss');
INSERT INTO `recruitment_lang` VALUES ('7', '5', 'en', '111', '111');
INSERT INTO `recruitment_lang` VALUES ('8', '6', 'vi', 'asdfas', 'asdfasdf');
INSERT INTO `recruitment_lang` VALUES ('9', '6', 'en', 'asdfas', 'ddd');
INSERT INTO `recruitment_lang` VALUES ('10', '7', 'vi', 'asd', 'asdasda');
INSERT INTO `recruitment_lang` VALUES ('11', '8', 'en', 'sss', 'ssss');
INSERT INTO `recruitment_lang` VALUES ('12', '9', 'vi', 'ádf', 'ádf');
INSERT INTO `recruitment_lang` VALUES ('13', '10', 'vi', 'ádf', 'ádf');

-- ----------------------------
-- Table structure for reply
-- ----------------------------
DROP TABLE IF EXISTS `reply`;
CREATE TABLE `reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reply
-- ----------------------------
INSERT INTO `reply` VALUES ('1', '1', 'phamxuanloc.01@gmail.com', '123', '123', '123', '<p>123</p>', '2016-05-10 16:15:45');
INSERT INTO `reply` VALUES ('2', '1', 'phamxuanloc.01@gmail.com', '123', '123', 'cxaccasc', '<p>dsadasdasd</p>', '2016-05-10 16:47:38');
INSERT INTO `reply` VALUES ('4', '9', 'phamxuanloc.01@gmail.com', 'abc', 'abc', '123', '<p>321</p>', '2016-05-18 17:04:03');
INSERT INTO `reply` VALUES ('5', '9', 'phamxuanloc.01@gmail.com', 'abc', 'abc', 'dđ', '<p>dđ</p>', '2016-05-18 17:08:41');
INSERT INTO `reply` VALUES ('6', '9', 'phamxuanloc.01@gmail.com', 'abc', 'abc', 'ádfas', '<p>&aacute;dfasdf</p>', '2016-05-18 17:12:22');
INSERT INTO `reply` VALUES ('7', '9', 'phamxuanloc.01@gmail.com', 'abc', 'abc', 'sdf', '<p>&aacute;dfa</p>', '2016-05-18 17:13:52');
INSERT INTO `reply` VALUES ('8', '9', 'phamxuanloc.01@gmail.com', 'abc', 'abc', 'dfaf', '<p>&aacute;dfaf</p>', '2016-05-18 17:16:10');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `is_backend_login` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Administrator', '{\"app\\\\modules\\\\admin\\\\controllers\\\\CategoryController\":{\"index\":\"1\"},\"navatech\\\\role\\\\controllers\\\\DefaultController\":{\"index\":\"1\",\"create\":\"1\",\"update\":\"1\",\"delete\":\"1\",\"view\":\"1\"}}', '1');

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8_unicode_ci,
  `type` smallint(2) NOT NULL DEFAULT '1',
  `store_range` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `sort_order` int(11) DEFAULT '50',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `code` (`code`),
  KEY `sort_order` (`sort_order`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES ('19', '0', 'general', 'General tab', '', '0', '', '', '', '1');
INSERT INTO `setting` VALUES ('39', '0', 'email', 'Email', '', '0', '', '', '', '2');
INSERT INTO `setting` VALUES ('41', '19', 'general_active', 'Bảo trì website ', '', '16', 'no,yes', '', 'no', '1');
INSERT INTO `setting` VALUES ('42', '19', 'general_language', 'Ngôn ngữ mặc định ', '', '11', 'vi,en', '', 'en', '2');
INSERT INTO `setting` VALUES ('43', '39', 'email_field', 'Địa chỉ Email', '', '2', '', '', 'phamxuanloc.01@gmail.com', '1');
INSERT INTO `setting` VALUES ('44', '39', 'password_mail', 'Mật khẩu', '', '9', '', '', 'locpro123', '1');
INSERT INTO `setting` VALUES ('46', '19', 'general_title', 'Tiêu đề', '', '1', '', '', 'Enesti', '3');
INSERT INTO `setting` VALUES ('47', '19', 'general_logo', 'Logo', '', '14', '', '@app/web/uploads/setting', 'logo.png', '5');
INSERT INTO `setting` VALUES ('48', '19', 'general_hotline', 'Hotline', '', '1', '', '', '', '8');
INSERT INTO `setting` VALUES ('49', '19', 'general_showroom', 'Bản đồ', '', '14', '', '', '', '7');
INSERT INTO `setting` VALUES ('51', '39', 'email_secure', 'Bảo mật email', '', '18', 'none,tls,ssl', '', 'tls', '3');
INSERT INTO `setting` VALUES ('52', '39', 'email_port', 'Cổng', '', '1', '', '', '587', '2');
INSERT INTO `setting` VALUES ('53', '39', 'email_host', 'Máy chủ gửi mail', '', '1', '', '', 'smtp.gmail.com', '1');
INSERT INTO `setting` VALUES ('54', '19', 'general_favicon', 'Favicon', '', '14', '', '@app/web/uploads/setting', 'google_plus.png', '6');

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES ('1', '1_image.jpg', 'http://google.com', '1', '1');
INSERT INTO `slider` VALUES ('2', '2_image.jpg', 'http://google.com', '2', '1');
INSERT INTO `slider` VALUES ('3', '3_image.jpg', 'http://google.com', '3', '1');

-- ----------------------------
-- Table structure for slider_lang
-- ----------------------------
DROP TABLE IF EXISTS `slider_lang`;
CREATE TABLE `slider_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slider_lang
-- ----------------------------
INSERT INTO `slider_lang` VALUES ('1', '2', 'vi', 'sdfsdf');
INSERT INTO `slider_lang` VALUES ('2', '3', 'vi', '123');
INSERT INTO `slider_lang` VALUES ('3', '4', 'vi', '123');
INSERT INTO `slider_lang` VALUES ('4', '1', 'vi', '1');
INSERT INTO `slider_lang` VALUES ('5', '1', 'en', '1');
INSERT INTO `slider_lang` VALUES ('6', '2', 'vi', '2');
INSERT INTO `slider_lang` VALUES ('7', '2', 'en', '2');
INSERT INTO `slider_lang` VALUES ('8', '3', 'vi', '3');
INSERT INTO `slider_lang` VALUES ('9', '3', 'en', '3');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `distributor_id` int(11) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `area` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_email` (`email`),
  UNIQUE KEY `user_unique_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin@gmail.com', '$2y$10$YiHv/jRh.F3B41YlRDTZCOtT970o6nNybcy2wwYS3ncRhpjmDW3gi', 'X2eREcnF64l7EPaOCzEqC0y9Yt2EtFCH', '1456216036', null, null, '127.0.0.1', '1456216036', '1456216036', '0', '1', null, null, null, null, null, null, null);
INSERT INTO `user` VALUES ('3', 'thucha', 'thuchm92@gmail.com', '$2y$10$yAbtdgnydxRuLkBWqhvh1ej.cykg6Ieoel00nI1vtYQq0Z.jYxhry', '0Qfg-uRVkYhjs7h6IufgZbXWDpa6ZUYk', '1456216722', null, null, '127.0.0.1', '1456216722', '1456216722', '0', '1', null, null, null, null, null, null, null);
INSERT INTO `user` VALUES ('4', 'abc', 'abc@gmail.com', '$2y$10$iCifn5MeoTWGriyoKt9HCOdO2hsWIRbvxCBpxlgcNP9MFvsN2t2/2', 'LjGME_pHmzp2vTn1MZ6BUHK3hjro5LYU', null, null, null, '127.0.0.1', '1462606831', '1462606831', '0', '1', null, null, null, null, null, null, null);
INSERT INTO `user` VALUES ('5', 'abcsss', 'abc@gmail.comsss', '$2y$10$4AkTKG1QpML4v4sTjYoKr.fF5E8b93noQEZl7/RAjfdMPHSWBxHn6', '0wt8JSKBCPo9Z30ajpcrDZD9ZP1b6wHf', null, null, null, '127.0.0.1', '1462607366', '1462607546', '0', '1', null, null, null, null, null, null, null);
