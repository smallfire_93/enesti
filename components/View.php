<?php
/**
 * Created by Navatech.
 * @project enesti-com-vn
 * @author  Phuong
 * @email   phuong17889[at]gmail.com
 * @date    6/27/2016
 * @time    10:26 AM
 */
namespace app\components;

use app\models\User;
use Yii;

/**
 * @property User $user
*/
class View extends \yii\web\View {

	/**@var User */
	public $user;

	public function init() {
		parent::init();
		$this->user = Yii::$app->user->getIdentity();
	}
}