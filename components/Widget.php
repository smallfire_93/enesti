<?php
namespace app\components;
use Yii;
class Widget extends \yii\bootstrap\Widget {

	public function init() {
		parent::init();
	}

	public function getViewPath() {
		$name = explode('\\', self::className());
		return \Yii::$app->view->theme->basePath . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . end($name);
	}
	/**
	 * @param array|string      $controller
	 * @param null|array|string $action
	 * @param null|array|string $params
	 *
	 * @return string
	 */
	public static function isActive($controller, $action = null, $params = null) {
		$string = '';
		if (!is_array($controller)) {
			$controller = [$controller];
		}
		if ($action !== null && !is_array($action)) {
			$action = [$action];
		}
		if ($params !== null && !is_array($params)) {
			$params = [$params];
		}
		if (in_array(Yii::$app->controller->id, $controller, true)) {
			if ($action === null || ($action != null && in_array(Yii::$app->controller->action->id, $action, true))) {
				if ($params === null || in_array($params, array_chunk(Yii::$app->controller->actionParams, 1, true), true)) {
					$string = 'active';
				}
			}
		}
		return $string;
	}
}