<?php
/**
 * Created by Navatech.
 * @project enesti-com-vn
 * @author  Phuong
 * @email   phuong17889[at]gmail.com
 * @date    12/03/2016
 * @time    2:07 SA
 */
namespace app\components;

use app\models\User;
use navatech\language\components\MultiLanguageController;
use navatech\language\Translate;
use Yii;
use yii\helpers\Url;
use yii\rest\Action;

class Controller extends MultiLanguageController {

	/**@var User */
	public $identity;

	public function init() {
		$this->identity = Yii::$app->getUser()->getIdentity();
		parent::init();
	}
	public function beforeAction($action) {
		if(Yii::$app->setting->get('web_active') == 'no' && Yii::$app->controller->action->id != 'maintain'){
			$this->redirect(Url::to(['/site/maintain']));
		}
		return parent::beforeAction($action);
	}
}