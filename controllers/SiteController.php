<?php
namespace app\controllers;

use app\components\Controller;
use app\models\Banner;
use app\models\Category;
use app\models\ContactForm;
use app\models\Distributor;
use app\models\LoginForm;
use app\models\Popup;
use app\models\Post;
use app\models\Product;
use navatech\language\Translate;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;

class SiteController extends Controller {

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		$hotnews      = Post::find()->where([
			'feature' => 1,
			'status'  => 1,
		])->limit(1)->orderBy(['id' => SORT_DESC])->one();
		$newest       = Post::find()->where(['status' => 1])->limit(2)->orderBy(['id' => SORT_DESC])->all();
		$hot_product  = Product::find()->where(['feature' => 1])->limit(3)->orderBy(['id' => SORT_DESC])->all();
		$banner       = Banner::findOne([
			'position' => 0,
			'status'   => 1,
		]);
		$popup        = Popup::findOne(['status' => 1]);
		$categories   = Category::find()->where([
			'status'    => 1,
			'type'      => 1,
			'parent_id' => 0,
		])->limit(6)->orderBy(['order' => SORT_ASC])->all();
		$distributors = new Distributor();
		return $this->render('index', [
			'hotnews'      => $hotnews,
			'hot_product'  => $hot_product,
			'newest'       => $newest,
			'categories'   => $categories,
			'distributors' => $distributors,
			'banner'       => $banner,
			'popup'        => $popup,
		]);
	}

	/**
	 * @return Action
	 */
	public function actionMaintain() {
		return $this->render('maintain');
	}

	public function actionLogin() {
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}
		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function actionContact() {
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');
			return $this->refresh();
		}
		return $this->render('contact', [
			'model' => $model,
		]);
	}

	public function actionAbout() {
		return $this->render('about');
	}

	public function actionTest() {
		Yii::$app->layout = false;
		echo Translate::welcome_back_x(['Jennifer Melzer']);
		echo Url::to([
				'contact/index',
				'lang' => Yii::$app->language,
			]) . '<br>';
		echo Url::to([
				'product/index',
				'lang' => Yii::$app->language,
			]) . '<br>';
		echo Url::to([
				'post/index',
				'lang' => Yii::$app->language,
			]) . '<br>';
	}
}
