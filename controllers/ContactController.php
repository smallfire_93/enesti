<?php
namespace app\controllers;

use app\components\Controller;
use app\models\Contact;
use navatech\language\Translate;
use Yii;

/**
 * ContactController implements the CRUD actions for Post model.
 */
class ContactController extends Controller {

	/**
	 * Lists all Contact models.
	 * @return mixed
	 */
	public function actionIndex() {
		$model  = new Contact();
		$top_id = $model->find()->orderBy('created_date DESC')->one();
		$top_id = $top_id->id + 1;
		if ($model->load(Yii::$app->request->post())) {
			$model->title = "Register #" . $top_id;
			if ($model->save()) {
				Yii::$app->session->setFlash('success', Translate::contact_success());
			} else {
				Yii::$app->session->setFlash('failed', Translate::contact_failed());
			}
		} else {
			return $this->render('index', [
				'model' => $model,
			]);
		}
		return $this->render('index', [
			'model' => $model,
		]);
	}
}
