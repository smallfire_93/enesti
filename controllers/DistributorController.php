<?php
namespace app\controllers;

use app\components\Controller;
use app\models\Distributor;
use app\models\Model;
use navatech\language\Translate;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * ProductController implements the CRUD actions for Post model.
 */
class DistributorController extends Controller {

	public function actionProfile() {
		if (Yii::$app->user->isGuest) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		$model           = $this->findModel($this->identity->id);
		$model->scenario = 'clientUpdate';
		if ($model->load(Yii::$app->request->post())) {
			$model->birthday = Model::format($model->birthday, 'd/m/Y', 'Y-m-d');
			if ($model->save()) {
				Yii::$app->session->setFlash("success", Translate::update_x(Translate::success()));
			} else {
				Yii::$app->session->setFlash("failed", Translate::update_x(Translate::failed()));
			}
		}
		return $this->render('profile', [
			'model' => $model,
		]);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Distributor the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Distributor::findOneTranslated($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
