<?php
namespace app\controllers;

use app\components\Controller;
use app\models\Category;
use app\models\Post;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller {

	/**
	 * Lists all Post models.
	 * @return mixed
	 */
	public function actionIndex() {
		$newest        = Post::find()->where(['status' => 1])->limit(2)->orderBy(['id' => SORT_DESC])->all();
		$categories    = Category::find()->where([
			'status'    => 1,
			'type'      => 1,
			'parent_id' => 0,
		])->orderBy(['order' => SORT_ASC])->all();
		$category_post = Category::find()->where([
			'status'    => 1,
			'type'      => 2,
			'parent_id' => 0,
		])->orderBy(['order' => SORT_ASC])->all();
		return $this->render('index', [
			'newest'        => $newest,
			'categories'    => $categories,
			'category_post' => $category_post,
		]);
	}

	public function actionCategory($id) {
		$dataProvider = new ActiveDataProvider([
			'query'      => Post::find()->where([
				'status'      => 1,
				'category_id' => $id,
			])->orderBy('id DESC'),
			'pagination' => [
				'pageSize'        => 10,
				'defaultPageSize' => 10,
			],
		]);
		return $this->render('category', ['posts' => $dataProvider]);
	}

	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Finds the Post model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Post the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Post::findOneTranslated($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
