<?php
namespace app\controllers;

use app\components\Controller;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * ProductController implements the CRUD actions for Post model.
 */
class ProductController extends Controller {

	public function actionIndex() {
		$dataProvider = new ActiveDataProvider([
			'query'      => Product::find()->where(['status' => 1])->orderBy('id DESC'),
			'pagination' => [
				'defaultPageSize' => 12,
				'pageSize' => 12,
			],
		]);
		return $this->render('index', ['products' => $dataProvider]);
	}

	public function actionView($id) {
		$model   = $this->findModel($id);
		$cat_ids = Category::find()->select('id')->where(['parent_id' => $model->category_id])->asArray()->all();
		$ids     = [];
		foreach ($cat_ids as $value) {
			$ids[] = $value['id'];
		}
		array_push($ids, $model->category_id);
		$categories   = Product::find()->where([
			'status'      => 1,
			'category_id' => $ids,
		])->andWhere('id!=' . $id)->orderBy('id DESC')->limit(4);
		$dataProvider = new ActiveDataProvider([
			'query'      => $categories,
			'pagination' => false,
		]);
		return $this->render('view', [
			'model'   => $model,
			'related' => $dataProvider,
		]);
	}

	public function actionCategory($id) {
		/**@var Category $title */
		$cat_ids = Category::find()->select('id')->where(['parent_id' => $id])->asArray()->all();
		$title   = Category::find()->where(['id' => $id])->one();
		$ids     = [];
		foreach ($cat_ids as $value) {
			$ids[] = $value['id'];
		}
		array_push($ids, $id);
		$categories   = Product::find()->where([
			'status'      => 1,
			'category_id' => $ids,
		])->orderBy('id DESC');
		$dataProvider = new ActiveDataProvider([
			'query'      => $categories,
			'pagination' => [
				'defaultPageSize' => 12,
				'pageSize'        => 12,
			],
		]);
		return $this->render('category', [
			'products' => $dataProvider,
			'title'    => $title->name,
		]);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Product::findOneTranslated($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
