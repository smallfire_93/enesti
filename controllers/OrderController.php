<?php
namespace app\controllers;

use app\components\Controller;
use app\models\Distributor;
use app\models\Order;
use app\models\OrderItem;
use app\models\Product;
use app\modules\admin\controllers\OrderItemController;
use navatech\language\Translate;
use navatech\role\helpers\RoleChecker;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class OrderController extends Controller {

	public function actionIndex() {
		$current    = date('Y-m-d H:i:s');
		$days       = - 15;
		$days_ago   = date('Y-m-d H:i:s', strtotime($days . ' days', strtotime($current)));
		$query      = Order::find()->where("created_date <= '" . $current . "' AND created_date >='" . $days_ago . "'");
		$count      = $query->count();
		$pagination = new Pagination([
			'totalCount'      => $count,
			'pageSize'        => 3,
			'defaultPageSize' => 3,
		]);
		$orders     = $query->offset($pagination->offset)->limit($pagination->limit)->orderBy('created_date DESC')->all();
		if (Yii::$app->request->isAjax) {
			$days       = - $_POST['data'];
			$days_ago   = date('Y-m-d H:i:s', strtotime($days . ' days', strtotime($current)));
			$query      = Order::find()->where("created_date <= '" . $current . "' AND created_date >='" . $days_ago . "'");
			$count      = $query->count();
			$pagination = new Pagination([
				'totalCount'      => $count,
				'pageSize'        => 3,
				'defaultPageSize' => 3,
			]);
			$orders     = $query->offset($pagination->offset)->limit($pagination->limit)->orderBy('created_date DESC')->all();
			return $this->renderAjax('_index', [
				'orders'     => $orders,
				'pagination' => $pagination,
			]);
		} else {
			return $this->render('index', [
				'orders'     => $orders,
				'pagination' => $pagination,
			]);
		}
	}

	/**
	 * @return string
	 */
	public function actionOrderitem() {
		$orderItem = new OrderItem();
		$order     = new Order();
		if (isset($_POST['OrderItem'])) {
			if ($order->load(Yii::$app->request->post()) && $order->save()) {
				$order_id = $order->getPrimaryKey();
			} else {
				$order_id = 1;
			};
			foreach ($_POST['OrderItem'] as $item) {
				if ($order->save()) {
					$orderItem = new OrderItem();
					$orderItem->setAttributes($item);
					$product = Product::findOne($item['product_id']);
					if ($product) {
						$orderItem->price = $product->price;
					}
					if (($orderItem->quantity) <= 0) {
						$orderItem->quantity = 0;
						$orderItem->price    = 0;
					}
					$orderItem->order_id = $order_id;
					if ($orderItem->save()) {
						Yii::$app->session->setFlash('success', Translate::order_success());
					}
				} else {
					if ($orderItem->save() == false) {
						Yii::$app->session->setFlash('failed', Translate::order_failed());
					}
				}
			}
			$check_order = OrderItem::findOne(['order_id' => $order_id]);
			if ($check_order == null) {
				$this->findModel($order_id)->delete();
			}
			if ($order->save()) {
				return $this->redirect('/order');
			}
		}
		if (isset($_POST['category'])) {
			$products = Product::findAll(['category_id' => $_POST['category']]);
			$html     = '<option value>Chọn sản phẩm</option>';
			foreach ($products as $product) {
				$html .= '<option value="' . $product->id . '">' . $product->title . '</option>';
			}
			return $html;
		} elseif (isset($_POST['product'])) {
			/**@var Product $product */
			$product = Product::find()->where(['id' => $_POST['product']])->one();
			$value   = [
				'product_code'  => $product->code,
				'product_price' => $product->price,
				'sale_off'      => $product->sale_off,
			];
			return json_encode($value);
		}
		$data_order                     = [];
		$data_order['distributor_id']   = null;
		$data_order['shipping_address'] = null;
		$data_order['phone_number']     = null;
		if (!Yii::$app->user->isGuest) {
			if (RoleChecker::isAuth(OrderItemController::className(), 'create', $this->identity->role_id)) {
				$id                             = $this->identity->id;
				$user                           = Distributor::findOne($id);
				$data_order['distributor_id']   = $user->id;
				$data_order['shipping_address'] = $user->shipping_address;
				$data_order['phone_number']     = $user->phone_number;
			}
		}
		return $this->render('orderItem', [
			'orderItem'  => $orderItem,
			'order'      => $order,
			'data_order' => $data_order,
		]);
	}

	public function actionUpdate($id) {
		$order     = $this->findModel($id);
		$orderItem = OrderItem::find()->where(['order_id' => $id])->all();
		if (Yii::$app->request->post()) {
			//			echo "<pre>";
			//			print_r($_POST);
			//			die;
			foreach ($_POST['OrderItem'] as $postItem) {
				foreach ($orderItem as $item) {
					$item->findOne($item->id);
				}
			}
		}
		$data_order                     = [];
		$data_order['distributor_id']   = null;
		$data_order['shipping_address'] = null;
		$data_order['phone_number']     = null;
		if (!Yii::$app->user->isGuest) {
			if (RoleChecker::isAuth(OrderItemController::className(), 'create', $this->identity->role_id)) {
				$id                             = $this->identity->id;
				$user                           = Distributor::findOne($id);
				$data_order['distributor_id']   = $user->id;
				$data_order['shipping_address'] = $user->shipping_address;
				$data_order['phone_number']     = $user->phone_number;
			}
		}
		return $this->render('update', [
			'orderItem'  => $orderItem,
			'order'      => $order,
			'data_order' => $data_order,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return null|Order
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id) {
		if (($model = Order::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
